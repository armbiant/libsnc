/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <sev/mem/ring_static.hpp>
#include <snc/device/handle.hpp>
#include <snc/mpath/elem/fader.hpp>
#include <snc/mpath/elem/sleep.hpp>
#include <snc/mpath/elem/stream_end.hpp>
#include <snc/mpath_stream/read.hpp>
#include <variant>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
class Stream_Read_Breaker_D : public snc::mpath_stream::Read
{
  public:
  // -- Types

  struct PState
  {
    static constexpr std::uint_fast8_t CUTTER_STOP = ( 1 << 0 );
    static constexpr std::uint_fast8_t CUTTER_SLEEP = ( 1 << 1 );
  };

  // -- Construction and setup

  Stream_Read_Breaker_D (
      snc::device::statics::handle::Statics device_statics_n );

  void
  setup ( bool stop_cutter_n );

  // --- Abstract interface

  bool
  open () override;

  void
  close () override;

  const snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  std::variant< snc::mpath::elem::Fader,
                snc::mpath::elem::Sleep,
                snc::mpath::elem::Stream_End >
      _elem_variant;
  sev::mem::Flags_Fast8 _pstate;
  const snc::device::statics::handle::Statics _device_statics;
};

} // namespace snc::mpath_feed_emb
