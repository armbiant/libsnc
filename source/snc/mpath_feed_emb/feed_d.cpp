/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "feed_d.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/mpath_feed/reader/station_constructor_d.hpp>
#include <snc/mpath_feed/speeder/station_constructor_d.hpp>
#include <snc/mpath_feed/stepper/station_constructor_d.hpp>

namespace snc::mpath_feed_emb
{

namespace
{

template < std::size_t DIM >
std::vector< snc::mpath_feed::Station_Contructor_Handle >
make_stations ( const Context_D< DIM > & context_n )
{
  // Station constructors
  auto con_reader = std::make_shared<
      snc::mpath_feed::reader::Station_Constructor_D< DIM > > ();
  auto con_stepper = std::make_shared<
      snc::mpath_feed::stepper::Station_Constructor_D< DIM > > ();
  auto con_speeder = std::make_shared<
      snc::mpath_feed::speeder::Station_Constructor_D< DIM > > ();
  {
    // Axes step lengths
    sev::lag::Vector< double, DIM > axis_step_length;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto & dsaxis = *context_n.axis_statics ( ii );
      axis_step_length[ ii ] = dsaxis.geo ().length_per_step ();
    }
    con_stepper->set_step_lengths ( axis_step_length );
    con_speeder->set_step_lengths ( axis_step_length );

    // Axes limits
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto & dsaxis = *context_n.axis_statics ( ii );
      auto & alimits ( con_speeder->axes_limits_ref ()[ ii ] );
      alimits.speed_reversible = dsaxis.geo ().speed_reversible ();
      alimits.speed_max = dsaxis.geo ().speed_max ();
      alimits.accel_max = dsaxis.geo ().accel_max ();
    }
  }

  std::vector< snc::mpath_feed::Station_Contructor_Handle > cons;
  cons.push_back ( std::move ( con_reader ) );
  cons.push_back ( std::move ( con_stepper ) );
  cons.push_back ( std::move ( con_speeder ) );
  return cons;
}

} // namespace

template < std::size_t DIM >
Feed_D< DIM >::Feed_D ( const sev::logt::Reference & log_parent_n,
                        const Context_D< DIM > & context_n )
: _log ( log_parent_n, sev::string::cat ( "Feed_D<", DIM, ">" ) )
, _feed ( _log, context_n.thread_tracker (), make_stations ( context_n ) )
, _muxer ( context_n, _log )
{
}

template < std::size_t DIM >
Feed_D< DIM >::~Feed_D ()
{
}

template < std::size_t DIM >
void
Feed_D< DIM >::session_begin (
    const std::function< void () > & process_request_callback_n )
{
  _feed.session_begin ( process_request_callback_n );
}

template < std::size_t DIM >
void
Feed_D< DIM >::session_end ()
{
  _feed.session_end ();
}

template < std::size_t DIM >
void
Feed_D< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  if ( _is_routing ) {
    return;
  }

  // Debug
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route begin: Job name: " << job_n->name ();
  }

  // Init state
  _is_routing = true;
  _is_finished = false;

  // Start muxer
  _muxer.route_begin ( job_n );
  // Start feed
  _feed.job_begin ( job_n );
}

template < std::size_t DIM >
void
Feed_D< DIM >::route_end ()
{
  DEBUG_ASSERT ( is_routing () );
  DEBUG_ASSERT ( is_finished () );
  if ( _is_routing && _is_finished ) {
    // Debug
    _log.cat ( sev::logt::FL_DEBUG_0, "Route end." );

    _feed.job_end ();
    _muxer.route_end ();
    _is_routing = false;
    _is_finished = false;
  }
}

template < std::size_t DIM >
void
Feed_D< DIM >::route_abort ( bool forced_n )
{
  if ( !is_routing () ) {
    return;
  }
  // Debug
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route abort: Forced: " << forced_n;
  }

  _muxer.route_abort ( forced_n );

  // -- Forced abort
  if ( forced_n ) {
    // Abort feed only when forced.
    // On a soft abort it will be shut down when the muxer has finished.
    _feed.job_abort ();
    // Muxer should be idle after a forced abort
    assert ( _muxer.is_idling () );
    // The muxer event still sits in the feed output buffer
    _muxer_cargo_current = nullptr;
  }
  request_event_processing ();
}

template < std::size_t DIM >
void
Feed_D< DIM >::acquire_messages ( Stream_Request & request_n )
{
  //_log.cat ( sev::logt::FL_DEBUG_0, "acquire_messages" );

  if ( _muxer.is_running () ) {
    _muxer.messages_acquire_begin ( &request_n );
    muxer_acquire_messages ();
    _muxer.messages_acquire_end ();
  }
}

template < std::size_t DIM >
inline void
Feed_D< DIM >::muxer_acquire_messages ()
{
  // Debug
  //_log.cat ( sev::logt::FL_DEBUG_0, "muxer_acquire_messages" );
  using Token = typename Muxer_D< DIM >::Token;
  DEBUG_ASSERT ( is_routing () );

  // Poll muxer
  do {
    switch ( _muxer.read_next () ) {
    case Token::NONE:
      break;
    case Token::CARGO_FEED:
      if ( muxer_cargo_feed () ) {
        continue;
      }
      break;
    case Token::CARGO_RELEASE:
      muxer_cargo_release ();
      continue;
      break;
    case Token::PROCESS:
      _muxer.messages_acquire_process ();
      continue;
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }

    // Leave loop by default
    break;
  } while ( true );

  // Finished muxer handling
  if ( _muxer.is_finished_aborting () ) {
    _feed.job_abort ();
  }
}

template < std::size_t DIM >
bool
Feed_D< DIM >::muxer_cargo_feed ()
{
  auto & con = _feed.cargo_connection_in ();
  if ( con.is_empty () ) {
    return false;
  }

  // Read cargo from feed buffer
  auto * cargo = static_cast< snc::mpath_feed::Cargo * > ( con.front () );
  // Feed the cargo to muxer
  if ( _muxer.cargo_feed ( cargo ) ) {
    // Feed success.
    // Store event pointer as "event in use" flag
    DEBUG_ASSERT ( _muxer_cargo_current == nullptr );
    _muxer_cargo_current = cargo;
  }
  return true;
}

template < std::size_t DIM >
void
Feed_D< DIM >::muxer_cargo_release ()
{
  auto * cargo = _muxer.cargo_release ();
  DEBUG_ASSERT ( cargo != nullptr );
  if ( cargo == nullptr ) {
    return;
  }

  // Check if this a "STREAM_END" cargo element
  if ( cargo->cargo_type () == snc::mpath_feed::Cargo_Type::ELEMENT ) {
    auto * celem =
        static_cast< snc::mpath_feed::cargo::Element_D< DIM > * > ( cargo );
    if ( celem->elem_type () == snc::mpath::elem::Type::STREAM_END ) {
      // Soft abort feed
      _feed.job_abort ();
    }
  }

  DEBUG_ASSERT ( _muxer_cargo_current != nullptr );
  if ( _muxer_cargo_current != nullptr ) {
    // Return the cargo event from the feed
    auto & con = _feed.cargo_connection_in ();
    DEBUG_ASSERT ( !con.is_empty () );
    DEBUG_ASSERT ( con.front () == _muxer_cargo_current );
    con.pop_not_empty ();
    _muxer_cargo_current->release ();
    _muxer_cargo_current = nullptr;

    // Event popped from buffer, accept more events
    request_event_processing ();
  }
}

template < std::size_t DIM >
inline void
Feed_D< DIM >::request_event_processing ()
{
  _feed.request_event_processing ();
}

template < std::size_t DIM >
void
Feed_D< DIM >::process_events ()
{
  //_log.cat ( sev::logt::FL_DEBUG_0, "process_events" );

  // Process feed events
  _feed.process_events ();
  // Process abort
  if ( _feed.is_job_aborting () ) {
    process_events_abort ();
  }
}

template < std::size_t DIM >
void
Feed_D< DIM >::process_events_abort ()
{
  // Shut down muxer when the feed is finished
  if ( _feed.is_finished_job_aborting () ) {
    if ( _muxer.is_running () && _muxer.is_idling () ) {
      if ( sev::change ( _is_finished, true ) ) {
        request_event_processing ();
      }
    }
  }
}

// -- Instantiation

template class Feed_D< 1 >;
template class Feed_D< 2 >;
template class Feed_D< 3 >;
template class Feed_D< 4 >;
template class Feed_D< 5 >;
template class Feed_D< 6 >;

} // namespace snc::mpath_feed_emb
