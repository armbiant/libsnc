/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "muxer_d.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/msg/message.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_feed/job.hpp>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
Muxer_D< DIM >::Muxer_D ( const Context_D< DIM > & context_n,
                          const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, sev::string::cat ( "Muxer<", DIM, ">" ) )
, _process_token ( Token::NONE )
, _context ( context_n )
, _gen_stepper ( _context, _log )
, _gen_sleep ( _context, _log )
, _gen_control_bool ( _context, _log )
, _gen_init ( _context, _log )
, _stream_breaker ( _context.device_statics () )
{
  _generators[ 0 ] = &_gen_stepper;
  _generators[ 1 ] = &_gen_sleep;
  _generators[ 2 ] = &_gen_control_bool;
  _generators[ 3 ] = &_gen_init;
}

template < std::size_t DIM >
Muxer_D< DIM >::~Muxer_D ()
{
}

template < std::size_t DIM >
inline void
Muxer_D< DIM >::set_process_token ( Token request_n )
{
  _process_token = request_n;
}

template < std::size_t DIM >
void
Muxer_D< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  // Debug
  _log.cat ( sev::logt::FL_DEBUG_0, "Route begin." );

  // Init state flags
  set_process_token ( Token::NONE );
  _is_running = true;
  _abort_state.clear ();

  _sequence_request = nullptr;
  _cargo_feed = nullptr;
  _gen_current = nullptr;

  // Start generators
  for ( auto * gen : _generators ) {
    gen->route_begin ( job_n );
  }

  // Begin with init generator
  _gen_current = &_gen_init;
  _gen_current->cargo_process_begin ();

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  _debug_file_cargo_feed.open ( job_n->name () + "_muxer_cargo_source.txt" );
#endif
}

template < std::size_t DIM >
void
Muxer_D< DIM >::route_end ()
{
  // Debug
  _log.cat ( sev::logt::FL_DEBUG_0, "Route end." );

  // Shut down generators
  for ( auto * gen : _generators ) {
    gen->route_end ();
  }
// Debug
#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  _debug_file_cargo_feed.close ();
#endif

  _is_running = false;
  _abort_state.clear ();
}

template < std::size_t DIM >
void
Muxer_D< DIM >::route_abort ( bool forced_n )
{
  // Debug
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route abort: Forced: " << forced_n;
  }

  const bool break_new ( !_abort_state.test_any ( Abort_State::ENABLED ) );
  const bool forced_new ( !_abort_state.test_any ( Abort_State::FORCED ) &&
                          forced_n );
  if ( break_new || forced_new ) {
    // -- Update break state
    if ( break_new ) {
      _abort_state.set ( Abort_State::ENABLED );
    }
    if ( forced_new ) {
      // Abort internal feeding and switch to abort finished state
      _stream_breaker.close ();
      _abort_state.set ( Abort_State::FORCED );
      _abort_state.set ( Abort_State::IFEED );
      _abort_state.set ( Abort_State::IFEED_DONE );
    }

    // -- Notify current generator
    if ( _gen_current != nullptr ) {
      // Abort current generator
      _gen_current->route_abort ( forced_n );
      // In forced break the generator drops its cargo and switches to
      // FINISH mode
      if ( forced_new ) {
        DEBUG_ASSERT ( _gen_current->read_next () ==
                       gen::Generator_Token::FINISH );
        release_current_generator ();
        // Idle and done
        set_process_token ( Token::NONE );
      } else {
        // Soft abort: Process generator
        set_process_token ( Token::PROCESS );
      }
    } else {
      // No current generator
      if ( forced_new ) {
        // Idle and done
        set_process_token ( Token::NONE );
      } else {
        // Soft abort: Start internal break stream
        internal_stream_start ();
        set_process_token ( Token::PROCESS );
      }
    }
  }
}

template < std::size_t DIM >
void
Muxer_D< DIM >::messages_acquire_begin ( Stream_Request * request_n )
{
  // Debug
  //_log.cat ( sev::logt::FL_DEBUG_0, "messages_acquire_begin()" );
  DEBUG_ASSERT ( _sequence_request == nullptr );
  _sequence_request = request_n;

  // Ask the current generator
  set_process_token ( Token::PROCESS );
}

template < std::size_t DIM >
void
Muxer_D< DIM >::messages_acquire_end ()
{
  // Debug
  //_log.cat ( sev::logt::FL_DEBUG_0, "messages_acquire_end()" );
  DEBUG_ASSERT ( _sequence_request != nullptr );
  _sequence_request = nullptr;
}

template < std::size_t DIM >
void
Muxer_D< DIM >::messages_acquire_process ()
{
  // Debug
  //_log.cat ( sev::logt::FL_DEBUG_0, "messages_acquire_process()" );
  DEBUG_ASSERT ( _sequence_request != nullptr );

  if ( _gen_current != nullptr ) {
    // Poll generator
    // Acquire messages from generator
    _gen_current->acquire_router_messages ( _sequence_request );

    // Evaluate generator state
    switch ( _gen_current->read_next () ) {
    case gen::Generator_Token::CARGO_FEED:
      if ( !_abort_state.test_any ( Abort_State::IFEED ) ) {
        // External cargo feed
        set_process_token ( Token::CARGO_FEED );
      } else {
        // Internal cargo feed
        internal_cargo_feed ();
        set_process_token ( Token::PROCESS );
      }
      break;
    case gen::Generator_Token::CARGO_RELEASE:
      if ( !_abort_state.test_any ( Abort_State::IFEED ) ) {
        // External cargo
        set_process_token ( Token::CARGO_RELEASE );
      } else {
        // Internal cargo feed
        internal_cargo_release ();
        set_process_token ( Token::PROCESS );
      }
      break;
    case gen::Generator_Token::PROCESS:
      set_process_token ( Token::PROCESS );
      break;
    case gen::Generator_Token::DEVICE_PROGRESS_WAIT:
      set_process_token ( Token::NONE );
      break;
    case gen::Generator_Token::FINISH:
      release_current_generator ();
      set_process_token ( Token::CARGO_FEED );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
  } else {
    // No generator
    if ( !_abort_state.test_any ( Abort_State::IFEED ) ) {
      // External cargo feed
      set_process_token ( Token::CARGO_FEED );
    } else {
      // Internal cargo feed
      if ( !_abort_state.test_any ( Abort_State::IFEED_DONE ) ) {
        // Feed from internal source
        internal_cargo_feed ();
        set_process_token ( Token::PROCESS );
      } else {
        // Idle and done
        set_process_token ( Token::NONE );
      }
    }
  }
}

template < std::size_t DIM >
void
Muxer_D< DIM >::release_current_generator ()
{
  if ( _gen_current == nullptr ) {
    return;
  }

  DEBUG_ASSERT ( _gen_current->cargo () == nullptr );
  _gen_current->cargo_process_end ();
  _gen_current = nullptr;

  // Start internal stream on demand
  if ( is_breaking () && !is_forced_breaking () ) {
    internal_stream_start ();
  }
}

template < std::size_t DIM >
bool
Muxer_D< DIM >::cargo_feed ( snc::mpath_feed::Cargo * cargo_n )
{
  DEBUG_ASSERT ( cargo_n != nullptr );

  if ( ( _cargo_feed == nullptr ) &&
       !_abort_state.test_any ( Abort_State::IFEED ) ) {
    // Feed generators
    if ( cargo_feed_generators ( cargo_n ) ) {
      _cargo_feed = cargo_n;
    }

    // Update processing state
    if ( _gen_current != nullptr ) {
      // A generator is running
      set_process_token ( Token::PROCESS );
    } else {
      // No generator running
      if ( _cargo_feed != nullptr ) {
        // Releasable cargo
        set_process_token ( Token::CARGO_RELEASE );
      } else {
        // No cargo either
        set_process_token ( Token::CARGO_FEED );
      }
    }
  } else {
    set_process_token ( Token::PROCESS );
  }

  return ( _cargo_feed == cargo_n );
}

template < std::size_t DIM >
bool
Muxer_D< DIM >::cargo_feed_generators ( snc::mpath_feed::Cargo * cargo_n )
{
  DEBUG_ASSERT ( cargo_n != nullptr );

  bool cargo_accepted = false;

  if ( _gen_current != nullptr ) {
    // There is a generator running
    // Try to feed it with the new cargo
    if ( _gen_current->cargo_feed ( cargo_n ) ) {
      // Generator accepted cargo
      cargo_accepted = true;
    } else {
      // Generator refused cargo. Finish it instead.
      _gen_current->cargo_process_finish ();
    }
  } else {
    // No generator is running.
    // Evaluate cargo type and try
    // to load a new generator.
    switch ( cargo_n->cargo_type () ) {
    case snc::mpath_feed::Cargo_Type::CSTEPS:
      _gen_current = &_gen_stepper;
      break;
    case snc::mpath_feed::Cargo_Type::ELEMENT:
      // Evaluate element type
      {
        auto & ccargo =
            static_cast< const snc::mpath_feed::cargo::Element_D< DIM > & > (
                *cargo_n );
        const snc::mpath::Element & elem = ccargo.ebuffer ().elem ();
        switch ( elem.elem_type () ) {
        case snc::mpath::elem::Type::SLEEP:
          _gen_current = &_gen_sleep;
          break;
        case snc::mpath::elem::Type::FADER:
          _gen_current = &_gen_control_bool;
          break;
        default:
          // Ignore
          break;
        }
      }
      break;
    default:
      // Ignore
      break;
    }

    // Start new generator
    if ( _gen_current != nullptr ) {
      _gen_current->cargo_process_begin ();
      _gen_current->cargo_feed ( cargo_n );
    }
    // Accept cargo.
    cargo_accepted = true;
  }

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  if ( cargo_accepted ) {
    snc::mpath_feed::debug_write_cargo< DIM > ( _debug_file_cargo_feed,
                                                *cargo_n );
    _debug_file_cargo_feed.flush ();
  }
#endif

  return cargo_accepted;
}

template < std::size_t DIM >
snc::mpath_feed::Cargo *
Muxer_D< DIM >::cargo_release ()
{
  //_log.cat ( sev::logt::FL_DEBUG_0, "cargo_release()" );

  if ( _gen_current != nullptr ) {
    // Call generator
    _gen_current->cargo_release ();
  }

  // Update processing state
  set_process_token ( Token::PROCESS );

  // Return and clear cargo
  snc::mpath_feed::Cargo * res = _cargo_feed;
  _cargo_feed = nullptr;
  return res;
}

template < std::size_t DIM >
void
Muxer_D< DIM >::internal_stream_start ()
{
  if ( _abort_state.test_any_set ( Abort_State::IFEED ) ) {
    return;
  }

  // Debug
  _log.cat ( sev::logt::FL_DEBUG_0, "Internal break stream start" );

  // Determine if cutter is running
  bool cutter_is_running ( false );
  {
    const auto ctl_bool =
        context ().device_statics ()->controls_i1 ().get_by_key ( "cutter" );
    if ( ctl_bool ) {
      bool switch_valid = false;
      bool switch_value = false;
      _gen_control_bool.switch_state_latest (
          ctl_bool->emb ().control_index (), switch_valid, switch_value );
      if ( switch_valid ) {
        if ( switch_value != ctl_bool->emb ().inverted () ) {
          cutter_is_running = true;
        }
      }
    }
  }
  // Start break stream
  _stream_breaker.setup ( cutter_is_running );
  _stream_breaker.open ();
}

template < std::size_t DIM >
void
Muxer_D< DIM >::internal_cargo_feed ()
{
  // Debug
  //_log.cat ( sev::logt::FL_DEBUG_0, "Internal cargo feed." );
  DEBUG_ASSERT ( !_abort_state.test_any ( Abort_State::IFEED_DONE ) );

  // -- Break stream feeding
  // The break cargo element gets invalidated in cargo_release()
  if ( !_cargo_elem_break.elem ().is_valid () ) {
    if ( _stream_breaker.is_good () ) {
      // - Read stream element
      auto * elem = _stream_breaker.read ();
      _cargo_elem_break.ebuffer ().copy ( *elem );
    } else {
      // - Finish stream
      _log.cat ( sev::logt::FL_DEBUG_0, "Break stream finished." );
      // Break stream finished
      _stream_breaker.close ();
      // Abort current generator
      if ( _gen_current != nullptr ) {
        _gen_current->cargo_process_finish ();
      }
      // Update internal feed state
      _abort_state.set ( Abort_State::IFEED_DONE );
    }
  }

  // Feed valid break stream element
  if ( _cargo_elem_break.elem ().is_valid () ) {
    if ( cargo_feed_generators ( &_cargo_elem_break ) ) {
      if ( _gen_current == nullptr ) {
        internal_cargo_release ();
      }
    }
  }
}

template < std::size_t DIM >
void
Muxer_D< DIM >::internal_cargo_release ()
{
  //_log.cat ( sev::logt::FL_DEBUG_0, "cargo_release()" );

  DEBUG_ASSERT ( _cargo_elem_break.ebuffer ().elem ().is_valid () );

  if ( _gen_current != nullptr ) {
    // Call generator
    _gen_current->cargo_release ();
  }
  // Reset internal cargo
  _cargo_elem_break.ebuffer ().reset ();
}

// -- Instantiation

template class Muxer_D< 1 >;
template class Muxer_D< 2 >;
template class Muxer_D< 3 >;
template class Muxer_D< 4 >;
template class Muxer_D< 5 >;
template class Muxer_D< 6 >;

} // namespace snc::mpath_feed_emb
