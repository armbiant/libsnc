/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <snc/mpath_feed/feed.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed_emb/context_d.hpp>
#include <snc/mpath_feed_emb/muxer_d.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>
#include <memory>

// -- Forward declaration1
namespace snc
{
class Sequence_Request;
}

namespace snc::mpath_feed_emb
{

/// @brief Container class for mpath stream to embedded device message
/// generation
///
/// "Feed" means the control/reader/stepper/speeder/ threads.
///
/// -- Soft breaking
/// For soft breaking
/// - the device must decelerate until hold
/// - device faders/switches must be set to a safe value (cutter off)
/// - the muxer must then be in transparent idle state (event pipe through)
/// - the feed stream must be aborted
/// - events from the from the feed must be read and returned until the feed has
/// dried up
/// - the feed must be shut down
///
/// -- Forced breaking
/// For forced breaking
/// - the muxer must immediately be switched into transparent idle state
/// - the feed must be shut down as in soft breaking
///
template < std::size_t DIM >
class Feed_D
{
  public:
  // -- Types

  using Stream_Request = snc::mpath_feed_emb::Stream_Request_D< DIM >;

  struct LEvent
  {
    static constexpr std::uint_fast32_t CONTROL_INCOME = ( 1 << 0 );
    static constexpr std::uint_fast32_t CONTROL_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t CARGO_INCOME = ( 1 << 2 );
    static constexpr std::uint_fast32_t CARGO_OUT = ( 1 << 3 );
  };

  struct Abort_State
  {
    static constexpr std::uint8_t ABORT = ( 1 << 0 );
    static constexpr std::uint8_t CONTROL_ABORT_SENT = ( 1 << 1 );
    static constexpr std::uint8_t CONTROL_ABORT_DONE = ( 1 << 2 );
  };

  // -- Construction

  Feed_D ( const sev::logt::Reference & log_parent_n,
           const Context_D< DIM > & context_n );

  ~Feed_D ();

  // -- Accessors

  const Context_D< DIM > &
  context () const
  {
    return _muxer.context ();
  }

  // -- Session control

  void
  session_begin ( const std::function< void () > & process_request_callback_n );

  void
  session_end ();

  // -- Routing session control

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n );

  void
  route_end ();

  void
  route_abort ( bool forced_n );

  // -- Session state

  bool
  is_routing () const
  {
    return _is_routing;
  }

  bool
  is_finished () const
  {
    return _is_finished;
  }

  // -- Device messages

  void
  acquire_messages ( Stream_Request & request_n );

  // -- Central processing

  /// @brief Processes internale events
  ///
  /// Muste be called when process_request_callback_n from begin_walking() was
  /// triggered.
  void
  process_events ();

  private:
  // -- Utility

  void
  muxer_acquire_messages ();

  /// @return True if a cargo element was accepted
  bool
  muxer_cargo_feed ();

  /// @return True if the release was successful
  void
  muxer_cargo_release ();

  void
  request_event_processing ();

  void
  process_events_abort ();

  private:
  // -- Attributes
  sev::logt::Context _log;
  bool _is_routing = false;
  bool _is_finished = false;
  snc::mpath_feed::Feed _feed;
  Muxer_D< DIM > _muxer;
  snc::mpath_feed::Cargo * _muxer_cargo_current = nullptr;
};

} // namespace snc::mpath_feed_emb
