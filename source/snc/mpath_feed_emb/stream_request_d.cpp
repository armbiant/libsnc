/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stream_request_d.hpp"
#include <snc/svs/fac/tracker/ship/sailor.hpp>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
Stream_Request_D< DIM >::Stream_Request_D ( Emb_Writer stream_n,
                                            const Tracker & tracker_n )
: stream ( stream_n )
{
  for ( std::size_t ii = 0; ii != axis_tracker.size (); ++ii ) {
    axis_tracker[ ii ] = tracker_n.local_axis_tracker ( ii );
  }
}

// -- Instantiation

template class Stream_Request_D< 1 >;
template class Stream_Request_D< 2 >;
template class Stream_Request_D< 3 >;
template class Stream_Request_D< 4 >;
template class Stream_Request_D< 5 >;
template class Stream_Request_D< 6 >;

} // namespace snc::mpath_feed_emb
