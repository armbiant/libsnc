/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <array>
#include <cstdint>
#include <numeric>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
class Axes_Mapping_D
{
  public:
  // -- Types

  using Maps = std::array< std::uint_least8_t, DIM >;

  // -- Construction

  Axes_Mapping_D () { std::iota ( _maps.begin (), _maps.end (), 0 ); }

  // -- Interface

  const Maps &
  maps () const
  {
    return _maps;
  }

  std::uint_least8_t
  map ( std::size_t axis_index_n ) const
  {
    return _maps[ axis_index_n ];
  }

  void
  set_map ( std::size_t axis_n, std::uint_least8_t map_n )
  {
    _maps[ axis_n ] = map_n;
  }

  private:
  Maps _maps;
};

} // namespace snc::mpath_feed_emb
