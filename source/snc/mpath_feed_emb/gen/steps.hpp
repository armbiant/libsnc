/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/lag/vector.hpp>
#include <sev/math/exponents.hpp>
#include <sev/math/numbers.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <snc/emb/msg/out/steps_u16_64.hpp>
#include <snc/math/vector.hpp>
#include <snc/mpath_feed/debug.hpp>
#include <snc/mpath_feed/speeder/step_celerator_d.hpp>
#include <snc/mpath_feed_emb/gen/generator.hpp>
#include <snc/mpath_feed_emb/gen/steps_axis.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>
#include <snc/utility/debug_file.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
class Steps : public Generator< DIM >
{
  // -- Types
  private:
  using Super = Generator< DIM >;
  using Stream_Request = typename Super::Stream_Request;

  public:
  struct Axis_Request
  {
    Axis_Request ( snc::svp::ship::pilot::Emb_Writer stream_n )
    : stream ( std::move ( stream_n ) )
    {
    }

    Steps_Axis * axis = nullptr;
    snc::svs::fac::tracker::ship::Local_Axis * tracker = nullptr;
    snc::svp::ship::pilot::Emb_Writer stream;
    std::size_t index = 0;
    std::size_t mapped_index = 0;
  };

  // -- Construction

  Steps ( const Context_D< DIM > & context_n,
          const sev::logt::Reference & log_parent_n );

  ~Steps ();

  // -- Generator interface

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n ) override;

  void
  route_abort ( bool forced_n ) override;

  void
  route_end () override;

  void
  cargo_process_begin () override;

  void
  cargo_process_end () override;

  void
  cargo_process_finish () override;

  bool
  cargo_feed ( snc::mpath_feed::Cargo * tile_n ) override;

  snc::mpath_feed::Cargo *
  cargo_release () override;

  void
  acquire_router_messages ( Stream_Request * request_n ) override;

  private:
  // -- Utility

  bool
  is_finished () const;

  void
  generate ( Stream_Request * request_n );

  bool
  generate_axis ( const Axis_Request & areq_n );

  bool
  feed_axis_steps_buffers ();

  bool
  feed_axis_steps_buffer ( Steps_Axis & axis_n );

  bool
  acquire_axes_step_splitters_slots ();

  void
  feed_grid_step_breakable ( const snc::mpath_feed::CStep< DIM > & step_n );

  void
  feed_grid_step ( const snc::mpath_feed::CStep< DIM > & step_n );

  void
  feed_grid_step_process_axes_deltas ();

  void
  finish_stepping_flush ();

  bool
  flush_pending_step ();

  void
  debug_axes ();

  void
  debug_write_step ( const snc::mpath_feed::CStep< DIM > & step_n );

  void
  debug_write_usecs ( std::size_t axis_index_n,
                      std::int8_t step_dir_n,
                      std::uint_fast32_t usecs_n );

  private:
  // -- Attributes
  /// @brief Input tile next step index
  const snc::mpath_feed::CStep< DIM > * _cargo_step_next = nullptr;
  const snc::mpath_feed::CStep< DIM > * _cargo_step_end = nullptr;

  // @brief May get filled from multiple tiles
  std::size_t _current_axis_index = 0;
  snc::emb::msg::out::Steps_U16_64 * _current_axis_message = nullptr;
  snc::emb::msg::out::Steps_U16_64 _axis_message;

  // Step temporary variables
  snc::mpath_feed::CStep< DIM > _step_prev;
  sev::lag::Vector< double, DIM > _step_speed_min;
  sev::lag::Vector< double, DIM > _step_speed_stoppable;

  bool _break_enabled = false;
  bool _break_complete = false;
  bool _axes_steps_buffer_full = false;
  bool _finish_stepping = false;
  bool _finish_stepping_flushed = false;

  // Axes
  std::array< Steps_Axis, DIM > _axis;

  // Breaking
  snc::mpath_feed::speeder::Step_Celerator_D< DIM > _celerator;

  // File debugging
  snc::utility::Debug_File _debug_file_steps;
  snc::utility::Debug_File _debug_file_usecs;
  std::array< std::uint_fast32_t, DIM > _axis_usecs_sum;
};

// -- Types

typedef Steps< 1 > Muxer_Gen_Steps1;
typedef Steps< 2 > Muxer_Gen_Steps2;
typedef Steps< 3 > Muxer_Gen_Steps3;
typedef Steps< 4 > Muxer_Gen_Steps4;
typedef Steps< 5 > Muxer_Gen_Steps5;
typedef Steps< 6 > Muxer_Gen_Steps6;

} // namespace snc::mpath_feed_emb::gen
