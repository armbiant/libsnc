/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed_emb/gen/generator.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
class Init : public Generator< DIM >
{
  // -- Types
  private:
  using Super = Generator< DIM >;
  using Stream_Request = typename Super::Stream_Request;

  public:
  // -- Construction

  Init ( const Context_D< DIM > & context_n,
         const sev::logt::Reference & log_parent_n );

  ~Init ();

  // -- Generator interface

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n ) override;

  void
  route_abort ( bool forced_n ) override;

  void
  route_end () override;

  void
  cargo_process_begin () override;

  void
  cargo_process_end () override;

  void
  cargo_process_finish () override;

  bool
  cargo_feed ( snc::mpath_feed::Cargo * tile_n ) override;

  snc::mpath_feed::Cargo *
  cargo_release () override;

  void
  acquire_router_messages ( Stream_Request * request_n ) override;

  private:
  // -- Attributes
  bool _message_submitted = false;
};

} // namespace snc::mpath_feed_emb::gen
