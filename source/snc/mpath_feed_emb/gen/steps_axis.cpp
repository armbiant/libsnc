/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "steps_axis.hpp"
#include <sev/assert.hpp>
#include <snc/emb/config/stepper.hpp>

namespace snc::mpath_feed_emb::gen
{

Steps_Axis::Steps_Axis ()
: step_splitters_rbuffer ( 256 )
, steps_rbuffer ( 256 )
{
}

void
Steps_Axis::init ( std::uint8_t axis_index_n )
{
  axis_index = axis_index_n;
  axis_dir_reversed = false;
  reset ();
}

void
Steps_Axis::reset ()
{
  pending_step_dir = 0;
  delta_step_dir = 0;

  pending_usecs = 0.0;
  delta_usecs = 0.0;

  num_step_usecs_available = 0;

  steps_rbuffer.clear ();
  step_splitters_rbuffer.clear ();
}

void
Steps_Axis::begin ( const snc::device::statics::Axis & statics_n )
{
  reset ();

  axis_dir_reversed = statics_n.emb ().direction_reversed ();
  step_slice_usecs_max = statics_n.emb ().stepper ()->step_slice_usecs_max ();
  num_usecs_queued_min =
      statics_n.emb ().stepper ()->steps_queue_hint_usecs_min ();
  num_usecs_queued_max =
      statics_n.emb ().stepper ()->steps_queue_hint_usecs_max ();
}

} // namespace snc::mpath_feed_emb::gen
