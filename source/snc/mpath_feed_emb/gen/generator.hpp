/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/mpath_feed/cargos.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed_emb/context_d.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

// -- Forward declaration
namespace snc::mpath_feed_emb
{
template < std::size_t DIM >
class Stream_Request_D;
}

namespace snc::mpath_feed_emb::gen
{

enum class Generator_Token
{
  CARGO_FEED,
  CARGO_RELEASE,
  PROCESS,
  DEVICE_PROGRESS_WAIT,
  FINISH
};

/// @brief Base class for Muxer generators
///
template < std::size_t DIM >
class Generator
{
  public:
  // -- Types

  using Stream_Request = snc::mpath_feed_emb::Stream_Request_D< DIM >;

  // -- Construction

  Generator ( const Context_D< DIM > & context_n,
              const sev::logt::Reference & log_parent_n,
              sev::unicode::View log_name_n );

  virtual ~Generator ();

  // -- Accessors

  const snc::mpath_feed::Cargo *
  cargo () const
  {
    return _cargo;
  }

  const snc::mpath_feed::cargo::Element_D< DIM > *
  cargo_elem () const
  {
    DEBUG_ASSERT ( cargo ()->cargo_type () ==
                   snc::mpath_feed::Cargo_Type::ELEMENT );
    return static_cast< const snc::mpath_feed::cargo::Element_D< DIM > * > (
        _cargo );
  }

  const snc::mpath_feed::cargo::CSteps< DIM > *
  cargo_steps () const
  {
    DEBUG_ASSERT ( cargo ()->cargo_type () ==
                   snc::mpath_feed::Cargo_Type::CSTEPS );
    return static_cast< const snc::mpath_feed::cargo::CSteps< DIM > * > (
        cargo () );
  }

  // -- Accessors

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  const Context_D< DIM > &
  context () const
  {
    return _context;
  }

  const snc::device::statics::handle::Statics &
  device_statics () const
  {
    return context ().device_statics ();
  }

  // -- Generator interface

  virtual void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n );

  virtual void
  route_abort ( bool forced_n );

  virtual void
  route_end () = 0;

  Generator_Token
  read_next () const
  {
    return _process_token;
  }

  virtual void
  cargo_process_begin ();

  virtual void
  cargo_process_end ();

  /// @brief Request cargo processing finish
  virtual void
  cargo_process_finish ();

  virtual bool
  cargo_feed ( snc::mpath_feed::Cargo * tile_n ) = 0;

  virtual snc::mpath_feed::Cargo *
  cargo_release ();

  /// @brief Return true if message generation was successful
  virtual void
  acquire_router_messages ( Stream_Request * request_n ) = 0;

  protected:
  // -- Utility

  void
  set_cargo ( snc::mpath_feed::Cargo * cargo_n )
  {
    _cargo = cargo_n;
  }

  void
  set_process_token ( Generator_Token request_n )
  {
    _process_token = request_n;
  }

  private:
  // -- Attributes
  sev::logt::Context _log;
  const Context_D< DIM > & _context;
  Generator_Token _process_token = Generator_Token::CARGO_FEED;
  snc::mpath_feed::Cargo * _cargo = nullptr;
};

} // namespace snc::mpath_feed_emb::gen
