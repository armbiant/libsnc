/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/ring_fixed.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/emb/step_splitter.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

namespace snc::mpath_feed_emb::gen
{

class Steps_Axis
{
  public:
  // -- Types

  struct Step
  {
    snc::emb::type::Step job_type;
    std::uint16_t usecs;
  };

  using Step_Splitter_RBuffer = sev::mem::Ring_Fixed< snc::emb::Step_Splitter >;
  using Step_RBuffer = sev::mem::Ring_Fixed< Step >;

  // -- Construction and setup

  Steps_Axis ();

  void
  init ( std::uint8_t axis_index_n );

  void
  reset ();

  // -- Interface

  void
  begin ( const snc::device::statics::Axis & statics_n );

  bool
  split_step ();

  snc::emb::type::Step
  axis_job_type ( std::int8_t step_dir_n );

  public:
  // -- Attributes
  std::uint8_t axis_index = 0;
  bool axis_dir_reversed = false;

  std::int8_t pending_step_dir = 0;
  std::int8_t delta_step_dir = 0;

  double pending_usecs = 0.0;
  double delta_usecs = 0.0;

  std::uint_fast32_t num_step_usecs_available = 0;
  std::uint_fast32_t step_slice_usecs_max = 0;
  std::uint_fast32_t num_usecs_queued_min = 0;
  std::uint_fast32_t num_usecs_queued_max = 0;

  Step_Splitter_RBuffer step_splitters_rbuffer;
  Step_RBuffer steps_rbuffer;
};

inline bool
Steps_Axis::split_step ()
{
  if ( !step_splitters_rbuffer.is_empty () && !steps_rbuffer.is_full () ) {
    // Take a slice from the first step splitter
    {
      auto & split_step = step_splitters_rbuffer.front ();
      {
        Step step;
        step.job_type = split_step.job_type ();
        step.usecs = split_step.acquire_usecs_next ();
        steps_rbuffer.push_back_not_full ( step );
      }
      if ( split_step.is_finished () ) {
        step_splitters_rbuffer.pop_front_not_empty ();
      }
    }
    return true;
  }
  return false;
}

inline snc::emb::type::Step
Steps_Axis::axis_job_type ( std::int8_t step_dir_n )
{
  snc::emb::type::Step job_type ( snc::emb::type::Step::HOLD );
  if ( step_dir_n != 0 ) {
    job_type =
        snc::emb::type::step_move ( ( step_dir_n > 0 ), axis_dir_reversed );
  }
  return job_type;
}

} // namespace snc::mpath_feed_emb::gen
