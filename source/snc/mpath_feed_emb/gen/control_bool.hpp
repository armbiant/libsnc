/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed_emb/gen/generator.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
class Control_Bool : public Generator< DIM >
{
  // -- Types
  private:
  using Super = Generator< DIM >;
  using Stream_Request = typename Super::Stream_Request;

  public:
  // -- Construction

  Control_Bool ( const Context_D< DIM > & context_n,
                 const sev::logt::Reference & log_parent_n );

  ~Control_Bool ();

  // -- Generator interface

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n ) override;

  void
  route_abort ( bool forced_n ) override;

  void
  route_end () override;

  void
  cargo_process_begin () override;

  void
  cargo_process_end () override;

  void
  cargo_process_finish () override;

  bool
  cargo_feed ( snc::mpath_feed::Cargo * tile_n ) override;

  snc::mpath_feed::Cargo *
  cargo_release () override;

  void
  acquire_router_messages ( Stream_Request * request_n ) override;

  void
  switch_state_latest ( uint8_t switch_index_n,
                        bool & value_valid_n,
                        bool & value_n ) const;

  private:
  // -- Utility
  void
  track_switch_state ( uint8_t switch_index_n, bool state_n );

  private:
  // -- Attributes
  bool _message_send = false;
  std::uint8_t _switch_index = 0;
  bool _switch_state = false;

  std::array< std::uint8_t, 32 > _switch_states;
};

} // namespace snc::mpath_feed_emb::gen
