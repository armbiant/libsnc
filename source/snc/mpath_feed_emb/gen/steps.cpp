/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "steps.hpp"
#include <sev/string/utility.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/mpath_feed/debug.hpp>
#include <snc/mpath_feed/job_d.hpp>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
Steps< DIM >::Steps ( const Context_D< DIM > & context_n,
                      const sev::logt::Reference & log_parent_n )
: Super ( context_n, log_parent_n, sev::string::cat ( "Steps<", DIM, ">" ) )
{
  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    _axis[ ii ].init ( ii );
  }
}

template < std::size_t DIM >
Steps< DIM >::~Steps ()
{
}

template < std::size_t DIM >
void
Steps< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  Super::route_begin ( job_n );

  const auto & job_dim =
      static_cast< const snc::mpath_feed::Job_D< DIM > & > ( *job_n );

  _cargo_step_next = 0;
  _cargo_step_end = 0;

  _current_axis_message = nullptr;
  _current_axis_index = 0;

  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    _axis[ ii ].begin ( *Super::context ().axis_statics ( ii ) );
  }
  _step_prev.pos = job_dim.begin_position ();
  _step_prev.tangent.fill ( 0.0 );
  _step_prev.speed = 0.0;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    const auto & dsaxis = *Super::context ().axis_statics ( ii );
    _step_speed_min[ ii ] = dsaxis.geo ().speed_min ();
    _step_speed_stoppable[ ii ] = dsaxis.geo ().speed_stoppable ();
  }

  _break_enabled = false;
  _break_complete = false;
  _axes_steps_buffer_full = false;
  _finish_stepping = false;
  _finish_stepping_flushed = false;

  // Setup celerator
  {
    // Axes limits
    snc::mpath_feed::Axes_Limits_D< DIM > axes_limits;

    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto & dsaxis = *Super::context ().axis_statics ( ii );
      snc::mpath_feed::Axis_Limits & alimits = axes_limits[ ii ];
      alimits.speed_reversible = dsaxis.geo ().speed_reversible ();
      alimits.speed_max = dsaxis.geo ().speed_max ();
      alimits.accel_max = dsaxis.geo ().accel_max ();
    }

    _celerator.set_axes_limits ( axes_limits );
  }

// Debugging
#if ( MPATH_WALKER_DEBUG_MUXER_STEPS )
  _debug_file_steps.open ( mpath_name_n + "_muxer_stepper_steps.txt" );
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    if ( _debug_file_steps.is_open () ) {
      logo << "Writing steps to file:\n";
    } else {
      logo << "Could not open steps file:\n";
    }
    logo << _debug_file_steps.file_name ();
  };
#endif
#if ( MPATH_WALKER_DEBUG_MUXER_USECS )
  _debug_file_usecs.open ( mpath_name_n + "_muxer_stepper_usecs.txt" );
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    if ( _debug_file_steps.is_open () ) {
      logo << "Writing usecs to file:\n";
    } else {
      logo << "Could not open usecs file:\n";
    }
    logo << _debug_file_usecs.file_name ();
  }
#endif
}

template < std::size_t DIM >
void
Steps< DIM >::route_abort ( bool forced_n )
{
  Super::route_abort ( forced_n );

  if ( !forced_n ) {
    // Soft breaking
    _break_enabled = true;
  } else {
    // Hard breaking
    _break_enabled = false;
    _break_complete = false;
    _axes_steps_buffer_full = false;
    _finish_stepping = true;
    _finish_stepping_flushed = true;
    for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
      _axis[ ii ].reset ();
    }
    // Forced break releases cargo immediately
    Super::set_cargo ( nullptr );
    _cargo_step_next = _cargo_step_end;
    // Release axis message
    _current_axis_message = nullptr;
    Super::set_process_token ( Generator_Token::FINISH );
  }
}

template < std::size_t DIM >
void
Steps< DIM >::route_end ()
{
  Super::route_end ();

  // Release axis message
  _current_axis_message = nullptr;

#if ( MPATH_WALKER_DEBUG_MUXER_STEPS )
  _debug_file_steps.close ();
#endif
#if ( MPATH_WALKER_DEBUG_MUXER_USECS )
  _debug_file_usecs.close ();
#endif
}

template < std::size_t DIM >
void
Steps< DIM >::cargo_process_begin ()
{
  Super::cargo_process_begin ();

  _cargo_step_next = 0;
  _cargo_step_end = 0;
  _axes_steps_buffer_full = false;
  _finish_stepping = false;
  _finish_stepping_flushed = false;
  // Reset axes
  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    _axis[ ii ].reset ();
  }
  Super::set_process_token ( Generator_Token::CARGO_FEED );
}

template < std::size_t DIM >
void
Steps< DIM >::cargo_process_end ()
{
  Super::cargo_process_end ();
  DEBUG_ASSERT ( Super::cargo () == nullptr );
}

template < std::size_t DIM >
bool
Steps< DIM >::cargo_feed ( snc::mpath_feed::Cargo * cargo_n )
{
  if ( cargo_n->cargo_type () != snc::mpath_feed::Cargo_Type::CSTEPS ) {
    return false;
  }
  Super::set_cargo ( cargo_n );

  // Debug
  // log ().cat ( sev::logt::FL_DEBUG_0, "cargo_feed()" );

  // Reset step reading index
  _cargo_step_next = Super::cargo_steps ()->begin ();
  _cargo_step_end = Super::cargo_steps ()->end ();
  Super::set_process_token ( Generator_Token::PROCESS );
  return true;
}

template < std::size_t DIM >
snc::mpath_feed::Cargo *
Steps< DIM >::cargo_release ()
{
  if ( _finish_stepping ) {
    Super::set_process_token ( is_finished () ? Generator_Token::FINISH
                                              : Generator_Token::PROCESS );
  } else {
    // Request another tile
    Super::set_process_token ( Generator_Token::CARGO_FEED );
  }
  return Super::cargo_release ();
}

template < std::size_t DIM >
bool
Steps< DIM >::is_finished () const
{
  bool res = false;
  if ( _finish_stepping && _finish_stepping_flushed ) {
    res = true;
    // Check if there are pending steps in the axis buffers
    for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
      const Steps_Axis & maxis = _axis[ ii ];
      if ( ( maxis.pending_step_dir != 0 ) ||
           !maxis.steps_rbuffer.is_empty () ||
           !maxis.step_splitters_rbuffer.is_empty () ) {
        res = false;
        break;
      }
    }
  }
  return res;
}

template < std::size_t DIM >
void
Steps< DIM >::acquire_router_messages ( Stream_Request * request_n )
{
  if ( _finish_stepping ) {
    if ( !_finish_stepping_flushed ) {
      finish_stepping_flush ();
    }
  }

  generate ( request_n );

  // Update processing request
  if ( Super::cargo () == nullptr ) {
    if ( _finish_stepping ) {
      Super::set_process_token ( is_finished ()
                                     ? Generator_Token::FINISH
                                     : Generator_Token::DEVICE_PROGRESS_WAIT );
    } else {
      // Request another tile
      Super::set_process_token ( Generator_Token::CARGO_FEED );
    }
  } else {
    Super::set_process_token ( ( _cargo_step_next == _cargo_step_end )
                                   ? Generator_Token::CARGO_RELEASE
                                   : Generator_Token::DEVICE_PROGRESS_WAIT );
  }
}

template < std::size_t DIM >
inline void
Steps< DIM >::generate ( Stream_Request * request_n )
{
  Axis_Request areq ( request_n->stream );
  while ( true ) {
    if ( _current_axis_message == nullptr ) {
      while ( true ) {
        // Find the axis with the most available step usecs
        std::uint_fast32_t usecs_avail_max = 0;
        // Loop through axes
        for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
          Steps_Axis & maxis = _axis[ ii ];
          if ( usecs_avail_max < maxis.num_step_usecs_available ) {
            usecs_avail_max = maxis.num_step_usecs_available;
            areq.index = ii;
          }
        }

        // No axis has step usecs available?
        if ( usecs_avail_max == 0 ) {
          if ( feed_axis_steps_buffers () ) {
            continue;
          }
          break;
        }

        // Axis found
        // Update references
        areq.axis = &_axis[ areq.index ];
        {
          areq.mapped_index =
              Super::context ().axes_mapping ().map ( areq.index );
          areq.tracker = &request_n->axis_tracker[ areq.mapped_index ];
        }

        // Check if the number of queued usecs is below the minimum
        if ( ( areq.tracker->queue_slots_free () != 0 ) &&
             ( areq.tracker->queue_usecs () <
               areq.axis->num_usecs_queued_min ) ) {
          _current_axis_index = areq.index;
          _current_axis_message = &_axis_message;
          _current_axis_message->reset ();
        }
        break;
      }
    } else {
      // Update references
      areq.index = _current_axis_index;
      areq.axis = &_axis[ areq.index ];
      {
        areq.mapped_index =
            Super::context ().axes_mapping ().map ( areq.index );
        areq.tracker = &request_n->axis_tracker[ areq.mapped_index ];
      }
    }

    if ( _current_axis_message != nullptr ) {
      if ( !generate_axis ( areq ) ) {
        break;
      }
    } else {
      break;
    }
  }
}

template < std::size_t DIM >
inline bool
Steps< DIM >::generate_axis ( const Axis_Request & areq_n )
{
  bool flush_success = true;
  bool step_fetch_good = true;
  bool step_job_type_change = false;

  // Debug
  // debug_axes();

  auto & steps = *_current_axis_message;
  do {
    if ( steps.values_full () ) {
      break;
    }
    if ( ( areq_n.tracker->queue_slots_free () == 0 ) ||
         ( areq_n.tracker->queue_usecs () >=
           areq_n.axis->num_usecs_queued_max ) ) {
      break;
    }

    if ( areq_n.axis->steps_rbuffer.is_empty () ) {
      if ( !feed_axis_steps_buffer ( *( areq_n.axis ) ) ) {
        step_fetch_good = false;
        break;
      }
    } else {
      // Use the first axis step from the buffer
      const Steps_Axis::Step & step = areq_n.axis->steps_rbuffer.front ();
      if ( steps.values_empty () ) {
        steps.set_stepper_index ( areq_n.mapped_index );
        steps.set_job_type ( step.job_type );
      }
      if ( steps.job_type () == step.job_type ) {
        steps.push_usecs ( step.usecs );
        // Update tracker
        {
          const std::uint_fast32_t usecs = step.usecs;
          areq_n.tracker->track ( 1, usecs );
          DEBUG_ASSERT ( areq_n.axis->step_slice_usecs_max >= usecs );
          DEBUG_ASSERT ( areq_n.axis->num_step_usecs_available >= usecs );
          areq_n.axis->num_step_usecs_available -= usecs;
        }
        areq_n.axis->steps_rbuffer.pop_front_not_empty ();

        // Debug
        // debug_axes();
      } else {
        // Job type missmatch
        step_job_type_change = true;
        break;
      }
    }
  } while ( true );

  if ( !steps.values_empty () ) {
    if ( step_fetch_good || step_job_type_change || _axes_steps_buffer_full ||
         _finish_stepping ) {
      // Flush
      areq_n.stream.push ( steps );
      _current_axis_message = nullptr;
      _axes_steps_buffer_full = false;
    } else {
      // Don't flush. Hold due to failed step fetching.
      flush_success = false;
    }
  } else {
    _current_axis_message = nullptr;
    flush_success = false;
  }

  return flush_success;
}

template < std::size_t DIM >
inline bool
Steps< DIM >::feed_axis_steps_buffers ()
{
  bool any_feed = false;
  for ( Steps_Axis & axis : _axis ) {
    if ( axis.steps_rbuffer.is_empty () ) {
      if ( feed_axis_steps_buffer ( axis ) ) {
        any_feed = true;
      }
    }
  }
  return any_feed;
}

template < std::size_t DIM >
bool
Steps< DIM >::feed_axis_steps_buffer ( Steps_Axis & axis_n )
{
  bool steps_splitted = false;
  do {
    if ( axis_n.split_step () ) {
      steps_splitted = true;
      break;
    }
    {
      // Try to feed another grid step
      const snc::mpath_feed::CStep< DIM > * step = nullptr;
      if ( Super::cargo () != nullptr ) {
        if ( acquire_axes_step_splitters_slots () ) {
          if ( _cargo_step_next != _cargo_step_end ) {
            step = _cargo_step_next;
            ++_cargo_step_next;
          }
        } else {
          _axes_steps_buffer_full = true;
        }
      }
      if ( step != nullptr ) {
        feed_grid_step_breakable ( *step );
      } else {
        break;
      }
    }
  } while ( true );
  return steps_splitted;
}

template < std::size_t DIM >
inline bool
Steps< DIM >::acquire_axes_step_splitters_slots ()
{
  for ( Steps_Axis & axis : _axis ) {
    while ( axis.step_splitters_rbuffer.is_full () ) {
      if ( !axis.split_step () ) {
        return false;
      }
    }
  }
  return true;
}

template < std::size_t DIM >
inline void
Steps< DIM >::feed_grid_step_breakable (
    const snc::mpath_feed::CStep< DIM > & step_n )
{
  if ( _break_enabled ) {
    if ( !_break_complete ) {
      // Calculated slowed down step
      snc::mpath_feed::CStep< DIM > step_decel ( step_n );
      _celerator.load_step ( _step_prev, step_decel );

      if ( _celerator.loaded_step_calc_vmin_vmax () ) {
        step_decel.speed = _celerator.vmin ();
      } else {
        // Deceleration failed !?
        // Force stop
        step_decel.speed = 0.0;
      }

      // Don't go too slow
      {
        bool speed_stoppable ( true );
        sev::lag::Vector< double, DIM > vspeed;
        step_decel.acquire_vspeed ( vspeed );
        for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
          if ( std::fabs ( vspeed[ ii ] ) > _step_speed_stoppable[ ii ] ) {
            speed_stoppable = false;
            break;
          }
        }
        if ( speed_stoppable ) {
          // Set final speed
          step_decel.speed = snc::fit_vector_into_maximum_limits_scale (
              step_decel.tangent, _step_speed_stoppable );
          _break_complete = true;
        }
      }
      // Feed the slowed down step
      feed_grid_step ( step_decel );
    } else {
      bool do_finish = ( Super::cargo () != nullptr );
      if ( do_finish ) {
        if ( _cargo_step_next != _cargo_step_end ) {
          do_finish = false;
        }
      }
      if ( do_finish && !_finish_stepping ) {
        _finish_stepping = true;
      }
    }
  } else {
    feed_grid_step ( step_n );
  }
}

template < std::size_t DIM >
void
Steps< DIM >::feed_grid_step ( const snc::mpath_feed::CStep< DIM > & step_n )
{
#if ( MPATH_WALKER_DEBUG_MUXER_STEPS )
  debug_write_step ( step_n );
#endif

  // Acquire step time
  double step_usecs;
  {
    double step_len;
    double step_speed;
    {
      // Calculate grid step length and normalized direction
      sev::lag::Vector< double, DIM > gstep_dir ( step_n.pos - _step_prev.pos );
      step_len = sev::lag::magnitude ( gstep_dir );
      gstep_dir /= step_len;

      // Use average speed between the step points
      {
        sev::lag::Vector< double, DIM > speed_average;
        step_n.acquire_vspeed ( speed_average );
        {
          sev::lag::Vector< double, DIM > prev_vspeed;
          _step_prev.acquire_vspeed ( prev_vspeed );
          speed_average += prev_vspeed;
        }
        speed_average /= 2.0;
        // Project it onto the normalized step direction
        step_speed = sev::lag::dot_prod ( speed_average, gstep_dir );
      }
      {
        const double step_speed_min =
            snc::fit_vector_into_maximum_limits_scale ( gstep_dir,
                                                        _step_speed_min );
        sev::math::assign_larger< double > ( step_speed, step_speed_min );
      }
    }

    // Debug
    DEBUG_ASSERT ( step_len < 10.0 );

    step_usecs = step_len / step_speed;
    step_usecs *= sev::math::mega_d;
  }
  // Keep step values for next step calculation
  _step_prev = step_n;

  // Save per axis step value deltas
  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    Steps_Axis & maxis = _axis[ ii ];
    maxis.delta_usecs = step_usecs;
    maxis.delta_step_dir = step_n.grid_step[ ii ];
  }

  // Process axis changes
  feed_grid_step_process_axes_deltas ();
}

template < std::size_t DIM >
void
Steps< DIM >::feed_grid_step_process_axes_deltas ()
{
  // Evaluate per axis step delta values
  for ( Steps_Axis & maxis : _axis ) {

    maxis.pending_usecs += maxis.delta_usecs;

    do {
      // Rounded pending useconds
      double pending_usecs_rounded = maxis.pending_usecs;
      pending_usecs_rounded += 0.5;
      pending_usecs_rounded = std::floor ( pending_usecs_rounded );

      bool create_step_from_pending ( false );
      {
        if ( maxis.delta_step_dir != 0 ) {
          if ( pending_usecs_rounded > 1.0e-6 ) {
            // New step
            create_step_from_pending = true;
          } else {
            // No pending usecs (first step).
            // Keep just the step direction
            maxis.pending_step_dir = maxis.delta_step_dir;
          }
        } else if ( pending_usecs_rounded > maxis.step_slice_usecs_max ) {
          // The pending step has been around for too long.
          // Submit some microseconds now
          pending_usecs_rounded = ( maxis.step_slice_usecs_max / 2 );
          create_step_from_pending = true;
        }
      }
      if ( create_step_from_pending ) {

        // Create new step slitter
        {
          DEBUG_ASSERT ( !maxis.step_splitters_rbuffer.is_full () );
          DEBUG_ASSERT ( pending_usecs_rounded > 0.0 );
          const std::uint_fast32_t usecs = pending_usecs_rounded;
          maxis.step_splitters_rbuffer.emplace_back_not_full (
              maxis.axis_job_type ( maxis.pending_step_dir ),
              usecs,
              maxis.step_slice_usecs_max );
          maxis.num_step_usecs_available += usecs;

          // TODO: Selectable debug output
          // usecs_file_write ( maxis.axis_index,
          // maxis.pending_step_dir, pending_usecs_rounded );
        }

        // Update pending usecs
        maxis.pending_usecs -= pending_usecs_rounded;
        maxis.pending_step_dir = maxis.delta_step_dir;

        // Reset axis deltas
        maxis.delta_usecs = 0.0;
        maxis.delta_step_dir = 0;
      } else {
        break;
      }
    } while ( !maxis.step_splitters_rbuffer.is_full () );
  }
}

template < std::size_t DIM >
void
Steps< DIM >::cargo_process_finish ()
{
  Super::cargo_process_finish ();

  DEBUG_ASSERT ( !_finish_stepping );
  _finish_stepping = true;
  Super::set_process_token ( Generator_Token::PROCESS );
}

template < std::size_t DIM >
void
Steps< DIM >::finish_stepping_flush ()
{
  if ( flush_pending_step () ) {
    // log ().cat ( sev::logt::FL_DEBUG_0, "finish_stepping_flush()" );
    _finish_stepping_flushed = true;
  }
}

template < std::size_t DIM >
bool
Steps< DIM >::flush_pending_step ()
{
  if ( !acquire_axes_step_splitters_slots () ) {
    return false;
  }

  // Feed axis step delta values to force a flush
  for ( Steps_Axis & maxis : _axis ) {
    const double usecs_feed = ( maxis.step_slice_usecs_max + 1 );
    maxis.delta_usecs = usecs_feed;
    maxis.delta_step_dir = 0;
  }
  feed_grid_step_process_axes_deltas ();

  // Debug
  // debug_axes();

  return true;
}

template < std::size_t DIM >
void
Steps< DIM >::debug_axes ()
{
  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    Steps_Axis & axis = _axis[ ii ];
    Super::log ().cat ( sev::logt::FL_DEBUG_0,
                        "Axis ",
                        ii,
                        "\n",
                        "  pending: usecs ",
                        axis.pending_usecs,
                        " steps_dir ",
                        axis.pending_step_dir,
                        "\n",
                        "  num_step_usecs_available ",
                        axis.num_step_usecs_available,
                        "\n",
                        "  step_splitters_rbuffer.size() ",
                        axis.step_splitters_rbuffer.size (),
                        " / ",
                        axis.step_splitters_rbuffer.capacity (),
                        "\n",
                        "  steps_rbuffer.size() ",
                        axis.steps_rbuffer.size (),
                        " / ",
                        axis.steps_rbuffer.capacity (),
                        "\n" );
  }
}

template < std::size_t DIM >
void
Steps< DIM >::debug_write_step ( const snc::mpath_feed::CStep< DIM > & step_n )
{
  sev::lag::Vector< double, DIM > dpos ( step_n.pos );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    const auto & dsaxis = *Super::device_statics ()->axes ().get ( ii );
    double lps ( dsaxis.geo ().length_per_step () );
    dpos[ ii ] /= lps;
    dpos[ ii ] += 0.5;
    dpos[ ii ] = std::floor ( dpos[ ii ] );
    dpos[ ii ] *= lps;
  }

  _debug_file_steps.write ( "pos: " );
  _debug_file_steps.write ( step_n.pos );
  _debug_file_steps.write ( " dpos: " );
  _debug_file_steps.write ( dpos );
  _debug_file_steps.write ( " tangent: " );
  _debug_file_steps.write ( step_n.tangent );
  _debug_file_steps.write ( " speed: " );
  _debug_file_steps.write ( step_n.speed );
  _debug_file_steps.write ( " speed_max: " );
  _debug_file_steps.write ( step_n.speed_max );
  _debug_file_steps.write ( "\n" );
}

template < std::size_t DIM >
void
Steps< DIM >::debug_write_usecs ( std::size_t axis_index_n,
                                  std::int8_t step_dir_n,
                                  std::uint_fast32_t usecs_n )
{
  _debug_file_usecs.write ( "axis: " );
  _debug_file_usecs.write ( axis_index_n );
  _debug_file_usecs.write ( " step_dir: " );
  _debug_file_usecs.write ( step_dir_n );
  _debug_file_usecs.write ( " usecs: " );
  _debug_file_usecs.write ( _axis_usecs_sum[ axis_index_n ] );
  _debug_file_usecs.write ( " usecs_n: " );
  _debug_file_usecs.write ( usecs_n );
  _debug_file_usecs.write ( "\n" );

  _axis_usecs_sum[ axis_index_n ] += usecs_n;
}

// -- Instantiation

template class Steps< 1 >;
template class Steps< 2 >;
template class Steps< 3 >;
template class Steps< 4 >;
template class Steps< 5 >;
template class Steps< 6 >;

} // namespace snc::mpath_feed_emb::gen
