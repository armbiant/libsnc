/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "control_bool.hpp"
#include <sev/assert.hpp>
#include <sev/string/utility.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <snc/mpath/elem/fader.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
Control_Bool< DIM >::Control_Bool ( const Context_D< DIM > & context_n,
                                    const sev::logt::Reference & log_parent_n )
: Super (
      context_n, log_parent_n, sev::string::cat ( "Control_Bool<", DIM, ">" ) )
{
}

template < std::size_t DIM >
Control_Bool< DIM >::~Control_Bool ()
{
}

template < std::size_t DIM >
void
Control_Bool< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  Super::route_begin ( job_n );

  _message_send = false;
  _switch_state = false;
  _switch_states.fill ( std::uint8_t ( 0 ) );

  Super::set_process_token ( Generator_Token::CARGO_FEED );
}

template < std::size_t DIM >
void
Control_Bool< DIM >::route_abort ( bool forced_n )
{
  Super::route_abort ( forced_n );

  if ( forced_n ) {
    _message_send = false;
    // Forced break releases cargo immediately
    Super::set_cargo ( nullptr );
    Super::set_process_token ( Generator_Token::FINISH );
  }
}

template < std::size_t DIM >
void
Control_Bool< DIM >::route_end ()
{
  Super::route_end ();
}

template < std::size_t DIM >
void
Control_Bool< DIM >::cargo_process_begin ()
{
  Super::cargo_process_begin ();

  Super::set_process_token ( Generator_Token::CARGO_FEED );
}

template < std::size_t DIM >
void
Control_Bool< DIM >::cargo_process_end ()
{
  Super::cargo_process_end ();
}

template < std::size_t DIM >
void
Control_Bool< DIM >::cargo_process_finish ()
{
  Super::cargo_process_finish ();

  Super::set_process_token ( Generator_Token::FINISH );
}

template < std::size_t DIM >
bool
Control_Bool< DIM >::cargo_feed ( snc::mpath_feed::Cargo * cargo_n )
{
  if ( cargo_n->cargo_type () != snc::mpath_feed::Cargo_Type::ELEMENT ) {
    return false;
  }
  if ( ( (snc::mpath_feed::cargo::Element_D< DIM > *)cargo_n )->elem_type () !=
       snc::mpath::elem::Type::FADER ) {
    return false;
  }

  Super::set_cargo ( cargo_n );

  {
    const snc::mpath::Element & elem = Super::cargo_elem ()->elem ();
    DEBUG_ASSERT ( elem.elem_type () == snc::mpath::elem::Type::FADER );
    typedef const snc::mpath::elem::Fader & EType;
    EType celem ( static_cast< EType > ( elem ) );

    if ( celem.fader_bits () == 1 ) {
      _switch_index = celem.fader_index ();
      _switch_state = ( celem.fader_value ().get_uint8 ( 0 ) != 0 );

      // Update state
      _message_send = true;
      Super::set_process_token ( Generator_Token::PROCESS );

      Super::log ().cat ( sev::logt::FL_DEBUG_0,
                          "Requesting switch index ",
                          uint32_t ( _switch_index ),
                          " switch state ",
                          uint32_t ( _switch_state ) );
    } else {
      // Update state
      _message_send = false;
      Super::set_process_token ( Generator_Token::CARGO_RELEASE );

      Super::log ().cat ( sev::logt::FL_DEBUG_0 | sev::logt::FL_WARNING,
                          "Ignoring fader_bits() != 1" );
    }
  }

  return true;
}

template < std::size_t DIM >
snc::mpath_feed::Cargo *
Control_Bool< DIM >::cargo_release ()
{
  if ( !_message_send ) {
    Super::set_process_token ( Generator_Token::FINISH );
  }
  return Super::cargo_release ();
}

template < std::size_t DIM >
void
Control_Bool< DIM >::acquire_router_messages ( Stream_Request * request_n )
{
  if ( _message_send ) {
    // Use the queue of this axis
    const auto axis_index = Super::context ().axes_mapping ().map ( 0 );
    auto & axis_tracker = request_n->axis_tracker[ axis_index ];

    if ( axis_tracker.queue_slots_free () != 0 ) {
      if ( _switch_index < Super::device_statics ()->controls_i1 ().size () ) {
        auto & message =
            request_n->stream
                .template emplace< snc::emb::msg::out::Steps_U16_64 > ();
        message.set_stepper_index ( axis_index );
        if ( _switch_state ) {
          message.set_job_type ( snc::emb::type::Step::SWITCH_SET_HIGH );
        } else {
          message.set_job_type ( snc::emb::type::Step::SWITCH_SET_LOW );
        }
        message.push_switch_index ( _switch_index );
        axis_tracker.track ( message );

        Super::log ().cat ( sev::logt::FL_DEBUG_0,
                            "Sending switch index ",
                            std::uint_fast32_t ( _switch_index ),
                            " switch state ",
                            std::uint_fast32_t ( _switch_state ) );
      } else {
        Super::log ().cat ( sev::logt::FL_DEBUG_0 | sev::logt::FL_WARNING,
                            "Invalid switch index ",
                            std::uint_fast32_t ( _switch_index ) );
      }

      track_switch_state ( _switch_index, _switch_state );
      _message_send = false;
    } else {
      Super::set_process_token ( Generator_Token::DEVICE_PROGRESS_WAIT );
    }
  }
  if ( !_message_send ) {
    Super::set_process_token ( ( Super::cargo () != nullptr )
                                   ? Generator_Token::CARGO_RELEASE
                                   : Generator_Token::FINISH );
  }
}

template < std::size_t DIM >
void
Control_Bool< DIM >::switch_state_latest ( std::uint8_t switch_index_n,
                                           bool & value_valid_n,
                                           bool & value_n ) const
{
  std::uint_fast32_t bit_pos ( std::uint_fast32_t ( switch_index_n ) * 2 );
  std::uint_fast32_t byte_index ( bit_pos / 8 );
  std::uint_fast32_t byte_shift ( bit_pos % 8 );

  const std::uint8_t & byte ( _switch_states[ byte_index ] );
  value_valid_n = ( ( byte & ( ( std::uint8_t ( 0x01 ) << byte_shift ) ) ) !=
                    std::uint8_t ( 0 ) );
  value_n = ( ( byte & ( ( std::uint8_t ( 0x02 ) << byte_shift ) ) ) !=
              std::uint8_t ( 0 ) );
}

template < std::size_t DIM >
void
Control_Bool< DIM >::track_switch_state ( std::uint8_t switch_index_n,
                                          bool state_n )
{
  std::uint_fast32_t bit_pos ( std::uint_fast32_t ( switch_index_n ) * 2 );
  std::uint_fast32_t byte_index ( bit_pos / 8 );
  std::uint_fast32_t byte_shift ( bit_pos % 8 );

  std::uint8_t & byte ( _switch_states[ byte_index ] );
  if ( state_n ) {
    // Set value valid and switch state bit
    byte |= ( std::uint8_t ( 0x03 ) << byte_shift );
  } else {
    // Set value valid bit
    byte |= ( std::uint8_t ( 0x01 ) << byte_shift );
    // Clear switch state bit
    byte &= ~( std::uint8_t ( 0x02 ) << byte_shift );
  }
}

// -- Instantiation

template class Control_Bool< 1 >;
template class Control_Bool< 2 >;
template class Control_Bool< 3 >;
template class Control_Bool< 4 >;
template class Control_Bool< 5 >;
template class Control_Bool< 6 >;

} // namespace snc::mpath_feed_emb::gen
