/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/pilot/emb_writer.hpp>
#include <snc/svs/fac/tracker/ship/client.hpp>
#include <snc/svs/fac/tracker/ship/local_axis.hpp>
#include <array>

// -- Forward declaration
namespace snc::svs::fac::tracker::ship
{
class Sailor;
}

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
class Stream_Request_D
{
  public:
  // -- Types

  using Emb_Writer = snc::svp::ship::pilot::Emb_Writer;
  using Tracker = snc::svs::fac::tracker::ship::Client;

  // -- Construction and setup

  Stream_Request_D ( Emb_Writer stream_n, const Tracker & tracker_n );

  public:
  // -- Attributes
  Emb_Writer stream;
  std::array< snc::svs::fac::tracker::ship::Local_Axis, DIM > axis_tracker;
};

} // namespace snc::mpath_feed_emb
