/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "context_d.hpp"
#include <snc/device/statics/statics.hpp>
#include <utility>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
Context_D< DIM >::Context_D (
    sev::thread::Tracker * thread_tracker_n,
    snc::device::statics::handle::Statics device_statics_n )
: _thread_tracker ( thread_tracker_n )
, _device_statics ( std::move ( device_statics_n ) )
{
  init_statics ();
}

template < std::size_t DIM >
Context_D< DIM >::Context_D (
    sev::thread::Tracker * thread_tracker_n,
    snc::device::statics::handle::Statics device_statics_n,
    const Axes_Mapping_D< DIM > & axes_mapping_n )
: _thread_tracker ( thread_tracker_n )
, _device_statics ( std::move ( device_statics_n ) )
, _axes_mapping ( axes_mapping_n )
{
  init_statics ();
}

template < std::size_t DIM >
Context_D< DIM >::~Context_D ()
{
}

template < std::size_t DIM >
void
Context_D< DIM >::init_statics ()
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    _axes_statics[ ii ] =
        _device_statics->axes ().get ( _axes_mapping.map ( ii ) );
  }
}

// -- Instantiation

template class Context_D< 1 >;
template class Context_D< 2 >;
template class Context_D< 3 >;
template class Context_D< 4 >;
template class Context_D< 5 >;
template class Context_D< 6 >;

} // namespace snc::mpath_feed_emb
