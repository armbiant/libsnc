/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/thread/tracker.hpp>
#include <snc/device/handle.hpp>
#include <snc/mpath_feed_emb/axes_mapping_d.hpp>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
class Context_D
{
  public:
  // -- Construction

  Context_D ( sev::thread::Tracker * thread_tracker_n,
              snc::device::statics::handle::Statics device_statics_n );

  Context_D ( sev::thread::Tracker * thread_tracker_n,
              snc::device::statics::handle::Statics device_statics_n,
              const Axes_Mapping_D< DIM > & axes_mapping_n );

  ~Context_D ();

  // -- Accessors

  sev::thread::Tracker *
  thread_tracker () const
  {
    return _thread_tracker;
  }

  const snc::device::statics::handle::Statics &
  device_statics () const
  {
    return _device_statics;
  }

  const Axes_Mapping_D< DIM > &
  axes_mapping () const
  {
    return _axes_mapping;
  }

  const snc::device::statics::handle::Axis &
  axis_statics ( std::size_t axis_index_n ) const
  {
    return _axes_statics[ axis_index_n ];
  }

  std::uint8_t
  stepper_sync_group_index () const
  {
    return _stepper_sync_group_index;
  }

  private:
  // -- Utility

  void
  init_statics ();

  private:
  // -- Attributes
  sev::thread::Tracker * _thread_tracker = nullptr;
  /// @brief Device statics
  const snc::device::statics::handle::Statics _device_statics;
  /// @brief Maps an MPath axis index to a device axis
  Axes_Mapping_D< DIM > _axes_mapping;
  std::array< snc::device::statics::handle::Axis, DIM > _axes_statics;
  std::uint8_t _stepper_sync_group_index = 0;
};

} // namespace snc::mpath_feed_emb
