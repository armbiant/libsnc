/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/condition.hpp>
#include <sev/event/timer_queue/chrono/queue.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/thread.hpp>
#include <snc/bat/detail/async.hpp>
#include <snc/bat/detail/cell_ring.hpp>
#include <snc/bat/starter.hpp>
#include <cstddef>
#include <functional>
#include <memory>
#include <vector>

namespace snc::bat::detail
{

/** @brief Battery processor implementation */
class Detail
{
  public:
  // -- Construction

  Detail ( sev::logt::Context log_n,
           sev::thread::Tracker * thread_tracker_n,
           sev::unicode::View thread_name_n );

  Detail ( const Detail & detail_n ) = delete;

  Detail ( Detail && detail_n ) = delete;

  ~Detail ();

  // -- Assigment operators

  Detail &
  operator= ( const Detail & detail_n ) = delete;

  Detail &
  operator= ( Detail && detail_n ) = delete;

  // -- Accessors

  /// @brief Logging context
  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  std::size_t
  num_cells () const
  {
    return _cell_context.cells ().size ();
  }

  // -- Session interface

  /// @brief Start the session
  bool
  start ( Starter && starter_n );

  /// @brief Abort the session
  void
  abort ();

  /// @brief Stop the session
  void
  stop ();

  /// @brief Returns trus if the session is running
  bool
  is_running () const
  {
    return _is_running;
  }

  /// @brief Returns trus if the session is aborting
  bool
  is_aborting () const
  {
    return _is_aborting;
  }

  private:
  // -- Utility

  void
  construct_cells ();

  void
  destruct_cells ();

  /// @brief Thread main function
  void
  operator() ();

  void
  fetch_events ();

  void
  cells_process_async ();

  void
  cells_process ();

  void
  loop_normal ();

  void
  loop_abort ();

  private:
  /// @brief Logging context
  sev::logt::Context _log;
  // -- Run state
  bool _is_running = false;
  bool _is_aborting = false;

  // -- Event loop
  sev::event::bit_accus_async::Condition _event_waiter;
  sev::mem::Flags_Fast32 _loop_events;
  /// @brief Timer queue
  sev::event::timer_queue::chrono::Queue_Steady _timer_queue_steady;
  /// @brief Asynchronous cell bit registers
  sev::mem::View< Async::Bits_Atomic > _processing_registers;
  /// @brief End of session reached callback
  std::function< void () > _end_callback;
  /// @brief List of cells that aren't ready to end
  std::vector< Cell * > _end_cells_not_ready;
  /// @brief Time of the last end message
  std::chrono::steady_clock::time_point _end_message_time;

  // -- Cell setup
  /// @brief Cell context
  Cell_Context _cell_context;
  /// @brief Cell factories
  Cell_Factories _cell_factories;

  // -- Resources
  /// @brief Cells memory arena
  std::vector< std::byte > _memory_arena;
  /// @brief Thread instance
  sev::thread::Thread _thread;
};

} // namespace snc::bat::detail
