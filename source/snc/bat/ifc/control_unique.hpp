/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::bat::ifc
{

// -- Forward declaration
class Client_Abstract;

/// @brief Service control interface class
class Control_Unique
{
  public:
  // -- Construction

  Control_Unique ( Client_Abstract * client_n = nullptr );

  Control_Unique ( const Control_Unique & control_n ) = delete;

  Control_Unique ( Control_Unique && control_n );

  ~Control_Unique ();

  // -- Assignment operators

  Control_Unique &
  operator= ( const Control_Unique & control_n ) = delete;

  Control_Unique &
  operator= ( Control_Unique && control_n );

  // -- Accessors

  Client_Abstract *
  client_abstract () const
  {
    return _client_abstract;
  }

  // -- Validity

  /// @brief An invalid interface has no effect
  bool
  is_valid () const;

  // @brief Release control and make invalid.
  void
  release ();

  /// @return is_valid()
  operator bool () const { return is_valid (); }

  private:
  Client_Abstract * _client_abstract = nullptr;
};

} // namespace snc::bat::ifc
