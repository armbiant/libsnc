/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "session.hpp"
#include <utility>

namespace snc::bat::ifc
{

Session::Session () {}

Session::~Session () = default;

void
Session::begin ()
{
  _is_good = true;
  if ( _begin_callback ) {
    _begin_callback ();
  }
}

void
Session::end ()
{
  _is_good = false;
  if ( _end_callback ) {
    _end_callback ();
  }
}

void
Session::set_begin_callback ( Callback callback_n )
{
  _begin_callback = std::move ( callback_n );
}

void
Session::set_end_callback ( Callback callback_n )
{
  _end_callback = std::move ( callback_n );
}

} // namespace snc::bat::ifc
