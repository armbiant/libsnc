/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_abstract.hpp"
#include <snc/bat/cell.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <stdexcept>
#include <utility>

namespace snc::bat::ifc
{

Client_Abstract::Client_Abstract ( User * user_n )
: _user ( user_n )
{
  if ( _user == nullptr ) {
    throw std::runtime_error ( "User is nullptr." );
  }
  // Register client at parent
  _user->clients_add ( this );
}

Client_Abstract::~Client_Abstract ()
{
  // Unregister client at parent
  _user->clients_drop ( this );
}

void
Client_Abstract::signal_state_connect () const
{
  if ( is_connected () ) {
    user ()->signal_connect ( &service_abstract ()->signal_state () );
  }
}

void
Client_Abstract::signal_state_disconnect () const
{
  if ( is_connected () ) {
    user ()->signal_disconnect ( &service_abstract ()->signal_state () );
  }
}

bool
Client_Abstract::connect ()
{
  if ( is_connected () ) {
    return true;
  }
  // Iterate services
  for ( auto * cell : user ()->cell_context ().cells () ) {
    for ( auto * service : cell->services () ) {
      // Service match?
      if ( this->connect_to ( service ) ) {
        _service_abstract = service;
        this->connecting ();
        return true;
      }
    }
  }
  return false;
}

void
Client_Abstract::connect_required ()
{
  if ( !connect () ) {
    throw std::runtime_error ( "Client connect: Service not found." );
  }
}

void
Client_Abstract::disconnect ()
{
  if ( !is_connected () ) {
    return;
  }
  this->disconnecting ();
  _service_abstract = nullptr;
}

void
Client_Abstract::connecting ()
{
  service_abstract ()->client_add ( this );
  signal_state_connect ();
}

void
Client_Abstract::disconnecting ()
{
  control_release ();
  signal_state_disconnect ();
  service_abstract ()->client_drop ( this );
}

bool
Client_Abstract::control_available () const
{
  return is_connected () && //
         !controlling () && //
         service_abstract ()->control_acquirable ();
}

bool
Client_Abstract::control_acquire ( Control_Unique * control_n )
{
  if ( is_connected () && //
       !controlling () && //
       service_abstract ()->control_acquire ( this ) ) {
    _controller = control_n;
    return true;
  }
  return false;
}

void
Client_Abstract::control_release ()
{
  if ( !controlling () ) {
    return;
  }
  _controller = nullptr;
  service_abstract ()->control_release ( this );
}

void
Client_Abstract::control_move ( Control_Unique * from_n, Control_Unique * to_n )
{
  if ( _controller != from_n ) {
    throw std::runtime_error ( "Bad control reference move" );
  }
  _controller = to_n;
}

} // namespace snc::bat::ifc
