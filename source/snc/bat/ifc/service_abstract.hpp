/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/bat/signal.hpp>
#include <cstdint>
#include <vector>

// -- Forward declaration
namespace snc::bat
{
class Cell;
}

namespace snc::bat::ifc
{

// -- Forward declaration
class Client_Abstract;

/** @brief Service base class
 */
class Service_Abstract
{
  public:
  // -- Types
  using Provider_Cell = snc::bat::Cell;
  using Client_Abstract = snc::bat::ifc::Client_Abstract;
  using Clients_List = std::vector< Client_Abstract * >;

  // -- Construction

  Service_Abstract ( Provider_Cell & provider_n,
                     sev::unicode::View log_name_n = "Service" );

  Service_Abstract ( const Service_Abstract & service_n ) = delete;

  Service_Abstract ( Service_Abstract && service_n ) = delete;

  virtual ~Service_Abstract ();

  // -- Assignment operators

  Service_Abstract &
  operator= ( const Service_Abstract & service_n ) = delete;

  Service_Abstract &
  operator= ( Service_Abstract && service_n ) = delete;

  // -- Accessors

  /// @brief Service provider that was passed to the contructor
  Provider_Cell &
  provider_cell ()
  {
    return _provider_cell;
  }

  /// @brief Service provider that was passed to the contructor
  const Provider_Cell &
  provider_cell () const
  {
    return _provider_cell;
  }

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  // -- State signal

  snc::bat::Signal &
  signal_state ()
  {
    return _signal_state;
  }

  const snc::bat::Signal &
  signal_state () const
  {
    return _signal_state;
  }

  // -- Clients

  const Clients_List &
  clients () const
  {
    return _clients.list;
  }

  void
  clients_disconnect ();

  // -- Control interface

  virtual bool
  control_acquirable () const;

  bool
  control_acquirable_change_poll ();

  bool
  control_acquire ( Client_Abstract * client_n );

  void
  control_release ( Client_Abstract * client_n );

  bool
  controlled () const
  {
    return ( _clients.controller != nullptr );
  }

  /// @brief Tests if the service is controlled by @a client_n.
  bool
  controlled_by ( const Client_Abstract * client_n ) const
  {
    return ( _clients.controller == client_n );
  }

  // -- Protected: control interface
  protected:
  void
  control_release ();

  /// @brief Called when @a client_n accepts the service control
  virtual void
  control_acquired ( Client_Abstract * client_n );

  /// @brief Called when @a client_n releases the service control
  virtual void
  control_released ( Client_Abstract * client_n );

  // -- Protected: signal interface
  protected:
  void
  clients_session_begin ();

  void
  clients_session_end ();

  // -- Client interface
  private:
  friend Client_Abstract;

  void
  client_add ( Client_Abstract * client_n );

  void
  client_drop ( Client_Abstract * client_n );

  private:
  // -- Resources
  Provider_Cell & _provider_cell;
  sev::logt::Context _log;
  snc::bat::Signal _signal_state;
  bool _control_was_acquirable = false;
  struct
  {
    /// @brief List of all connected clients
    Clients_List list;
    /// @brief Service controlling client
    Client_Abstract * controller = nullptr;
  } _clients;
};

} // namespace snc::bat::ifc
