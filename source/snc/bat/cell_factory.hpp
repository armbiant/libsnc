/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_factory_abstract.hpp>
#include <snc/bat/cell_init.hpp>
#include <cstddef>
#include <new>
#include <tuple>
#include <utility>

namespace snc::bat
{

/// @brief Cell factory and owner
///
template < typename CT, typename... Args >
class Cell_Factory : public Cell_Factory_Abstract
{
  public:
  // -- Types

  using Tuple = std::tuple< Args... >;

  // -- Construction

  Cell_Factory () = default;

  Cell_Factory ( Args... args_n )
  : _args ( std::move ( args_n )... )
  {
  }

  // -- Tuple arguments

  const Tuple &
  args () const
  {
    return _args;
  }

  Tuple &
  args ()
  {
    return _args;
  }

  template < std::size_t I >
  typename std::tuple_element< I, std::tuple< Args... > >::type const &
  arg ( std::size_t index_n ) const
  {
    return std::get< I > ( _args );
  }

  template < std::size_t I, typename T >
  void
  set_arg ( const T & value_n )
  {
    std::get< I > ( _args ) = value_n;
  }

  protected:
  // -- Cell construction interface

  std::size_t
  cell_size () const override;

  Cell *
  cell_init ( void * memory_n, const Cell_Init & cell_init_n ) const override;

  private:
  // -- Utility

  template < std::size_t... I >
  Cell *
  _cell_init ( void * memory_n,
               const Cell_Init & cell_init_n,
               std::index_sequence< I... > ) const
  {
    return new ( memory_n ) CT ( cell_init_n, std::get< I > ( _args )... );
  }

  private:
  // -- Attributes
  Tuple _args;
};

template < typename CT, typename... Args >
std::size_t
Cell_Factory< CT, Args... >::cell_size () const
{
  return sizeof ( CT );
}

template < typename CT, typename... Args >
Cell *
Cell_Factory< CT, Args... >::cell_init ( void * memory_n,
                                         const Cell_Init & cell_init_n ) const
{
  return _cell_init (
      memory_n, cell_init_n, std::make_index_sequence< sizeof...( Args ) >{} );
}

} // namespace snc::bat
