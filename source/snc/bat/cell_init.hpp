/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_context.hpp>
#include <functional>

namespace snc::bat
{

/// @brief Cell specific construction variables
///
class Cell_Init
{
  public:
  // -- Construction

  Cell_Init ( Cell_Context & context_n,
              std::function< void () > processing_request_async_n )
  : _cell_context ( context_n )
  , _processing_request_async ( std::move ( processing_request_async_n ) )
  {
  }

  Cell_Init ( const Cell_Init & Cell_Construct_n ) = delete;

  Cell_Init ( Cell_Init && Cell_Construct_n ) = delete;

  ~Cell_Init ();

  // -- Assignment operators

  Cell_Init &
  operator= ( const Cell_Init & Cell_Construct_n ) = delete;

  Cell_Init &
  operator= ( Cell_Init && Cell_Construct_n ) = delete;

  // -- Accessors

  /// @brief Cell context
  Cell_Context &
  cell_context () const
  {
    return _cell_context;
  }

  /// @brief Processing request
  const std::function< void () > &
  processing_request_async () const
  {
    return _processing_request_async;
  }

  private:
  // -- Attributes
  Cell_Context & _cell_context;
  std::function< void () > _processing_request_async;
};

} // namespace snc::bat
