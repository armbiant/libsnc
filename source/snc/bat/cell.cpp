/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cell.hpp"
#include <sev/utility.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/bat/signal.hpp>
#include <algorithm>
#include <utility>

namespace
{
template < class T >
void
vector_drop ( std::vector< T * > & list_n, T * item_n )
{
  auto it = std::find ( list_n.begin (), list_n.end (), item_n );
  if ( it != list_n.end () ) {
    list_n.erase ( it );
  }
}
} // namespace

namespace snc::bat
{

Cell::Cell ( const Cell_Init & cell_init_n,
             sev::unicode::View cell_type_n,
             sev::unicode::View log_name_n )
: _cell_context ( cell_init_n.cell_context () )
, _cell_type ( cell_type_n.string () )
, _log ( _cell_context.log_parent (), log_name_n )
, _processing_request ( [ this ] () { this->request_processing (); } )
, _processing_request_async (
      std::move ( cell_init_n.processing_request_async () ) )
{
}

Cell::~Cell () = default;

void
Cell::cell_session_begin ()
{
  _cell_session.is_running = true;
  _cell_session.is_good = true;
}

void
Cell::cell_session_abort ()
{
  DEBUG_ASSERT ( _cell_session.is_running );
  _cell_session.is_aborting = true;
  _cell_session.is_good = false;
}

bool
Cell::cell_session_is_ready_to_end () const
{
  return _cell_session.is_aborting;
}

void
Cell::cell_session_end ()
{
  DEBUG_ASSERT ( _cell_session.is_running );
  DEBUG_ASSERT ( _cell_session.is_aborting );
  _cell_session.is_running = false;
  _cell_session.is_aborting = false;
  _cell_session.is_good = false;

  // Disconnect clients from provided services
  for ( auto & svs : _services ) {
    svs->clients_disconnect ();
  }

  // Disconnect clients from remote services
  for ( auto & clt : _clients ) {
    clt->disconnect ();
  }

  // Disconnect from signals
  signal_disconnect_all ();
}

void
Cell::signal_connect ( Signal * signal_n )
{
  // Check for invalid argument
  if ( signal_n == nullptr ) {
    return;
  }

  // Check if we're already connected
  if ( std::find ( _signal_connections.cbegin (),
                   _signal_connections.cend (),
                   signal_n ) != _signal_connections.cend () ) {
    return;
  }

  // -- Connect
  signal_n->connect ( this );
  _signal_connections.push_back ( signal_n );
}

void
Cell::signal_disconnect ( Signal * signal_n )
{
  // Check for invalid argument
  if ( signal_n == nullptr ) {
    return;
  }

  // Find connection
  auto it = std::find (
      _signal_connections.cbegin (), _signal_connections.cend (), signal_n );
  if ( it != _signal_connections.cend () ) {
    _signal_connections.erase ( it );
    signal_n->disconnect ( this );
  }
}

void
Cell::signal_disconnect_all ()
{
  while ( !_signal_connections.empty () ) {
    _signal_connections.back ()->disconnect ( this );
    _signal_connections.pop_back ();
  }
}

void
Cell::clients_add ( Client_Abstract * client_n )
{
  _clients.push_back ( client_n );
}

void
Cell::clients_drop ( Client_Abstract * client_n )
{
  vector_drop ( _clients, client_n );
}

void
Cell::services_add ( Service_Abstract * service_n )
{
  _services.push_back ( service_n );
}

void
Cell::services_drop ( Service_Abstract * service_n )
{
  vector_drop ( _services, service_n );
}

void
Cell::request_processing () const
{
  if ( sev::change ( _processing_requested, true ) ) {
    _cell_context.register_for_processing ( this );
  }
}

void
Cell::process_async ()
{
}

void
Cell::process ()
{
}

} // namespace snc::bat
