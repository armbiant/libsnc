/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "signal.hpp"
#include <snc/bat/cell.hpp>
#include <algorithm>

namespace snc::bat
{

Signal::Signal ( sev::unicode::View name_n )
: _name ( name_n.string () )
{
}

Signal::~Signal () = default;

void
Signal::send () const
{
  for ( auto * cell : _client_cells ) {
    cell->request_processing ();
  }
}

void
Signal::connect ( Cell * cell_n )
{
  // Allow only a single connection
  auto it =
      std::find ( _client_cells.cbegin (), _client_cells.cend (), cell_n );
  if ( it == _client_cells.cend () ) {
    _client_cells.push_back ( cell_n );
  }
}

void
Signal::disconnect ( Cell * cell_n )
{
  // Check if the cell is even connected
  auto it =
      std::find ( _client_cells.cbegin (), _client_cells.cend (), cell_n );
  if ( it != _client_cells.cend () ) {
    _client_cells.erase ( it );
  }
}

} // namespace snc::bat
