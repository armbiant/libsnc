/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cell_context.hpp"

namespace snc::bat
{

Cell_Context::Cell_Context ( const Cell_Context & cell_context_n ) = default;

Cell_Context::Cell_Context ( Cell_Context && cell_context_n ) = default;

Cell_Context::~Cell_Context () = default;

Cell_Context &
Cell_Context::operator= ( const Cell_Context & cell_context_n ) = default;

Cell_Context &
Cell_Context::operator= ( Cell_Context && cell_context_n ) = default;

void
Cell_Context::register_for_processing ( const Cell * cell_n )
{
  _processing_ring.push ( const_cast< Cell * > ( cell_n ) );
  // Notify event accu if this was the first push
  if ( _processing_ring.size () == 1 ) {
    _event_accu->set ( detail::LEvent::PROCESS );
  }
}

} // namespace snc::bat
