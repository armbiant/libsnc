/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pool.hpp"
#include <sev/assert.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/res/lease.hpp>
#include <stdexcept>

namespace snc::res
{

Pool::Pool () {}

Pool::~Pool ()
{
  reset ();
}

void
Pool::reset ()
{
  if ( _leases != 0 || _control_leases.i1 != 0 || _axis_leases != 0 ) {
    throw std::runtime_error ( "Leases aren't empty" );
  }

  _axes.clear ();
  _controls.i1.clear ();
  _statics.reset ();
}

void
Pool::fill ( snc::device::statics::handle::Statics statics_n )
{
  reset ();

  _statics = std::move ( statics_n );
  if ( !_statics ) {
    return;
  }

  // Allocate registers
  _controls.i1.resize ( _statics->controls_i1 ().size () );
  _axes.resize ( _statics->axes ().size () );
}

bool
Pool::probe ( const Lease & lease_n ) const
{
  // We need to be configured
  if ( !_statics ) {
    return false;
  }
  // Don't allow double leases
  if ( lease_n.is_leasing () ) {
    return false;
  }

  // Check controls_bool
  for ( std::uint_fast32_t index : lease_n.controls_i1 () ) {
    if ( index >= _controls.i1.size () ) {
      return false;
    }
    if ( _controls.i1[ index ].clerk != nullptr ) {
      return false;
    }
  }

  // Check axes
  for ( std::uint_fast32_t index : lease_n.axes () ) {
    if ( index >= _axes.size () ) {
      return false;
    }
    if ( _axes[ index ].clerk != nullptr ) {
      return false;
    }
  }

  // Return success
  return true;
}

bool
Pool::acquire ( Lease & lease_n )
{
  // Perform checks
  if ( !probe ( lease_n ) ) {
    return false;
  }

  // Register controls_bool
  for ( std::uint_fast32_t index : lease_n.controls_i1 () ) {
    _controls.i1[ index ].clerk = lease_n.clerk ();
    ++_leases;
    ++_control_leases.i1;
  }

  // Register axes
  for ( std::uint_fast32_t index : lease_n.axes () ) {
    _axes[ index ].clerk = lease_n.clerk ();
    ++_leases;
    ++_axis_leases;
  }

  // Set lease flag
  lease_n.set_is_leasing ( true );

  // Return success
  return true;
}

void
Pool::release ( Lease & lease_n )
{
  if ( !lease_n.is_leasing () ) {
    return;
  }

  // Unregister axes
  for ( std::uint_fast32_t axis_index : lease_n.axes () ) {
    // Unregister clerk
    auto & reg = _axes[ axis_index ].clerk;
    DEBUG_ASSERT ( reg == lease_n.clerk () );
    reg = nullptr;

    // Update statistics
    DEBUG_ASSERT ( _leases != 0 );
    DEBUG_ASSERT ( _axis_leases != 0 );
    --_leases;
    --_axis_leases;
  }

  // Unregister controls_bool
  for ( std::uint_fast32_t index : lease_n.controls_i1 () ) {
    // Unregister clerk
    auto & reg = _controls.i1[ index ].clerk;
    DEBUG_ASSERT ( reg == lease_n.clerk () );
    reg = nullptr;

    // Update statistics
    DEBUG_ASSERT ( _leases != 0 );
    DEBUG_ASSERT ( _control_leases.i1 != 0 );
    --_leases;
    --_control_leases.i1;
  }

  // Clear lease flag
  lease_n.set_is_leasing ( false );
}

} // namespace snc::res
