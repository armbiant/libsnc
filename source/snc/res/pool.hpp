/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <cstdint>
#include <memory>
#include <vector>

namespace snc::svp::office
{
class Clerk;
}
namespace snc::res
{
class Lease;
}

namespace snc::res
{

class Pool
{
  public:
  // -- Types

  struct Lease_Control
  {
    snc::svp::office::Clerk * clerk = nullptr;
  };

  struct Lease_Axis
  {
    snc::svp::office::Clerk * clerk = nullptr;
  };

  // -- Construction

  Pool ();

  ~Pool ();

  // -- Setup

  void
  reset ();

  void
  fill ( snc::device::statics::handle::Statics statics_n );

  // -- Interface

  const auto &
  statics () const
  {
    return _statics;
  }

  bool
  probe ( const Lease & lease_n ) const;

  bool
  acquire ( Lease & lease_n );

  void
  release ( Lease & lease_n );

  // -- Lists

  const auto &
  axes () const
  {
    return _axes;
  }

  // -- Statistics

  std::uint_fast32_t
  leases () const
  {
    return _leases;
  }

  std::uint_fast32_t
  control_i1_leases () const
  {
    return _control_leases.i1;
  }

  std::uint_fast32_t
  axis_leases () const
  {
    return _axis_leases;
  }

  private:
  // - References
  snc::device::statics::handle::Statics _statics;
  // -- Attributes: Statistics
  std::uint_fast32_t _leases = 0;
  struct
  {
    std::uint_fast32_t i1 = 0;
  } _control_leases;
  std::uint_fast32_t _axis_leases = 0;
  // -- Attributes: Registers
  struct
  {
    std::vector< Lease_Control > i1;
  } _controls;
  std::vector< Lease_Axis > _axes;
};

} // namespace snc::res
