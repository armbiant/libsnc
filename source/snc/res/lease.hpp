/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace snc::svp::office
{
class Clerk;
}
namespace snc::res
{
class Pool;
}

namespace snc::res
{

class Lease
{
  public:
  // -- Construction

  Lease ( snc::svp::office::Clerk * clerk_n );

  ~Lease ();

  // -- Setup

  /// @brief Clears all registrations
  void
  clear_registers ();

  // -- Accessors

  snc::svp::office::Clerk *
  clerk () const
  {
    return _clerk;
  }

  bool
  is_leasing () const
  {
    return _is_leasing;
  }

  // -- Control i1 registration

  const std::vector< std::uint_fast32_t > &
  controls_i1 () const
  {
    return _controls_i1;
  }

  bool
  has_control_i1 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i1 ( std::uint_fast32_t index_n );

  // -- Axes registration

  const std::vector< std::uint_fast32_t > &
  axes () const
  {
    return _axes;
  }

  bool
  has_axis ( std::uint_fast32_t index_n ) const;

  void
  register_axis ( std::uint_fast32_t index_n );

  void
  unregister_axis ( std::uint_fast32_t index_n );

  // -- Information

  std::string
  registrations_string () const;

  private:
  // -- Private interface
  friend class snc::res::Pool;

  void
  set_is_leasing ( bool flag_n );

  private:
  // -- Attributes
  snc::svp::office::Clerk * _clerk = nullptr;
  bool _is_leasing = false;
  std::vector< std::uint_fast32_t > _controls_i1;
  std::vector< std::uint_fast32_t > _axes;
};

} // namespace snc::res
