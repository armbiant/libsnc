/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "lease.hpp"
#include <sev/assert.hpp>
#include <algorithm>
#include <sstream>
#include <stdexcept>

namespace snc::res
{

Lease::Lease ( snc::svp::office::Clerk * clerk_n )
: _clerk ( clerk_n )
{
}

Lease::~Lease ()
{
  DEBUG_ASSERT ( !is_leasing () );
}

void
Lease::clear_registers ()
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Clearing registers while leasing is not allowed." );
  }

  _axes.clear ();
  _controls_i1.clear ();
}

bool
Lease::has_control_i1 ( std::uint_fast32_t index_n ) const
{
  auto it = std::find ( _controls_i1.cbegin (), _controls_i1.cend (), index_n );
  return it != _controls_i1.cend ();
}

void
Lease::register_control_i1 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i1 while leasing is not allowed." );
  }

  if ( has_control_i1 ( index_n ) ) {
    return;
  }
  _controls_i1.push_back ( index_n );
  // Just for the beauty of it
  std::sort ( _controls_i1.begin (), _controls_i1.end () );
}

bool
Lease::has_axis ( std::uint_fast32_t index_n ) const
{
  auto it = std::find ( _axes.cbegin (), _axes.cend (), index_n );
  return it != _axes.cend ();
}

void
Lease::register_axis ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering axes while leasing is not allowed." );
  }
  if ( has_axis ( index_n ) ) {
    return;
  }

  // Register
  _axes.push_back ( index_n );
}

void
Lease::unregister_axis ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Unregistering axes while leasing is not allowed." );
  }

  auto it = std::find ( _axes.cbegin (), _axes.cend (), index_n );
  if ( it != _axes.cend () ) {
    _axes.erase ( it );
  }
}

void
Lease::set_is_leasing ( bool flag_n )
{
  _is_leasing = flag_n;
}

std::string
Lease::registrations_string () const
{
  std::ostringstream ostr;
  std::string_view group_sep = "";

  // Controls i1
  if ( !controls_i1 ().empty () ) {
    ostr << group_sep << "controls_i1 {";
    std::string_view sep = "";
    for ( std::uint_fast32_t ctl_index : controls_i1 () ) {
      ostr << sep << ctl_index;
      sep = ",";
    }
    ostr << "}";
    group_sep = " ";
  }

  // Axes
  if ( !axes ().empty () ) {
    ostr << group_sep << "axes {";
    std::string_view sep = "";
    for ( std::uint_fast32_t axis_index : axes () ) {
      ostr << sep << axis_index;
      sep = ",";
    }
    ostr << "}";
    group_sep = " ";
  }
  return ostr.str ();
}

} // namespace snc::res
