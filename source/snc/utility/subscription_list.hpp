/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/stack_fixed.hpp>
#include <cstddef>

namespace snc::utility
{

/** Subscription list
 *
 * Uses delayed subscription or unsubscription to avoid removes or
 * inserts while the subscribed items list is iterated.
 */
template < typename T >
class Subscription_List
{
  // -- Types
  private:
  struct Subscription
  {
    T * item = nullptr;
    bool subscribe = false;
  };

  public:
  // -- Constrution

  Subscription_List ();

  Subscription_List ( const Subscription_List & list_n ) = delete;

  Subscription_List ( Subscription_List && list_n ) = delete;

  ~Subscription_List ();

  // -- Assignment operators

  Subscription_List &
  operator= ( const Subscription_List & list_n ) = delete;

  Subscription_List &
  operator= ( Subscription_List && list_n ) = delete;

  // -- Capacity

  std::size_t
  capacity () const
  {
    return _list.capacity ();
  }

  void
  ensure_minimum_capacity ( std::size_t size_n );

  // -- Subscription

  /// @brief Subscribe or unsubscribe
  void
  subscribe ( T * item_n, bool subscribe_n );

  // -- Subscribed items

  /// @brief Apply subscriptions
  void
  apply_subscriptions ();

  const sev::mem::Stack_Fixed< T * > &
  list () const
  {
    return _list;
  }

  private:
  // -- Attributes
  sev::mem::Stack_Fixed< T * > _list;
  sev::mem::Stack_Fixed< Subscription > _subscriptions;
};

template < typename T >
Subscription_List< T >::Subscription_List () = default;

template < typename T >
Subscription_List< T >::~Subscription_List () = default;

template < typename T >
void
Subscription_List< T >::ensure_minimum_capacity ( std::size_t size_n )
{
  _list.ensure_minimum_capacity ( size_n );
  _subscriptions.ensure_minimum_capacity ( size_n );
}

template < typename T >
void
Subscription_List< T >::subscribe ( T * item_n, bool subscribe_n )
{
  DEBUG_ASSERT ( item_n != nullptr );

  auto it = std::find_if ( _subscriptions.begin (),
                           _subscriptions.end (),
                           [ item_n ] ( const Subscription & sub_n ) {
                             return sub_n.item == item_n;
                           } );
  if ( it != _subscriptions.end () ) {
    // Overwrite existing entry
    it->subscribe = subscribe_n;
  } else {
    // Add new entry
    if ( _subscriptions.is_full () ) {
      ensure_minimum_capacity ( capacity () + 1 );
    }
    _subscriptions.push_not_full ( Subscription{ item_n, subscribe_n } );
  }
}

template < typename T >
void
Subscription_List< T >::apply_subscriptions ()
{
  if ( _subscriptions.is_empty () ) {
    return;
  }

  for ( auto & sub : _subscriptions ) {
    // Find existing subscription
    auto it = std::find ( _list.begin (), _list.end (), sub.item );
    if ( sub.subscribe ) {
      // Append item on demand
      if ( it == _list.end () ) {
        if ( _list.is_full () ) {
          ensure_minimum_capacity ( capacity () + 1 );
        }
        _list.push_not_full ( sub.item );
      }
    } else {
      // Remove item on demand
      if ( it != _list.end () ) {
        _list.erase ( it );
      }
    }
  }

  // Clear subscriptions
  _subscriptions.clear ();
}

} // namespace snc::utility
