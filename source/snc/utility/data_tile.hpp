/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/list/se/item.hpp>
#include <sev/mem/view.hpp>
#include <array>
#include <cstddef>
#include <cstdint>

namespace snc::utility
{

/** Data tile
 *
 */
class Data_Tile : public sev::mem::list::se::Item
{
  // -- Constrution

  public:
  Data_Tile ( std::byte * buffer_n, std::size_t capacity_n )
  : _data ( buffer_n )
  , _capacity ( capacity_n )
  {
  }

  Data_Tile ( sev::mem::View< std::byte > buffer_n )
  : _data ( buffer_n.data () )
  , _capacity ( buffer_n.size () )
  {
  }

  virtual ~Data_Tile ();

  // -- Setup

  /// @brief Reset to construction state
  void
  reset ()
  {
    _size = 0;
  }

  // -- Data accessors

  std::byte *
  data ()
  {
    return _data;
  }

  const std::byte *
  data () const
  {
    return _data;
  }

  std::size_t
  size () const
  {
    return _size;
  }

  void
  set_size ( std::size_t size_n )
  {
    _size = size_n;
  }

  void
  size_increment ( std::size_t delta_n )
  {
    _size += delta_n;
  }

  void
  size_decrement ( std::size_t delta_n )
  {
    _size -= delta_n;
  }

  std::size_t
  capacity () const
  {
    return _capacity;
  }

  // -- Meta information

  sev::mem::View< std::byte >
  data_view () const
  {
    return sev::mem::View< std::byte > ( _data, _size );
  }

  sev::mem::View< std::byte >
  buffer_view () const
  {
    return sev::mem::View< std::byte > ( _data, _capacity );
  }

  std::size_t
  size_free () const
  {
    return ( _capacity - _size );
  }

  private:
  // -- Attributes
  std::byte * _data = nullptr;
  std::size_t _size = 0;
  const std::size_t _capacity = 0;
};

/** Static sized data tile
 *
 */
template < std::size_t BSIZE >
class Data_Tile_N : public Data_Tile
{
  // -- Construction

  public:
  Data_Tile_N ();

  ~Data_Tile_N () override = default;

  private:
  // -- Attributes
  std::array< std::byte, BSIZE > _storage;
};

template < std::size_t BSIZE >
Data_Tile_N< BSIZE >::Data_Tile_N ()
: Data_Tile ( _storage.data (), BSIZE )
{
  _storage.fill ( std::byte ( 0 ) );
}

} // namespace snc::utility
