/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "data_tile.hpp"

namespace snc::utility
{

Data_Tile::~Data_Tile ()
{
  static_assert ( sizeof ( std::byte ) == sizeof ( std::uint8_t ),
                  "Byte size missmatch" );
}

} // namespace snc::utility
