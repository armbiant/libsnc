/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>
#include <cstdint>

namespace snc::usb
{

/// @brief USB port id
///
class Device_Port_Id
{
  public:
  // -- Construction

  Device_Port_Id () = default;

  Device_Port_Id ( libusb_device * dev_n )
  {
    if ( dev_n != nullptr ) {
      load ( dev_n );
    }
  }

  // -- Setup

  void
  clear ()
  {
    _is_valid = false;
    _bus_number = 0;
    _bus_address = 0;
    _port_number = 0;
  }

  void
  load ( libusb_device * dev_n );

  // -- Accessors

  /// @brief True if loaded successfully
  bool
  is_valid () const
  {
    return _is_valid;
  }

  /// @brief Number of the bus
  std::uint8_t
  bus_number () const
  {
    return _bus_number;
  }

  /// @brief Address of the device on the bus
  std::uint8_t
  bus_address () const
  {
    return _bus_address;
  }

  /// @brief On a bus the same port number is always
  ///        assigned to the same slot in a hub.
  std::uint8_t
  port_number () const
  {
    return _port_number;
  }

  bool
  operator== ( const snc::usb::Device_Port_Id & port_id_n ) const
  {
    return ( ( _is_valid == port_id_n._is_valid ) &&
             ( _bus_number == port_id_n._bus_number ) &&
             ( _bus_address == port_id_n._bus_address ) &&
             ( _port_number == port_id_n._port_number ) );
  }

  bool
  operator!= ( const snc::usb::Device_Port_Id & port_id_n ) const
  {
    return !operator== ( port_id_n );
  }

  public:
  // -- Attributes
  bool _is_valid = false;
  std::uint8_t _bus_number = 0;
  std::uint8_t _bus_address = 0;
  std::uint8_t _port_number = 0;
};

} // namespace snc::usb
