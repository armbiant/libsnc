/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <optional>
#include <regex>

namespace snc::usb
{

/// @brief USB device descriptor query
///
class Device_Query
{
  public:
  // -- Types

  struct IString
  {
    IString ( std::string regex_string_n )
    : regex_string ( std::move ( regex_string_n ) )
    , regex ( regex_string )
    {
    }

    const std::string regex_string;
    const std::regex regex;
  };

  // -- Construction

  Device_Query ();

  public:
  // -- Attributes
  std::optional< IString > manufacturer;
  std::optional< IString > product;
  std::optional< IString > serial_number;
};

} // namespace snc::usb
