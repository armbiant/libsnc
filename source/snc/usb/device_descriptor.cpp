/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device_descriptor.hpp"

namespace snc::usb
{

Device_Descriptor::Device_Descriptor ()
{
  _bcd_usb.fill ( 0 );
}

void
Device_Descriptor::clear ()
{
  _is_valid = false;
  _bcd_usb.fill ( 0 );
  _iManufacturer = 0;
  _iProduct = 0;
  _iSerial = 0;
  _string_manufacturer.clear ();
  _string_product.clear ();
  _string_serial.clear ();
}

void
Device_Descriptor::load ( libusb_device_handle * dev_n )
{
  clear ();
  if ( dev_n != nullptr ) {
    _is_valid = true;
  }
}

} // namespace snc::usb
