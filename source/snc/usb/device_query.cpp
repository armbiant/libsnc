/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device_query.hpp"

namespace snc::usb
{

Device_Query::Device_Query () = default;

} // namespace snc::usb
