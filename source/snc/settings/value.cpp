/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "value.hpp"
#include <sev/assert.hpp>
#include <atomic>
#include <string>

namespace snc::settings
{

class Value::Shared
{
  protected:
  std::atomic< std::size_t > users = 1;
};

namespace
{
struct S_String : public Value::Shared
{
  S_String ( std::string_view value_n )
  : text ( value_n )
  {
  }

  void
  unref ()
  {
    auto ret = users.fetch_sub ( 1 );
    DEBUG_ASSERT ( ret > 0 );
    if ( ret == 1 ) {
      delete this;
    }
  }

  void
  ref ()
  {
    users.fetch_add ( 1 );
  }

  std::string text;
};
} // namespace

Value::Value ()
: _type ( Type::NONE )
{
}

Value::Value ( const Value & value_n )
: _type ( value_n._type )
, _uni ( value_n._uni )
{
  if ( is_string () ) {
    DEBUG_ASSERT ( _uni.sv != nullptr );
    // Add reference
    static_cast< S_String * > ( _uni.sv )->ref ();
  }
}

Value::Value ( Value && value_n )
: _type ( value_n._type )
, _uni ( value_n._uni )
{
  // Invalidate source
  value_n._type = Type::NONE;
  value_n._uni.sv = nullptr;
}

Value::Value ( bool value_n )
{
  _type = Type::BOOL;
  _uni.bv = value_n;
}

Value::Value ( double value_n )
{
  _type = Type::DOUBLE;
  _uni.dv = value_n;
}

Value::Value ( std::string_view value_n )
{
  _type = Type::STRING;
  _uni.sv = new S_String ( value_n );
}

Value::~Value ()
{
  if ( is_string () ) {
    DEBUG_ASSERT ( _uni.sv != nullptr );
    // Drop reference
    static_cast< S_String * > ( _uni.sv )->unref ();
  }
}

Value &
Value::operator= ( const Value & value_n )
{
  if ( this == &value_n ) {
    return *this;
  }

  // Drop an old reference
  if ( is_string () ) {
    static_cast< S_String * > ( _uni.sv )->unref ();
  }

  // Copy values
  _type = value_n._type;
  _uni = value_n._uni;
  // Add shared reference on demand
  if ( is_string () ) {
    static_cast< S_String * > ( _uni.sv )->ref ();
  }

  return *this;
}

Value &
Value::operator= ( Value && value_n )
{
  if ( this == &value_n ) {
    return *this;
  }

  // Drop an old reference
  if ( is_string () ) {
    static_cast< S_String * > ( _uni.sv )->unref ();
  }

  _type = value_n._type;
  _uni = value_n._uni;
  // Invalidate source
  value_n._type = Type::NONE;
  value_n._uni.sv = nullptr;

  return *this;
}

std::string_view
Value::type_name ( Type type_n )
{
  switch ( type_n ) {
  case Type::NONE:
    return "NONE";
  case Type::BOOL:
    return "BOOL";
  case Type::DOUBLE:
    return "DOUBLE";
  case Type::STRING:
    return "STRING";
  }
  return "INVALID_TYPE_VALUE";
}

bool
Value::as_bool () const
{
  if ( !is_bool () ) {
    return false;
  }
  return _uni.bv;
}

double
Value::as_double () const
{
  if ( !is_double () ) {
    return 0.0;
  }
  return _uni.dv;
}

std::string_view
Value::as_string () const
{
  if ( !is_string () ) {
    return std::string_view ();
  }
  DEBUG_ASSERT ( _uni.sv != nullptr );
  // Drop reference
  return static_cast< S_String * > ( _uni.sv )->text;
}

bool
Value::operator== ( const Value & value_n ) const
{
  if ( type () == value_n.type () ) {
    auto & ua = _uni;
    auto & ub = value_n._uni;

    switch ( type () ) {
    case Type::NONE:
      return true;
    case Type::BOOL:
      return ( ua.bv == ub.bv );
    case Type::DOUBLE:
      return ( ua.dv == ub.dv );
    case Type::STRING: {
      return ( ( ua.sv == ub.sv ) ||
               ( static_cast< S_String * > ( ua.sv )->text ==
                 static_cast< S_String * > ( ub.sv )->text ) );
    }
    }
  }
  return false;
}

bool
Value::operator!= ( const Value & value_n ) const
{
  if ( type () == value_n.type () ) {
    auto & ua = _uni;
    auto & ub = value_n._uni;

    switch ( type () ) {
    case Type::NONE:
      return false;
    case Type::BOOL:
      return ( ua.bv != ub.bv );
    case Type::DOUBLE:
      return ( ua.dv != ub.dv );
    case Type::STRING: {
      return ( ( ua.sv != ub.sv ) &&
               ( static_cast< S_String * > ( ua.sv )->text !=
                 static_cast< S_String * > ( ub.sv )->text ) );
    }
    }
  }

  return true;
}

} // namespace snc::settings
