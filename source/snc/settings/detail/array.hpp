/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/detail/entity.hpp>
#include <memory>
#include <vector>

namespace snc::settings::detail
{

// -- Forward declaration
class Node;

class Array : public Entity
{
  public:
  // -- Types
  using Map = std::vector< std::unique_ptr< Node > >;

  // -- Construction

  Array ();

  ~Array ();

  // -- Map

  const Map &
  map () const
  {
    return _map;
  }

  std::size_t
  size () const
  {
    return _map.size ();
  }

  void
  reserve ( std::size_t size_n )
  {
    _map.reserve ( size_n );
  }

  // -- Access

  const Node *
  get ( std::size_t index_n ) const;

  Node *
  get ( std::size_t index_n );

  Node *
  get_or_create ( std::size_t index_n );

  private:
  Map _map;
};

} // namespace snc::settings::detail
