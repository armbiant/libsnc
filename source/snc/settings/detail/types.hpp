/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::settings::detail
{

/// @brief In a plain tree (e.g. json export) a node can only have one function.
enum Plain_Mode : std::uint8_t
{
  NONE,

  VALUE,
  ARRAY,
  BRANCH
};

} // namespace snc::settings::detail
