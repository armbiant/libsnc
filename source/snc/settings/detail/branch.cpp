/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "branch.hpp"
#include <snc/settings/detail/node.hpp>
#include <algorithm>

namespace snc::settings::detail
{

Branch::Branch () {}

Branch::~Branch () {}

const Node *
Branch::get ( std::string_view name_n ) const
{
  auto it = _map.find ( name_n );
  if ( it != _map.cend () ) {
    return it->second.first.get ();
  }
  return nullptr;
}

Node *
Branch::get ( std::string_view name_n )
{
  auto it = _map.find ( name_n );
  if ( it != _map.cend () ) {
    return it->second.first.get ();
  }
  return nullptr;
}

Node *
Branch::get_or_create ( std::string_view name_n )
{
  auto it = _map.find ( name_n );
  if ( it == _map.end () ) {
    // Not exiting entry. Create new one.
    // Store name data in allocated (non moving) memory
    auto name_data = std::make_unique< char[] > ( name_n.size () );
    std::copy ( name_n.begin (), name_n.end (), name_data.get () );
    std::string_view name_view ( name_data.get (), name_n.size () );
    it = _map.emplace ( name_view,
                        Entry ( std::make_unique< Node > (),
                                std::move ( name_data ) ) )
             .first;
  }

  return it->second.first.get ();
}

} // namespace snc::settings::detail
