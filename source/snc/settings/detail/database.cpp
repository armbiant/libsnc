/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "database.hpp"
#include <json/reader.h>
#include <json/writer.h>
#include <snc/settings/detail/address_parser.hpp>
#include <snc/settings/detail/array.hpp>
#include <snc/settings/detail/branch.hpp>
#include <snc/settings/detail/leaf.hpp>
#include <istream>
#include <iterator>
#include <ostream>
#include <stack>
#include <stdexcept>

namespace snc::settings::detail
{

Database::Database ()
{
  _root.branch ();
  _root.set_plain_mode ( Plain_Mode::BRANCH );
}

Database::~Database () = default;

std::pair< bool, Value >
Database::get_value ( std::string_view address_n ) const
{
  {
    std::lock_guard< std::mutex > mlock ( _mutex );
    if ( auto * node = get_node ( address_n ) ) {
      if ( auto leaf = node->leaf () ) {
        return { true, leaf->value () };
      }
    }
  }
  return { false, {} };
}

void
Database::set_value ( std::string_view address_n, Value && value_n )
{
  if ( !Address_Parser::valid_address ( address_n ) ) {
    return;
  }
  {
    std::lock_guard< std::mutex > mlock ( _mutex );
    if ( auto * node = get_or_create_leaf ( address_n ) ) {
      node->leaf ().set_value ( std::move ( value_n ) );
    }
  }
}

Json::Value
Database::json_export_value () const
{
  Json::Value val;

  auto json_value = [] ( const Node & node_n ) -> Json::Value {
    auto & val = node_n.leaf ()->value ();
    switch ( val.type () ) {
    case Value::Type::BOOL:
      return Json::Value ( val.as_bool () );
    case Value::Type::DOUBLE:
      return Json::Value ( val.as_double () );
    case Value::Type::STRING: {
      auto sview = val.as_string ();
      return Json::Value ( sview.data (), sview.data () + sview.size () );
    }
    default:
      break;
    }
    return Json::Value ();
  };

  struct Step
  {
    Step ( const Node & node_n, Json::Value & jval_n )
    : node ( node_n )
    , jval ( jval_n )
    , is_array ( node.plain_mode () == Plain_Mode::ARRAY )
    {
      if ( is_array ) {
        jval = Json::Value ( Json::arrayValue );
        jval.resize ( node.array ()->size () );

        const auto & map = node.array ()->map ();
        array_iter[ 0 ] = map.begin ();
        array_iter[ 1 ] = map.begin ();
        array_iter[ 2 ] = map.end ();
      } else {
        jval = Json::Value ( Json::objectValue );

        const auto & map = node.branch ()->map ();
        branch_iter[ 0 ] = map.begin ();
        branch_iter[ 1 ] = map.end ();
      }
    }

    const Node & node;
    Json::Value & jval;
    bool is_array = false;
    std::array< Array::Map::const_iterator, 3 > array_iter;
    std::array< Branch::Map::const_iterator, 2 > branch_iter;
  };

  std::stack< Step > stack;

  {
    std::lock_guard< std::mutex > mlock ( _mutex );
    if ( _root.plain_mode () == Plain_Mode::BRANCH ) {
      stack.emplace ( _root, val );
    }
    while ( !stack.empty () ) {
      // Array
      auto & step = stack.top ();
      if ( step.is_array ) {
        auto & itc = step.array_iter[ 0 ];
        auto & itb = step.array_iter[ 1 ];
        auto & ite = step.array_iter[ 2 ];
        if ( itc == ite ) {
          stack.pop ();
        } else {
          Json::ArrayIndex ae_index = std::distance ( itb, itc );
          const Node & ae_node = **itc;
          ++itc;

          switch ( ae_node.plain_mode () ) {
          case Plain_Mode::NONE:
            break;
          case Plain_Mode::VALUE:
            step.jval[ ae_index ] = json_value ( ae_node );
            break;
          case Plain_Mode::ARRAY:
          case Plain_Mode::BRANCH:
            stack.emplace ( ae_node, step.jval[ ae_index ] );
            break;
          }
        }
      } else {
        // Branch
        auto & itc = step.branch_iter[ 0 ];
        auto & ite = step.branch_iter[ 1 ];
        if ( itc == ite ) {
          stack.pop ();
        } else {
          const std::string be_name ( itc->first );
          const Node & be_node = *itc->second.first;
          ++itc;

          switch ( be_node.plain_mode () ) {
          case Plain_Mode::NONE:
            break;
          case Plain_Mode::VALUE:
            step.jval[ be_name ] = json_value ( be_node );
            break;
          case Plain_Mode::ARRAY:
          case Plain_Mode::BRANCH:
            stack.emplace ( be_node, step.jval[ be_name ] );
            break;
          }
        }
      }
    }
  }

  return val;
}

void
Database::json_export_stream ( std::ostream & ostream_n ) const
{
  {
    std::unique_ptr< Json::StreamWriter > writer;
    {
      Json::StreamWriterBuilder builder;
      builder[ "commentStyle" ] = "None";
      builder[ "indentation" ] = "  ";
      builder[ "precision" ] = std::numeric_limits< double >::max_digits10;
      writer.reset ( builder.newStreamWriter () );
    }
    writer->write ( json_export_value (), &ostream_n );
  }
  ostream_n << std::endl;
}

void
Database::json_import_value ( const Json::Value & jval_n )
{
  if ( !jval_n.isObject () && !jval_n.isArray () ) {
    return;
  }

  struct Step
  {
    Step ( const Json::Value & jval_n, Node & node_n )
    : jval ( jval_n )
    , node ( node_n )
    , is_array ( jval.isArray () )
    {
      if ( is_array ) {
        node.array ().reserve ( jval.size () );
        node.set_plain_mode ( Plain_Mode::ARRAY );
      } else {
        node.branch ();
        node.set_plain_mode ( Plain_Mode::BRANCH );
      }
      iter[ 0 ] = jval.begin ();
      iter[ 1 ] = jval.end ();
    }

    const Json::Value & jval;
    Node & node;
    bool is_array = false;
    std::array< Json::ValueConstIterator, 2 > iter;
  };

  std::stack< Step > stack;

  auto node_setup = [ &stack ] ( Node & node_n, const Json::Value & jval_n ) {
    switch ( jval_n.type () ) {
    case Json::ValueType::nullValue:
      node_n.leaf ();
      node_n.set_plain_mode ( Plain_Mode::VALUE );
      break;
    case Json::ValueType::booleanValue:
      node_n.leaf ().set_value ( jval_n.asBool () );
      node_n.set_plain_mode ( Plain_Mode::VALUE );
      break;
    case Json::ValueType::intValue:
    case Json::ValueType::uintValue:
    case Json::ValueType::realValue:
      node_n.leaf ().set_value ( jval_n.asDouble () );
      node_n.set_plain_mode ( Plain_Mode::VALUE );
      break;
    case Json::ValueType::stringValue:
      node_n.leaf ().set_value ( Value ( jval_n.asString () ) );
      node_n.set_plain_mode ( Plain_Mode::VALUE );
      break;
    case Json::ValueType::arrayValue:
    case Json::ValueType::objectValue:
      stack.emplace ( jval_n, node_n );
      break;
    }
  };

  {
    std::lock_guard< std::mutex > mlock ( _mutex );

    // Initialize stack
    node_setup ( _root, jval_n );

    while ( !stack.empty () ) {
      auto & step = stack.top ();
      auto & itc = step.iter[ 0 ];
      auto & ite = step.iter[ 1 ];
      if ( itc == ite ) {
        stack.pop ();
        continue;
      }

      if ( step.is_array ) {
        // Array
        auto ae_index = itc.index ();
        const Json::Value & ae_jval = *itc;
        ++itc;

        node_setup ( *step.node.array ().get_or_create ( ae_index ), ae_jval );
      } else {
        // Object
        std::string oe_name = itc.name ();
        const Json::Value & oe_jval = *itc;
        ++itc;

        node_setup ( *step.node.branch ().get_or_create ( oe_name ), oe_jval );
      }
    }
  }
}

std::pair< bool, std::string >
Database::json_import_string ( std::string_view json_n )
{
  std::pair< bool, std::string > res ( true, {} );
  Json::Value jval;
  {
    std::unique_ptr< Json::CharReader > reader;
    {
      Json::CharReaderBuilder builder;
      builder[ "collectComments" ] = false;
      reader.reset ( builder.newCharReader () );
    }
    res.first = reader->parse (
        json_n.data (), json_n.data () + json_n.size (), &jval, &res.second );
  }
  if ( res.first ) {
    json_import_value ( jval );
  }
  return res;
}

std::pair< bool, std::string >
Database::json_import_stream ( std::istream & istream_n )
{
  std::string content ( ( std::istreambuf_iterator< char > ( istream_n ) ),
                        ( std::istreambuf_iterator< char > () ) );
  return json_import_string ( content );
}

const Node *
Database::get_node ( std::string_view address_n ) const
{
  const Node * node = &_root;

  Address_Parser adp ( address_n );
  while ( node && adp.iterate () ) {
    // Bad address?
    if ( !adp.good () ) {
      node = nullptr;
      break;
    }

    switch ( adp.fragment () ) {
    case Address_Parser::Fragment::NONE:
      node = nullptr;
      break;

    case Address_Parser::Fragment::NAME:
      // Branch child
      if ( auto * branch = node->branch () ) {
        node = branch->get ( adp.name () );
      } else {
        node = nullptr;
      }
      break;

    case Address_Parser::Fragment::ARRAY_INDEX:
      // Array child
      if ( auto * array = node->array () ) {
        node = array->get ( adp.array_index () );
      } else {
        node = nullptr;
      }
      break;
    }
  }

  return node;
}

Node *
Database::get_or_create_node ( std::string_view address_n )
{
  Address_Parser adp ( address_n );
  Node * node = &_root;
  while ( node && adp.iterate () ) {
    // Bad address?
    if ( !adp.good () ) {
      node = nullptr;
      break;
    }

    switch ( adp.fragment () ) {
    case Address_Parser::Fragment::NONE:
      node = nullptr;
      break;

    case Address_Parser::Fragment::NAME:
      // Branch child
      node = node->branch ().get_or_create ( adp.name () );
      break;

    case Address_Parser::Fragment::ARRAY_INDEX:
      // Array child
      node = node->array ().get_or_create ( adp.array_index () );
      break;
    }
  }

  return node;
}

Node *
Database::get_or_create_leaf ( std::string_view address_n )
{
  Address_Parser adp ( address_n );
  Node * node = &_root;
  while ( node && adp.iterate () ) {
    // Bad address?
    if ( !adp.good () ) {
      node = nullptr;
      break;
    }

    switch ( adp.fragment () ) {
    case Address_Parser::Fragment::NONE:
      node = nullptr;
      break;

    case Address_Parser::Fragment::NAME:
      // Branch child
      node->set_plain_mode ( Plain_Mode::BRANCH );
      node = node->branch ().get_or_create ( adp.name () );
      if ( adp.last_fragment () ) {
        node->set_plain_mode ( Plain_Mode::VALUE );
      }
      break;

    case Address_Parser::Fragment::ARRAY_INDEX:
      // Array child
      node->set_plain_mode ( Plain_Mode::ARRAY );
      node = node->array ().get_or_create ( adp.array_index () );
      if ( adp.last_fragment () ) {
        node->set_plain_mode ( Plain_Mode::VALUE );
      }
      break;
    }
  }

  return node;
}

} // namespace snc::settings::detail
