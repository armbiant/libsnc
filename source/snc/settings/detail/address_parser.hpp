/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <string_view>

namespace snc::settings::detail
{

class Address_Parser
{
  public:
  // -- Types
  enum class Fragment : std::uint8_t
  {
    NONE,
    NAME,
    ARRAY_INDEX
  };

  // -- Construction

  Address_Parser ( const std::string_view & address_n )
  : _last_fragment ( address_n.empty () )
  , _it_cur ( address_n.cbegin () )
  , _it_beg ( address_n.cbegin () )
  , _it_end ( address_n.cend () )
  {
  }

  // -- Iteration

  static bool
  valid_address ( const std::string_view & address_n );

  /// @return True if characters were parsed
  bool
  iterate ();

  // -- Iteration state

  bool
  good () const
  {
    return !_bad;
  }

  bool
  last_fragment () const
  {
    return _last_fragment;
  }

  Fragment
  fragment () const
  {
    return _fragment;
  }

  bool
  is_name () const
  {
    return _fragment == Fragment::NAME;
  }

  bool
  is_array_index () const
  {
    return _fragment == Fragment::ARRAY_INDEX;
  }

  const std::string_view &
  name () const
  {
    return _name;
  }

  std::size_t
  array_index () const
  {
    return _array_index;
  }

  private:
  // -- Utility

  void
  parse_entity_name ();

  void
  parse_array_index ();

  private:
  bool _bad = false;
  bool _last_fragment = false;
  Fragment _fragment = Fragment::NONE;
  std::size_t _array_index = 0;
  std::string_view _name;
  std::string_view::const_iterator _it_cur;
  std::string_view::const_iterator _it_beg;
  std::string_view::const_iterator _it_end;
};

} // namespace snc::settings::detail
