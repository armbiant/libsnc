/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <json/value.h>
#include <snc/settings/detail/node.hpp>
#include <snc/settings/value.hpp>
#include <iosfwd>
#include <mutex>

namespace snc::settings::detail
{

class Database
{
  public:
  // -- Construction

  Database ();

  ~Database ();

  // -- Value interface

  std::pair< bool, Value >
  get_value ( std::string_view address_n ) const;

  void
  set_value ( std::string_view address_n, Value && value_n );

  // -- Json export

  Json::Value
  json_export_value () const;

  void
  json_export_stream ( std::ostream & ostream_n ) const;

  // -- Json import

  void
  json_import_value ( const Json::Value & jval_n );

  std::pair< bool, std::string >
  json_import_string ( std::string_view json_n );

  std::pair< bool, std::string >
  json_import_stream ( std::istream & istream_n );

  private:
  // -- Utility

  const Node *
  get_node ( std::string_view address_n ) const;

  /// @brief Get or create a node
  Node *
  get_or_create_node ( std::string_view address_n );

  /// @brief Get or create a leaf node
  Node *
  get_or_create_leaf ( std::string_view address_n );

  private:
  // -- Attributes
  mutable std::mutex _mutex;
  Node _root;
};

} // namespace snc::settings::detail
