/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "leaf.hpp"
#include <utility>

namespace snc::settings::detail
{

Leaf::Leaf () {}

Leaf::~Leaf () {}

void
Leaf::set_value ( Value && value_n )
{
  if ( _value == value_n ) {
    return;
  }

  _value = std::move ( value_n );
}

} // namespace snc::settings::detail
