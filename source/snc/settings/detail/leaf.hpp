/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/detail/entity.hpp>
#include <snc/settings/value.hpp>

namespace snc::settings::detail
{

class Leaf : public Entity
{
  public:
  // -- Types

  Leaf ();

  ~Leaf ();

  // -- Value

  const Value &
  value () const
  {
    return _value;
  }

  void
  set_value ( Value && value_n );

  private:
  Value _value;
};

} // namespace snc::settings::detail
