/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "array.hpp"
#include <snc/settings/detail/node.hpp>
#include <utility>

namespace snc::settings::detail
{

Array::Array () {}

Array::~Array () {}

const Node *
Array::get ( std::size_t index_n ) const
{
  if ( index_n < _map.size () ) {
    return _map[ index_n ].get ();
  }
  return nullptr;
}

Node *
Array::get ( std::size_t index_n )
{
  if ( index_n < _map.size () ) {
    return _map[ index_n ].get ();
  }
  return nullptr;
}

Node *
Array::get_or_create ( std::size_t index_n )
{
  // Resize on demand
  std::size_t min_size = ( index_n + 1 );
  if ( _map.size () < min_size ) {
    _map.resize ( min_size );
  }

  // Create or change type on demand
  auto & sptr = _map[ index_n ];
  if ( !sptr ) {
    sptr = std::make_unique< Node > ();
  }
  return sptr.get ();
}

} // namespace snc::settings::detail
