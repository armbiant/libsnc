/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "address_parser.hpp"
#include <charconv>

namespace snc::settings::detail
{

bool
Address_Parser::valid_address ( const std::string_view & address_n )
{
  Address_Parser adp ( address_n );
  while ( adp.iterate () ) {
  }
  return adp.good ();
}

bool
Address_Parser::iterate ()
{
  // Reset state
  _array_index = 0;
  _name = std::string_view ();

  if ( _bad || ( _it_cur == _it_end ) ) {
    return false;
  }

  if ( _it_cur == _it_beg ) {
    // First address section
    parse_entity_name ();
  } else {
    if ( *_it_cur == '[' ) {
      parse_array_index ();
    } else if ( *_it_cur == '.' ) {
      ++_it_cur; // Skip over dot
      parse_entity_name ();
    } else {
      // Bad sub fragment type
      _bad = true;
      _fragment = Fragment::NONE;
    }
  }

  return true;
}

void
Address_Parser::parse_entity_name ()
{
  _fragment = Fragment::NAME;

  const auto itb = _it_cur;
  const auto ite = _it_end;
  std::string_view::const_iterator itc = itb;

  while ( ( itc != ite ) && ( *itc != '.' ) && ( *itc != '[' ) ) {
    ++itc;
  }

  // Name finished
  auto name_length = std::distance ( itb, itc );
  if ( name_length > 0 ) {
    // Accept name
    _name = std::string_view ( &*itb, name_length );
  } else {
    // Zero length name
    _bad = true;
  }

  // Update state iterator
  if ( itc == ite ) {
    _last_fragment = true;
  }
  _it_cur = itc;
}

void
Address_Parser::parse_array_index ()
{
  _fragment = Fragment::ARRAY_INDEX;

  const auto itb = ++_it_cur; // Skip opening bracket
  const auto ite = _it_end;
  std::string_view::const_iterator itc = itb;

  // Find closing bracket
  while ( ( itc != ite ) && ( *itc != ']' ) ) {
    ++itc;
  }
  // Early string end?
  if ( itc == ite ) {
    _it_cur = itc;
    _bad = true;
    return;
  }

  // Move beyond closing bracket and update state iterator
  ++itc;
  if ( itc == ite ) {
    _last_fragment = true;
  }
  _it_cur = itc;

  // Evaluate number string
  auto number_length = std::distance ( itb, itc ) - 1;
  // Zero length number string?
  if ( number_length == 0 ) {
    _bad = true;
    return;
  }

  const char * beg = &*itb;
  auto ret = std::from_chars ( beg, beg + number_length, _array_index );
  if ( ret.ec == std::errc::invalid_argument ) {
    _bad = true;
  }
}

} // namespace snc::settings::detail
