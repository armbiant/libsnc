/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "utility.hpp"

namespace snc::settings::qt
{

QVariant
make_qvariant ( const Value & value_n )
{
  switch ( value_n.type () ) {
  case Value::Type::NONE:
    return QVariant ();
  case Value::Type::BOOL:
    return QVariant ( value_n.as_bool () );
  case Value::Type::DOUBLE:
    return QVariant ( value_n.as_double () );
  case Value::Type::STRING: {
    auto vstr = value_n.as_string ();
    return QVariant ( QString::fromUtf8 ( vstr.data (), vstr.size () ) );
  }
  }
  return QVariant ();
}

Value
make_value ( const QVariant & var_n )
{
  switch ( var_n.type () ) {
  case QMetaType::Bool:
    return Value ( var_n.toBool () );
  case QMetaType::Double:
  case QMetaType::Int:
  case QMetaType::UInt:
  case QMetaType::LongLong:
  case QMetaType::ULongLong:
    return Value ( var_n.toDouble () );
  case QMetaType::QString:
    return Value ( var_n.toString ().toStdString () );
  default:
    break;
  }

  if ( var_n.isValid () ) {
    return Value ( var_n.toBool () );
  }

  return Value ();
}

} // namespace snc::settings::qt
