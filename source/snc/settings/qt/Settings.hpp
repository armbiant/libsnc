/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/types.hpp>
#include <QObject>
#include <QString>
#include <QVariant>

namespace snc::settings::qt
{

/** Settings tree
 */
class Settings : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Settings ( QObject * parent_n = nullptr );

  /// @brief Creates another view into the database
  Settings ( Database_Reference database_n, QObject * parent_n = nullptr );

  ~Settings ();

  // -- Database

  /// @brief Shared database accessor
  Database_Reference
  database () const;

  // -- Reading

  /// @brief Get the value at @a address_n or return default_n.
  Q_INVOKABLE
  QVariant
  get ( const QString & address_n,
        const QVariant & default_n = QVariant () ) const;

  // -- Writing

  Q_INVOKABLE
  void
  set ( const QString & address_n, const QVariant & value_n );

  // -- Json export

  Q_INVOKABLE
  QString
  jsonExportString () const;

  // -- Json import

  Q_INVOKABLE
  bool
  jsonImportString ( const QString & json_n );

  private:
  // -- Implementation
  Database_Reference _db;
};

} // namespace snc::settings::qt
