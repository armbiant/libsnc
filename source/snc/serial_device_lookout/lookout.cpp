/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "lookout.hpp"
#include <sev/assert.hpp>
#include <sev/event/queue_io/reference.hpp>
#include <snc/serial_device_lookout/thread.hpp>

namespace snc::serial_device_lookout
{

Lookout::Lookout ( sev::thread::Tracker * thread_tracker_n,
                   const sev::logt::Reference & log_parent_n )
: _log ( log_parent_n, "Emb_Device_Lookout" )
, _thread_tracker ( thread_tracker_n )
{
}

Lookout::~Lookout ()
{
  DEBUG_ASSERT ( !is_running () );
}

void
Lookout::set_event_callback ( const std::function< void () > & callback_n,
                              bool call_on_demand_n )
{
  _event_accu.set_callback ( callback_n, call_on_demand_n );
}

void
Lookout::clear_event_callback ()
{
  return _event_accu.clear_callback ();
}

void
Lookout::add_usb_device_query (
    std::shared_ptr< const snc::usb::Device_Query > query_n )
{
  if ( !is_running () || is_aborting () ) {
    DEBUG_ASSERT ( false );
    return;
  }

  {
    auto * event = _epools.pool< events::out::USB_Query_Add > ().acquire ();
    event->set_query ( query_n );
    _connection.push ( event );
  }
  _usb_device_queries.push_back ( query_n );
}

void
Lookout::start ()
{
  if ( _thread ) {
    return;
  }

  _log.cat ( sev::logt::FL_DEBUG_0, "Starting thread." );

  // Queues
  auto queue_ref = sev::event::queue_io::Reference_2::created ();
  _connection.set_incoming_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::EVENT_IN ); } );
  _connection.set_push_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::EVENT_OUT ); } );
  _connection.connect ( queue_ref.link_a () );

  _epools.pool< events::out::Thread_Abort > ().set_size ( 1 );

  // Thread
  _thread =
      std::make_unique< Thread > ( _log, queue_ref.link_b (), _thread_tracker );
  _thread->start ();
}

void
Lookout::abort ()
{
  if ( !_abort_state.is_empty () ) {
    return;
  }

  _usb_device_queries.clear ();
  _abort_state.set ( Abort_State::ENABLED );
  _event_accu.set ( LEvent::PROCESS );
}

void
Lookout::events ( const Callbacks & callbacks_n )
{
  sev::mem::Flags_Fast32 loop_events;
  while ( true ) {
    // Fetch events
    loop_events.set ( _event_accu.fetch_and_clear () );
    if ( !loop_events ) {
      break;
    }
    loop_events.unset ( LEvent::PROCESS );

    // Feed event queue
    _connection.feed_queue ( loop_events, LEvent::EVENT_IN, LEvent::EVENT_OUT );

    // Release returned events
    _connection.out ().release_all_with_reset ();

    // Process new events
    _connection.in ().process_all (
        [ this, &callbacks_n ] ( const sev::event::Event & event_n ) {
          switch ( event_n.type () ) {
          case events::in::Type::THREAD_DONE:
            DEBUG_ASSERT ( _abort_state.test_any ( Abort_State::ENABLED ) );
            DEBUG_ASSERT ( _abort_state.test_any ( Abort_State::REQUESTED ) );
            _abort_state.set ( Abort_State::ACKNOWLEDGED );
            break;
          case events::in::Type::DEVICE_NEW: {
            auto & cevent =
                static_cast< const events::in::Device_New & > ( event_n );
            // Add to devices list
            _devices.push_back ( cevent.letter () );
            // Notify callback
            if ( callbacks_n.device_new ) {
              _event_device = cevent.letter ();
              callbacks_n.device_new ();
              _event_device.reset ();
            }
          } break;
          case events::in::Type::DEVICE_REMOVE: {
            auto & cevent =
                static_cast< const events::in::Device_Remove & > ( event_n );
            // Remove from devices list
            _devices.erase (
                std::remove_if (
                    _devices.begin (),
                    _devices.end (),
                    [ &cevent ] (
                        const std::shared_ptr< snc::serial_device::Letter > &
                            item ) { return item == cevent.letter (); } ),
                _devices.end () );
            // Notify callback
            if ( callbacks_n.device_remove ) {
              _event_device = cevent.letter ();
              callbacks_n.device_remove ();
              _event_device.reset ();
            }
          } break;
          default:
            break;
          }
        } );

    // Abort processing
    if ( !_abort_state.is_empty () ) {
      // Send thread abort
      if ( !_abort_state.test_any_set ( Abort_State::REQUESTED ) ) {
        _log.cat ( sev::logt::FL_DEBUG_0, "Sending thread abort request." );
        _connection.push (
            _epools.pool< events::out::Thread_Abort > ().acquire () );
      }

      // Join thread
      if ( _abort_state.test_any ( Abort_State::ACKNOWLEDGED ) ) {
        if ( _connection.all_empty () && _epools.tracker ().all_home () ) {
          // Join thread
          _log.cat ( sev::logt::FL_DEBUG_0, "Join thread: Begin." );
          _thread.reset ();
          _log.cat ( sev::logt::FL_DEBUG_0, "Join thread: Done." );

          // Reset state
          _connection.disconnect ();
          _epools.tracker ().clear_all_capacities ();
          _abort_state.clear ();
          _devices.clear ();

          // Notify callback
          if ( callbacks_n.stopped ) {
            callbacks_n.stopped ();
          }
        }
      }
    }
  }
}

} // namespace snc::serial_device_lookout
