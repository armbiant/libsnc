/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "thread.hpp"
#include <sev/assert.hpp>
#include <sev/system.hpp>
#include <sev/unicode/convert.hpp>
#include <snc/serial_device/letter_usb.hpp>
#include <string_view>

namespace snc::serial_device_lookout
{

Thread::Thread ( const sev::logt::Reference & log_parent_n,
                 const sev::event::queue_io::Link_2 & link_n,
                 sev::thread::Tracker * thread_tracker_n )
: _log ( log_parent_n, "T" )
, _thread ( "Emb. device lookout", thread_tracker_n )
{
  _connection.set_incoming_notifier (
      [ &ec = _event_collector ] () { ec.set ( LEvent::CONTROL_IN ); } );
  _connection.set_push_notifier (
      [ &le = _loop_events ] () { le.set ( LEvent::CONTROL_OUT ); } );
  _connection.connect ( link_n );

  // Push event collector socket id to poll list
  {
    pollfd pfd;
    pfd.fd = _event_collector.socket_fd ();
    pfd.events = _event_collector.socket_events ();
    _pollfds.push_back ( pfd );
  }

  _epools.pool< events::in::Thread_Done > ().set_size ( 1 );
  _epools.pool< events::in::Device_New > ().set_size ( 4 );
  _epools.pool< events::in::Device_Remove > ().set_size ( 4 );

  _devices.reserve ( 16 );
  _usb_devices_arrived.reserve ( 32 );
}

Thread::~Thread () {}

bool
Thread::usb_context_open ()
{
  DEBUG_ASSERT ( _lusb_context == nullptr );
  DEBUG_ASSERT ( _lusb_context_timeouts == false );

  if ( libusb_init ( &_lusb_context ) != 0 ) {
    // Context creation failed
    _lusb_context = nullptr;
    return false;
  }

  // Set debug level
  libusb_set_option (
      _lusb_context, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_NONE );

  // Determine if timeout calls are required
  _lusb_context_timeouts =
      ( libusb_pollfds_handle_timeouts ( _lusb_context ) == 0 );

  // Acquire pollfds
  {
    const libusb_pollfd ** usb_pollfds ( libusb_get_pollfds ( _lusb_context ) );
    for ( const libusb_pollfd ** pit = usb_pollfds; ( *pit ) != nullptr;
          ++pit ) {
      ::pollfd pfd;
      pfd.fd = ( *pit )->fd;
      pfd.events = ( *pit )->events;
      _pollfds.push_back ( pfd );
    }
    libusb_free_pollfds ( usb_pollfds );
  }

  // Install pollfd change callbacks
  {
    libusb_set_pollfd_notifiers ( _lusb_context,
                                  &Thread::lusb_cb_pollfd_added,
                                  &Thread::lusb_cb_pollfd_removed,
                                  this // Pass this as user data
    );
  }

  // Install hotplug callbacks
  {
    libusb_hotplug_register_callback ( _lusb_context,
                                       static_cast< libusb_hotplug_event > (
                                           LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED |
                                           LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT ),
                                       LIBUSB_HOTPLUG_ENUMERATE,
                                       LIBUSB_HOTPLUG_MATCH_ANY,
                                       LIBUSB_HOTPLUG_MATCH_ANY,
                                       LIBUSB_HOTPLUG_MATCH_ANY,
                                       &Thread::lusb_cb_hotplug,
                                       this,
                                       &_lusb_hotplug_handle );
  }
  return true;
}

void
Thread::usb_context_close ()
{
  if ( _lusb_context == nullptr ) {
    return;
  }

  libusb_exit ( _lusb_context );
  _lusb_context = nullptr;
  _lusb_context_timeouts = false;
}

void
Thread::start ()
{
  if ( !_thread.is_running () ) {
    DEBUG_ASSERT ( _connection.is_connected () );

    _loop_events.clear ();
    _abort_events.clear ();

    _thread.start ( [ this ] () { this->operator() (); } );
  }
}

void
Thread::join ()
{
  if ( _thread.is_running () ) {
    _thread.join ();
  }
}

void
Thread::operator() ()
{
  usb_context_open ();
  {
    // Enter main loop
    _log.cat ( sev::logt::FL_DEBUG_0, "Thread main loop begin" );
    trun_loop ();
    _log.cat ( sev::logt::FL_DEBUG_0, "Thread main loop done" );
  }
  usb_context_close ();
}

void
Thread::trun_loop ()
{
  while ( true ) {
    // -- Wait for events
    wait_for_events ();

    // -- Queue io
    _connection.feed_queue (
        _loop_events, LEvent::CONTROL_IN, LEvent::CONTROL_OUT );

    // -- Event processing
    process_control_events ();
    process_libusb_events ();

    // Shut down handling
    if ( !_abort_events.is_empty () && process_shut_down () ) {
      break;
    }
  }
}

bool
Thread::process_shut_down ()
{
  if ( !_epools.tracker ().all_home () || !_connection.all_empty () ) {
    return false;
  }

  if ( _abort_events.test_any ( Abort_Event::SHUT_DOWN ) ) {
    if ( !_abort_events.test_any_set ( Abort_Event::SHUT_DOWN_SENT ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Sending thread done event" );
      _connection.push (
          _epools.pool< events::in::Thread_Done > ().acquire () );
      return false;
    }
  }

  return true;
}

void
Thread::wait_for_events ()
{
  int poll_ret = 0;
  if ( _loop_events.is_empty () ) {
    // Poll timeout in milli seconds
    int timeout_ms ( -1 );
    // Check if libusb requires timeouts
    if ( _lusb_context_timeouts ) {
      timeval tval;
      if ( libusb_get_next_timeout ( _lusb_context, &tval ) != 0 ) {
        int usb_ms = ( tval.tv_sec * 1000 );
        usb_ms += ( tval.tv_usec + ( 1000 - 1 ) ) / 1000;
        sev::math::assign_larger ( usb_ms, 0 );
        _loop_events.set ( LEvent::LIBUSB );
      }
    }
    // Poll file descriptors
    poll_ret = poll ( &_pollfds.front (), _pollfds.size (), timeout_ms );
  }

  if ( poll_ret == 0 ) {
    // Poll timeout
  } else if ( poll_ret > 0 ) {
    // Poll socket events
    auto ite = _pollfds.cend ();
    auto it = _pollfds.cbegin ();

    // control queues events
    if ( it->revents != 0 ) {
      _loop_events.set ( _event_collector.fetch_and_clear () );
    }
    ++it;

    // libusb events
    for ( ; it != ite; ++it ) {
      if ( it->revents != 0 ) {
        _loop_events.set ( LEvent::LIBUSB );
        break;
      }
    }
  } else {
    // Poll error
    if ( errno == EINTR ) {
      // Call was interrupted, do nothing
    } else {
      _log.cat ( sev::logt::FL_ERROR,
                 "poll() error:\n",
                 sev::strerror_string ( errno ) );
    }
  }
}

void
Thread::process_control_events ()
{
  // Release returned events
  _connection.out ().release_all_with_reset ();

  // Process new events
  _connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        switch ( event_n.type () ) {
        case events::out::Type::THREAD_ABORT:
          process_control_event_thread_abort ( event_n );
          break;
        case events::out::Type::USB_QUERY_ADD:
          process_control_event_usb_query_add ( event_n );
          break;
        default:
          _log.cat ( sev::logt::FL_WARNING,
                     "Unknown control event ",
                     &event_n,
                     " of type ",
                     event_n.type () );
          DEBUG_ASSERT ( false );
          break;
        }
      } );
}

void
Thread::process_control_event_thread_abort ( const sev::event::Event & event_n )
{
  (void)event_n;
  {
    _log.cat ( sev::logt::FL_DEBUG_0, "Thread abort event" );
    _abort_events.set ( Abort_Event::SHUT_DOWN );
  }
}

void
Thread::process_control_event_usb_query_add (
    const sev::event::Event & event_n )
{
  auto & cevent = static_cast< const events::out::USB_Query_Add & > ( event_n );
  _usb_device_queries.push_back ( cevent.query () );

  // Checks if the device on the USB port is emitted already
  auto emitted =
      [ this ] ( const snc::usb::Device_Port_Id & port_id_n ) -> bool {
    for ( auto const & usb_device : _devices ) {
      auto letter_usb = dynamic_cast< snc::serial_device::Letter_USB const * > (
          usb_device.get () );
      if ( letter_usb && ( letter_usb->usb_port_id () == port_id_n ) ) {
        return true;
      }
    }
    return false;
  };

  // Check known USB devices against the new query
  for ( auto & usb_device : _usb_devices ) {
    // Continue if device doesn't match the query
    if ( !probe_usb_query ( *usb_device, *cevent.query () ) ) {
      continue;
    }
    // Continue if device was emitted already
    if ( emitted ( usb_device->port_id ) ) {
      continue;
    }
    // Emit newly found device
    emit_emb_usb_device ( usb_device->port_id );
  }
}

void
Thread::process_libusb_events ()
{
  if ( _loop_events.test_any_unset ( LEvent::LIBUSB ) ) {
    timeval tval;
    tval.tv_sec = 0;
    tval.tv_usec = 0;
    int ret =
        libusb_handle_events_timeout_completed ( _lusb_context, &tval, 0 );
    if ( ret != 0 ) {
      _log.cat ( sev::logt::FL_ERROR,
                 "libusb_handle_events_timeout_completed error" );
    }
  }

  // Process arrived hotplug devices.
  // This must happen outside of the hotplug callback.
  if ( !_usb_devices_arrived.empty () ) {
    for ( libusb_device_handle * device_handle : _usb_devices_arrived ) {
      // Add usb device and probe
      if ( usb_add_device ( device_handle ) ) {
        auto & usb_device = *_usb_devices.back ();
        if ( probe_usb_queries ( usb_device ) ) {
          emit_emb_usb_device ( usb_device.port_id );
        }
      }
      libusb_close ( device_handle );
    }
    _usb_devices_arrived.clear ();
  }
}

void
Thread::lusb_cb_pollfd_added ( int fd_n, short events_n, void * user_data_n )
{
  Thread & cinst ( *static_cast< Thread * > ( user_data_n ) );

  bool found ( false );
  for ( auto & item : cinst._pollfds ) {
    if ( item.fd == fd_n ) {
      found = true;
      break;
    }
  }
  if ( !found ) {
    pollfd pfd;
    pfd.fd = fd_n;
    pfd.events = events_n;
    cinst._pollfds.push_back ( pfd );
  }
}

void
Thread::lusb_cb_pollfd_removed ( int fd_n, void * user_data_n )
{
  Thread & cinst ( *static_cast< Thread * > ( user_data_n ) );

  auto it = cinst._pollfds.begin ();
  while ( it != cinst._pollfds.end () ) {
    if ( it->fd == fd_n ) {
      it = cinst._pollfds.erase ( it );
    } else {
      ++it;
    }
  }
}

int
Thread::lusb_cb_hotplug ( libusb_context * context_n,
                          libusb_device * device_n,
                          libusb_hotplug_event event_n,
                          void * user_data_n )
{
  (void)context_n;
  Thread & cinst ( *static_cast< Thread * > ( user_data_n ) );
  switch ( event_n ) {
  case LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED:
    cinst.lusb_hotplug_device_arrived ( device_n );
    break;
  case LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT:
    cinst.lusb_hotplug_device_left ( device_n );
    break;
  default:
    break;
  }
  return 0;
}

void
Thread::lusb_hotplug_device_arrived ( libusb_device * device_n )
{
  libusb_device_handle * device_handle = nullptr;

  // Open USB device
  if ( ::libusb_open ( device_n, &device_handle ) == 0 ) {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB hotplug device opened" );
    _usb_devices_arrived.push_back ( device_handle );
  } else {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB hotplug device opening failed" );
  }
}

void
Thread::lusb_hotplug_device_left ( libusb_device * device_n )
{
  std::shared_ptr< snc::serial_device::Letter > entry;
  // Find device
  {
    snc::usb::Device_Port_Id const port_id ( device_n );
    auto it = _devices.begin ();
    while ( it != _devices.end () ) {
      auto letter_usb =
          dynamic_cast< snc::serial_device::Letter_USB * > ( it->get () );
      if ( letter_usb && ( letter_usb->usb_port_id () == port_id ) ) {
        entry = std::move ( *it );
        it = _devices.erase ( it );
        break;
      } else {
        ++it;
      }
    }
  }
  // Send remove event
  if ( entry ) {
    auto * event = _epools.pool< events::in::Device_Remove > ().acquire ();
    event->set_letter ( std::move ( entry ) );
    _connection.push ( event );
  }
}

void
Thread::emit_emb_usb_device ( const snc::usb::Device_Port_Id & id_n )
{
  auto letter_usb = std::make_shared< snc::serial_device::Letter_USB > ();
  letter_usb->usb_port_id_ref () = id_n;

  _devices.emplace_back ( letter_usb );
  {
    auto * event = _epools.pool< events::in::Device_New > ().acquire ();
    event->set_letter ( letter_usb );
    _connection.push ( event );
  }
}

bool
Thread::probe_usb_queries ( USB_Device & usb_device_n )
{
  for ( auto const & query : _usb_device_queries ) {
    if ( probe_usb_query ( usb_device_n, *query ) ) {
      return true;
    }
  }
  return false;
}

bool
Thread::probe_usb_query ( USB_Device & usb_device_n,
                          const snc::usb::Device_Query & query_n )
{
  if ( query_n.manufacturer ) {
    if ( !std::regex_match ( usb_device_n.iManufacturer,
                             query_n.manufacturer->regex ) ) {
      return false;
    }
  }

  if ( query_n.product ) {
    if ( !std::regex_match ( usb_device_n.iProduct, query_n.product->regex ) ) {
      return false;
    }
  }

  if ( query_n.serial_number ) {
    if ( !std::regex_match ( usb_device_n.iSerialNumber,
                             query_n.serial_number->regex ) ) {
      return false;
    }
  }

  return true;
}

bool
Thread::usb_add_device ( libusb_device_handle * device_handle_n )
{
  bool res = false;
  {
    auto usb_device = std::make_shared< USB_Device > ();
    if ( usb_add_device_load ( device_handle_n, *usb_device ) ) {
      _usb_devices.emplace_back ( std::move ( usb_device ) );
      res = true;
    }
  }
  return res;
}

bool
Thread::usb_add_device_load ( libusb_device_handle * device_handle_n,
                              USB_Device & usb_device_n )
{
  // Read device descriptor
  libusb_device_descriptor device_descriptor;
  if ( libusb_get_device_descriptor ( libusb_get_device ( device_handle_n ),
                                      &device_descriptor ) != 0 ) {
    _log.cat ( sev::logt::FL_DEBUG_0, "Device descriptor eading failed" );
    return false;
  }

  // Read port id
  usb_device_n.port_id.load ( libusb_get_device ( device_handle_n ) );

  // Read strings
  if ( !usb_add_device_istring ( device_handle_n,
                                 device_descriptor.iManufacturer,
                                 usb_device_n.iManufacturer ) ) {
    return false;
  }
  if ( !usb_add_device_istring ( device_handle_n,
                                 device_descriptor.iProduct,
                                 usb_device_n.iProduct ) ) {
    return false;
  }
  if ( !usb_add_device_istring ( device_handle_n,
                                 device_descriptor.iSerialNumber,
                                 usb_device_n.iSerialNumber ) ) {
    return false;
  }

  // Success
  return true;
}

bool
Thread::usb_add_device_istring ( libusb_device_handle * device_handle_n,
                                 int string_index_n,
                                 std::string & istring_n )
{
  // Some devices choke on size > 255
  const unsigned int tbuff_size ( 255 );
  unsigned char tbuf[ tbuff_size ];
  {
    int ret = libusb_get_string_descriptor (
        device_handle_n, 0, 0, tbuf, tbuff_size );
    if ( ret < 0 ) {
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Root string descriptor reading failed" );
      return false;
    }
    if ( ret < 4 ) {
      // Invalid descriptor size
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Root string descriptor has invalid size" );
      return false;
    }
  }
  {
    // Use first language id (see USB 2.0 specs)
    int langid ( tbuf[ 2 ] | ( tbuf[ 3 ] << 8 ) );
    int ret = libusb_get_string_descriptor (
        device_handle_n, string_index_n, langid, tbuf, tbuff_size );
    if ( ret < 0 ) {
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Default language string descriptor reading failed" );
      return false;
    }

    unsigned int tbuff_num_read = ret;
    if ( tbuff_num_read < 2 ) {
      // Invalid descriptor size
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Default language string descriptor has invalid size" );
      return false;
    } else if ( tbuf[ 1 ] != LIBUSB_DT_STRING ) {
      // Invalid string descriptor type byte
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Default language string descriptor has invalid type byte" );
      return false;
    } else if ( tbuf[ 0 ] > tbuff_num_read ) {
      // Descriptor wasn't read completely
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Default language string descriptor is too long" );
      return false;
    }
  }

  {
    unsigned int num_utf16_chars ( ( tbuf[ 0 ] - 2 ) / 2 );
    unsigned char * uchar ( &tbuf[ 2 ] );
    sev::unicode::convert (
        istring_n,
        std::u16string_view ( reinterpret_cast< const char16_t * > ( uchar ),
                              num_utf16_chars ) );
  }
  return true;
}

} // namespace snc::serial_device_lookout
