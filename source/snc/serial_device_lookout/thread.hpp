/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>
#include <sev/event/bit_accus_async/socket.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/thread.hpp>
#include <snc/serial_device_lookout/events.hpp>
#include <snc/usb/device_descriptor.hpp>
#include <snc/usb/device_port_id.hpp>
#include <snc/usb/device_query.hpp>
#include <poll.h>
#include <vector>

namespace snc::serial_device_lookout
{

class Thread
{
  public:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t CONTROL_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t CONTROL_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t LIBUSB = ( 1 << 2 );
  };

  struct Abort_Event
  {
    static constexpr std::uint_fast32_t SHUT_DOWN = ( 1 << 0 );
    static constexpr std::uint_fast32_t SHUT_DOWN_SENT = ( 1 << 1 );
  };

  struct USB_Device
  {
    snc::usb::Device_Port_Id port_id;

    std::string iManufacturer;
    std::string iProduct;
    std::string iSerialNumber;
  };

  enum class Socket_Index
  {
    CONTROL,
    LIBUSB_BEGIN
  };

  // -- Construction

  Thread ( const sev::logt::Reference & log_parent_n,
           const sev::event::queue_io::Link_2 & link_n,
           sev::thread::Tracker * thread_tracker_n );

  ~Thread ();

  // -- Start / stop interface

  void
  start ();

  void
  join ();

  bool
  is_running () const
  {
    return _thread.is_running ();
  }

  void
  operator() ();

  private:
  // -- Utility
  bool
  usb_context_open ();

  void
  usb_context_close ();

  void
  trun_loop ();

  bool
  process_shut_down ();

  void
  wait_for_events ();

  void
  process_control_events ();

  void
  process_control_event_thread_abort ( const sev::event::Event & event_n );

  void
  process_control_event_usb_query_add ( const sev::event::Event & event_n );

  void
  process_libusb_events ();

  static void
  lusb_cb_pollfd_added ( int fd_n, short events_n, void * user_data_n );

  static void
  lusb_cb_pollfd_removed ( int fd_n, void * user_data_n );

  static int
  lusb_cb_hotplug ( libusb_context * context_n,
                    libusb_device * device_n,
                    libusb_hotplug_event event_n,
                    void * user_data_n );

  void
  lusb_hotplug_device_arrived ( libusb_device * device_n );

  void
  lusb_hotplug_device_left ( libusb_device * device_n );

  void
  emit_emb_usb_device ( const snc::usb::Device_Port_Id & id_n );

  bool
  probe_usb_queries ( USB_Device & usb_device_n );

  bool
  probe_usb_query ( USB_Device & usb_device_n,
                    const snc::usb::Device_Query & query_n );

  bool
  usb_add_device ( libusb_device_handle * device_handle_n );

  bool
  usb_add_device_load ( libusb_device_handle * device_handle_n,
                        USB_Device & usb_device_n );

  bool
  usb_add_device_istring ( libusb_device_handle * device_handle_n,
                           int string_index_n,
                           std::string & istring_n );

  private:
  // -- Attributes
  /// @brief Logging context
  sev::logt::Context _log;

  // Loop
  sev::mem::Flags_Fast32 _loop_events;
  sev::mem::Flags_Fast32 _abort_events;

  sev::event::bit_accus_async::Socket _event_collector;

  // libusb structs
  libusb_context * _lusb_context = nullptr;
  libusb_hotplug_callback_handle _lusb_hotplug_handle;
  bool _lusb_context_timeouts = false;

  /// @brief Poll structs buffer
  std::vector< ::pollfd > _pollfds;

  // Event queues: Controller
  sev::event::queue_io::Connection_2 _connection;

  // Events pools
  sev::event::Tracker_Pools< events::in::Thread_Done,
                             events::in::Device_New,
                             events::in::Device_Remove >
      _epools;

  /// @brief USB device identificator
  std::vector< std::shared_ptr< snc::serial_device::Letter > > _devices;

  std::vector< std::shared_ptr< const snc::usb::Device_Query > >
      _usb_device_queries;
  std::vector< std::shared_ptr< USB_Device > > _usb_devices;
  std::vector< libusb_device_handle * > _usb_devices_arrived;

  /// @brief Thread instance
  sev::thread::Thread _thread;
};

} // namespace snc::serial_device_lookout
