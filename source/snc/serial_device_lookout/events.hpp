/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/usb/device_query.hpp>
#include <cstdint>
#include <memory>
#include <string>

// -- Forward declaration
namespace snc::serial_device
{
class Letter;
}

namespace snc::serial_device_lookout::events::in
{

struct Type
{
  // -- Invalid
  static constexpr std::uint_fast32_t INVALID = 0;
  // -- Thread runtime control
  static constexpr std::uint_fast32_t THREAD_DONE = 1;
  // -- Device events
  static constexpr std::uint_fast32_t DEVICE_NEW = 2;
  static constexpr std::uint_fast32_t DEVICE_REMOVE = 3;
};

class Thread_Done : public sev::event::Event
{
  public:
  static constexpr std::uint_fast32_t etype = Type::THREAD_DONE;

  Thread_Done ()
  : sev::event::Event ( etype )
  {
  }
};

class Device_ : public sev::event::Event
{
  public:
  void
  reset ()
  {
    sev::event::Event::reset ();
    _letter.reset ();
  }

  std::shared_ptr< snc::serial_device::Letter > const &
  letter () const
  {
    return _letter;
  }

  void
  set_letter ( std::shared_ptr< snc::serial_device::Letter > letter_n )
  {
    _letter = std::move ( letter_n );
  }

  protected:
  Device_ ( std::uint_fast32_t etype_n )
  : sev::event::Event ( etype_n )
  {
  }

  private:
  std::shared_ptr< snc::serial_device::Letter > _letter;
};

class Device_New : public Device_
{
  public:
  static constexpr std::uint_fast32_t etype = Type::DEVICE_NEW;

  Device_New ()
  : Device_ ( etype )
  {
  }
};

class Device_Remove : public Device_
{
  public:
  static constexpr std::uint_fast32_t etype = Type::DEVICE_REMOVE;

  Device_Remove ()
  : Device_ ( etype )
  {
  }
};

} // namespace snc::serial_device_lookout::events::in

namespace snc::serial_device_lookout::events::out
{

struct Type
{
  // -- Invalid
  static constexpr std::uint_fast32_t INVALID = 0;
  // -- Thread runtime control
  static constexpr std::uint_fast32_t THREAD_ABORT = 1;
  // -- Query eventy
  static constexpr std::uint_fast32_t USB_QUERY_ADD = 2;
};

class Thread_Abort : public sev::event::Event
{
  public:
  static constexpr std::uint_fast32_t etype = Type::THREAD_ABORT;

  Thread_Abort ()
  : sev::event::Event ( etype )
  {
  }
};

class USB_Query_ : public sev::event::Event
{
  public:
  void
  reset ()
  {
    sev::event::Event::reset ();
    _query.reset ();
  }

  std::shared_ptr< const snc::usb::Device_Query > const &
  query () const
  {
    return _query;
  }

  void
  set_query ( std::shared_ptr< const snc::usb::Device_Query > device_n )
  {
    _query = std::move ( device_n );
  }

  protected:
  USB_Query_ ( std::uint_fast32_t etype_n )
  : sev::event::Event ( etype_n )
  {
  }

  private:
  std::shared_ptr< const snc::usb::Device_Query > _query;
};

class USB_Query_Add : public USB_Query_
{
  public:
  static constexpr std::uint_fast32_t etype = Type::USB_QUERY_ADD;

  USB_Query_Add ()
  : USB_Query_ ( etype )
  {
  }
};

} // namespace snc::serial_device_lookout::events::out
