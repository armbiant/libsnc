/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "handle.hpp"
#include <snc/device/info.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::statics::handle
{

// -- Utility
Statics_Writeable
make ()
{
  return std::make_shared< snc::device::statics::Statics > ();
}

} // namespace snc::device::statics::handle

namespace snc::device::handle
{

Info_Writeable
make_info ( const snc::device::statics::handle::Statics & device_statics_n )
{
  return std::make_shared< snc::device::Info > ( device_statics_n );
}

} // namespace snc::device::handle
