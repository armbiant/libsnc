/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <chrono>
#include <cstdint>
#include <utility>

namespace snc::device::statics::control
{

/// @brief Boolean control
///
class I1 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Switch index

    std::uint_fast32_t
    control_index () const
    {
      return _control_index;
    }

    void
    set_control_index ( std::uint_fast32_t index_n )
    {
      _control_index = index_n;
    }

    // -- Inverted

    bool
    inverted () const
    {
      return _inverted;
    }

    void
    set_inverted ( bool flag_n )
    {
      _inverted = flag_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _control_index = Item::invalid_index_fast32;
    bool _inverted = false;
  };

  // -- Construction

  I1 ( std::uint_fast32_t index_n = 0 );

  ~I1 ();

  // -- Default state

  bool
  default_state () const
  {
    return _default_state;
  }

  void
  set_default_state ( bool state_n )
  {
    _default_state = state_n;
  }

  // -- User settable

  bool
  user_settable () const
  {
    return _user_settable;
  }

  void
  set_user_settable ( bool flag_n )
  {
    _user_settable = flag_n;
  }

  // -- Warm up duration

  const std::chrono::nanoseconds &
  warm_up_duration () const
  {
    return _warm_up_duration;
  }

  void
  set_warm_up_duration ( std::chrono::nanoseconds dur_n )
  {
    _warm_up_duration = dur_n;
  }

  // -- Cool down duration

  const std::chrono::nanoseconds &
  cool_down_duration () const
  {
    return _cool_down_duration;
  }

  void
  set_cool_down_duration ( std::chrono::nanoseconds dur_n )
  {
    _cool_down_duration = dur_n;
  }

  // -- Embedded device sensor

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  private:
  // -- Attributes
  bool _default_state = false;
  bool _user_settable = false;
  std::chrono::nanoseconds _warm_up_duration = {};
  std::chrono::nanoseconds _cool_down_duration = {};
  Emb _emb;
};

} // namespace snc::device::statics::control
