/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i16.hpp"

namespace snc::device::statics::control
{

I16::I16 ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

I16::~I16 () = default;

} // namespace snc::device::statics::control
