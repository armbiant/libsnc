/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <cstdint>

namespace snc::device::statics::control
{

/// @brief 64 bit control
///
class I64 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Switch index

    std::uint_fast32_t
    control_index () const
    {
      return _control_index;
    }

    void
    set_control_index ( std::uint_fast32_t index_n )
    {
      _control_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _control_index = Item::invalid_index_fast32;
  };

  // -- Construction

  I64 ( std::uint_fast32_t index_n = 0 );

  ~I64 ();

  // -- Default state

  std::uint64_t
  default_state () const
  {
    return _default_state;
  }

  void
  set_default_state ( std::uint64_t state_n )
  {
    _default_state = state_n;
  }

  // -- User settable

  bool
  user_settable () const
  {
    return _user_settable;
  }

  void
  set_user_settable ( bool flag_n )
  {
    _user_settable = flag_n;
  }

  // -- Embedded device sensor

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  private:
  // -- Attributes
  std::uint64_t _default_state = 0;
  bool _user_settable = false;
  Emb _emb;
};

} // namespace snc::device::statics::control
