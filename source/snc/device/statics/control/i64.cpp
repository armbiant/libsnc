/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i64.hpp"

namespace snc::device::statics::control
{

I64::I64 ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

I64::~I64 () = default;

} // namespace snc::device::statics::control
