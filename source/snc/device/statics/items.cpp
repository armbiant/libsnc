/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "items.hpp"

namespace snc::device::statics
{

Items_Base::Items_Base () = default;

Items_Base::~Items_Base () = default;

} // namespace snc::device::statics
