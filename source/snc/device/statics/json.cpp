/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "json.hpp"
#include <json/reader.h>
#include <json/value.h>
#include <sev/lag/math.hpp>
#include <sev/math/numbers.hpp>
#include <sev/string/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/math/stepping.hpp>
#include <snc/usb/device_query.hpp>
#include <functional>

namespace
{

// -- Forward declaration
class Private;

struct Json_Evaluator
{
  using Err_Reg =
      std::function< void ( std::string_view key_n, std::string_view text_n ) >;
  using Opt_Object =
      std::optional< std::reference_wrapper< const Json::Value > >;
  using Opt_Bool = std::optional< bool >;
  using Opt_Double = std::optional< double >;
  using Opt_String = std::optional< std::string >;

  Json_Evaluator ( std::string name_n,
                   const Json::Value & json_n,
                   Private * priv_n )
  : _priv ( priv_n )
  , _json ( json_n )
  , _name ( std::move ( name_n ) )
  {
  }

  const std::string &
  name () const
  {
    return _name;
  }

  void
  error ( std::string_view text_n ) const;

  private:
  bool
  require_match ( const Json::Value & value_n,
                  bool match_n,
                  std::string_view key_n,
                  std::string_view type_n ) const;

  public:
  Opt_Object
  require_object ( const char * key_n ) const;

  Opt_Object
  require_array ( const char * key_n ) const;

  Opt_Double
  require_double ( const char * key_n ) const;

  Opt_String
  require_string ( const char * key_n ) const;

  private:
  bool
  optional_match ( const Json::Value & value_n,
                   bool match_n,
                   std::string_view key_n,
                   std::string_view type_n ) const;

  public:
  Opt_Object
  optional_object ( const char * key_n ) const;

  Opt_Object
  optional_array ( const char * key_n ) const;

  Opt_Bool
  optional_bool ( const char * key_n ) const;

  Opt_Double
  optional_double ( const char * key_n ) const;

  Opt_String
  optional_string ( const char * key_n ) const;

  private:
  // -- Utility
  Private * _priv;
  const Json::Value & _json;
  std::string _name;
};

bool
Json_Evaluator::require_match ( const Json::Value & value_n,
                                bool match_n,
                                std::string_view key_n,
                                std::string_view type_n ) const
{
  if ( match_n ) {
    return true;
  }
  // Error messages
  if ( value_n.isNull () ) {
    error ( sev::string::cat (
        "Missing value \"", key_n, "\" of type ", type_n, "." ) );
  } else {
    error ( sev::string::cat ( key_n, " is not of type ", type_n, "." ) );
  }
  return false;
};

Json_Evaluator::Opt_Object
Json_Evaluator::require_object ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( require_match ( jval, jval.isObject (), key_n, "object" ) ) {
    return Opt_Object ( std::in_place, jval );
  }
  return {};
};

Json_Evaluator::Opt_Object
Json_Evaluator::require_array ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( require_match ( jval, jval.isArray (), key_n, "array" ) ) {
    return Opt_Object ( std::in_place, jval );
  }
  return {};
};

Json_Evaluator::Opt_Double
Json_Evaluator::require_double ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( require_match ( jval, jval.isDouble (), key_n, "double" ) ) {
    return Opt_Double ( std::in_place, jval.asDouble () );
  }
  return {};
};

Json_Evaluator::Opt_String
Json_Evaluator::require_string ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( require_match ( jval, jval.isString (), key_n, "double" ) ) {
    return Opt_String ( std::in_place, jval.asString () );
  }
  return {};
};

bool
Json_Evaluator::optional_match ( const Json::Value & value_n,
                                 bool match_n,
                                 std::string_view key_n,
                                 std::string_view type_n ) const
{
  if ( !match_n && !value_n.isNull () ) {
    error ( sev::string::cat ( key_n, " is not of type ", type_n, "." ) );
  }
  return match_n;
};

Json_Evaluator::Opt_Object
Json_Evaluator::optional_object ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( optional_match ( jval, jval.isObject (), key_n, "object" ) ) {
    return Opt_Object ( std::in_place, jval );
  }
  return {};
};

Json_Evaluator::Opt_Object
Json_Evaluator::optional_array ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( optional_match ( jval, jval.isArray (), key_n, "array" ) ) {
    return Opt_Object ( std::in_place, jval );
  }
  return {};
};

Json_Evaluator::Opt_Bool
Json_Evaluator::optional_bool ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( optional_match ( jval, jval.isBool (), key_n, "bool" ) ) {
    return Opt_Bool ( std::in_place, jval.asBool () );
  }
  return {};
};

Json_Evaluator::Opt_Double
Json_Evaluator::optional_double ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( optional_match ( jval, jval.isDouble (), key_n, "double" ) ) {
    return Opt_Double ( std::in_place, jval.asDouble () );
  }
  return {};
};

Json_Evaluator::Opt_String
Json_Evaluator::optional_string ( const char * key_n ) const
{
  const auto & jval = _json[ key_n ];
  if ( optional_match ( jval, jval.isString (), key_n, "string" ) ) {
    return Opt_String ( std::in_place, jval.asString () );
  }
  return {};
};

class Private
{
  public:
  // -- Construction

  Private ()
  : statics ( snc::device::statics::handle::make () )
  , emb_config ( std::make_shared< snc::emb::config::Config > () )
  {
    statics->set_emb_device_config ( emb_config );
  }

  // -- Interface

  bool
  parse ( std::string_view json_utf8_n );

  void
  register_error ( std::string err_n )
  {
    errors.emplace_back ( std::move ( err_n ) );
  }

  private:
  // -- Utility

  void
  parse_emb ( const Json::Value & emb_n );

  void
  parse_emb_usb_device ( const Json::Value & usb_dev_n );

  void
  parse_emb_sensors ( const Json::Value & sensors_n );

  void
  parse_emb_sensors_bool ( const Json::Value & sbool_n );

  void
  parse_emb_controls ( const Json::Value & controls_n );

  void
  parse_emb_controls_bool ( const Json::Value & cbool_n );

  void
  parse_emb_steppers ( const Json::Value & steppers_n );

  void
  parse_emb_pwm ( const Json::Value & pwm_n );

  void
  parse_emb_outputs ( const Json::Value & outputs_n );

  void
  parse_emb_output ( const Json::Value & output_n, std::size_t index_n );

  void
  parse_sensors ( const Json::Value & sensors_n );

  void
  parse_sensors_bool ( const Json::Value & sbool_n );

  void
  parse_sensor_bool ( const Json::Value & sensor_n, std::size_t index_n );

  void
  parse_controls ( const Json::Value & controls_n );

  void
  parse_controls_bool ( const Json::Value & cbool_n );

  void
  parse_control_bool ( const Json::Value & control_n, std::size_t index_n );

  void
  parse_axes ( const Json::Value & axes_n );

  void
  parse_axis ( const Json::Value & axis_n, std::size_t index_n );

  void
  finalize ();

  const Json_Evaluator
  make_je ( std::string name_n, const Json::Value & json_n )
  {
    return Json_Evaluator ( std::move ( name_n ), json_n, this );
  }

  // -- Attributes
  public:
  // - Objects
  snc::device::statics::handle::Statics_Writeable statics;
  std::shared_ptr< snc::emb::config::Config > emb_config;
  std::vector< std::string > errors;

  private:
  // - Json
  Json::Value json_root;
  Json::Reader json_reader;
};

bool
Private::parse ( std::string_view json_utf8_n )
{
  if ( json_utf8_n.empty () ) {
    register_error ( "Empty Json content" );
    return false;
  }

  if ( !json_reader.parse ( json_utf8_n.data (),
                            json_utf8_n.data () + json_utf8_n.size (),
                            json_root ) ) {
    register_error ( json_reader.getFormattedErrorMessages () );
    return false;
  }

  {
    auto je = make_je ( "root", json_root );
    if ( auto emb = je.require_object ( "emb" ) ) {
      parse_emb ( *emb );
    }
    if ( auto sensors = je.optional_object ( "sensors" ) ) {
      parse_sensors ( *sensors );
    }
    if ( auto controls = je.optional_object ( "controls" ) ) {
      parse_controls ( *controls );
    }
    if ( auto axes = je.optional_array ( "axes" ) ) {
      parse_axes ( *axes );
    }
  }
  finalize ();

  return errors.empty ();
}

void
Private::parse_emb ( const Json::Value & emb_n )
{
  auto je = make_je ( "emb", emb_n );

  if ( auto usb_dev = je.require_object ( "usb_device" ) ) {
    parse_emb_usb_device ( *usb_dev );
  }
  if ( auto sensors = je.require_object ( "sensors" ) ) {
    parse_emb_sensors ( *sensors );
  }
  if ( auto controls = je.require_object ( "controls" ) ) {
    parse_emb_controls ( *controls );
  }
  if ( auto steppers = je.require_array ( "steppers" ) ) {
    parse_emb_steppers ( *steppers );
  }
  if ( auto pwm = je.require_array ( "pwm" ) ) {
    parse_emb_pwm ( *pwm );
  }
  if ( auto outputs = je.require_array ( "outputs" ) ) {
    parse_emb_outputs ( *outputs );
  }
}

void
Private::parse_emb_usb_device ( const Json::Value & usb_dev_n )
{
  auto je = make_je ( "emb.usb_device", usb_dev_n );

  auto istring = je.require_object ( "iString" );
  if ( !istring ) {
    return;
  }

  // USB device query
  auto query = std::make_shared< snc::usb::Device_Query > ();

  // iString regular expressions
  {
    auto je_is = make_je ( "emb.usb_device.iString", *istring );
    if ( auto val = je_is.optional_string ( "Manufacturer" ) ) {
      query->manufacturer.emplace ( *val );
    }
    if ( auto val = je_is.optional_string ( "Product" ) ) {
      query->product.emplace ( *val );
    }
    if ( auto val = je_is.optional_string ( "SerialNumber" ) ) {
      query->serial_number.emplace ( *val );
    }
  }

  statics->set_usb_device_query ( std::move ( query ) );
}

void
Private::parse_emb_sensors ( const Json::Value & sensors_n )
{
  auto je = make_je ( "emb.sensors", sensors_n );
  if ( auto sbool = je.optional_array ( "bool" ) ) {
    parse_emb_sensors_bool ( *sbool );
  }
}

void
Private::parse_emb_sensors_bool ( const Json::Value & sbool_n )
{
  emb_config->sensors_bool ().resize ( sbool_n.size () );
}

void
Private::parse_emb_controls ( const Json::Value & controls_n )
{
  auto je = make_je ( "emb.controls", controls_n );
  if ( auto cbool = je.optional_array ( "bool" ) ) {
    parse_emb_controls_bool ( *cbool );
  }
}

void
Private::parse_emb_controls_bool ( const Json::Value & cbool_n )
{
  emb_config->controls_bool ().resize ( cbool_n.size () );
}

void
Private::parse_emb_steppers ( const Json::Value & steppers_n )
{
  emb_config->steppers ().resize ( steppers_n.size () );
}

void
Private::parse_emb_pwm ( const Json::Value & pwm_n )
{
  auto num = pwm_n.size ();
  emb_config->freq_gens ().resize ( num );
  for ( decltype ( num ) ii = 0; ii < num; ++ii ) {
    // PWM object
    const auto & pwm = pwm_n[ ii ];
    // Json evaluator
    auto je = make_je ( sev::string::cat ( "emb.pwm[", ii, "]" ), pwm );
    // PWM config
    auto & fgen = emb_config->freq_gens ()[ ii ];
    // Period length
    if ( auto val = je.optional_double ( "period" ) ) {
      fgen.set_pulse_usecs_total ( ( *val ) * ( 1000 * 1000 ) );
    }
    // High phase length
    if ( auto val = je.optional_double ( "high" ) ) {
      fgen.set_pulse_usecs_high ( ( *val ) * ( 1000 * 1000 ) );
    }
  }
}

void
Private::parse_emb_outputs ( const Json::Value & outputs_n )
{
  auto num = outputs_n.size ();
  for ( decltype ( num ) ii = 0; ii < num; ++ii ) {
    parse_emb_output ( outputs_n[ ii ], ii );
  }
}

void
Private::parse_emb_output ( const Json::Value & output_n, std::size_t index_n )
{
  auto je =
      make_je ( sev::string::cat ( "emb.outputs[", index_n, "]" ), output_n );

  // Output index
  std::size_t idx = index_n;
  if ( auto val = je.optional_double ( "index" ) ) {
    idx = *val;
  }

  // Get or create output config
  auto & out_cfg = emb_config->output_get_or_create ( idx );

  // Output type
  if ( auto tval = je.optional_string ( "type" ) ) {
    if ( *tval == "SWITCH_PLAIN" ) {
      out_cfg.set_type ( snc::emb::type::Output::SWITCH_PLAIN );
      // Control index
      if ( auto val = je.require_double ( "control" ) ) {
        out_cfg.set_control_index ( *val );
      }
    } else if ( *tval == "SWITCH_FREQUENCY" ) {
      out_cfg.set_type ( snc::emb::type::Output::SWITCH_FREQUENCY );
      // Control index
      if ( auto val = je.require_double ( "control" ) ) {
        out_cfg.set_control_index ( *val );
      }
      // PWM index
      if ( auto val = je.require_double ( "pwm" ) ) {
        out_cfg.set_freq_gen_index ( *val );
      }
    } else if ( *tval == "STEPPER_DIRECTION" ) {
      out_cfg.set_type ( snc::emb::type::Output::STEPPER_DIRECTION );
      // Stepper index
      if ( auto val = je.require_double ( "stepper" ) ) {
        out_cfg.set_stepper_index ( *val );
      }
    } else if ( *tval == "STEPPER_CLOCK" ) {
      out_cfg.set_type ( snc::emb::type::Output::STEPPER_CLOCK );
      // Stepper index
      if ( auto val = je.require_double ( "stepper" ) ) {
        out_cfg.set_stepper_index ( *val );
      }
    } else {
      // Bad type
      je.error ( sev::string::cat ( "Invalid type: \"", *tval, "\"." ) );
    }
  }
}

void
Private::parse_sensors ( const Json::Value & sensors_n )
{
  auto je = make_je ( "sensors", sensors_n );
  // Sensors: Bool
  if ( auto sbool = je.optional_array ( "bool" ) ) {
    parse_sensors_bool ( *sbool );
  }
}

void
Private::parse_sensors_bool ( const Json::Value & sbool_n )
{
  auto je = make_je ( "sensors.bool", sbool_n );
  auto num = sbool_n.size ();
  for ( decltype ( num ) ii = 0; ii < num; ++ii ) {
    parse_sensor_bool ( sbool_n[ ii ], ii );
  }
}

void
Private::parse_sensor_bool ( const Json::Value & sensor_n, std::size_t index_n )
{
  auto je =
      make_je ( sev::string::cat ( "sensors.bool[", index_n, "]" ), sensor_n );

  if ( !sensor_n.isObject () ) {
    je.error ( "Array entry is not an object" );
    return;
  }

  // Handle
  snc::device::statics::sensor::handle::I1_Writeable sensor_cfg;

  // Id and name
  if ( auto val = je.require_string ( "id" ) ) {
    std::string name;
    if ( auto nval = je.optional_string ( "name" ) ) {
      name = *nval;
    }
    sensor_cfg = statics->sensors_i1 ().create ( *val, name );
  }

  // Emb
  if ( auto emb = je.require_object ( "emb" ) ) {
    auto je_emb = make_je ( je.name () + ".emb", *emb );
    // Emb index
    if ( auto val = je_emb.require_double ( "sensor" ) ) {
      sensor_cfg->emb ().set_sensor_index ( *val );
    }
    // Emb inverted
    if ( auto val = je_emb.optional_bool ( "inverted" ) ) {
      sensor_cfg->emb ().set_inverted ( *val );
    }
  }
}

void
Private::parse_controls ( const Json::Value & controls_n )
{
  auto je = make_je ( "controls", controls_n );
  // Sensors: Bool
  if ( auto cbool = je.optional_array ( "bool" ) ) {
    parse_controls_bool ( *cbool );
  }
}

void
Private::parse_controls_bool ( const Json::Value & cbool_n )
{
  auto je = make_je ( "controls.bool", cbool_n );
  auto num = cbool_n.size ();
  for ( decltype ( num ) ii = 0; ii < num; ++ii ) {
    parse_control_bool ( cbool_n[ ii ], ii );
  }
}

void
Private::parse_control_bool ( const Json::Value & control_n,
                              std::size_t index_n )
{
  // Json evaluator
  auto je = make_je ( sev::string::cat ( "controls.bool[", index_n, "]" ),
                      control_n );

  if ( !control_n.isObject () ) {
    je.error ( "Array entry is not an object" );
    return;
  }

  // Handle
  snc::device::statics::control::handle::I1_Writeable control_cfg;

  // Id and name
  if ( auto val = je.require_string ( "id" ) ) {
    std::string name;
    if ( auto nval = je.optional_string ( "name" ) ) {
      name = *nval;
    }
    control_cfg = statics->controls_i1 ().create ( *val, name );
  }

  // User settable
  if ( auto val = je.optional_bool ( "user_settable" ) ) {
    control_cfg->set_user_settable ( *val );
  }

  // Warm up duration
  if ( auto val = je.optional_double ( "warm_up_duration" ) ) {
    control_cfg->set_warm_up_duration (
        std::chrono::duration_cast< std::chrono::nanoseconds > (
            std::chrono::duration< double > ( *val ) ) );
  }
  // Cool down duration
  if ( auto val = je.optional_double ( "cool_down_duration" ) ) {
    control_cfg->set_cool_down_duration (
        std::chrono::duration_cast< std::chrono::nanoseconds > (
            std::chrono::duration< double > ( *val ) ) );
  }

  // Emb
  if ( auto emb = je.require_object ( "emb" ) ) {
    auto je_emb = make_je ( je.name () + ".emb", *emb );
    // Emb index
    if ( auto val = je_emb.require_double ( "control" ) ) {
      control_cfg->emb ().set_control_index ( *val );
    }
    // Emb inverted
    if ( auto val = je_emb.optional_bool ( "inverted" ) ) {
      control_cfg->emb ().set_inverted ( *val );
    }
  }
}

void
Private::parse_axes ( const Json::Value & axes_n )
{
  auto num = axes_n.size ();
  for ( decltype ( num ) ii = 0; ii < num; ++ii ) {
    const auto & axis = axes_n[ ii ];
    if ( !axis.isObject () ) {
      continue;
    }
    parse_axis ( axis, ii );
  }
}

void
Private::parse_axis ( const Json::Value & axis_n, std::size_t index_n )
{
  // Json evaluator
  auto je = make_je ( sev::string::cat ( "axes[", index_n, "]" ), axis_n );

  // Handle
  snc::device::statics::handle::Axis_Writeable axis_cfg;

  // Id and name
  if ( auto val = je.require_string ( "id" ) ) {
    std::string name;
    if ( auto nval = je.optional_string ( "name" ) ) {
      name = *nval;
    }
    axis_cfg = statics->axes ().create ( *val, name );
  }

  // Emb
  if ( auto emb = je.require_object ( "emb" ) ) {
    auto je_emb = make_je ( je.name () + ".emb", *emb );
    // Emb stepper index
    if ( auto val = je_emb.require_double ( "stepper" ) ) {
      std::uint_fast32_t esidx = *val;
      axis_cfg->emb ().set_stepper_index ( esidx );
      // Acquire emb config reference
      if ( esidx < emb_config->steppers ().size () ) {
        axis_cfg->emb ().set_stepper (
            std::shared_ptr< const snc::emb::config::Stepper > (
                emb_config, &emb_config->steppers ()[ esidx ] ) );
      } else {
        je_emb.error ( "stepper index exceeds available emb stepper count." );
      }
    }
    // Emb reversed
    if ( auto val = je_emb.optional_bool ( "reversed" ) ) {
      axis_cfg->emb ().set_direction_reversed ( *val );
    }
  }

  // Geometry: rotational
  if ( auto val = je.optional_bool ( "rotational" ) ) {
    axis_cfg->geo ().set_rotational ( *val );
    if ( axis_cfg->geo ().rotational () ) {
      // Initialize length to two pi.
      axis_cfg->geo ().set_length ( sev::lag::two_pi_d );
    }
  }
  // Geometry: geo
  if ( auto geo = je.require_object ( "geo" ) ) {
    auto je_geo = make_je ( je.name () + ".geo", *geo );
    // Length
    if ( auto val = je_geo.optional_double ( "length" ) ) {
      axis_cfg->geo ().set_length ( *val * 1000.0 );
    }
    // Speed
    if ( auto val = je_geo.optional_double ( "speed_max" ) ) {
      axis_cfg->geo ().set_speed_max ( *val * 1000.0 );
    }
    // Acceleration
    if ( auto val = je_geo.optional_double ( "accel_max" ) ) {
      axis_cfg->geo ().set_accel_max ( *val * 1000.0 );
    }
  }

  // Stepper
  if ( auto stepper = je.require_object ( "stepper" ) ) {
    auto je_stepper = make_je ( je.name () + ".stepper", *stepper );
    double steps_full = 200.0;
    double steps_micro = 1.0;
    double lead = 0.0;
    // Full steps
    if ( auto val = je_stepper.require_double ( "full" ) ) {
      steps_full = *val;
    }
    // Micro steps
    if ( auto val = je_stepper.optional_double ( "micro" ) ) {
      steps_micro = *val;
    }
    // Lead (length per axis revolution)
    if ( axis_cfg->geo ().rotational () ) {
      // On rotational axes initialize lead to full rotation.
      lead = sev::lag::two_pi_d;
      if ( auto val = je_stepper.optional_double ( "lead" ) ) {
        lead = *val;
      }
    } else {
      // Linear axes require lead information
      if ( auto val = je_stepper.require_double ( "lead" ) ) {
        lead = ( *val * 1000.0 );
      }
    }

    double steps_per_rev = ( steps_full * steps_micro );
    axis_cfg->geo ().set_length_per_step ( lead / steps_per_rev );
  }

  // Derived geometry and stepping values
  {
    // Speeds
    axis_cfg->geo ().set_speed_stoppable ( snc::speed_accelerate_over_length (
        axis_cfg->geo ().accel_max (), axis_cfg->geo ().length_per_step () ) );
    axis_cfg->geo ().set_speed_reversible ( snc::speed_accelerate_over_length (
        axis_cfg->geo ().accel_max (),
        axis_cfg->geo ().length_per_step () / 2.0 ) );
    axis_cfg->geo ().set_speed_min (
        std::min ( axis_cfg->geo ().speed_reversible () / 4.0, 0.01 ) );

    // Stepping
    if ( axis_cfg->geo ().length () != 0.0 ) {
      axis_cfg->set_step_index_min ( 0 );
      axis_cfg->set_step_index_max ( std::floor (
          axis_cfg->geo ().length () / axis_cfg->geo ().length_per_step () ) );
    } else {
      axis_cfg->set_step_index_min ( INT64_MIN );
      axis_cfg->set_step_index_max ( INT64_MAX );
    }
  }

  // Align
  if ( auto align = je.optional_object ( "align" ) ) {
    auto je_align = make_je ( je.name () + ".align", *align );
    // Alignable
    if ( auto val = je_align.optional_bool ( "alignable" ) ) {
      axis_cfg->align ().set_alignable ( *val );
    }
    // Bottom end sensor
    if ( auto val = je_align.optional_string ( "bottom_end_sensor" ) ) {
      if ( auto handle = statics->sensors_i1 ().get_by_key ( *val ) ) {
        axis_cfg->sensors ().set_low_end ( std::move ( handle ) );
      } else {
        je_align.error ( sev::string::cat ( "Unknown sensor ", *val ) );
      }
    }
    // Top end sensor
    if ( auto val = je_align.optional_string ( "top_end_sensor" ) ) {
      if ( auto handle = statics->sensors_i1 ().get_by_key ( *val ) ) {
        axis_cfg->sensors ().set_high_end ( std::move ( handle ) );
      } else {
        je_align.error ( sev::string::cat ( "Unknown sensor ", *val ) );
      }
    }
  }

  // Manual
  if ( auto manual = je.optional_object ( "manual" ) ) {
    auto je_manual = make_je ( je.name () + ".manual", *manual );
    // Movable
    if ( auto val = je_manual.optional_bool ( "movable" ) ) {
      axis_cfg->manual ().set_movable ( *val );
    }
    // Alignment required
    if ( auto val = je_manual.optional_bool ( "align_required" ) ) {
      axis_cfg->manual ().set_alignment_required ( *val );
    }
  }
}

void
Private::finalize ()
{
  const auto & axes = statics->axes ();

  // Speed total
  {
    double smax = 0.0;
    for ( std::size_t ii = 0; ii != 3; ++ii ) {
      smax += sev::lag::square ( axes.get ( ii )->geo ().speed_max () );
    }
    smax = std::sqrt ( smax );

    statics->set_speed_max ( smax );
  }
  // Speed planar
  {
    double smin = axes.get ( 0 )->geo ().speed_min ();
    sev::math::assign_smaller< double > ( smin,
                                          axes.get ( 1 )->geo ().speed_min () );

    double smax = 0.0;
    smax += sev::lag::square ( axes.get ( 0 )->geo ().speed_max () );
    smax += sev::lag::square ( axes.get ( 1 )->geo ().speed_max () );
    smax = std::sqrt ( smax );

    statics->set_speed_planar_max ( smax );
  }
  // Speed normal
  {
    statics->set_speed_normal_max ( axes.get ( 2 )->geo ().speed_max () );
  }
}

void
Json_Evaluator::error ( std::string_view text_n ) const
{
  _priv->register_error ( sev::string::cat ( _name, ": ", text_n ) );
}

} // namespace

namespace snc::device::statics::json
{

Reader::Reader () = default;

Reader::~Reader () = default;

void
Reader::reset ()
{
  _statics.reset ();
  _errors.clear ();
}

std::string
Reader::error_string () const
{
  std::string res;
  std::string_view sep = "";
  for ( const auto & err : _errors ) {
    res += sep;
    res += err;
    sep = "\n";
  }
  return res;
}

bool
Reader::parse ( std::string_view json_utf8_n )
{
  reset ();
  {
    auto impl = std::make_unique< Private > ();
    if ( impl->parse ( json_utf8_n ) ) {
      // Success
      _statics = impl->statics;
    } else {
      // Get errors
      _errors.swap ( impl->errors );
    }
  }
  return good ();
}

} // namespace snc::device::statics::json
