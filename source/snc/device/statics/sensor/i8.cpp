/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i8.hpp"

namespace snc::device::statics::sensor
{

I8::I8 ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

I8::~I8 () = default;

} // namespace snc::device::statics::sensor
