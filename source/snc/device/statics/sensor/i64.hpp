/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <cstdint>

namespace snc::device::statics::sensor
{

/// @brief 64 bit sensor
///
class I64 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Sensor index

    std::uint_fast32_t
    sensor_index () const
    {
      return _sensor_index;
    }

    void
    set_sensor_index ( std::uint_fast32_t index_n )
    {
      _sensor_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _sensor_index = Item::invalid_index_fast32;
  };

  // -- Construction and setup

  I64 ( std::uint_fast32_t index_n = 0 );

  ~I64 ();

  // -- Default state

  std::uint64_t
  default_state () const
  {
    return _default_state;
  }

  void
  set_default_state ( std::uint64_t state_n )
  {
    _default_state = state_n;
  }

  // -- Embedded device sensor

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  private:
  // -- Attributes
  std::uint64_t _default_state = 0;
  Emb _emb;
};

} // namespace snc::device::statics::sensor
