/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i1.hpp"

namespace snc::device::statics::sensor
{

I1::I1 ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

I1::~I1 () = default;

} // namespace snc::device::statics::sensor
