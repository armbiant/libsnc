/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <cstdint>

namespace snc::device::statics::sensor
{

/// @brief Boolean sensor
///
class I1 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Sensor index

    std::uint_fast32_t
    sensor_index () const
    {
      return _sensor_index;
    }

    void
    set_sensor_index ( std::uint_fast32_t index_n )
    {
      _sensor_index = index_n;
    }

    // -- Inverted

    bool
    inverted () const
    {
      return _inverted;
    }

    void
    set_inverted ( bool flag_n )
    {
      _inverted = flag_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _sensor_index = Item::invalid_index_fast32;
    bool _inverted = false;
  };

  // -- Construction and setup

  I1 ( std::uint_fast32_t index_n = 0 );

  ~I1 ();

  // -- Default state

  bool
  default_state () const
  {
    return _default_state;
  }

  void
  set_default_state ( bool state_n )
  {
    _default_state = state_n;
  }

  // -- Embedded device sensor

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  private:
  // -- Attributes
  bool _default_state = false;
  Emb _emb;
};

} // namespace snc::device::statics::sensor
