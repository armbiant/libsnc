/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/math/exponents.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/statics/item.hpp>
#include <cmath>
#include <cstdint>
#include <memory>
#include <string>

// -- Forward declaration
namespace snc::emb::config
{
class Stepper;
}

namespace snc::device::statics
{

namespace axis
{

/// @brief Axis emb-device settings
///
class Emb
{
  public:
  // -- Types

  using Emb_Config_Handle = std::shared_ptr< const snc::emb::config::Stepper >;

  // -- Embedded axis index

  std::uint_fast32_t
  stepper_index () const
  {
    return _stepper_index;
  }

  void
  set_stepper_index ( std::uint_fast32_t index_n )
  {
    _stepper_index = index_n;
  }

  // -- Emb stepper config

  const Emb_Config_Handle &
  stepper () const
  {
    return _stepper;
  }

  void
  set_stepper ( Emb_Config_Handle stepper_n )
  {
    _stepper = std::move ( stepper_n );
  }

  // -- Step direction

  /// @brief Inverts the axis movement direction
  bool
  direction_reversed () const
  {
    return _direction_reversed;
  }

  void
  set_direction_reversed ( bool flag_n )
  {
    _direction_reversed = flag_n;
  }

  private:
  // -- Attributes
  std::uint_fast32_t _stepper_index = Item::invalid_index_fast32;
  Emb_Config_Handle _stepper;
  bool _direction_reversed = false;
};

/// @brief Axis geometry
///
class Geo
{
  public:
  // -- Rotational

  bool
  linear () const
  {
    return !_rotational;
  }

  bool
  rotational () const
  {
    return _rotational;
  }

  void
  set_rotational ( bool value_n )
  {
    _rotational = value_n;
  }

  // -- Length

  double
  length () const
  {
    return _length;
  }

  void
  set_length ( double value_n )
  {
    _length = value_n;
  }

  // -- Size: Length per step

  double
  length_per_step () const
  {
    return _length_per_step;
  }

  void
  set_length_per_step ( double value_n )
  {
    _length_per_step = value_n;
  }

  // -- Compute: Length <-> steps

  double
  length_from_steps ( double steps_n ) const
  {
    return _length_per_step * steps_n;
  }

  double
  steps_from_length ( double length_n ) const
  {
    return length_n / _length_per_step;
  }

  // -- Speed: Maximum

  double
  speed_max () const
  {
    return _speed_max;
  }

  void
  set_speed_max ( double value_n )
  {
    _speed_max = value_n;
  }

  // -- Speed: Minimum

  double
  speed_min () const
  {
    return _speed_min;
  }

  void
  set_speed_min ( double value_n )
  {
    _speed_min = value_n;
  }

  // -- Speed: Stoppable

  double
  speed_stoppable () const
  {
    return _speed_stoppable;
  }

  void
  set_speed_stoppable ( double value_n )
  {
    _speed_stoppable = value_n;
  }

  // -- Speed: Direction reversible

  double
  speed_reversible () const
  {
    return _speed_reversible;
  }

  void
  set_speed_reversible ( double value_n )
  {
    _speed_reversible = value_n;
  }

  // -- Acceleration: Maximum

  double
  accel_max () const
  {
    return _accel_max;
  }

  void
  set_accel_max ( double value_n )
  {
    _accel_max = value_n;
  }

  /// @brief Step micro seconds at a speed
  double
  step_usecs_at_speed ( double speed_length_s_n ) const
  {
    return ( _length_per_step / speed_length_s_n ) * sev::math::mega_d;
  }

  /// @brief Speed for step micro seconds
  double
  speed_at_step_usecs ( double step_usecs_n ) const
  {
    return ( _length_per_step / step_usecs_n ) * sev::math::mega_d;
  }

  private:
  // -- Attributes
  bool _rotational = false;
  // - Length
  double _length = 0.0;
  double _length_per_step = 0.0;
  // - Speed
  double _speed_max = 0.0;
  double _speed_min = 0.0;
  double _speed_stoppable = 0.0;
  double _speed_reversible = 0.0;
  // - Acceleration
  double _accel_max = 0.0;
};

/// @brief Axis sensors
///
class Sensors
{
  public:
  // -- Low end

  const sensor::handle::I1 &
  low_end () const
  {
    return _low_end;
  }

  void
  set_low_end ( sensor::handle::I1 sensor_n )
  {
    _low_end = std::move ( sensor_n );
  }

  // -- High end

  const sensor::handle::I1 &
  high_end () const
  {
    return _high_end;
  }

  void
  set_high_end ( sensor::handle::I1 sensor_n )
  {
    _high_end = std::move ( sensor_n );
  }

  private:
  // -- Attributes
  sensor::handle::I1 _low_end;
  sensor::handle::I1 _high_end;
};

/// @brief Axis aligning
///
class Align
{
  public:
  // -- Alignable

  bool
  alignable () const
  {
    return _alignable;
  }

  void
  set_alignable ( bool flag_n )
  {
    _alignable = flag_n;
  }

  private:
  // -- Attributes
  bool _alignable = false;
};

/// @brief Axis manual control
///
class Manual
{
  public:
  // -- Movable

  bool
  movable () const
  {
    return _movable;
  }

  void
  set_movable ( bool flag_n )
  {
    _movable = flag_n;
  }

  // -- Alignment required

  bool
  alignment_required () const
  {
    return _alignment_required;
  }

  void
  set_alignment_required ( bool flag_n )
  {
    _alignment_required = flag_n;
  }

  private:
  // -- Attributes
  bool _movable = false;
  bool _alignment_required = false;
};

} // namespace axis

/// @brief Axis static settings
///
class Axis : public Item
{
  public:
  // -- Construction

  Axis ( std::uint_fast32_t index_n = 0 );

  ~Axis ();

  // -- Length

  std::int_fast64_t
  step_index_min () const
  {
    return _step_index_min;
  }

  void
  set_step_index_min ( std::int_fast64_t value_n )
  {
    _step_index_min = value_n;
  }

  std::int_fast64_t
  step_index_max () const
  {
    return _step_index_max;
  }

  void
  set_step_index_max ( std::int_fast64_t value_n )
  {
    _step_index_max = value_n;
  }

  std::uint_fast64_t
  num_steps_total () const
  {
    return ( step_index_max () - step_index_min () );
  }

  // -- Embedded device

  axis::Emb &
  emb ()
  {
    return _emb;
  }

  const axis::Emb &
  emb () const
  {
    return _emb;
  }

  // -- Geometry

  axis::Geo &
  geo ()
  {
    return _geo;
  }

  const axis::Geo &
  geo () const
  {
    return _geo;
  }

  // -- Sensors

  axis::Sensors &
  sensors ()
  {
    return _sensors;
  }

  const axis::Sensors &
  sensors () const
  {
    return _sensors;
  }

  // -- Alignment

  axis::Align &
  align ()
  {
    return _align;
  }

  const axis::Align &
  align () const
  {
    return _align;
  }

  // -- Manual

  axis::Manual &
  manual ()
  {
    return _manual;
  }

  const axis::Manual &
  manual () const
  {
    return _manual;
  }

  private:
  // -- Attributes
  axis::Emb _emb;
  axis::Geo _geo;
  axis::Sensors _sensors;
  axis::Align _align;
  axis::Manual _manual;

  std::int_fast64_t _step_index_min = 0;
  std::int_fast64_t _step_index_max = 0;
};

} // namespace snc::device::statics
