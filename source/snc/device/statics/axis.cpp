/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "axis.hpp"
#include <sev/math/exponents.hpp>

namespace snc::device::statics
{

namespace axis
{
} // namespace axis

Axis::Axis ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

Axis::~Axis () = default;

} // namespace snc::device::statics
