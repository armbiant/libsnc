/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstddef>
#include <memory>
#include <string_view>
#include <vector>

namespace snc::device::statics
{

/** @brief Abstract items base model
 */
class Items_Base
{
  public:
  // -- Construction

  Items_Base ();

  ~Items_Base ();
};

/** @brief Templated items base model
 */
template < typename T >
class Items : public Items_Base
{
  // -- Types
  public:
  using Item = T;
  using Item_Handle = std::shared_ptr< const Item >;
  using Item_Handle_Writeable = std::shared_ptr< Item >;

  // -- Construction

  Items ();

  ~Items ();

  // -- Setup

  void
  clear ();

  // -- List

  std::size_t
  size () const
  {
    return _items.size ();
  }

  const auto &
  items () const
  {
    return _items;
  }

  // -- Item access

  const Item_Handle &
  get ( std::size_t index_n ) const
  {
    return _items[ index_n ];
  }

  Item_Handle
  get_by_key ( std::string_view key_n ) const;

  Item_Handle
  get_by_name ( std::string_view name_n ) const;

  // -- Appending

  Item_Handle_Writeable
  create ( std::string_view key_n, std::string_view name_n );

  private:
  // -- Attributes
  std::vector< Item_Handle > _items;
};

template < typename T >
Items< T >::Items ()
{
}

template < typename T >
Items< T >::~Items () = default;

template < typename T >
void
Items< T >::clear ()
{
  _items.clear ();
}

template < typename T >
typename Items< T >::Item_Handle
Items< T >::get_by_key ( std::string_view key_n ) const
{
  for ( const auto & item : _items ) {
    if ( item->key () == key_n ) {
      return item;
    }
  }
  return Item_Handle ();
}

template < typename T >
typename Items< T >::Item_Handle
Items< T >::get_by_name ( std::string_view name_n ) const
{
  for ( const auto & item : _items ) {
    if ( item->name () == name_n ) {
      return item;
    }
  }
  return Item_Handle ();
}

template < typename T >
typename Items< T >::Item_Handle_Writeable
Items< T >::create ( std::string_view key_n, std::string_view name_n )
{
  auto item = std::make_shared< Item > ( _items.size () );
  item->set_key ( key_n );
  item->set_name ( name_n );
  _items.push_back ( item );
  return item;
}

} // namespace snc::device::statics
