/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "statics.hpp"
#include <sev/math/numbers.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/emb/config/stepper.hpp>

namespace snc::device::statics
{

static constexpr std::uint_fast32_t step_slice_usecs_default = 30000;

Statics::Statics () = default;

Statics::~Statics () = default;

std::uint_fast32_t
Statics::acquire_step_slice_usecs_min () const
{
  std::uint_fast32_t usecs = 0;
  for ( auto const & axis : axes ().items () ) {
    auto & stepper = axis->emb ().stepper ();
    if ( stepper ) {
      sev::math::assign_larger< std::uint_fast32_t > (
          usecs, stepper->step_slice_usecs_min () );
    }
  }
  if ( usecs == 0 ) {
    usecs = step_slice_usecs_default;
  }
  return usecs;
}

std::uint_fast32_t
Statics::acquire_step_slice_usecs_max () const
{
  std::uint_fast32_t usecs = 0;
  bool value_valid ( false );
  for ( auto const & axis : axes ().items () ) {
    std::uint_fast32_t axis_usecs =
        axis->emb ().stepper ()->step_slice_usecs_min ();
    if ( !value_valid ) {
      usecs = axis_usecs;
      value_valid = true;
    } else {
      sev::math::assign_smaller< std::uint_fast32_t > ( usecs, axis_usecs );
    }
  }
  if ( !value_valid ) {
    usecs = step_slice_usecs_default;
  }
  return usecs;
}

} // namespace snc::device::statics
