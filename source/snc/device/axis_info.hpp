/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>

namespace snc::device
{

/** @brief Axis statics and state pair */
class Axis_Info
{
  public:
  // -- Construction

  Axis_Info ();

  Axis_Info ( const Axis_Info & axis_n );

  Axis_Info ( Axis_Info && axis_n );

  Axis_Info ( statics::handle::Axis statics_n, state::handle::Axis state_n );

  ~Axis_Info ();

  // -- Assignment operators

  Axis_Info &
  operator= ( const Axis_Info & axis_n );

  Axis_Info &
  operator= ( Axis_Info && axis_n );

  // -- Cast operators

  operator bool () const { return ( _statics && _state ); };

  // -- Setup

  void
  reset ();

  void
  reset ( statics::handle::Axis statics_n, state::handle::Axis state_n );

  // -- Accessors

  const auto &
  statics () const
  {
    return _statics;
  }

  const auto &
  state () const
  {
    return _state;
  }

  // -- Interface

  bool
  is_unaligned () const;

  bool
  is_manually_movable () const;

  private:
  statics::handle::Axis _statics;
  state::handle::Axis _state;
};

} // namespace snc::device
