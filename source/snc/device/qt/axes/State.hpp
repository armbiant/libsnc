/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <QObject>

namespace snc::device::qt::axes
{

/*** Qt wrapper class */
class State : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( double aligned READ aligned NOTIFY alignedChanged )
  Q_PROPERTY ( double position READ position NOTIFY positionChanged )
  Q_PROPERTY ( double radians READ radians NOTIFY positionChanged )
  Q_PROPERTY ( double degrees READ degrees NOTIFY positionChanged )
  Q_PROPERTY ( double speed READ speed NOTIFY speedChanged )
  Q_PROPERTY ( double speedRoundsPerMinute READ speedRoundsPerMinute NOTIFY
                   speedChanged )

  public:
  // -- Construction

  State ( QObject * qparent_n, int index_n );

  ~State ();

  // -- Index

  int
  index ()
  {
    return _index;
  }

  // -- Alignment

  bool
  aligned () const
  {
    return _is_aligned;
  }

  Q_SIGNAL
  void
  alignedChanged ();

  // -- Position

  double
  position () const
  {
    return _position;
  }

  double
  radians () const;

  double
  degrees () const;

  Q_SIGNAL
  void
  positionChanged ();

  // -- Speed

  double
  speed () const
  {
    return _speed;
  }

  Q_SIGNAL
  void
  speedChanged ();

  double
  speedRoundsPerMinute () const;

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  int _index = 0;
  bool _is_aligned = false;
  double _position = 0.0;
  double _speed = 0.0;
  snc::device::state::handle::Axis _source;
};

} // namespace snc::device::qt::axes
