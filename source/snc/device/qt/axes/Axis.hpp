/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/qt/axes/State.hpp>
#include <QObject>

namespace snc::device::qt::axes
{

/*** Qt wrapper class */
class Axis : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( QObject * state READ state CONSTANT )

  Q_PROPERTY ( bool valid READ isValid NOTIFY isValidChanged () )
  Q_PROPERTY ( QString key READ key NOTIFY keyChanged )
  Q_PROPERTY ( QString name READ name NOTIFY nameChanged )
  Q_PROPERTY ( bool isLinear READ isLinear NOTIFY geometryChanged )
  Q_PROPERTY ( bool isRotational READ isRotational NOTIFY geometryChanged )
  Q_PROPERTY ( double length READ length NOTIFY lengthChanged )
  Q_PROPERTY (
      double lengthPerStep READ lengthPerStep NOTIFY lengthPerStepChanged )
  Q_PROPERTY ( double speedMin READ speedMin NOTIFY speedMinChanged )
  Q_PROPERTY ( double speedMax READ speedMax NOTIFY speedMaxChanged )
  Q_PROPERTY ( double speedMaxRoundsPerMinute READ speedMaxRoundsPerMinute
                   NOTIFY speedMaxChanged )
  Q_PROPERTY ( double accelMax READ accelMax NOTIFY accelMaxChanged )

  public:
  // -- Construction

  Axis ( QObject * qparent_n, int index_n );

  ~Axis ();

  // -- Index

  int
  index ()
  {
    return _index;
  }

  // -- Valid

  bool
  isValid () const
  {
    return _statics.operator bool ();
  }

  Q_SIGNAL
  void
  isValidChanged ();

  // -- Key

  const QString &
  key () const
  {
    return _key;
  }

  Q_SIGNAL
  void
  keyChanged ();

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  Q_SIGNAL
  void
  nameChanged ();

  // -- Geometry

  bool
  isLinear () const
  {
    return _isLinear;
  }

  bool
  isRotational () const
  {
    return _isRotational;
  }

  Q_SIGNAL
  void
  geometryChanged ();

  // -- Length

  double
  length () const
  {
    return _length;
  }

  Q_SIGNAL
  void
  lengthChanged ();

  // -- Step

  double
  lengthPerStep () const
  {
    return _lengthPerStep;
  }

  Q_SIGNAL
  void
  lengthPerStepChanged ();

  // -- Speed: Minimum

  double
  speedMin () const
  {
    return _speedMin;
  }

  Q_SIGNAL
  void
  speedMinChanged ();

  // -- Speed: Maximum

  double
  speedMax () const
  {
    return _speedMax;
  }

  double
  speedMaxRoundsPerMinute () const;

  Q_SIGNAL
  void
  speedMaxChanged ();

  // -- Acceleration: Maximum

  double
  accelMax () const
  {
    return _accelMax;
  }

  Q_SIGNAL
  void
  accelMaxChanged ();

  // -- State

  auto *
  state ()
  {
    return &_state;
  }

  const auto *
  state () const
  {
    return &_state;
  }

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  int _index = 0;
  QString _key;
  QString _name;
  bool _isLinear = false;
  bool _isRotational = false;
  double _length = 0.0;
  double _lengthPerStep = 0.0;
  double _speedMin = 0.0;
  double _speedMax = 0.0;
  double _accelMax = 0.0;
  snc::device::statics::handle::Axis _statics;
  snc::device::qt::axes::State _state;
};

} // namespace snc::device::qt::axes
