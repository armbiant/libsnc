/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/device/qt/sensors/I1.hpp>
#include <QObject>

namespace snc::device::qt
{

/*** Qt wrapper class */
class Sensors : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( snc::device::qt::sensors::I1 * i1 READ i1 CONSTANT )

  public:
  // -- Construction

  Sensors ( QObject * qparent_n, snc::device::Info & info_n );

  ~Sensors ();

  // -- Boolean

  snc::device::qt::sensors::I1 *
  i1 ()
  {
    return &_i1;
  }

  // -- Interface

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  private:
  // -- Attributes
  snc::device::qt::sensors::I1 _i1;
};

} // namespace snc::device::qt
