/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <QModelIndex>
#include <memory>
#include <vector>

namespace snc::device::qt
{

template < typename T >
class ItemList
{
  public:
  // -- Types

  using Handle = std::shared_ptr< T >;
  using List = std::vector< Handle >;

  // -- Construction

  ItemList ( QObject * parent_n );

  ~ItemList ();

  // -- Valid items

  std::size_t
  count_valid () const
  {
    return _valid.size ();
  }

  auto &
  valid ()
  {
    return _valid;
  }

  const auto &
  valid () const
  {
    return _valid;
  }

  // -- Available items

  std::size_t
  count_available () const
  {
    return _available.size ();
  }

  auto &
  available ()
  {
    return _available;
  }

  const auto &
  available () const
  {
    return _available;
  }

  // -- Search

  Handle
  getOrCreateShared ( std::size_t index_n );

  QObject *
  getOrCreate ( int index_n );

  QObject *
  getByKey ( const QString & key_n );

  QObject *
  getByName ( const QString & name_n );

  // -- Model indices

  bool
  validParentIndex ( const QModelIndex & index_n ) const
  {
    return !index_n.isValid ();
  }

  bool
  validIndex ( const QModelIndex & index_n ) const
  {
    return index_n.isValid () && //
           ( std::size_t ( index_n.row () ) < _valid.size () ) &&
           validParentIndex ( index_n.parent () );
  }

  void
  create_valid ( std::size_t size_n );

  void
  remove_valid ( std::size_t size_n );

  void
  ensure_size_available ( std::size_t size_n );

  private:
  QObject * _parent = nullptr;
  List _valid;
  List _available;
};

} // namespace snc::device::qt
