/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/device/qt/Axes.hpp>
#include <snc/device/qt/Controls.hpp>
#include <snc/device/qt/Sensors.hpp>
#include <snc/device/qt/State.hpp>
#include <snc/device/qt/Statics.hpp>
#include <QObject>

namespace snc::device::qt
{

/*** Qt wrapper info wrapper class */
class Info : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( snc::device::qt::Statics * statics READ statics CONSTANT )
  Q_PROPERTY ( snc::device::qt::State * state READ state CONSTANT )
  Q_PROPERTY ( snc::device::qt::Sensors * sensors READ sensors CONSTANT )
  Q_PROPERTY ( snc::device::qt::Controls * controls READ controls CONSTANT )
  Q_PROPERTY ( snc::device::qt::Axes * axes READ axes CONSTANT )

  public:
  // -- Construction

  Info ( QObject * qparent_n );

  ~Info ();

  // -- Plain

  const snc::device::Info &
  info ()
  {
    return _info;
  }

  // -- Update

  void
  update_info ( const snc::device::Info & info_n );

  Info &
  operator= ( const snc::device::Info & info_n )
  {
    update_info ( info_n );
    return *this;
  }

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  // -- Statics

  auto *
  statics ()
  {
    return &_statics;
  }

  const auto *
  statics () const
  {
    return &_statics;
  }

  // -- State

  auto *
  state ()
  {
    return &_state;
  }

  const auto *
  state () const
  {
    return &_state;
  }

  // -- Sensors

  auto *
  sensors ()
  {
    return &_sensors;
  }

  const auto *
  sensors () const
  {
    return &_sensors;
  }

  // -- Controls

  auto *
  controls ()
  {
    return &_controls;
  }

  const auto *
  controls () const
  {
    return &_controls;
  }

  // -- Controls

  auto *
  axes ()
  {
    return &_axes;
  }

  const auto *
  axes () const
  {
    return &_axes;
  }

  private:
  // -- Attributes
  snc::device::Info _info;
  Statics _statics;
  State _state;
  Sensors _sensors;
  Controls _controls;
  Axes _axes;
};

} // namespace snc::device::qt
