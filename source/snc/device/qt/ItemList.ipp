/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "ItemList.hpp"
#include <stdexcept>

namespace snc::device::qt
{

template < typename T >
ItemList< T >::ItemList ( QObject * parent_n )
: _parent ( parent_n )
{
}

template < typename T >
ItemList< T >::~ItemList () = default;

template < typename T >
typename ItemList< T >::Handle
ItemList< T >::getOrCreateShared ( std::size_t index_n )
{
  ensure_size_available ( index_n + 1 );
  return _available.at ( index_n );
}

template < typename T >
QObject *
ItemList< T >::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    throw std::runtime_error ( "Negative index." );
  }
  ensure_size_available ( index_n + 1 );
  return _available.at ( index_n ).get ();
}

template < typename T >
QObject *
ItemList< T >::getByKey ( const QString & key_n )
{
  for ( auto & item : _available ) {
    if ( item->key () == key_n ) {
      return item.get ();
    }
  }
  return nullptr;
}

template < typename T >
QObject *
ItemList< T >::getByName ( const QString & name_n )
{
  for ( auto & item : _available ) {
    if ( item->name () == name_n ) {
      return item.get ();
    }
  }
  return nullptr;
}

template < typename T >
void
ItemList< T >::create_valid ( std::size_t delta_n )
{
  auto valid_size_new = _valid.size () + delta_n;
  ensure_size_available ( valid_size_new );
  for ( std::size_t ii = 0; ii != delta_n; ++ii ) {
    _valid.push_back ( _available[ _valid.size () ] );
  }
}

template < typename T >
void
ItemList< T >::remove_valid ( std::size_t delta_n )
{
  _valid.resize ( _valid.size () - delta_n );
}

template < typename T >
void
ItemList< T >::ensure_size_available ( std::size_t size_n )
{
  while ( _available.size () < size_n ) {
    std::size_t index = _available.size ();
    _available.push_back ( std::make_shared< T > ( _parent, index ) );
  }
}

} // namespace snc::device::qt
