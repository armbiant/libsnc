/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Statics.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::qt
{

Statics::Statics ( QObject * qparent_n, snc::device::Info & info_n )
: QObject ( qparent_n )
, _info ( info_n )
{
}

Statics::~Statics () = default;

void
Statics::update_statics ()
{
  // Update values
  if ( sev::change ( _speedMax, _info.statics ()->speed_max () ) ) {
    emit speedMaxChanged ();
  }
  if ( sev::change ( _speedPlanarMax,
                     _info.statics ()->speed_planar_max () ) ) {
    emit speedPlanarMaxChanged ();
  }
  if ( sev::change ( _speedNormalMax,
                     _info.statics ()->speed_normal_max () ) ) {
    emit speedNormalMaxChanged ();
  }
}

} // namespace snc::device::qt
