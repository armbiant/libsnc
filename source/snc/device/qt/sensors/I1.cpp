/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I1.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/info.hpp>
#include <snc/device/qt/ItemList.ipp>
#include <snc/device/statics/statics.hpp>
#include <stdexcept>

// -- Instantiation
template class snc::device::qt::ItemList<
    snc::device::qt::sensors::i1::Sensor >;

namespace snc::device::qt::sensors
{

I1::I1 ( QObject * qparent_n, snc::device::Info & info_n )
: QAbstractListModel ( qparent_n )
, _info ( info_n )
, _items ( this )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::listItem, "listItem" );
}

I1::~I1 () = default;

std::shared_ptr< const i1::Sensor >
I1::getOrCreateShared ( std::size_t index_n )
{
  _items.ensure_size_available ( index_n + 1 );
  return _items.available ().at ( index_n );
}

QObject *
I1::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    throw std::runtime_error ( "Negative index." );
  }
  _items.ensure_size_available ( index_n + 1 );
  return _items.available ().at ( index_n ).get ();
}

QObject *
I1::getByKey ( const QString & key_n )
{
  return _items.getByKey ( key_n );
}

QObject *
I1::getByName ( const QString & name_n )
{
  return _items.getByName ( name_n );
}

QHash< int, QByteArray >
I1::roleNames () const
{
  return _roleNames;
}

int
I1::rowCount ( const QModelIndex & parent_n ) const
{
  return _items.validParentIndex ( parent_n ) ? _items.count_valid () : 0;
}

QVariant
I1::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( _items.validIndex ( index_n ) ) {
    switch ( role_n ) {
    case ExtraRole::listItem:
      return QVariant::fromValue (
          _items.valid ().at ( index_n.row () ).get () );
    default:
      break;
    }
  }
  return QVariant ();
}

void
I1::update_statics ()
{
  const std::size_t num_old = _items.count_valid ();
  const std::size_t num_new = _info.statics ()->sensors_i1 ().size ();
  if ( num_old != num_new ) {
    if ( num_old < num_new ) {
      // Add items
      beginInsertRows ( QModelIndex (), num_old, num_new - 1 );
      _items.create_valid ( num_new - num_old );
      endInsertRows ();
    } else {
      // Remove items
      beginRemoveRows ( QModelIndex (), num_new, num_old - 1 );
      _items.remove_valid ( num_old - num_new );
      endRemoveRows ();
    }
    emit countChanged ();
  }

  // Update statics of all items
  for ( auto & item : _items.available () ) {
    item->update_statics ( _info );
  }
}

void
I1::update_valid_properties ()
{
  for ( auto & sensor : _items.valid () ) {
    sensor->update_properties ();
  }
}

void
I1::update_available_properties ()
{
  for ( auto & sensor : _items.available () ) {
    sensor->update_properties ();
  }
}

} // namespace snc::device::qt::sensors
