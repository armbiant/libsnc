/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/device/qt/sensors/i1/State.hpp>
#include <QObject>

namespace snc::device::qt::sensors::i1
{

/*** Qt wrapper class */
class Sensor : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( QObject * state READ state CONSTANT )

  Q_PROPERTY ( bool valid READ isValid NOTIFY isValidChanged () )
  Q_PROPERTY ( QString key READ key NOTIFY keyChanged )
  Q_PROPERTY ( QString name READ name NOTIFY nameChanged )

  public:
  // -- Construction

  Sensor ( QObject * qparent_n, int index_n );

  ~Sensor ();

  // -- Index

  int
  index () const
  {
    return _index;
  }

  // -- Valid

  bool
  isValid () const
  {
    return _statics.operator bool ();
  }

  Q_SIGNAL
  void
  isValidChanged ();

  // -- Key

  const QString &
  key () const
  {
    return _key;
  }

  Q_SIGNAL
  void
  keyChanged ();

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  Q_SIGNAL
  void
  nameChanged ();

  // -- State

  auto *
  state ()
  {
    return &_state;
  }

  const auto *
  state () const
  {
    return &_state;
  }

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  int _index = 0;
  QString _key;
  QString _name;
  snc::device::statics::sensor::handle::I1 _statics;
  snc::device::qt::sensors::i1::State _state;
};

} // namespace snc::device::qt::sensors::i1
