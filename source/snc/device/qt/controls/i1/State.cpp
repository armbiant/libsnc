/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "State.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/control/i1.hpp>

namespace snc::device::qt::controls::i1
{

State::State ( QObject * qparent_n, int index_n )
: QObject ( qparent_n )
, _index ( index_n )
{
}

State::~State () = default;

void
State::update_statics ( const snc::device::Info & info_n )
{
  _source = info_n.state ().control_i1_shared ( index () );
}

void
State::update_properties ()
{
  if ( _source ) {
    // -- Valid sensor
    auto & src = *_source;
    if ( sev::change ( _value, src.state () ) ) {
      emit valueChanged ();
    }
  } else {
    // -- Invalid sensor
    if ( sev::change ( _value, false ) ) {
      emit valueChanged ();
    }
  }
}

} // namespace snc::device::qt::controls::i1
