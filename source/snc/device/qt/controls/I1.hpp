/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/device/qt/ItemList.hpp>
#include <snc/device/qt/controls/i1/Control.hpp>
#include <QAbstractListModel>
#include <memory>
#include <vector>

namespace snc::device::qt::controls
{

/*** Qt wrapper class */
class I1 : public QAbstractListModel
{
  Q_OBJECT

  // -- Types

  enum ExtraRole
  {
    listItem = Qt::UserRole
  };

  // -- Properties

  Q_PROPERTY ( int count READ countInt NOTIFY countChanged )

  public:
  // -- Construction

  I1 ( QObject * qparent_n, snc::device::Info & info_n );

  ~I1 ();

  // -- Number of controls

  std::size_t
  count ()
  {
    return _items.count_valid ();
  }

  int
  countInt ()
  {
    return _items.count_valid ();
  }

  Q_SIGNAL
  void
  countChanged ();

  // -- Get

  std::shared_ptr< const i1::Control >
  getOrCreateShared ( std::size_t index_n );

  Q_INVOKABLE
  QObject *
  getOrCreate ( int index_n );

  Q_INVOKABLE
  QObject *
  getByKey ( const QString & key_n );

  Q_INVOKABLE
  QObject *
  getByName ( const QString & name_n );

  // -- QAbstractListModel interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  // -- Interface

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  private:
  // -- Attributes
  snc::device::Info & _info;
  ItemList< i1::Control > _items;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::device::qt::controls
