/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/qt/controls/I1.hpp>
#include <QObject>

namespace snc::device::qt
{

/*** Qt wrapper class */
class Controls : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( snc::device::qt::controls::I1 * i1 READ i1 CONSTANT )

  public:
  // -- Construction

  Controls ( QObject * qparent_n, snc::device::Info & info_n );

  ~Controls ();

  // -- Boolean

  snc::device::qt::controls::I1 *
  i1 ()
  {
    return &_i1;
  }

  // -- Interface

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  private:
  // -- Attributes
  snc::device::qt::controls::I1 _i1;
};

} // namespace snc::device::qt
