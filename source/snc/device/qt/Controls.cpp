/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Controls.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::device::qt
{

Controls::Controls ( QObject * qparent_n, snc::device::Info & info_n )
: QObject ( qparent_n )
, _i1 ( this, info_n )
{
}

Controls::~Controls () = default;

void
Controls::update_statics ()
{
  _i1.update_statics ();
}

void
Controls::update_valid_properties ()
{
  _i1.update_valid_properties ();
}

void
Controls::update_available_properties ()
{
  _i1.update_available_properties ();
}

} // namespace snc::device::qt
