/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Axes.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/qt/ItemList.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemList< snc::device::qt::axes::Axis >;

namespace snc::device::qt
{

Axes::Axes ( QObject * qparent_n, snc::device::Info & info_n )
: QAbstractListModel ( qparent_n )
, _info ( info_n )
, _items ( this )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::listItem, "listItem" );
}

Axes::~Axes () = default;

std::shared_ptr< axes::Axis >
Axes::getOrCreateShared ( std::size_t index_n )
{
  _items.ensure_size_available ( index_n + 1 );
  return _items.available ().at ( index_n );
}

QObject *
Axes::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    throw std::runtime_error ( "Negative index." );
  }
  _items.ensure_size_available ( index_n + 1 );
  return _items.available ().at ( index_n ).get ();
}

QObject *
Axes::getByKey ( const QString & key_n )
{
  return _items.getByKey ( key_n );
}

QObject *
Axes::getByName ( const QString & name_n )
{
  return _items.getByName ( name_n );
}

QHash< int, QByteArray >
Axes::roleNames () const
{
  return _roleNames;
}

int
Axes::rowCount ( const QModelIndex & parent_n ) const
{
  return _items.validParentIndex ( parent_n ) ? _items.count_valid () : 0;
}

QVariant
Axes::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( _items.validIndex ( index_n ) ) {
    switch ( role_n ) {
    case ExtraRole::listItem:
      return QVariant::fromValue (
          _items.valid ().at ( index_n.row () ).get () );
    default:
      break;
    }
  }
  return QVariant ();
}

void
Axes::update_statics ()
{
  const std::size_t num_old = _items.count_valid ();
  const std::size_t num_new = _info.statics ()->axes ().size ();
  if ( num_old != num_new ) {
    if ( num_old < num_new ) {
      // Add items
      beginInsertRows ( QModelIndex (), num_old, num_new - 1 );
      _items.create_valid ( num_new - num_old );
      endInsertRows ();
    } else {
      // Remove items
      beginRemoveRows ( QModelIndex (), num_new, num_old - 1 );
      _items.remove_valid ( num_old - num_new );
      endRemoveRows ();
    }
    emit countChanged ();
  }

  // Update statics of all items
  for ( auto & item : _items.available () ) {
    item->update_statics ( _info );
  }
}

void
Axes::update_valid_properties ()
{
  for ( auto & item : _items.valid () ) {
    item->update_properties ();
  }
}

void
Axes::update_available_properties ()
{
  for ( auto & item : _items.available () ) {
    item->update_properties ();
  }
}

} // namespace snc::device::qt
