/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Sensors.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::device::qt
{

Sensors::Sensors ( QObject * qparent_n, snc::device::Info & info_n )
: QObject ( qparent_n )
, _i1 ( this, info_n )
{
}

Sensors::~Sensors () = default;

void
Sensors::update_statics ()
{
  _i1.update_statics ();
}

void
Sensors::update_valid_properties ()
{
  _i1.update_valid_properties ();
}

void
Sensors::update_available_properties ()
{
  _i1.update_available_properties ();
}

} // namespace snc::device::qt
