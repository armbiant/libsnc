/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "State.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::device::qt
{

State::State ( QObject * qparent_n, snc::device::Info & info_n )
: QObject ( qparent_n )
, _info ( info_n )
{
}

State::~State () = default;

void
State::update_properties ()
{
  // Update serial bitrate
  {
    bool in_changed = false;
    bool out_changed = false;
    bool total_changed = false;
    {
      auto & serial = _info.state ().io_stats ().serial ();
      double rate_in = serial.in ().frame_bytes_per_second ();
      double rate_out = serial.out ().frame_bytes_per_second ();
      double rate_total = rate_in + rate_out;
      if ( _serialBitrateIn != rate_in ) {
        _serialBitrateIn = rate_in;
        in_changed = true;
      }
      if ( _serialBitrateOut != rate_out ) {
        _serialBitrateOut = rate_out;
        out_changed = true;
      }
      if ( _serialBitrateTotal != rate_total ) {
        _serialBitrateTotal = rate_total;
        total_changed = true;
      }
    }
    // Emit signals
    if ( in_changed ) {
      emit serialBitrateInChanged ();
    }
    if ( out_changed ) {
      emit serialBitrateOutChanged ();
    }
    if ( total_changed ) {
      emit serialBitrateTotalChanged ();
    }
  }
  // Update latency
  {
    std::chrono::nanoseconds latency_ns = _info.state ().io_stats ().latency ();
    double latency_ms = ( latency_ns.count () / ( 1000.0 * 1000.0 ) );
    if ( _latency != latency_ms ) {
      _latency = latency_ms;
      emit latencyChanged ();
    }
  }
}

} // namespace snc::device::qt
