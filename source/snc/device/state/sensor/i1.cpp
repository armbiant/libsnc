/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i1.hpp"
#include <snc/device/statics/sensor/i1.hpp>

namespace snc::device::state::sensor
{

void
I1::reset ( const Statics & statics_n )
{
  Sensor::reset ();
  _state = statics_n.default_state ();
}

} // namespace snc::device::state::sensor
