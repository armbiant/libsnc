/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i32.hpp"
#include <snc/device/statics/sensor/i32.hpp>

namespace snc::device::state::sensor
{

void
I32::reset ( const Statics & statics_n )
{
  Sensor::reset ();
  _state = statics_n.default_state ();
}

} // namespace snc::device::state::sensor
