/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "axis.hpp"

namespace snc::device::state
{

Axis::Axis () = default;

void
Axis::reset ( const Statics & stats_n [[maybe_unused]] )
{
  _is_aligned = false;
  _steps_queue_underrun = false;
  _steps_queue_overrun = false;
  _steps_queue_length = 0;
  _steps_queue_microseconds = 0;
  _step_current.reset ();
  _step_latest.reset ();

  _kinematics_current.reset ();
  _kinematics_current_complete.reset ();
  _kinematics_latest.reset ();
}

double
Axis::acquire_speed_current () const
{
  double speed = 0.0;
  {
    double speed_cur = kinematics_current ().speed ();
    double speed_cur_abs = std::fabs ( speed_cur );
    double speed_cur_comp = kinematics_current_complete ().speed ();
    double speed_cur_comp_abs = std::fabs ( speed_cur_comp );
    const double lim = 1.0e-9;
    if ( ( speed_cur_comp_abs < lim ) && ( speed_cur_abs > lim ) ) {
      speed = speed_cur;
    } else {
      if ( speed_cur_abs < speed_cur_comp_abs ) {
        speed = speed_cur;
      } else {
        speed = speed_cur_comp;
      }
    }
  }
  return speed;
}

} // namespace snc::device::state
