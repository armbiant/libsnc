/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/axis.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::device::statics
{
class Axis;
}

namespace snc::device::state
{

namespace axis
{

class Kinematics
{
  public:
  // -- Construction

  Kinematics () = default;

  // -- Setup

  void
  reset ()
  {
    _position = 0.0;
    _speed = 0.0;
  }

  // -- Positions

  double
  position () const
  {
    return _position;
  }

  void
  set_position ( double value_n )
  {
    _position = value_n;
  }

  // -- Speed

  /// @brief miliimeter / second
  double
  speed () const
  {
    return _speed;
  }

  void
  set_speed ( double value_n )
  {
    _speed = value_n;
  }

  public:
  // -- Attributes
  double _position = 0.0;
  double _speed = 0.0;
};

class Step
{
  public:
  // -- Construction

  Step () = default;

  // -- Setup

  void
  reset ()
  {
    _position = 0;
    _count = 0;
    _speed = 0.0;
  }

  // -- Position

  std::int_fast64_t
  position () const
  {
    return _position;
  }

  void
  set_position ( std::int_fast64_t value_n )
  {
    _position = value_n;
  }

  // -- Step count

  std::uint_fast64_t
  count () const
  {
    return _count;
  }

  void
  set_count ( std::uint_fast64_t value_n )
  {
    _count = value_n;
  }

  // -- Speed in steps per second

  /// @return Steps per second
  double
  speed () const
  {
    return _speed;
  }

  /// @arg value_n Steps per second
  void
  set_speed ( double value_n )
  {
    _speed = value_n;
  }

  public:
  // -- Attributes
  std::int_fast64_t _position = 0;
  std::uint_fast64_t _count = 0;
  double _speed = 0.0;
};

} // namespace axis

class Axis
{
  public:
  // -- Types

  using Statics = snc::device::statics::Axis;

  // -- Construction

  Axis ();

  // -- Setup

  /// @brief Reset state
  void
  reset ( const Statics & stats_n );

  // -- Align state

  bool
  is_aligned () const
  {
    return _is_aligned;
  }

  void
  set_is_aligned ( bool flag_n )
  {
    _is_aligned = flag_n;
  }

  // Steps queue

  bool
  steps_queue_underrun () const
  {
    return _steps_queue_underrun;
  }

  void
  set_steps_queue_underrun ( bool flag_n )
  {
    _steps_queue_underrun = flag_n;
  }

  bool
  steps_queue_overrun () const
  {
    return _steps_queue_overrun;
  }

  void
  set_steps_queue_overrun ( bool flag_n )
  {
    _steps_queue_overrun = flag_n;
  }

  std::uint_fast32_t
  steps_queue_length () const
  {
    return _steps_queue_length;
  }

  void
  set_steps_queue_length ( std::uint_fast32_t value_n )
  {
    _steps_queue_length = value_n;
  }

  bool
  steps_queue_empty () const
  {
    return ( steps_queue_length () == 0 );
  }

  std::uint_fast32_t
  steps_queue_microseconds () const
  {
    return _steps_queue_microseconds;
  }

  void
  set_steps_queue_microseconds ( std::uint_fast32_t value_n )
  {
    _steps_queue_microseconds = value_n;
  }

  axis::Step &
  step_current ()
  {
    return _step_current;
  }

  const axis::Step &
  step_current () const
  {
    return _step_current;
  }

  axis::Step &
  step_current_complete ()
  {
    return _step_current_complete;
  }

  const axis::Step &
  step_current_complete () const
  {
    return _step_current_complete;
  }

  axis::Step &
  step_latest ()
  {
    return _step_latest;
  }

  const axis::Step &
  step_latest () const
  {
    return _step_latest;
  }

  // -- Kinematics

  axis::Kinematics &
  kinematics_current ()
  {
    return _kinematics_current;
  }

  const axis::Kinematics &
  kinematics_current () const
  {
    return _kinematics_current;
  }

  axis::Kinematics &
  kinematics_current_complete ()
  {
    return _kinematics_current_complete;
  }

  const axis::Kinematics &
  kinematics_current_complete () const
  {
    return _kinematics_current_complete;
  }

  axis::Kinematics &
  kinematics_latest ()
  {
    return _kinematics_latest;
  }

  const axis::Kinematics &
  kinematics_latest () const
  {
    return _kinematics_latest;
  }

  double
  acquire_speed_current () const;

  private:
  // -- Attributes
  bool _is_aligned = false;

  // -- Step queue state
  bool _steps_queue_underrun = false;
  bool _steps_queue_overrun = false;
  std::uint_fast32_t _steps_queue_length = 0;
  std::uint_fast32_t _steps_queue_microseconds = 0;
  axis::Step _step_current;
  axis::Step _step_current_complete;
  axis::Step _step_latest;

  // -- Attributes
  axis::Kinematics _kinematics_current;
  axis::Kinematics _kinematics_current_complete;
  axis::Kinematics _kinematics_latest;
};

} // namespace snc::device::state
