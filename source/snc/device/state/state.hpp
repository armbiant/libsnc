/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/mem/view.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/state/control/i1.hpp>
#include <snc/device/state/control/i16.hpp>
#include <snc/device/state/control/i32.hpp>
#include <snc/device/state/control/i64.hpp>
#include <snc/device/state/control/i8.hpp>
#include <snc/device/state/sensor/i1.hpp>
#include <snc/device/state/sensor/i16.hpp>
#include <snc/device/state/sensor/i32.hpp>
#include <snc/device/state/sensor/i64.hpp>
#include <snc/device/state/sensor/i8.hpp>
#include <snc/emb/io_stats.hpp>
#include <cstddef>
#include <memory>
#include <tuple>

namespace snc::device::state
{

class State
{
  public:
  // -- Types

  using Items_Tuple = std::tuple< sev::mem::View< sensor::I1 >,
                                  sev::mem::View< sensor::I8 >,
                                  sev::mem::View< sensor::I16 >,
                                  sev::mem::View< sensor::I32 >,
                                  sev::mem::View< sensor::I64 >,
                                  sev::mem::View< control::I1 >,
                                  sev::mem::View< control::I8 >,
                                  sev::mem::View< control::I16 >,
                                  sev::mem::View< control::I32 >,
                                  sev::mem::View< control::I64 >,
                                  sev::mem::View< Axis > >;

  // -- Construction

  State ();

  State ( const State & state_n );

  State ( snc::device::statics::handle::Statics statics_n );

  ~State ();

  // -- Assigment

  void
  assign ( const State & state_n );

  State &
  operator= ( const State & state_n )
  {
    assign ( state_n );
    return *this;
  }

  // -- Setup

  /// @brief Set statics and reset state to the statics initial state
  void
  reset ( snc::device::statics::handle::Statics statics_n );

  /// @brief Reset state to the statics initial state
  void
  reset ();

  // -- Statics

  const snc::device::statics::handle::Statics &
  statics () const
  {
    return _statics;
  }

  // -- Items tuple

  const Items_Tuple &
  items () const
  {
    return _items;
  }

  Items_Tuple &
  items ()
  {
    return _items;
  }

  template < typename T >
  const auto &
  items_by_type () const
  {
    return reinterpret_cast< const sev::mem::View< const T > & > (
        std::get< sev::mem::View< T > > ( _items ) );
  }

  template < typename T >
  auto &
  items_by_type ()
  {
    return std::get< sev::mem::View< T > > ( _items );
  }

  template < typename T >
  std::shared_ptr< const T >
  item_shared ( std::size_t index_n ) const
  {
    const auto & items = items_by_type< T > ();
    if ( index_n < items.size () ) {
      return std::shared_ptr< const T > ( _shared, &items[ index_n ] );
    }
    return {};
  }

  template < typename T >
  std::shared_ptr< T >
  item_shared_writeable ( std::size_t index_n )
  {
    const auto & items = items_by_type< T > ();
    if ( index_n < items.size () ) {
      return std::shared_ptr< T > ( _shared, &items[ index_n ] );
    }
    return {};
  }

  // -- Sensors: I1

  const sev::mem::View< const sensor::I1 > &
  sensors_i1 () const
  {
    return items_by_type< sensor::I1 > ();
  }

  const sev::mem::View< sensor::I1 > &
  sensors_i1_writeable ()
  {
    return items_by_type< sensor::I1 > ();
  }

  sensor::handle::I1
  sensor_i1_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I1 > ( index_n );
  }

  sensor::handle::I1_Writeable
  sensor_i1_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I1 > ( index_n );
  }

  // -- Controls: I1

  const sev::mem::View< const control::I1 > &
  controls_i1 () const
  {
    return items_by_type< control::I1 > ();
  }

  const sev::mem::View< control::I1 > &
  controls_i1_writeable ()
  {
    return items_by_type< control::I1 > ();
  }

  control::handle::I1
  control_i1_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I1 > ( index_n );
  }

  control::handle::I1_Writeable
  control_i1_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I1 > ( index_n );
  }

  // -- Axes

  const sev::mem::View< Axis > &
  axes ()
  {
    return items_by_type< Axis > ();
  }

  const sev::mem::View< const Axis > &
  axes () const
  {
    return items_by_type< Axis > ();
  }

  handle::Axis
  axis_shared ( std::size_t index_n ) const
  {
    return item_shared< Axis > ( index_n );
  }

  // -- Axes derived values

  bool
  all_axes_queues_empty () const;

  bool
  all_axes_aligned () const;

  void
  acquire_position_current ( sev::lag::Vector3d & pos_n ) const;

  void
  acquire_position_latest ( sev::lag::Vector3d & pos_n ) const;

  // -- IO statistics

  snc::emb::IO_Stats &
  io_stats ()
  {
    return *_io_stats;
  }

  const snc::emb::IO_Stats &
  io_stats () const
  {
    return *_io_stats;
  }

  private:
  // -- Attributes
  snc::device::statics::handle::Statics _statics;
  sev::mem::View< std::byte > _data;
  // -- Data views
  Items_Tuple _items;
  snc::emb::IO_Stats * _io_stats = nullptr;

  // -- Data buffer
  struct Shared;
  std::shared_ptr< Shared > _shared;
};

} // namespace snc::device::state
