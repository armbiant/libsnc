/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::device::state
{

class Control
{
  public:
  // -- Construction

  Control () = default;

  void
  reset ()
  {
    _is_valid = false;
  }

  // -- State is valid

  bool
  is_valid () const
  {
    return _is_valid;
  }

  void
  set_valid ( bool flag_n )
  {
    _is_valid = flag_n;
  }

  private:
  // -- Attributes
  bool _is_valid = false;
};

} // namespace snc::device::state
