/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "axis_info.hpp"
#include <snc/device/state/axis.hpp>
#include <snc/device/statics/axis.hpp>

namespace snc::device
{

Axis_Info::Axis_Info () = default;

Axis_Info::Axis_Info ( const Axis_Info & axis_n ) = default;

Axis_Info::Axis_Info ( Axis_Info && axis_n ) = default;

Axis_Info::Axis_Info ( statics::handle::Axis statics_n,
                       state::handle::Axis state_n )
: _statics ( std::move ( statics_n ) )
, _state ( std::move ( state_n ) )
{
}

Axis_Info::~Axis_Info () = default;

Axis_Info &
Axis_Info::operator= ( const Axis_Info & axis_n ) = default;

Axis_Info &
Axis_Info::operator= ( Axis_Info && axis_n ) = default;

void
Axis_Info::reset ()
{
  _statics.reset ();
  _state.reset ();
}

void
Axis_Info::reset ( statics::handle::Axis statics_n,
                   state::handle::Axis state_n )
{
  _statics = std::move ( statics_n );
  _state = std::move ( state_n );
}

bool
Axis_Info::is_unaligned () const
{
  if ( !_statics || !_state ) {
    return true;
  }
  return ( _statics->manual ().alignment_required () &&
           !_state->is_aligned () );
}

bool
Axis_Info::is_manually_movable () const
{
  return ( _statics && _statics->manual ().movable () );
}

} // namespace snc::device
