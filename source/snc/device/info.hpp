/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/axis_info.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/state/state.hpp>
#include <memory>

namespace snc::device
{

/** @brief Device statics and state. */
class Info
{
  public:
  // -- Construction

  Info ();

  Info ( const Info & info_n );

  Info ( Info && info_n ) = delete;

  Info ( const statics::handle::Statics & statics_n );

  ~Info ();

  // -- Assignment operators

  Info &
  operator= ( const Info & info_n )
  {
    assign ( info_n );
    return *this;
  }

  Info &
  operator= ( Info && info_n ) = delete;

  // -- Setup

  /// @brief Reset to statics construction construction state
  void
  reset ( const statics::handle::Statics & statics_n );

  /// @brief Reset to default construction state
  void
  reset ();

  /// @brief Reset state only
  void
  reset_state ();

  void
  assign ( const Info & info_n );

  // -- Statics

  const auto &
  statics () const
  {
    return _statics;
  }

  // -- State

  const auto &
  state () const
  {
    return _state;
  }

  auto &
  state_ref ()
  {
    return _state;
  }

  // -- Interface

  Axis_Info
  axis ( std::uint_fast32_t index_n ) const;

  private:
  statics::handle::Statics _statics;
  state::State _state;
};

namespace handle
{
using Info = std::shared_ptr< const snc::device::Info >;
using Info_Writeable = std::shared_ptr< snc::device::Info >;
} // namespace handle

} // namespace snc::device
