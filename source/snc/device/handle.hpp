/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <memory>

namespace snc::device
{

template < typename T >
using Handle = std::shared_ptr< T >;

//
// -- Statics: Forward declaration
//
namespace statics
{
class Statics;
class Axis;
namespace sensor
{
class I1;
class I8;
class I16;
class I32;
class I64;
} // namespace sensor
namespace control
{
class I1;
class I8;
class I16;
class I32;
class I64;
} // namespace control
}; // namespace statics

//
// -- Statics: Handles
//
namespace statics::handle
{
using Statics = Handle< const snc::device::statics::Statics >;
using Statics_Writeable = Handle< snc::device::statics::Statics >;

using Axis = Handle< const snc::device::statics::Axis >;
using Axis_Writeable = Handle< snc::device::statics::Axis >;

// -- Functions
handle::Statics_Writeable
make ();
} // namespace statics::handle

namespace statics::sensor::handle
{
using I1 = Handle< const snc::device::statics::sensor::I1 >;
using I1_Writeable = Handle< snc::device::statics::sensor::I1 >;

using I8 = Handle< const snc::device::statics::sensor::I8 >;
using I8_Writeable = Handle< snc::device::statics::sensor::I8 >;

using I16 = Handle< const snc::device::statics::sensor::I16 >;
using I16_Writeable = Handle< snc::device::statics::sensor::I16 >;

using I32 = Handle< const snc::device::statics::sensor::I32 >;
using I32_Writeable = Handle< snc::device::statics::sensor::I32 >;

using I64 = Handle< const snc::device::statics::sensor::I64 >;
using I64_Writeable = Handle< snc::device::statics::sensor::I64 >;
} // namespace statics::sensor::handle

namespace statics::control::handle
{
using I1 = Handle< const snc::device::statics::control::I1 >;
using I1_Writeable = Handle< snc::device::statics::control::I1 >;

using I8 = Handle< const snc::device::statics::control::I8 >;
using I8_Writeable = Handle< snc::device::statics::control::I8 >;

using I16 = Handle< const snc::device::statics::control::I16 >;
using I16_Writeable = Handle< snc::device::statics::control::I16 >;

using I32 = Handle< const snc::device::statics::control::I32 >;
using I32_Writeable = Handle< snc::device::statics::control::I32 >;

using I64 = Handle< const snc::device::statics::control::I64 >;
using I64_Writeable = Handle< snc::device::statics::control::I64 >;
} // namespace statics::control::handle

//
// -- State: Forward declaration
//
namespace state
{
class State;
class Axis;
namespace sensor
{
class I1;
class I8;
class I16;
class I32;
class I64;
} // namespace sensor
namespace control
{
class I1;
class I8;
class I16;
class I32;
class I64;
} // namespace control
} // namespace state

//
// -- State: Handles
//
namespace state::handle
{
using State = Handle< const snc::device::state::State >;
using State_Writeable = Handle< snc::device::state::State >;

using Axis = Handle< const snc::device::state::Axis >;
using Axis_Writeable = Handle< snc::device::state::Axis >;
} // namespace state::handle

namespace state::sensor::handle
{
using I1 = Handle< const snc::device::state::sensor::I1 >;
using I1_Writeable = Handle< snc::device::state::sensor::I1 >;

using I8 = Handle< const snc::device::state::sensor::I8 >;
using I8_Writeable = Handle< snc::device::state::sensor::I8 >;

using I16 = Handle< const snc::device::state::sensor::I16 >;
using I16_Writeable = Handle< snc::device::state::sensor::I16 >;

using I32 = Handle< const snc::device::state::sensor::I32 >;
using I32_Writeable = Handle< snc::device::state::sensor::I32 >;

using I64 = Handle< const snc::device::state::sensor::I64 >;
using I64_Writeable = Handle< snc::device::state::sensor::I64 >;
} // namespace state::sensor::handle

namespace state::control::handle
{
using I1 = Handle< const snc::device::state::control::I1 >;
using I1_Writeable = Handle< snc::device::state::control::I1 >;

using I8 = Handle< const snc::device::state::control::I8 >;
using I8_Writeable = Handle< snc::device::state::control::I8 >;

using I16 = Handle< const snc::device::state::control::I16 >;
using I16_Writeable = Handle< snc::device::state::control::I16 >;

using I32 = Handle< const snc::device::state::control::I32 >;
using I32_Writeable = Handle< snc::device::state::control::I32 >;

using I64 = Handle< const snc::device::state::control::I64 >;
using I64_Writeable = Handle< snc::device::state::control::I64 >;
} // namespace state::control::handle

//
// -- Info: Forward declaration
//
class Info;

//
// -- Info: Handles
//
namespace handle
{
using Info = Handle< const snc::device::Info >;
using Info_Writeable = Handle< snc::device::Info >;

// -- Utility
Info_Writeable
make_info (
    const snc::device::statics::handle::Statics & device_statics_n = {} );
} // namespace handle

} // namespace snc::device
