/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/fac/tracker/dash/events.hpp>
#include <snc/svs/fac/tracker/office/clerk.hpp>

namespace snc::svs::fac::tracker::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Sailor-Tracker" )
, _info ( this )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::tracker::office::Clerk,
                             snc::device::statics::handle::Statics > (
      _info.info ().statics () );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _info );
    break;
  default:
    DEBUG_ASSERT ( false );
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();
  _info.update_valid_properties ();
}

} // namespace snc::svs::fac::tracker::dash
