/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/svp/ship/events.hpp>
#include <cstdint>
#include <memory>

namespace snc::svs::fac::tracker::ship::event::in
{
} // namespace snc::svs::fac::tracker::ship::event::in

namespace snc::svs::fac::tracker::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t DEVICE_STATE = 0;
};

using Out = snc::svp::ship::event::Out;

class Device_State : public Out
{
  public:
  static constexpr auto etype = Type::DEVICE_STATE;

  Device_State ( Pair pair_n, const snc::device::Info & device_info_n )
  : Out ( etype, pair_n )
  , _device_info ( device_info_n )
  {
  }

  const auto &
  device_info () const
  {
    return _device_info;
  }

  void
  set_device_info ( snc::device::Info & device_info_n )
  {
    _device_info.assign ( device_info_n );
  }

  public:
  snc::device::Info _device_info;
};

} // namespace snc::svs::fac::tracker::ship::event::out