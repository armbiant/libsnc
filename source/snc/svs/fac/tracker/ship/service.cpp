/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/msg/in/all.hpp>
#include <snc/svs/fac/tracker/ship/sailor.hpp>

namespace snc::svs::fac::tracker::ship
{

namespace
{
// -- Anonymous callbacks
void
set_io_stats_anon ( snc::bat::ifc::Service_Abstract * service_n,
                    const snc::serial_device::Stats_IO & stats_n )
{
  static_cast< Service * > ( service_n )->set_io_stats ( stats_n );
}

void
data_in_anon ( snc::bat::ifc::Service_Abstract * service_n,
               const snc::utility::Data_Tile * tile_n )
{
  static_cast< Service * > ( service_n )->device_in_process_data ( tile_n );
}

void
emb_out_anon ( snc::bat::ifc::Service_Abstract * service_n,
               snc::emb::msg::Stream & stream_n,
               std::size_t offset_n )
{
  static_cast< Service * > ( service_n )
      ->device_out_track ( stream_n, offset_n );
}

void
abort_anon ( snc::bat::ifc::Service_Abstract * service_n )
{
  static_cast< Service * > ( service_n )->clear_tracker ();
}

} // namespace

Service::Service ( Provider & provider_n,
                   const snc::device::handle::Info_Writeable & device_info_n )
: Super ( provider_n )
, _device_info ( device_info_n )
, _emb_router_state ( device_statics ()->emb_device_config () )
, _serial ( &provider_cell () )
, _event_pool_state ( provider ().office_io ().epool_tracker () )
{
  // -- Office event generation timer
  _office_egen_interval =
      std::chrono::microseconds ( sev::math::mega_ull / 60 );
  _office_egen_timer.connect (
      provider_cell ().cell_context ().timer_queue () );
  _office_egen_timer.set_callback ( [ &oio = provider ().office_io () ] () {
    oio.notification_set ( Sailor::Office_Message::DEVICE_STATE );
  } );
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  _serial.connect_required ();
  _serial.set_io_stats_cback ( this, &set_io_stats_anon );
  _serial.set_data_in_cback ( this, &data_in_anon );
  _serial.set_emb_out_cback ( this, &emb_out_anon );
  _serial.set_abort_cback ( this, &abort_anon );

  // Message tracker capacities
  _message_tracker.set_rail_capacity ( 1024 * 1024 );
  _message_tracker.set_pool_capacity ( 4 );

  // Setup sensor mappers: I1
  {
    auto & states = device_state ().sensors_i1 ();
    auto & stats = device_statics ()->sensors_i1 ().items ();
    std::size_t num_sensors = states.size ();
    _map_sensors_i1.reserve ( num_sensors );
    for ( std::size_t ii = 0; ii != num_sensors; ++ii ) {
      const auto & stat = *stats[ ii ];
      const std::size_t emb_index = stat.emb ().sensor_index ();
      if ( emb_index >= emb_router_state ().sensors_bool ().size () ) {
        // Invalid emb index
        continue;
      }
      _map_sensors_i1.emplace_back ( Map_Sensor_I1{
          &emb_router_state ().sensors_bool ()[ emb_index ],
          stat.emb ().inverted (),
          device_state_ref ().sensor_i1_writeable_shared ( ii ) } );
    }
    _map_sensors_i1.shrink_to_fit ();
  }

  // Setup control mappers: I1
  {
    auto & states = device_state ().controls_i1 ();
    auto & stats = device_statics ()->controls_i1 ().items ();
    const std::size_t num_controls = states.size ();
    _map_controls_i1.reserve ( num_controls );
    for ( std::size_t ii = 0; ii != num_controls; ++ii ) {
      const auto & stat = *stats[ ii ];
      const std::size_t emb_index = stat.emb ().control_index ();
      if ( emb_index >= emb_router_state ().controls_bool ().size () ) {
        // Invalid emb index
        continue;
      }
      _map_controls_i1.emplace_back ( Map_Control_I1{
          &emb_router_state ().controls_bool ()[ emb_index ],
          stat.emb ().inverted (),
          device_state_ref ().control_i1_writeable_shared ( ii ) } );
    }
    _map_controls_i1.shrink_to_fit ();
  }

  // Setup axes
  {
    _axes.reserve ( device_statics ()->axes ().size () );
    for ( const auto & axis : device_statics ()->axes ().items () ) {
      _axes.emplace_back ( *axis );
    }
    for ( auto & axis : _axes ) {
      axis.session_begin ();
    }
  }

  _latest_message_send_time = std::chrono::steady_clock::now ();
  update_messages_track_queues_empty ();
}

void
Service::session_abort ()
{
  _office_egen_timer.stop ();
  clear_tracker ();
}

void
Service::session_end ()
{
  // Reset axes
  for ( auto & axis : _axes ) {
    axis.session_end ();
  }

  clear_tracker ();
}

void
Service::clear_tracker ()
{
  if ( _message_tracker.is_empty () ) {
    return;
  }

  // Clear message track list
  _message_tracker.clear ();
}

void
Service::request_device_state_message ()
{
  // Return if generation is scheduled already
  if ( _office_egen_timer.is_running () ) {
    return;
  }

  // Start timer
  auto tnow = std::chrono::steady_clock::now ();
  auto elapsed = tnow - _office_egen_timer.point ();
  if ( elapsed <= _office_egen_interval ) {
    // Wait for the rest of the interval
    auto remain = _office_egen_interval - elapsed;
    _office_egen_timer.start_single_point ( tnow + remain );
  } else {
    // The interval has elapsed already.
    _office_egen_timer.set_point ( tnow );
    provider ().office_io ().notification_set (
        Sailor::Office_Message::DEVICE_STATE );
  }
}

void
Service::set_axis_aligned ( std::size_t axis_index_n, bool flag_n )
{
  if ( axis_index_n >= _axes.size () ) {
    DEBUG_ASSERT ( axis_index_n < _axes.size () );
    return;
  }

  const auto & axis_stat = device_statics ()->axes ().get ( axis_index_n );
  auto & axis_state = device_state_ref ().axes ()[ axis_index_n ];
  if ( flag_n ) {
    std::int64_t bpos = axis ( axis_index_n ).step_current ().position;
    if ( axis_stat->sensors ().high_end () ) {
      std::int64_t pdelta = axis_stat->step_index_max ();
      if ( axis_stat->emb ().direction_reversed () ) {
        bpos += pdelta;
      } else {
        bpos -= pdelta;
      }
    }
    _axes[ axis_index_n ].set_step_base_position ( bpos );
  }
  axis_state.set_is_aligned ( flag_n );

  update_device_state ();

  // Send device state immediately
  provider ().office_io ().notification_set (
      Sailor::Office_Message::DEVICE_STATE );
}

void
Service::set_io_stats ( const snc::serial_device::Stats_IO & stats_n )
{
  device_state_ref ().io_stats ().set_serial ( stats_n );
  // Request state message
  request_device_state_message ();
}

void
Service::update_device_state ()
{
  // Map sensors: I1
  for ( const auto & map : _map_sensors_i1 ) {
    auto & state = *map.state;
    state.set_valid ( true );
    state.set_state ( map.emb_sensor->state () != map.inverted );
  }

  // Map controls: I1
  for ( const auto & map : _map_controls_i1 ) {
    auto & state = *map.state;
    state.set_valid ( true );
    state.set_state ( map.emb_control->state () != map.inverted );
  }

  // Axes
  {
    const std::size_t num_axes = device_statics ()->axes ().size ();
    for ( std::size_t ii = 0; ii != num_axes; ++ii ) {
      update_device_state_axis ( ii );
    }
  }

  // IO Latency
  device_state_ref ().io_stats ().set_latency ( latency () );

  // Request state message
  request_device_state_message ();
}

void
Service::update_device_state_axis ( std::size_t axis_index_n )
{
  const auto & axis_rtrack = axis ( axis_index_n );
  const bool emb_direction_reversed = axis_rtrack.emb_direction_reversed ();
  auto & axis_state = device_state_ref ().axes ()[ axis_index_n ];
  {
    // Steps queue
    axis_state.set_steps_queue_length ( axis_rtrack.steps_queue_length () );
    axis_state.set_steps_queue_microseconds (
        axis_rtrack.steps_queue_length_usecs () );
  }
  // Step count
  {
    axis_state.step_current ().set_count ( axis_rtrack.step_current ().count );
    axis_state.step_current_complete ().set_count (
        axis_rtrack.step_current_complete ().count );
    axis_state.step_latest ().set_count ( axis_rtrack.step_latest ().count );
  }
  // Step position
  {
    // Step position
    std::array< std::int64_t, 3 > step_pos;
    step_pos[ 0 ] = axis_rtrack.step_current ().position;
    step_pos[ 1 ] = axis_rtrack.step_current_complete ().position;
    step_pos[ 2 ] = axis_rtrack.step_latest ().position;
    {
      const std::int64_t base = axis_rtrack.step_base_position ();
      if ( emb_direction_reversed ) {
        for ( auto & spos : step_pos ) {
          spos = base - spos;
        }
      } else {
        for ( auto & spos : step_pos ) {
          spos = spos - base;
        }
      }
    }
    axis_state.step_current ().set_position ( step_pos[ 0 ] );
    axis_state.step_current_complete ().set_position ( step_pos[ 1 ] );
    axis_state.step_latest ().set_position ( step_pos[ 2 ] );
  }
  // Step speed
  {
    std::array< double, 3 > speeds;
    speeds[ 0 ] = axis_rtrack.step_current ().steps_per_second ();
    if ( axis_rtrack.step_current ().is_forward == emb_direction_reversed ) {
      speeds[ 0 ] = -speeds[ 0 ];
    }
    speeds[ 1 ] = axis_rtrack.step_current_complete ().steps_per_second ();
    if ( axis_rtrack.step_current_complete ().is_forward ==
         emb_direction_reversed ) {
      speeds[ 1 ] = -speeds[ 1 ];
    }
    speeds[ 2 ] = axis_rtrack.step_latest ().steps_per_second ();
    if ( axis_rtrack.step_latest ().is_forward == emb_direction_reversed ) {
      speeds[ 2 ] = -speeds[ 2 ];
    }
    axis_state.step_current ().set_speed ( speeds[ 0 ] );
    axis_state.step_current_complete ().set_speed ( speeds[ 1 ] );
    axis_state.step_latest ().set_speed ( speeds[ 2 ] );
  }

  // Length position and speed
  {
    sev::lag::Vector< double, 6 > len_vals;
    {
      const auto & step = axis_state.step_current ();
      len_vals[ 0 ] = step.position ();
      len_vals[ 1 ] = step.speed ();
    }
    {
      const auto & step = axis_state.step_current_complete ();
      len_vals[ 2 ] = step.position ();
      len_vals[ 3 ] = step.speed ();
    }
    {
      const auto & step = axis_state.step_latest ();
      len_vals[ 4 ] = step.position ();
      len_vals[ 5 ] = step.speed ();
    }
    // Scale length
    len_vals *= axis_rtrack.length_per_step ();
    {
      auto & kin = axis_state.kinematics_current ();
      kin.set_position ( len_vals[ 0 ] );
      kin.set_speed ( len_vals[ 1 ] );
    }
    {
      auto & kin = axis_state.kinematics_current_complete ();
      kin.set_position ( len_vals[ 2 ] );
      kin.set_speed ( len_vals[ 3 ] );
    }
    {
      auto & kin = axis_state.kinematics_latest ();
      kin.set_position ( len_vals[ 4 ] );
      kin.set_speed ( len_vals[ 5 ] );
    }
  }

  // Look for buffer under-/overruns
  {
    const std::uint_fast32_t qlen = axis_state.steps_queue_length ();
    if ( qlen == 0 ) {
      double speed = axis_state.kinematics_latest ().speed ();
      if ( speed > axis_rtrack.speed_stoppable () ) {
        axis_state.set_steps_queue_underrun ( true );
      }
    } else if ( qlen > axis_rtrack.steps_queue_capacity () ) {
      axis_state.set_steps_queue_overrun ( true );
    }
  }
}

void
Service::device_in_process_data ( const snc::utility::Data_Tile * tile_n )
{
  _emb_deserializer.install_input_data ( tile_n );
  while ( _emb_deserializer.deserialize () ) {
    device_in_track ( _emb_deserializer.message () );
  }
  _emb_deserializer.release_input_data ();

  update_device_state ();
}

inline void
Service::device_in_track ( const snc::emb::msg::Message & message_n )
{
  using Type = snc::emb::msg::in::Type;

  switch ( static_cast< Type > ( message_n.message_type () ) ) {
  case Type::STATE_STEPPER:
    device_in_track_state_axes ( message_n );
    break;
  case Type::STATE_SENSORS_32:
    device_in_track_state_sensors ( message_n );
    break;
  case Type::STATE_SWITCHES_32:
    device_in_track_state_switches ( message_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Service::device_in_track_state_axes ( const snc::emb::msg::Message & message_n )
{
  {
    auto & cmsg =
        static_cast< const snc::emb::msg::in::State_Stepper & > ( message_n );
    if ( cmsg.stepper_index () < _emb_router_state.steppers ().size () ) {
      _emb_router_state.steppers ()[ cmsg.stepper_index () ]
          .set_steps_queue_length ( cmsg.steps_queue_length () );
    }
    _emb_router_state.set_lastest_message_uid ( cmsg.latest_message_uid () );
  }
  device_in_update_axes ();
}

void
Service::device_in_track_state_sensors (
    const snc::emb::msg::Message & message_n )
{
  {
    auto & cmsg = static_cast< const snc::emb::msg::in::State_Sensors_32 & > (
        message_n );

    // Update device state
    constexpr std::size_t num_bits = 32;
    std::size_t num =
        std::min ( _emb_router_state.sensors_bool ().size (), num_bits );
    for ( std::size_t ii = 0; ii != num; ++ii ) {
      _emb_router_state.sensors_bool ()[ ii ].set_state ( cmsg.is_high ( ii ) );
    }
    _emb_router_state.set_lastest_message_uid ( cmsg.latest_message_uid () );
  }
  device_in_update_axes ();
}

void
Service::device_in_track_state_switches (
    const snc::emb::msg::Message & message_n )
{
  {
    auto & cmsg = static_cast< const snc::emb::msg::in::State_Switches_32 & > (
        message_n );

    // Update device state
    constexpr std::size_t num_bits = 32;
    std::size_t num =
        std::min ( _emb_router_state.controls_bool ().size (), num_bits );
    for ( std::size_t ii = 0; ii != num; ++ii ) {
      _emb_router_state.controls_bool ()[ ii ].set_state (
          cmsg.is_high ( ii ) );
    }
    _emb_router_state.set_lastest_message_uid ( cmsg.latest_message_uid () );
  }
  device_in_update_axes ();
}

void
Service::device_in_update_axes ()
{
  // Axis queues tracking

  std::size_t num_messages_acked = 0;
  bool message_uid_found = false;
  // Find latest acknowledged message uid in the message tracker queue
  {
    const std::uint16_t emb_latest_message_uid =
        emb_router_state ().lastest_message_uid ();
    auto it = _message_tracker.cbegin ();
    auto ite = _message_tracker.cend ();
    for ( ; it != ite; ++it ) {
      ++num_messages_acked;
      auto * msg = reinterpret_cast<
          const Track_Message< snc::emb::msg::out::Message > * > ( it.data () );
      if ( msg->message_uid () == emb_latest_message_uid ) {
        message_uid_found = true;
        break;
      }
    }
  }

  if ( message_uid_found ) {
    std::chrono::steady_clock::time_point latest_acked_send_time;

    // Remove acknowledged messages from the message tracked queue
    // and update the per axis acknowledged message uids
    for ( std::size_t ii = 0; ii != num_messages_acked; ++ii ) {
      {
        using Type = snc::emb::msg::out::Type;

        auto * data = _message_tracker.front ();
        const auto * msg = reinterpret_cast<
            const Track_Message< snc::emb::msg::out::Message > * > ( data );

        switch ( static_cast< Type > ( msg->message_type () ) ) {
        case Type::STEPS_U16_64: {
          auto * mtrack = reinterpret_cast<
              const Track_Message< snc::emb::msg::out::Steps_U16_64 > * > (
              data );

          std::size_t axis_index = mtrack->stepper_index ();
          if ( axis_index < _axes.size () ) {
            _axes[ axis_index ].device_in_set_message_uid_acked (
                mtrack->message_uid () );
          }
          latest_acked_send_time = mtrack->send_time;
        } break;

        case Type::STEPPER_SYNC: {
          auto * mtrack = reinterpret_cast<
              const Track_Message< snc::emb::msg::out::Stepper_Sync > * > (
              data );

          // Sync messages are tracked by all axes
          for ( auto & axis : _axes ) {
            axis.device_in_set_message_uid_acked ( mtrack->message_uid () );
          }
          latest_acked_send_time = mtrack->send_time;
        } break;

        default: {
          auto * mtrack = reinterpret_cast<
              const Track_Message< snc::emb::msg::out::Message > * > ( data );
          latest_acked_send_time = mtrack->send_time;
        } break;
        }
      }

      _message_tracker.pop_front ();
    }

    // Update latency
    _latency = ( std::chrono::steady_clock::now () - latest_acked_send_time );
  }

  // Update axes tracking
  {
    const std::size_t num_axes =
        std::min ( _axes.size (), _emb_router_state.steppers ().size () );
    for ( std::size_t ii = 0; ii != num_axes; ++ii ) {
      _axes[ ii ].device_in_queue_length (
          _emb_router_state.steppers ()[ ii ].steps_queue_length () );
    }
  }

  update_messages_track_queues_empty ();
}

void
Service::device_out_track ( snc::emb::msg::Stream & stream_n,
                            std::size_t offset_n )
{
  if ( stream_n.size () <= offset_n ) {
    return;
  }

  device_out_track_begin ();
  {
    auto ite = stream_n.buffer ().cend ();
    auto itc = stream_n.buffer ().cbegin ();
    itc += offset_n;
    for ( ; itc != ite; ++itc ) {
      device_out_track ( *itc );
    }
  }
  device_out_track_end ();
}

void
Service::device_out_track_begin ()
{
  _latest_message_send_time = std::chrono::steady_clock::now ();
}

void
Service::device_out_track_end ()
{
  update_messages_track_queues_empty ();
  update_device_state ();
}

void
Service::device_out_track ( const snc::emb::msg::Message & msg_n )
{
  using Type = snc::emb::msg::out::Type;

  _latest_message_send_uid = msg_n.message_uid ();

  switch ( static_cast< Type > ( msg_n.message_type () ) ) {
  case Type::STEPS_U16_64: {
    auto & cmsg =
        static_cast< const snc::emb::msg::out::Steps_U16_64 & > ( msg_n );
    std::size_t axis_index = cmsg.stepper_index ();
    if ( axis_index < _axes.size () ) {
      _axes[ axis_index ].device_out_track_steps ( cmsg );
    }
    device_out_track_push ( cmsg );
  } break;

  case Type::STEPPER_SYNC: {
    auto & cmsg =
        static_cast< const snc::emb::msg::out::Stepper_Sync & > ( msg_n );
    for ( auto & item : _axes ) {
      item.device_out_track_stepper_sync ( cmsg );
    }
    device_out_track_push ( cmsg );
  } break;

  default: {
    // Use basic message type for tracking
    auto & cmsg = static_cast< const snc::emb::msg::out::Message & > ( msg_n );
    device_out_track_push ( cmsg );
  } break;
  }
}

template < typename MType >
inline void
Service::device_out_track_push ( const MType & msg_n )
{
  _message_tracker.emplace_back< Track_Message< MType > > (
      msg_n, _latest_message_send_time );
}

inline void
Service::update_messages_track_queues_empty ()
{
  _messages_track_queues_empty = _message_tracker.is_empty ();
  if ( !_messages_track_queues_empty ) {
    return;
  }

  for ( auto & axis : _axes ) {
    if ( !axis.steps_queue_empty () ) {
      _messages_track_queues_empty = false;
      break;
    }
  }
}

} // namespace snc::svs::fac::tracker::ship
