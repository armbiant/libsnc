/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/emb/state/controls.hpp>

namespace snc::svs::fac::tracker::ship
{

class Map_Control_I1
{
  public:
  snc::emb::state::Control_Bool const * emb_control = nullptr;
  bool inverted = false;
  snc::device::state::control::handle::I1_Writeable state;
};

} // namespace snc::svs::fac::tracker::ship
