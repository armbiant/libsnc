/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/tracker/ship/service.hpp>

namespace snc::svs::fac::tracker::ship
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

void
Client::set_axis_aligned ( std::uint_fast32_t axis_index_n,
                           bool aligned_n ) const
{
  if ( is_connected () ) {
    service ()->set_axis_aligned ( axis_index_n, aligned_n );
  }
}

snc::svs::fac::tracker::ship::Local_Axis
Client::local_axis_tracker ( std::uint_fast32_t axis_index_n ) const
{
  snc::svs::fac::tracker::ship::Local_Axis track_local;
  track_local.init ( service ()->axis ( axis_index_n ) );
  return track_local;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::tracker::ship
