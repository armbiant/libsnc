/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/rails/rails.hpp>
#include <sev/mem/ring_resizing.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/emb/msg/stream.hpp>
#include <snc/emb/serial/deserializer.hpp>
#include <snc/emb/state/state.hpp>
#include <snc/svs/fac/serial_device/ship/client_track.hpp>
#include <snc/svs/fac/tracker/ship/axis.hpp>
#include <snc/svs/fac/tracker/ship/events.hpp>
#include <snc/svs/fac/tracker/ship/map_controls.hpp>
#include <snc/svs/fac/tracker/ship/map_sensors.hpp>
#include <vector>

namespace snc::svs::fac::tracker::ship
{

// -- Froward declaration
class Sailor;

class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Sailor;

  template < typename MSG >
  struct Track_Message : public MSG
  {
    Track_Message ( const MSG & msg,
                    std::chrono::steady_clock::time_point send_time_n )
    : MSG ( msg )
    , send_time ( std::move ( send_time_n ) )
    {
    }

    std::chrono::steady_clock::time_point send_time;
  };

  public:
  // -- Construction

  Service ( Provider & provider_n,
            const snc::device::handle::Info_Writeable & device_info_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Device statics

  const snc::device::handle::Info_Writeable &
  device_info () const
  {
    return _device_info;
  }

  const snc::device::statics::handle::Statics &
  device_statics () const
  {
    return _device_info->statics ();
  }

  // -- Device state

  const snc::device::state::State &
  device_state () const
  {
    return _device_info->state ();
  }

  snc::device::state::State &
  device_state_ref ()
  {
    return _device_info->state_ref ();
  }

  // -- Tracking state

  const std::shared_ptr< const snc::emb::config::Config > &
  emb_config () const
  {
    return _emb_router_state.config ();
  }

  const snc::emb::state::State &
  emb_router_state () const
  {
    return _emb_router_state;
  }

  const std::chrono::steady_clock::duration &
  latency () const
  {
    return _latency;
  }

  const Axis &
  axis ( std::size_t index_n ) const
  {
    return _axes[ index_n ];
  }

  bool
  messages_track_queues_empty () const
  {
    return _messages_track_queues_empty;
  }

  // -- Cell session inteface

  void
  session_begin ();

  void
  session_abort ();

  void
  session_end ();

  // -- State manipulation interface

  void
  clear_tracker ();

  void
  set_axis_aligned ( std::size_t axis_index_n, bool flag_n );

  void
  set_io_stats ( const snc::serial_device::Stats_IO & stats_n );

  // -- Embedded device message processing interface

  void
  device_in_process_data ( const snc::utility::Data_Tile * tile_n );

  private:
  void
  device_in_track ( const snc::emb::msg::Message & message_n );

  void
  device_in_track_state_axes ( const snc::emb::msg::Message & message_n );

  void
  device_in_track_state_sensors ( const snc::emb::msg::Message & message_n );

  void
  device_in_track_state_switches ( const snc::emb::msg::Message & message_n );

  void
  device_in_update_axes ();

  public:
  void
  device_out_track ( snc::emb::msg::Stream & stream_n, std::size_t offset_n );

  private:
  void
  device_out_track_begin ();

  void
  device_out_track ( const snc::emb::msg::Message & msg_n );

  void
  device_out_track_end ();

  template < typename MType >
  void
  device_out_track_push ( const MType & msg_n );

  // -- Utility

  void
  update_messages_track_queues_empty ();

  protected:
  // -- Utility

  void
  update_device_state ();

  void
  update_device_state_axis ( std::size_t axis_index_n );

  void
  request_device_state_message ();

  private:
  // -- Device information
  snc::device::handle::Info_Writeable _device_info;

  // -- Tracking
  snc::emb::state::State _emb_router_state;
  std::chrono::steady_clock::time_point _latest_message_send_time;
  std::chrono::steady_clock::duration _latency;
  std::uint16_t _latest_message_send_uid = 0;

  sev::mem::rails::Rails _message_tracker;
  std::vector< Map_Sensor_I1 > _map_sensors_i1;
  std::vector< Map_Control_I1 > _map_controls_i1;
  std::vector< Axis > _axes;
  bool _messages_track_queues_empty = true;

  // -- Resources
  snc::svs::fac::serial_device::ship::Client_Track _serial;
  snc::emb::serial::Deserializer _emb_deserializer;

  // -- Office event io
  std::chrono::steady_clock::duration _office_egen_interval;
  sev::event::timer_queue::chrono::Client_Steady _office_egen_timer;
  sev::event::Pool< event::out::Device_State > _event_pool_state;
};

} // namespace snc::svs::fac::tracker::ship
