/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sailor.hpp"

namespace snc::svs::fac::tracker::ship
{

Sailor::Sailor ( const Sailor_Init & init_n )
: Super ( init_n, "Tracker" )
, _service ( *this, init_n.device_info () )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 8, *_service.device_info () );
}

Sailor::~Sailor () {}

void
Sailor::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.session_begin ();
}

void
Sailor::cell_session_abort ()
{
  Super::cell_session_abort ();
  _service.session_abort ();
}

void
Sailor::cell_session_end ()
{
  _service.session_end ();
  Super::cell_session_end ();
}

void
Sailor::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::DEVICE_STATE,
                                                _event_pool_state ) ) {
    event->set_device_info ( *_service.device_info () );
    office_io ().submit ( event );

    // Reset over-/underflow flags
    for ( auto & axis : _service.device_state_ref ().axes () ) {
      axis.set_steps_queue_underrun ( false );
      axis.set_steps_queue_overrun ( false );
    }
  }
}

} // namespace snc::svs::fac::tracker::ship
