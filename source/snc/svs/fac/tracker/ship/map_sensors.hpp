/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/emb/state/sensors.hpp>

namespace snc::svs::fac::tracker::ship
{

class Map_Sensor_I1
{
  public:
  snc::emb::state::Sensor_Bool const * emb_sensor = nullptr;
  bool inverted = false;
  snc::device::state::sensor::handle::I1_Writeable state;
};

} // namespace snc::svs::fac::tracker::ship
