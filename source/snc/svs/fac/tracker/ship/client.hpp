/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/config/config.hpp>
#include <snc/emb/msg/stream.hpp>
#include <snc/svs/fac/base/client.hpp>
#include <snc/svs/fac/tracker/ship/local_axis.hpp>
#include <snc/utility/data_tile.hpp>
#include <memory>

namespace snc::svs::fac::tracker::ship
{

// -- Forward declaration
class Service;

/** @brief Client for device info access */
class Client : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override;

  // -- Interface

  void
  set_axis_aligned ( std::uint_fast32_t axis_index_n, bool aligned_n ) const;

  snc::svs::fac::tracker::ship::Local_Axis
  local_axis_tracker ( std::uint_fast32_t axis_index_n ) const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::tracker::ship
