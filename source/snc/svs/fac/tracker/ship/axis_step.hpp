/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/math/exponents.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::ship
{

class Axis_Step
{
  public:
  // -- Construction and setup
  Axis_Step () = default;

  void
  reset ()
  {
    is_valid = false;
    is_forward = false;
    num_usecs = 0;
    position = 0;
    count = 0;
  }

  void
  invalidate ()
  {
    is_valid = false;
  }

  // -- Interface

  double
  steps_per_second () const
  {
    double res = 0.0;
    if ( is_valid && ( num_usecs != 0 ) ) {
      res = ( sev::math::mega_d / double ( num_usecs ) );
    }
    return res;
  }

  public:
  // -- Attributes
  bool is_valid = false;
  bool is_forward = false;
  std::uint_fast32_t num_usecs = 0;
  std::int64_t position = 0;
  std::uint64_t count = 0;
};

} // namespace snc::svs::fac::tracker::ship
