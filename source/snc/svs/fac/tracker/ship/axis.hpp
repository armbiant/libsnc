/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/ring_resizing.hpp>
#include <snc/device/handle.hpp>
#include <snc/emb/msg/message.hpp>
#include <snc/emb/msg/out/stepper_sync.hpp>
#include <snc/emb/msg/out/steps_u16_64.hpp>
#include <snc/emb/type/step.hpp>
#include <snc/svs/fac/tracker/ship/axis_step.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::ship
{

class Axis
{
  public:
  // -- Types

  struct Step_Track
  {
    std::uint16_t message_uid;
    snc::emb::type::Step job_type;
    std::uint16_t usecs;
  };

  using Steps_Track_List = sev::mem::Ring_Resizing< Step_Track >;

  // -- Construction

  Axis ( const snc::device::statics::Axis & statics_n );

  // -- Tracking session

  void
  session_begin ();

  void
  session_end ();

  // -- Router steps queue size

  bool
  emb_direction_reversed () const
  {
    return _emb_direction_reversed;
  }

  double
  length_per_step () const
  {
    return _length_per_step;
  }

  double
  speed_stoppable () const
  {
    return _speed_stoppable;
  }

  std::uint_fast32_t
  steps_queue_capacity () const
  {
    return _steps_queue_capacity;
  }

  std::size_t
  steps_queue_length () const
  {
    return _steps_track_list.size ();
  }

  std::uint_fast32_t
  steps_queue_length_free () const
  {
    std::uint_fast32_t res = steps_queue_capacity ();
    sev::math::subtract_minimum_zero< std::uint_fast32_t > (
        res, steps_queue_length () );
    return res;
  }

  bool
  steps_queue_full () const
  {
    return ( steps_queue_length_free () == 0 );
  }

  bool
  steps_queue_empty () const
  {
    return ( steps_queue_length () == 0 );
  }

  std::uint_fast32_t
  steps_queue_length_usecs () const
  {
    return _steps_queue_length_usecs;
  }

  // -- Step base position

  std::int64_t
  step_base_position () const
  {
    return _step_base_position;
  }

  void
  set_step_base_position ( std::int64_t pos_n )
  {
    _step_base_position = pos_n;
  }

  // -- Current step

  const Axis_Step &
  step_current () const
  {
    return _step_current;
  }

  const Axis_Step &
  step_current_complete () const
  {
    return _step_current_complete;
  }

  const Axis_Step &
  step_latest () const
  {
    return _step_latest;
  }

  // -- Device in tracking

  std::uint16_t
  device_in_message_uid_acked () const
  {
    return _device_in_message_uid_acked;
  }

  void
  device_in_set_message_uid_acked ( std::uint16_t uid_n )
  {
    _device_in_message_uid_acked = uid_n;
  }

  void
  device_in_queue_length ( std::size_t device_queue_length_n );

  private:
  // -- Utility

  void
  device_in_untrack_step ();

  void
  device_in_current_step_forward_peek ();

  public:
  // -- Device out tracking

  void
  device_out_track_steps ( const snc::emb::msg::out::Steps_U16_64 & message_n );

  void
  device_out_track_stepper_sync (
      const snc::emb::msg::out::Stepper_Sync & message_n );

  private:
  // -- Utility

  void
  device_out_track_steps_each (
      const snc::emb::msg::out::Steps_U16_64 & ajob_n );

  private:
  // -- Attributes
  Steps_Track_List _steps_track_list;
  bool _emb_direction_reversed = false;
  double _length_per_step = 0.0;
  double _speed_stoppable = 0.0;
  std::uint_fast32_t _steps_queue_capacity = 0;
  std::uint_fast32_t _steps_queue_length_usecs = 0;
  std::uint_fast32_t _step_current_num_peeks = 0;
  std::uint16_t _device_in_message_uid_acked = 0;
  std::int64_t _step_base_position = 0;
  /// @brief Step current (acknowledged by the device)
  Axis_Step _step_current;
  /// @brief Step current acknowledged by the device and completed
  Axis_Step _step_current_complete;
  /// @brief Step latest (latest step in the queue)
  Axis_Step _step_latest;
};

} // namespace snc::svs::fac::tracker::ship
