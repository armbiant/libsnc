/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/sailor.hpp>
#include <snc/svs/fac/tracker/ship/service.hpp>

namespace snc::svs::fac::tracker::ship
{

class Sailor : public snc::svp::ship::Sailor
{
  // -- Types
  private:
  using Super = snc::svp::ship::Sailor;

  public:
  struct Office_Message
  {
    static constexpr std::uint_fast32_t DEVICE_STATE = ( 1 << 0 );
  };

  // -- Construction

  Sailor ( const Sailor_Init & init_n );

  ~Sailor ();

  // -- Cell session inteface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  void
  cell_session_end () override;

  // -- Office event processing

  void
  office_notify () override;

  private:
  Service _service;

  // -- Office event io
  sev::event::Pool< event::out::Device_State > _event_pool_state;
};

} // namespace snc::svs::fac::tracker::ship
