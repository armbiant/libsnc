/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/svs/fac/tracker/ship/events.hpp>
#include <snc/svs/fac/tracker/ship/sailor.hpp>

namespace snc::svs::fac::tracker::office
{

Clerk::Clerk ( const Clerk_Init & init_n,
               const snc::device::statics::handle::Statics & device_statics_n )
: Super ( init_n, "Sailor-Tracker" )
, _service ( *this, device_statics_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( *_service.device_info_root_handle () );
  }
}

void
Clerk::bridge_session_begin ()
{
  Super::bridge_session_begin ();

  _service.session_begin ();
  _service.reset_device_state ();

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  pick_n ( make_sailor_factory< snc::svs::fac::tracker::ship::Sailor > () );
}

void
Clerk::bridge_session_end ()
{
  _service.reset_device_state ();
  _service.session_end ();

  dash_io ().notification_set ( Dash_Message::STATE );

  Super::bridge_session_end ();
}

void
Clerk::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::Type::DEVICE_STATE:
    bridge_event_device_state ( event_n );
    break;
  default:
    break;
  }
}

void
Clerk::bridge_event_device_state ( Bridge_Event & event_n )
{
  {
    auto & cevent =
        static_cast< const ship::event::out::Device_State & > ( event_n );
    _service.update_device_state ( cevent.device_info () );
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

} // namespace snc::svs::fac::tracker::office
