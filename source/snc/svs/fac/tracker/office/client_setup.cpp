/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_setup.hpp"
#include <snc/svs/fac/tracker/office/clerk.hpp>

namespace snc::svs::fac::tracker::office
{

Client_Setup::Client_Setup ( User * user_n )
: Super ( user_n )
{
}

Client_Setup::~Client_Setup () = default;

void
Client_Setup::set_statics (
    const snc::device::statics::handle::Statics & statics_n ) const
{
  if ( is_connected () ) {
    service ()->set_statics ( statics_n );
  }
}

void
Client_Setup::reset_statics () const
{
  if ( is_connected () ) {
    service ()->reset_statics ();
  }
}

} // namespace snc::svs::fac::tracker::office
