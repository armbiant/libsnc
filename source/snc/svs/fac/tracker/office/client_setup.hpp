/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/svs/fac/tracker/office/client.hpp>

namespace snc::svs::fac::tracker::office
{

/** @brief Client to setup the tracker */
class Client_Setup : public Client
{
  // -- Types
  private:
  using Super = Client;

  public:
  // -- Construction

  Client_Setup ( User * user_n );

  ~Client_Setup () override;

  // -- Interface

  void
  set_statics ( const snc::device::statics::handle::Statics & statics_n ) const;

  void
  reset_statics () const;
};

} // namespace snc::svs::fac::tracker::office
