/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/device/handle.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/tracker/dash/events.hpp>
#include <snc/svs/fac/tracker/office/service.hpp>

namespace snc::svs::fac::tracker::office
{

class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  public:
  /// @brief Front message requests
  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n,
          const snc::device::statics::handle::Statics & device_statics_n );

  ~Clerk () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_end () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_device_state ( Bridge_Event & event_n );

  private:
  Service _service;
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::fac::tracker::office
