/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/svs/fac/base/client.hpp>

namespace snc::svs::fac::tracker::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override;

  // -- Interface

  snc::device::handle::Info
  device_info_root_handle () const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  connecting () override;
};

} // namespace snc::svs::fac::tracker::office
