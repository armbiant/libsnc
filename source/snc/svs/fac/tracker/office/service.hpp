/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/svs/fac/base/service.hpp>

namespace snc::svs::fac::tracker::office
{

// -- Forward declaration
class Clerk;

/** @brief Service */
class Service : public snc::svs::fac::base::Service
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Service;
  using Provider = Clerk;

  public:
  // -- Construction

  Service ( Provider & provider_n,
            const snc::device::statics::handle::Statics & device_statics_n );

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  snc::device::handle::Info
  device_info_root_handle () const
  {
    return _device_info_root;
  }

  // -- Interface

  void
  set_statics ( snc::device::statics::handle::Statics statics_n );

  void
  reset_statics ();

  void
  reset_device_state ();

  void
  update_device_state ( const snc::device::Info & device_info_n );

  private:
  snc::device::handle::Info_Writeable _device_info_root;
};

} // namespace snc::svs::fac::tracker::office
