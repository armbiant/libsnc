/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>

namespace snc::svs::fac::base
{

// -- Forward declaration
class Service;

/** @brief Factor axis service base class */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override;

  // -- Session

  Service *
  service_base () const;

  bool
  session_is_good () const;

  // -- Device info

  const snc::device::handle::Info &
  device_info_handle () const;

  const auto &
  device_info () const
  {
    return *device_info_handle ();
  }

  // -- Device statics

  const auto &
  device_statics () const
  {
    return *device_info_handle ()->statics ();
  }

  const auto &
  device_statics_handle () const
  {
    return device_info_handle ()->statics ();
  }

  // -- Device state

  const auto &
  device_state () const
  {
    return device_info_handle ()->state ();
  }
};

} // namespace snc::svs::fac::base
