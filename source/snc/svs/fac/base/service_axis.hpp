/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/axis_info.hpp>
#include <snc/svs/fac/base/service.hpp>
#include <snc/svs/fac/machine_startup/office/client.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
}

namespace snc::svs::fac::base
{

/** @brief Factor axis service base class */
class Service_Axis : public Service
{
  // -- Types
  private:
  using Super = Service;

  public:
  using Provider_Factor = snc::svp::office::Factor;

  // -- Construction

  Service_Axis ( Provider_Factor & provider_n,
                 std::uint_fast32_t axis_index_n );

  ~Service_Axis ();

  // -- Accessors

  /// @brief Axis to control
  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  const auto &
  axis_info () const
  {
    return _axis_info;
  }

  // -- Session

  void
  session_begin () override;

  void
  session_end () override;

  // -- Client control interface

  bool
  control_acquirable () const override;

  private:
  const std::uint_fast32_t _axis_index = 0;
  snc::device::Axis_Info _axis_info;
  snc::svs::fac::machine_startup::office::Client _machine_startup;
};

} // namespace snc::svs::fac::base
