/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/base/service.hpp>

namespace snc::svs::fac::base
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

Service *
Client::service_base () const
{
  return dynamic_cast< Service * > ( service_abstract () );
}

const snc::device::handle::Info &
Client::device_info_handle () const
{
  return is_connected () ? service_base ()->device_info_handle ()
                         : snc::svs::fac::base::Service::device_info_fallback;
}

bool
Client::session_is_good () const
{
  return is_connected () ? service_base ()->session_is_good () : false;
}

} // namespace snc::svs::fac::base
