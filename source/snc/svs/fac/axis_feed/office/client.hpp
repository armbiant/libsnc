/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/control_unique.hpp>
#include <snc/device/axis_info.hpp>
#include <snc/svs/ctl/base/client_axis.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>
#include <snc/svs/fac/axis_feed/office/state.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_feed::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::ctl::base::Client_Axis
{
  // -- Types
  private:
  using Super = snc::svs::ctl::base::Client_Axis;

  public:
  // -- Construction

  Client ( User * user_n, std::uint_fast32_t axis_index_n );

  ~Client ();

  // -- Service state interface

  bool
  session_is_good () const;

  snc::device::Axis_Info
  axis_info () const;

  bool
  is_active () const;

  bool
  is_potentially_feeding () const;

  State
  acquire_state () const;

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client *
    client () const
    {
      return static_cast< Client * > ( client_abstract () );
    }

    // -- Control interface

    /// @brief Break feeding at the maximum acceleration
    bool
    break_hard ();

    /// @brief Break feeding using the current mode's acceleration
    bool
    break_normal ();

    /// @brief Set feeding mode
    bool
    set_mode ( const Mode & mode_n );
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::axis_feed::office
