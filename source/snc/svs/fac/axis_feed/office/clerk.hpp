/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/axis_feed/dash/events.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>
#include <snc/svs/fac/axis_feed/office/service.hpp>
#include <snc/svs/fac/axis_feed/office/state.hpp>
#include <snc/svs/fac/axis_feed/ship/events.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <memory>

namespace snc::svs::fac::axis_feed::office
{

// -- Forward declaration
class Service;

/** @brief Clerk */
class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  struct Bridge_Message
  {
    static constexpr std::uint_fast32_t MODE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n,
          std::uint_fast32_t axis_index_n,
          std::uint_fast32_t sensor_index_n );

  ~Clerk ();

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_begin_sailor () override;

  void
  bridge_session_abort () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_state ( Bridge_Event & event_n );

  // -- Central processing

  void
  process () override;

  private:
  // -- Service interface
  friend snc::svs::fac::axis_feed::office::Service;

  void
  bridge_notify_mode ( const Mode & mode_n );

  void
  state_changed ();

  private:
  // -- Resources
  Service _service;
  std::uint_fast32_t _sensor_index = 0;
  // -- State
  State _state_emitted;
  // -- Bridge event io
  sev::event::Pool< ship::event::in::Mode > _event_pool_bridge_mode;
  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::fac::axis_feed::office
