/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/axis_feed/office/service.hpp>

namespace snc::svs::fac::axis_feed::office
{

Client::Client ( User * user_n, std::uint_fast32_t axis_index_n )
: Super ( user_n, axis_index_n )
{
}

Client::~Client () = default;

bool
Client::session_is_good () const
{
  return is_connected () ? service ()->session_is_good () : false;
}

snc::device::Axis_Info
Client::axis_info () const
{
  return is_connected () ? service ()->axis_info () : snc::device::Axis_Info ();
}

bool
Client::is_active () const
{
  return is_connected () ? !service ()->state ().is_active () : true;
}

bool
Client::is_potentially_feeding () const
{
  return is_connected () ? service ()->state ().is_potentially_feeding ()
                         : false;
}

State
Client::acquire_state () const
{
  return is_connected () ? service ()->state () : State ();
}

bool
Client::Control::break_hard ()
{
  return is_valid () ? client ()->service ()->control_break_hard () : false;
}

bool
Client::Control::break_normal ()
{
  return is_valid () ? client ()->service ()->control_break_normal () : false;
}

bool
Client::Control::set_mode ( const Mode & mode_n )
{
  return is_valid () ? client ()->service ()->control_set_mode ( mode_n )
                     : false;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return connect_to_axis< Service > ( sb_n );
}

} // namespace snc::svs::fac::axis_feed::office
