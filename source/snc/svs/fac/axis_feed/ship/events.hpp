/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>
#include <snc/svs/fac/axis_feed/ship/state.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_feed::ship::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t MODE = 0;
};

using In = snc::svp::ship::event::In;

class Mode : public In
{
  public:
  static constexpr auto etype = Type::MODE;

  Mode ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _mode.reset ();
  }

  // -- Mode

  const snc::svs::fac::axis_feed::Mode &
  mode () const
  {
    return _mode;
  }

  void
  set_mode ( snc::svs::fac::axis_feed::Mode req_n )
  {
    _mode = req_n;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_feed::Mode _mode;
};

} // namespace snc::svs::fac::axis_feed::ship::event::in

namespace snc::svs::fac::axis_feed::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::ship::event::out;

/// @brief Pilot state event
using State = State_T< Type::STATE, State >;

} // namespace snc::svs::fac::axis_feed::ship::event::out
