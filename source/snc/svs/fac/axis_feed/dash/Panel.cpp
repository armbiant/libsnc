/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/fac/axis_feed/dash/events.hpp>
#include <snc/svs/fac/axis_feed/office/clerk.hpp>

namespace snc::svs::fac::axis_feed::dash
{

Panel::Panel ( const Panel_Init & init_n,
               std::uint_fast32_t axis_index_n,
               std::uint_fast32_t sensor_index_n )
: Super ( init_n, sev::string::cat ( "Pilot-Feed-Axis[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
, _sensor_index ( sensor_index_n )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::axis_feed::office::Clerk,
                             std::uint_fast32_t,
                             std::uint_fast32_t > ( axis_index (),
                                                    sensor_index () );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.active, state ().is_active () ) ) {
    emit activeChanged ();
  }
  if ( sev::change ( _qtState.feeding, state ().is_feeding () ) ) {
    emit feedingChanged ();
  }
  {
    Mode c_mode = Mode::NONE;
    {
      using PMode = snc::svs::fac::axis_feed::Mode::Dir;
      switch ( state ().mode ().dir () ) {
      case PMode::NONE:
        c_mode = Mode::NONE;
        break;
      case PMode::FORWARD:
        c_mode = Mode::FORWARD;
        break;
      case PMode::BACKWARD:
        c_mode = Mode::BACKWARD;
        break;
      }
    }
    if ( sev::change ( _qtState.mode, c_mode ) ) {
      emit modeChanged ();
    }
  }
}

} // namespace snc::svs::fac::axis_feed::dash
