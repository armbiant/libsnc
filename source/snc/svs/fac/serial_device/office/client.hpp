/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/serial_device/letter.hpp>
#include <memory>

namespace snc::svs::fac::serial_device::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  using Device_Letter_Handle = std::shared_ptr< snc::serial_device::Letter >;

  // -- Construction

  Client ( User * user_n );

  ~Client ();

  // -- Interface

  Device_Letter_Handle
  serial_device () const;

  void
  set_serial_device ( const Device_Letter_Handle & serial_device_n );

  void
  clear_serial_device ();

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::serial_device::office
