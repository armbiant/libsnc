/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/serial_device/office/service.hpp>

namespace snc::svs::fac::serial_device::office
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

Client::Device_Letter_Handle
Client::serial_device () const
{
  if ( is_connected () ) {
    return service ()->serial_device ();
  }
  return {};
}

void
Client::set_serial_device ( const Device_Letter_Handle & serial_device_n )
{
  if ( is_connected () ) {
    service ()->set_serial_device ( serial_device_n );
  }
}

void
Client::clear_serial_device ()
{
  if ( is_connected () ) {
    service ()->set_serial_device ( {} );
  }
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::serial_device::office
