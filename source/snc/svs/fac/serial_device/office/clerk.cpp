/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/serial_device/ship/sailor.hpp>

namespace snc::svs::fac::serial_device::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Serial-Device" )
, _service ( *this )
, _ship_session ( this )
{
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _ship_session.connect_required ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  pick_n (
      make_sailor_factory< snc::svs::fac::serial_device::ship::Sailor,
                           std::shared_ptr< snc::serial_device::Letter > > (
          _service.serial_device () ) );
}

void
Clerk::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::Type::DEVICE_ERROR:
    bridge_event_device_error ( event_n );
    break;
  default:
    Super::bridge_event ( event_n );
    break;
  }
}

void
Clerk::bridge_event_device_error ( Bridge_Event & event_n )
{
  auto & cevent =
      static_cast< const ship::event::out::Device_Error & > ( event_n );
  (void)cevent;

  _ship_session.abort ( true );
}

} // namespace snc::svs::fac::serial_device::office
