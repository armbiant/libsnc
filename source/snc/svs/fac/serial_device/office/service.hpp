/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/serial_device/letter.hpp>
#include <memory>

namespace snc::svs::fac::serial_device::office
{

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  public:
  using Device_Letter_Handle = std::shared_ptr< snc::serial_device::Letter >;

  // -- Construction

  Service ( Provider_Cell & provider_n );

  // -- Accessors

  const Device_Letter_Handle &
  serial_device () const
  {
    return _serial_device;
  }

  // -- Client interface

  void
  set_serial_device ( const Device_Letter_Handle & serial_device_n );

  private:
  // -- Attributes
  Device_Letter_Handle _serial_device;
};

} // namespace snc::svs::fac::serial_device::office
