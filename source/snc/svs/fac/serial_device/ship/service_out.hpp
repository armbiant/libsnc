/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/emb/msg/stream.hpp>
#include <snc/utility/subscription_list.hpp>

// -- Forward declaration
namespace snc::svp::ship
{
class Pilot_Emb;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Sailor;

class Service_Out : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Sailor;

  public:
  // -- Construction

  Service_Out ( Provider & provider_n );

  ~Service_Out ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Client interface

  void
  subscribe ( snc::svp::ship::Pilot_Emb * pilot_n, bool subscribe_n );

  void
  request_pick_up ();

  // -- Provider interface

  void
  ensure_minimum_capacity ( std::size_t size_n );

  void
  pick_up ( snc::emb::msg::Stream & stream_n );

  private:
  // -- Resources
  snc::utility::Subscription_List< snc::svp::ship::Pilot_Emb > _pilots;
};

} // namespace snc::svs::fac::serial_device::ship
