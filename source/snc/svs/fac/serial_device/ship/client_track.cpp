/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_track.hpp"
#include <snc/svs/fac/serial_device/ship/service_track.hpp>

namespace snc::svs::fac::serial_device::ship
{

Client_Track::Client_Track ( User * user_n )
: Super ( user_n )
{
}

Client_Track::~Client_Track () = default;

Service_Track *
Client_Track::service () const
{
  return static_cast< Service_Track * > ( service_abstract () );
}

bool
Client_Track::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service_Track * > ( sb_n ) != nullptr );
}

void
Client_Track::disconnecting ()
{
  if ( is_connected () ) {
    service ()->clear_callbacks ();
  }
  Super::disconnecting ();
}

void
Client_Track::set_io_stats_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                   IO_Stats_Callback cback_n )
{
  if ( is_connected () ) {
    service ()->set_io_stats_cback ( service_n, cback_n );
  }
}

void
Client_Track::set_data_in_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                  Data_In_Callback cback_n )
{
  if ( is_connected () ) {
    service ()->set_data_in_cback ( service_n, cback_n );
  }
}

void
Client_Track::set_emb_out_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                  Emb_Out_Callback cback_n )
{
  if ( is_connected () ) {
    service ()->set_emb_out_cback ( service_n, cback_n );
  }
}

void
Client_Track::set_abort_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                Abort_Callback cback_n )
{
  if ( is_connected () ) {
    service ()->set_abort_cback ( service_n, cback_n );
  }
}

} // namespace snc::svs::fac::serial_device::ship
