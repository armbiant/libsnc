/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_out.hpp"
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/serial_device/ship/service_out.hpp>

namespace snc::svs::fac::serial_device::ship
{

Client_Out::Client_Out ( User * user_n )
: Super ( user_n )
{
}

Client_Out::~Client_Out () = default;

void
Client_Out::subscribe ( bool flag_n )
{
  if ( is_connected () ) {
    if ( auto * pilot =
             dynamic_cast< snc::svp::ship::Pilot_Emb * > ( user () ) ) {
      service ()->subscribe ( pilot, flag_n );
    }
  }
}

void
Client_Out::request_pick_up ()
{
  if ( is_connected () ) {
    service ()->request_pick_up ();
  }
}

Service_Out *
Client_Out::service () const
{
  return static_cast< Service_Out * > ( service_abstract () );
}

bool
Client_Out::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service_Out * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::serial_device::ship
