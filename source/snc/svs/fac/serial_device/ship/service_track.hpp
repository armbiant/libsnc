/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>

// -- Forward declaration
namespace snc::serial_device
{
class Stats_IO;
}
namespace snc::utility
{
class Data_Tile;
}
namespace snc::emb::msg
{
class Stream;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Sailor;

class Service_Track : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Sailor;
  using IO_Stats_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   const snc::serial_device::Stats_IO & stats_n );
  using Data_In_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   const snc::utility::Data_Tile * tile_n );
  using Emb_Out_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   snc::emb::msg::Stream & stream_n,
                   std::size_t offset_n );
  using Abort_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n );

  public:
  // -- Construction

  Service_Track ( Provider & provider_n );

  ~Service_Track ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Provider interface

  void
  set_io_stats ( const snc::serial_device::Stats_IO & stats_n );

  void
  data_in ( const snc::utility::Data_Tile * tile_n );

  void
  emb_out ( snc::emb::msg::Stream & stream_n, std::size_t offset_n );

  void
  abort ();

  // -- Client interface

  void
  clear_callbacks ();

  void
  set_io_stats_cback ( snc::bat::ifc::Service_Abstract * service_n,
                       IO_Stats_Callback cback_n );

  void
  set_data_in_cback ( snc::bat::ifc::Service_Abstract * service_n,
                      Data_In_Callback cback_n );

  void
  set_emb_out_cback ( snc::bat::ifc::Service_Abstract * service_n,
                      Emb_Out_Callback cback_n );

  void
  set_abort_cback ( snc::bat::ifc::Service_Abstract * service_n,
                    Abort_Callback cback_n );

  private:
  struct
  {
    snc::bat::ifc::Service_Abstract * service = nullptr;
    IO_Stats_Callback callback = nullptr;
  } _io_stats;
  struct
  {
    snc::bat::ifc::Service_Abstract * service = nullptr;
    Data_In_Callback callback = nullptr;
  } _data_in;
  struct
  {
    snc::bat::ifc::Service_Abstract * service = nullptr;
    Emb_Out_Callback callback = nullptr;
  } _emb_out;
  struct
  {
    snc::bat::ifc::Service_Abstract * service = nullptr;
    Abort_Callback callback = nullptr;
  } _abort;
};

} // namespace snc::svs::fac::serial_device::ship
