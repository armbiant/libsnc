/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/client.hpp>

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Service_Out;

/** @brief Client */
class Client_Out : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  // -- Construction

  Client_Out ( User * user_n );

  ~Client_Out () override;

  // -- Interface

  void
  subscribe ( bool flag_n = true );

  void
  request_pick_up ();

  // -- Service connection
  protected:
  Service_Out *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::serial_device::ship
