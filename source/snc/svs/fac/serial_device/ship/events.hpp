/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>

namespace snc::svs::fac::serial_device::ship::event::in
{
} // namespace snc::svs::fac::serial_device::ship::event::in

namespace snc::svs::fac::serial_device::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t DEVICE_ERROR = 0;
};

using Out = snc::svp::ship::event::Out;

class Device_Error : public Out
{
  public:
  static constexpr auto etype = Type::DEVICE_ERROR;

  Device_Error ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

} // namespace snc::svs::fac::serial_device::ship::event::out
