/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/serial_device/stats_io.hpp>
#include <snc/svs/fac/base/client.hpp>
#include <cstddef>

// -- Forward declaration
namespace snc::bat::ifc
{
class Service_Abstract;
}
namespace snc::serial_device
{
class Stats_IO;
}
namespace snc::utility
{
class Data_Tile;
}
namespace snc::emb::msg
{
class Stream;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Service_Track;

/** @brief Client */
class Client_Track : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  using IO_Stats_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   const snc::serial_device::Stats_IO & stats_n );
  using Data_In_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   const snc::utility::Data_Tile * tile_n );
  using Emb_Out_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n,
                   snc::emb::msg::Stream & stream_n,
                   std::size_t offset_n );
  using Abort_Callback =
      void ( * ) ( snc::bat::ifc::Service_Abstract * service_n );

  // -- Construction

  Client_Track ( User * user_n );

  ~Client_Track () override;

  // -- Interface

  void
  set_io_stats_cback ( snc::bat::ifc::Service_Abstract * service_n,
                       IO_Stats_Callback cback_n );

  void
  set_data_in_cback ( snc::bat::ifc::Service_Abstract * service_n,
                      Data_In_Callback cback_n );

  void
  set_emb_out_cback ( snc::bat::ifc::Service_Abstract * service_n,
                      Emb_Out_Callback cback_n );

  void
  set_abort_cback ( snc::bat::ifc::Service_Abstract * service_n,
                    Abort_Callback cback_n );

  // -- Service connection
  protected:
  Service_Track *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  disconnecting () override;
};

} // namespace snc::svs::fac::serial_device::ship
