/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sailor.hpp"
#include <snc/svs/fac/tracker/ship/sailor.hpp>
#include <algorithm>
#include <cmath>

namespace snc::svs::fac::serial_device::ship
{

Sailor::Sailor (
    const Sailor_Init & init_n,
    const std::shared_ptr< snc::serial_device::Letter > & serial_device_n )
: Super ( init_n, "Sailor-Serial-Device" )
, _service_track ( *this )
, _service_out ( *this )
, _event_pool_device_error ( office_io ().epool_tracker () )
{
  _event_accu_async.set_callback ( processing_request_async () );
  _event_accu.set_callback ( processing_request () );

  // Emb_Device
  if ( auto * letter_usb =
           dynamic_cast< snc::serial_device::Letter_USB const * > (
               serial_device_n.get () ) ) {
    _dev.device_usb.emplace ( *letter_usb );
    _dev.device = &_dev.device_usb.value ();
  } else {
    throw std::runtime_error ( "Unknown serial device type" );
  }

  // Emb_Device connection setup
  _dev.connection.set_incoming_notifier (
      [ &ec = _event_accu_async ] () { ec.set ( LEvent::DEVICE_IN ); } );
  _dev.connection.set_push_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::DEVICE_OUT ); } );

  // Emb_Device event allocation
  _dev.epools.pool< snc::serial_device::event::in::Data > ().set_size (
      write_tiles_max );
  _dev.epools.pool< snc::serial_device::event::in::Abort_Stop_IO > ().set_size (
      1 );
  _dev.epools.pool< snc::serial_device::event::in::Abort_Shut_Down > ()
      .set_size ( 1 );

  // Emb_Device serial data tiles
  _emb.tile_owner = std::make_unique< Write_Tile[] > ( write_tiles_max );
  for ( std::size_t ii = 0; ii != write_tiles_max; ++ii ) {
    _emb.serializer.free_tile_push ( &_emb.tile_owner[ ii ] );
  }

  // Emb_Device messages buffer
  _emb.messages_out.buffer ().set_rail_capacity ( 1024 * 1024 );
  _emb.messages_out.buffer ().set_pool_capacity ( 4 );

  // Statistics timer
  _timer_stats.set_duration ( std::chrono::milliseconds ( 500 ) );
  _timer_stats.set_callback ( [ this ] () { this->timeout_statistics (); } );
  _timer_stats.connect ( cell_context ().timer_queue () );

  // Allocate offic events
  office_io ().allocate ( _event_pool_device_error, 2 );
}

Sailor::~Sailor () = default;

void
Sailor::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Update capacities
  _service_out.ensure_minimum_capacity ( cell_context ().cells ().size () );

  // Open Emb-Device
  {
    {
      auto queue_emb_device = sev::event::queue_io::Reference_2::created ();
      _dev.connection.connect ( queue_emb_device.link_a () );
      snc::serial_device::Context context;
      context.log_parent = log ();
      context.thread_tracker = cell_context ().thread_tracker ();
      context.link = queue_emb_device.link_b ();
      _dev.device->open ( std::move ( context ) );
    }

    if ( _dev.device->is_open () ) {
      _dev.run_state = Dev::Run_State::RUNNING;
      // Begin transfer statistics
      _dev.io_stats.begin_frame ();

      log ().cat ( sev::logt::FL_DEBUG_0, "Device opened." );
    } else {
      log ().cat ( sev::logt::FL_DEBUG_0, "Device opening failed." );
    }
  }

  // Start statistics timer
  _timer_stats.start_periodical_now ();
}

void
Sailor::cell_session_abort ()
{
  Super::cell_session_abort ();
  device_abort ();
}

bool
Sailor::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () &&
         ( _dev.run_state == Dev::Run_State::NONE ) &&
         ( _dev.abort_state == Dev::Abort_State::NONE ) &&
         _dev.connection.all_empty () && _dev.epools.tracker ().all_home ();
}

void
Sailor::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::DEVICE_ERROR,
                                                _event_pool_device_error ) ) {
    office_io ().submit ( event );
  }
}

void
Sailor::device_abort ()
{
  if ( _dev.run_state != Dev::Run_State::RUNNING ) {
    return;
  }

  // Aborting
  log ().cat ( sev::logt::FL_DEBUG_0, "Device abort!" );

  // Switch to abort state
  _dev.run_state = Dev::Run_State::ABORTING;
  _dev.abort_state = Dev::Abort_State::BEGIN;

  // Clear messages buffer
  _emb.messages_out.clear ();
  // Stop statistics timer
  _timer_stats.stop ();
  // Clear tracker
  _service_track.abort ();
}

void
Sailor::timeout_statistics ()
{
  if ( _dev.run_state != Dev::Run_State::RUNNING ) {
    return;
  }

  // Send statistics
  _dev.io_stats.end_frame ();
  _service_track.set_io_stats ( _dev.io_stats );
  _dev.io_stats.begin_next_frame ();
}

void
Sailor::pilot_messages_request ()
{
  _event_accu.set ( LEvent::DEVICE_GENERATE );
}

void
Sailor::process_async ()
{
  _event_accu.set_silent ( _event_accu_async.fetch_and_clear () );
  process ();
}

void
Sailor::process ()
{
  // Maximum number of inner loop repeats
  int semaphore = 4;
  while ( true ) {
    // Emb_Device event io
    if ( _event_accu.test_any ( LEvent::DEVICE_IN | LEvent::DEVICE_OUT ) ) {
      process_dev_io ();
    }

    switch ( _dev.run_state ) {
    case Dev::Run_State::RUNNING:
      process_emb_feed ();
      break;

    case Dev::Run_State::ABORTING:
      process_dev_abort ();
      break;

    default:
      break;
    };

    // Loop break or repeat
    if ( _event_accu.is_empty () ) {
      break;
    }
    if ( semaphore == 0 ) {
      _event_accu.notify_not_empty ();
      break;
    }
    --semaphore;
  }
}

void
Sailor::process_dev_io ()
{
  // Feed device event queue
  _dev.connection.feed_queue (
      _event_accu.flags (), LEvent::DEVICE_IN, LEvent::DEVICE_OUT );

  // Release returned events
  _dev.connection.out ().release_all ();

  // Process device events
  bool any = _dev.connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        using Type = snc::serial_device::event::out::Type;
        switch ( event_n.type () ) {
        case Type::DATA_IN:
          process_dev_event_data_in ( event_n );
          break;
        case Type::DATA_SENT:
          process_dev_event_data_sent ( event_n );
          break;
        case Type::DEVICE_ERROR:
          process_dev_event_error ( event_n );
          break;
        case Type::ABORT_STOP_IO_DONE:
          process_dev_event_stop_io_done ( event_n );
          break;
        case Type::ABORT_SHUT_DOWN_DONE:
          process_dev_event_shut_down_done ( event_n );
          break;
        default:
          DEBUG_ASSERT ( false );
        }
      } );
  if ( !any ) {
    return;
  }

  // Request pilot message generation
  _event_accu.set_silent ( LEvent::DEVICE_GENERATE );
}

void
Sailor::process_dev_event_data_in ( const sev::event::Event & event_n )
{
  auto & cevent =
      static_cast< const snc::serial_device::event::out::Data_In & > (
          event_n );
  // Serial io statistics
  _dev.io_stats.in ().increment_size_frame ( cevent.data_tile ()->size () );
  // Tracking
  _service_track.data_in ( cevent.data_tile () );
}

void
Sailor::process_dev_event_data_sent ( const sev::event::Event & event_n )
{
  auto & cevent =
      static_cast< const snc::serial_device::event::out::Data_Sent & > (
          event_n );
  // Serial io statistics
  _dev.io_stats.out ().increment_size_frame ( cevent.data_tile ()->size () );
  // Release tile
  _emb.serializer.free_tile_push ( cevent.data_tile () );
}

void
Sailor::process_dev_event_error ( const sev::event::Event & event_n
                                  [[maybe_unused]] )
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Device error" );

  device_abort ();
  // Request office message
  office_io ().notification_set ( Office_Message::DEVICE_ERROR );
}

void
Sailor::process_dev_event_stop_io_done ( const sev::event::Event & event_n
                                         [[maybe_unused]] )
{
  DEBUG_ASSERT ( _dev.run_state == Dev::Run_State::ABORTING );
  DEBUG_ASSERT ( _dev.abort_state == Dev::Abort_State::STOP_IO_SENT );
  _dev.abort_state = Dev::Abort_State::STOP_IO_DONE;

  log ().cat ( sev::logt::FL_DEBUG_0, "Device abort: Stop IO done." );
}

void
Sailor::process_dev_event_shut_down_done ( const sev::event::Event & event_n
                                           [[maybe_unused]] )
{
  DEBUG_ASSERT ( _dev.run_state == Dev::Run_State::ABORTING );
  DEBUG_ASSERT ( _dev.abort_state == Dev::Abort_State::SHUT_DOWN_SENT );
  _dev.abort_state = Dev::Abort_State::SHUT_DOWN_DONE;

  log ().cat ( sev::logt::FL_DEBUG_0, "Device abort: Shut down done." );
}

void
Sailor::process_dev_abort ()
{
  // Clear event bits
  _event_accu.unset ( LEvent::DEVICE_GENERATE );

  // Send stop IO
  if ( _dev.abort_state == Dev::Abort_State::BEGIN ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Emb-Device abort: Sending stop IO request." );
    _dev.connection.push (
        _dev.epools.pool< snc::serial_device::event::in::Abort_Stop_IO > ()
            .acquire () );
    _dev.abort_state = Dev::Abort_State::STOP_IO_SENT;
    return;
  }

  // Abort
  if ( _dev.abort_state == Dev::Abort_State::STOP_IO_DONE ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Emb-Device abort: Sending shut down request." );
    _dev.connection.push (
        _dev.epools.pool< snc::serial_device::event::in::Abort_Shut_Down > ()
            .acquire () );
    _dev.abort_state = Dev::Abort_State::SHUT_DOWN_SENT;
    return;
  }

  // Check closing conditions
  if ( _dev.abort_state != Dev::Abort_State::SHUT_DOWN_DONE ) {
    return;
  }
  if ( !_dev.connection.all_empty () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Emb-Device abort: Waiting for unprocessed events." );
    return;
  }
  if ( !_dev.epools.tracker ().all_home () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Emb-Device abort: Waiting for event return." );
    return;
  }

  // Close device
  _dev.device->close ();
  _dev.connection.disconnect ();
  _dev.run_state = Dev::Run_State::NONE;
  _dev.abort_state = Dev::Abort_State::NONE;
  _dev.device = nullptr;
  _dev.device_usb.reset ();

  log ().cat ( sev::logt::FL_DEBUG_0, "Emb-Device abort: Device closed." );
}

void
Sailor::process_emb_feed ()
{
  // Messages generation
  if ( _event_accu.test_any_unset ( LEvent::DEVICE_GENERATE ) ) {
    const std::size_t size_begin = _emb.messages_out.size ();
    // Generate messages
    _service_out.pick_up ( _emb.messages_out );
    // Track messages
    _service_track.emb_out ( _emb.messages_out, size_begin );
  }

  // Messages serialization
  if ( !_emb.messages_out.is_empty () ) {
    //  Serialize as many messages as possible
    if ( _emb.serializer.serialize_begin () ) {
      while ( !_emb.messages_out.is_empty () ) {
        if ( !_emb.serializer.serialize ( _emb.messages_out.front () ) ) {
          break;
        }
        _emb.messages_out.pop_front ();
      }
      _emb.serializer.serialize_end ();
    }
  }

  // Serial data submission
  {
    // Send filled data tiles to device
    auto & epool = _dev.epools.pool< snc::serial_device::event::in::Data > ();
    while ( !epool.is_empty () && _emb.serializer.filled_tile_available () ) {
      auto * event = epool.pop_not_empty ();
      event->set_data_tile ( _emb.serializer.filled_tile_pop () );
      _dev.connection.push ( event );
    }
  }
}

} // namespace snc::svs::fac::serial_device::ship
