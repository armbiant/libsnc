/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/event/tracker_pools.hpp>
#include <snc/emb/msg/stream.hpp>
#include <snc/emb/serial/serializer.hpp>
#include <snc/serial_device/device.hpp>
#include <snc/serial_device/events.hpp>
#include <snc/serial_device/letter.hpp>
#include <snc/serial_device/stats_io.hpp>
#include <snc/serial_device/usb/device.hpp>
#include <snc/svp/ship/sailor.hpp>
#include <snc/svs/fac/serial_device/ship/events.hpp>
#include <snc/svs/fac/serial_device/ship/service_out.hpp>
#include <snc/svs/fac/serial_device/ship/service_track.hpp>
#include <snc/utility/data_tile.hpp>

namespace snc::svs::fac::serial_device::ship
{

class Sailor : public snc::svp::ship::Sailor
{
  public:
  // -- Static variables

  static constexpr std::size_t write_tiles_max = 16;
  static constexpr std::size_t write_tile_size = 512;

  // -- Types
  private:
  using Super = snc::svp::ship::Sailor;

  struct LEvent
  {
    static constexpr std::uint_fast32_t DEVICE_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t DEVICE_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t DEVICE_GENERATE = ( 1 << 3 );
  };

  public:
  struct Office_Message
  {
    static constexpr std::uint8_t DEVICE_ERROR = ( 1 << 0 );
  };

  using Write_Tile = snc::utility::Data_Tile_N< write_tile_size >;

  // -- Construction

  Sailor (
      const Sailor_Init & init_n,
      const std::shared_ptr< snc::serial_device::Letter > & serial_device_n );

  ~Sailor ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Office event processing

  void
  office_notify () override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  void
  process_dev_io ();

  void
  process_dev_event_data_in ( const sev::event::Event & event_n );

  void
  process_dev_event_data_sent ( const sev::event::Event & event_n );

  void
  process_dev_event_error ( const sev::event::Event & event_n );

  void
  process_dev_event_stop_io_done ( const sev::event::Event & event_n );

  void
  process_dev_event_shut_down_done ( const sev::event::Event & event_n );

  void
  process_emb_feed ();

  void
  process_dev_abort ();

  // -- Interface

  /// @brief Request device message generation
  void
  pilot_messages_request ();

  // -- Utility
  private:
  void
  device_abort ();

  void
  timeout_statistics ();

  private:
  // -- Event bits
  sev::event::bit_accus_async::Callback _event_accu_async;
  sev::event::Bit_Accu_Fast32 _event_accu;

  // -- Resources
  Service_Track _service_track;
  Service_Out _service_out;

  // -- Embedded device
  struct Dev
  {
    // -- Types
    enum class Run_State : std::uint8_t
    {
      NONE,
      RUNNING,
      ABORTING
    };

    enum class Abort_State : std::uint8_t
    {
      NONE,
      BEGIN,
      STOP_IO_SENT,
      STOP_IO_DONE,
      SHUT_DOWN_SENT,
      SHUT_DOWN_DONE
    };

    // -- Construction

    Dev () = default;

    // -- Attributes
    // - Device
    std::optional< snc::serial_device::usb::Device > device_usb;
    snc::serial_device::Device * device = nullptr;
    // - State
    Run_State run_state = Run_State::NONE;
    Abort_State abort_state = Abort_State::NONE;
    // - Connection
    sev::event::queue_io::Connection_2 connection;
    // - Event pools
    sev::event::Tracker_Pools< snc::serial_device::event::in::Data,
                               snc::serial_device::event::in::Abort_Stop_IO,
                               snc::serial_device::event::in::Abort_Shut_Down >
        epools;
    // - Transfer statistics
    snc::serial_device::Stats_IO io_stats;
  } _dev;

  struct Emb
  {
    // - Messages buffer
    snc::emb::msg::Stream messages_out;
    // - Serializer
    std::unique_ptr< Write_Tile[] > tile_owner;
    snc::emb::serial::Serializer serializer;
  } _emb;

  // -- Statistics timer
  sev::event::timer_queue::chrono::Client_Steady _timer_stats;

  // -- Dash event io
  sev::event::Pool< ship::event::out::Device_Error > _event_pool_device_error;
};

} // namespace snc::svs::fac::serial_device::ship
