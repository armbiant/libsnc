/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::fac::axis_align
{

enum class Sequence : std::uint8_t
{
  IDLE,
  APPROACH,
  APPROACH_STOP,
  WITHDRAW,
  WITHDRAW_STOP,
  BREAK
};

} // namespace snc::svs::fac::axis_align
