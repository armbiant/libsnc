/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot.hpp"
#include <sev/math/numbers.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <snc/math/stepping.hpp>
#include <snc/svs/fac/tracker/ship/sailor.hpp>
#include <algorithm>
#include <cmath>

namespace snc::svs::fac::axis_align::ship
{

Pilot::Pilot ( const Sailor_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Pilot-Align-Axis[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 2 );

  if ( _axis_index >= device_statics ().axes ().size () ) {
    throw std::runtime_error (
        sev::string::cat ( "Invalid axis index ",
                           _axis_index,
                           " in ",
                           device_statics ().axes ().size (),
                           " available axes" ) );
  }

  _axis_static = device_statics ().axes ().get ( _axis_index );
  _axis_state = &device_state ().axes ()[ _axis_index ];

  if ( _axis_static->sensors ().low_end () ) {
    _sensor_state =
        &device_state ()
             .sensors_i1 ()[ _axis_static->sensors ().low_end ()->index () ];
  } else if ( _axis_static->sensors ().high_end () ) {
    _sensor_state =
        &device_state ()
             .sensors_i1 ()[ _axis_static->sensors ().high_end ()->index () ];
  }

  _step_splitter.reset ();
  _step_splitter.set_usecs_max (
      _axis_static->emb ().stepper ()->step_slice_usecs_max () );

  _emb_axis_index = _axis_static->emb ().stepper_index ();
  _emb_axis_dir_flipped = _axis_static->emb ().direction_reversed ();

  // Job types
  {
    const bool sape = _axis_static->sensors ().high_end ().operator bool ();
    _job_type_withdraw =
        snc::emb::type::step_move ( !sape, _emb_axis_dir_flipped );
    _job_type_approach =
        snc::emb::type::step_move ( sape, _emb_axis_dir_flipped );
    _job_type_current = _job_type_approach;
  }

  // Slow movement step micros maximum
  _speed_reversible = _axis_static->geo ().speed_reversible ();
  _speed_stoppable = _axis_static->geo ().speed_stoppable ();

  _length_per_step = _axis_static->geo ().length_per_step ();
  _accel = _axis_static->geo ().accel_max ();

  double accel_length = 0.0;
  double job_length = 0.0;
  if ( _axis_static->geo ().linear () ) {
    accel_length = 1.0;
    job_length = accel_length / 2.0;
  } else if ( _axis_static->geo ().rotational () ) {
    accel_length = sev::lag::pi_d / 20.0;
    job_length = accel_length / 2.0;
  }

  // Stopping should be possible wihin this length
  const double job_usecs_min = 10 * sev::math::kilo_ui;

  std::size_t num_accel_steps = 0;
  std::size_t num_accel_steps_max =
      std::floor ( accel_length / _length_per_step );
  sev::math::assign_larger ( num_accel_steps_max, std::size_t ( 1 ) );

  std::size_t num_job_steps_max = std::floor ( job_length / _length_per_step );
  sev::math::assign_larger ( num_job_steps_max, std::size_t ( 1 ) );
  sev::math::assign_smaller (
      num_job_steps_max, snc::emb::msg::out::Steps_U16_64::num_values_max () );

  // One job must have a minimum duration to avoid buffer underruns
  double speed_max = 0.0;
  {
    double step_usecs =
        std::ceil ( double ( job_usecs_min ) / double ( num_job_steps_max ) );
    speed_max = speed_at_step_usecs ( step_usecs );
  }

  // Fast movement speed
  _speed_fast = 0.0;
  for ( std::size_t ii = 0; ii != num_accel_steps_max; ++ii ) {
    _speed_fast = snc::speed_accelerate_over_length (
        _speed_fast, _accel, _length_per_step );
    if ( _speed_fast > speed_max ) {
      _speed_fast = speed_max;
      break;
    }
    ++num_accel_steps;
  }

  // Maximum number of steps in a speed holding axis job
  _num_steps_fast = num_accel_steps;
  _num_steps_hold_speed = num_job_steps_max;

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
    logo << "Setup:\n";
    logo << "  speed_fast " << _speed_fast << "\n";
    logo << "  num_steps_fast " << _num_steps_fast << "\n";
    logo << "  num_steps_hold_speed " << _num_steps_hold_speed << "\n";
  }
}

Pilot::~Pilot () = default;

void
Pilot::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Job::etype:
    office_event_job ( event_n );
    break;
  default:
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot::office_event_job ( Office_Event & event_n )
{
  const auto & job =
      static_cast< const ship::event::in::Job & > ( event_n ).job ();

  switch ( job.cmd () ) {
  case Job::Command::INVALID:
    break;

  case Job::Command::ABORT:
    if ( movement_stoppable () ) {
      select_sequence ( Sequence::IDLE );
    } else {
      select_sequence ( Sequence::BREAK );
    }
    break;

  case Job::Command::ALIGN:
    if ( _state.sequence () == Sequence::IDLE ) {
      // Start alignment
      _state.set_speed_target ( _speed_fast );
      select_sequence ( Sequence::APPROACH );
      tracker ().set_axis_aligned ( _axis_index, false );
      device_messages_register ( true );
    } else {
      // Ignore when we're not IDLE
    }
    break;

  case Job::Command::SET:
    if ( movement_stoppable () ) {
      // Set aligned when movement stopped
      tracker ().set_axis_aligned ( _axis_index, true );
      select_sequence ( Sequence::IDLE );
    } else {
      // Convert to BREAK job
      select_sequence ( Sequence::BREAK );
    }
    break;
  }
}

void
Pilot::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

void
Pilot::select_sequence ( Sequence sequence_n )
{
  if ( _state.sequence () != sequence_n ) {
    _state.set_sequence ( sequence_n );
    office_io ().notification_set ( Office_Message::STATE );
  }

  switch ( sequence_n ) {
  case Sequence::IDLE:
    _state.set_speed_target ( 0.0 );
    device_messages_register ( false );
    break;

  case Sequence::APPROACH:
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Approaching at ",
                 _state.speed_target (),
                 " mm/s." );
    _job_type_current = _job_type_approach;
    // Final approach. Keep number of step in device queue low.
    _num_steps_device_limit = ( _state.speed_target () <= _speed_stoppable )
                                  ? 1
                                  : _num_steps_hold_speed;
    _speed_current = 0.0;
    break;

  case Sequence::APPROACH_STOP:
    log ().cat ( sev::logt::FL_DEBUG_0, "Approaching stopped." );
    break;

  case Sequence::WITHDRAW:
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Withdrawing at ",
                 _state.speed_target (),
                 " mm/s." );
    _job_type_current = _job_type_withdraw;
    _num_steps_device_limit = _num_steps_hold_speed;
    _speed_current = 0.0;
    break;

  case Sequence::WITHDRAW_STOP:
    log ().cat ( sev::logt::FL_DEBUG_0, "Withdrawing stopped." );
    break;

  case Sequence::BREAK:
    _state.set_speed_target ( 0.0 );
    log ().cat ( sev::logt::FL_DEBUG_0, "Breaking." );
    break;
  }
}

void
Pilot::device_messages_acquire ( Emb_Writer stream_n )
{
  // Update position in aligment sequence
  while ( update_sequence () ) {
  }

  if ( _state.sequence () == Sequence::IDLE ) {
    return;
  }

  device_messages_generate_steps ( stream_n );
}

void
Pilot::reduce_speed ()
{
  double speed_target = ( _state.speed_target () / 2.0 );
  sev::math::assign_larger< double > ( speed_target, _speed_stoppable );
  _state.set_speed_target ( speed_target );
}

bool
Pilot::update_sequence ()
{
  Sequence const seq = _state.sequence ();

  switch ( _state.sequence () ) {
  case Sequence::IDLE:
    break;

  case Sequence::APPROACH:
    if ( _sensor_state->state () ) {
      select_sequence ( Sequence::APPROACH_STOP );
    }
    break;

  case Sequence::APPROACH_STOP:
    if ( movement_stoppable () ) {
      if ( _state.speed_target () <= _speed_stoppable ) {
        // Waiting for the steps queue to be empty isn't the best
        // sollution because it allows some overshoot over the point
        // where the sensor state switched. A more precise sollution
        // would allow going back to the step position where the switch
        // state change happened. But still the error should be within
        // the range of only a small number of steps.

        // Aligned!
        _step_splitter.reset ();
        tracker ().set_axis_aligned ( _axis_index, true );
        select_sequence ( Sequence::IDLE );
        log ().cat ( sev::logt::FL_DEBUG_0, "Aligned!" );
      } else {
        // Lower speed and swap direction
        reduce_speed ();
        select_sequence ( Sequence::WITHDRAW );
      }
    }
    break;

  case Sequence::WITHDRAW:
    if ( !_sensor_state->state () ) {
      select_sequence ( Sequence::WITHDRAW_STOP );
    }
    break;

  case Sequence::WITHDRAW_STOP:
    if ( movement_stoppable () ) {
      // Lower speed and swap direction
      reduce_speed ();
      select_sequence ( Sequence::APPROACH );
    }
    break;

  case Sequence::BREAK:
    if ( movement_stoppable () ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Breaking finished." );
      select_sequence ( Sequence::IDLE );
    }
    break;
  }

  // Return true if the sequence changed
  return ( seq != _state.sequence () );
}

void
Pilot::device_messages_generate_steps ( Emb_Writer stream_n )
{
  std::uint_fast32_t num_steps_pushable = _num_steps_device_limit;
  sev::math::subtract_minimum_zero< std::uint_fast32_t > (
      num_steps_pushable, _axis_state->steps_queue_length () );

  snc::emb::msg::out::Steps_U16_64 * message = nullptr;

  while ( num_steps_pushable != 0 ) {
    if ( _step_splitter.is_finished () && !device_messages_generate_step () ) {
      break;
    }

    // Make new message on demand
    if ( ( message == nullptr ) || message->values_full () ||
         ( message->job_type () != _step_splitter.job_type () ) ) {
      stream_n.acquire ( message );
      message->set_stepper_index ( _emb_axis_index );
      message->set_job_type ( _step_splitter.job_type () );
    }
    message->push_usecs ( _step_splitter.acquire_usecs_next () );
    --num_steps_pushable;
  }
}

inline bool
Pilot::device_messages_generate_step ()
{
  bool generate = false;

  switch ( _state.sequence () ) {
  case Sequence::IDLE:
    break;

  case Sequence::APPROACH:
  case Sequence::WITHDRAW:
    // Accelerate on demand
    if ( _speed_current < _state.speed_target () ) {
      _speed_current = snc::speed_accelerate_over_length (
          _speed_current, _accel, _length_per_step );
      sev::math::assign_smaller< double > ( _speed_current,
                                            _state.speed_target () );
    }
    generate = true;
    break;

  case Sequence::APPROACH_STOP:
  case Sequence::WITHDRAW_STOP:
  case Sequence::BREAK:
    // Decelerate untip stoppable
    if ( _speed_current > _speed_reversible ) {
      _speed_current = snc::speed_accelerate_over_length (
          _speed_current, -_accel, _length_per_step );
      sev::math::assign_larger< double > ( _speed_current, _speed_reversible );
      generate = true;
    }
    break;
  }

  if ( generate ) {
    // Setup step splitter
    const std::uint_fast32_t usecs =
        std::ceil ( step_usecs_at_speed ( _speed_current ) );
    _step_splitter.setup_step ( _job_type_current, usecs );
  } else {
    // Reset step splitter
    _step_splitter.reset ();
  }
  return generate;
}

} // namespace snc::svs::fac::axis_align::ship
