/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/state/sensor/i1.hpp>
#include <snc/device/state/state.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/emb/step_splitter.hpp>
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/axis_align/ship/events.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::ship
{

class Pilot : public snc::svp::ship::Pilot_Emb
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Emb;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot ( const Sailor_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Pilot ();

  // -- Bridge session interface

  private:
  void
  prepare_axes_data ();

  // -- Office event processing

  public:
  void
  office_event ( Office_Event & event_n ) override;

  void
  office_event_job ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Embedded device interface

  public:
  void
  device_messages_acquire ( Emb_Writer stream_n ) override;

  private:
  void
  select_sequence ( Sequence sequence_n );

  bool
  update_sequence ();

  void
  start_waiting ();

  void
  device_messages_generate_steps ( Emb_Writer stream_n );

  bool
  device_messages_generate_step ();

  private:
  // -- Utility

  void
  reduce_speed ();

  bool
  speed_current_reversible () const
  {
    return ( _speed_current <= _speed_reversible );
  }

  bool
  movement_stoppable () const
  {
    return ( _speed_current <= _speed_reversible ) &&
           ( _axis_state->steps_queue_length () == 0 );
  }

  double
  step_usecs_at_speed ( double speed_n ) const
  {
    return ( _length_per_step * sev::math::mega_d ) / speed_n;
  }

  double
  speed_at_step_usecs ( double usecs_n ) const
  {
    return ( _length_per_step * sev::math::mega_d ) / usecs_n;
  }

  private:
  // -- Attributes
  const std::uint_fast32_t _axis_index = 0;
  State _state;

  // -- Device information
  snc::device::statics::handle::Axis _axis_static;
  const snc::device::state::Axis * _axis_state = nullptr;
  const snc::device::state::sensor::I1 * _sensor_state = nullptr;

  // The current step
  snc::emb::Step_Splitter _step_splitter;

  // Axis job message job types
  std::uint8_t _emb_axis_index = 0;
  bool _emb_axis_dir_flipped = false;
  snc::emb::type::Step _job_type_withdraw;
  snc::emb::type::Step _job_type_approach;
  snc::emb::type::Step _job_type_current;

  double _speed_reversible = 0.0;
  double _speed_stoppable = 0.0;
  double _speed_fast = 0.0;
  /// @brief Current acceleration / deceleration speed
  double _speed_current = 0.0;

  double _length_per_step = 0.0;
  double _accel = 0.0;

  std::uint_fast32_t _num_steps_fast = 0;
  std::uint_fast32_t _num_steps_hold_speed = 0;
  std::uint_fast32_t _num_steps_device_limit = 0;

  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::axis_align::ship
