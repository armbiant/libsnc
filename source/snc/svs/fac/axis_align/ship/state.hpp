/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_align/types.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::ship
{

class State
{
  public:
  // -- Setup

  void
  reset ()
  {
    _sequence = Sequence::IDLE;
    _speed_target = 0.0;
  }

  // -- Sequence

  Sequence
  sequence () const
  {
    return _sequence;
  }

  void
  set_sequence ( Sequence sequence_n )
  {
    _sequence = sequence_n;
  }

  // -- Speed

  double
  speed_target () const
  {
    return _speed_target;
  }

  void
  set_speed_target ( double speed_n )
  {
    _speed_target = speed_n;
  }

  private:
  // -- Attributes
  Sequence _sequence = Sequence::IDLE;
  double _speed_target = 0.0;
};

} // namespace snc::svs::fac::axis_align::ship
