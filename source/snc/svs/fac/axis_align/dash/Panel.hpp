/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/fac/axis_align/office/state.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  enum Sequence
  {
    IDLE,
    APPROACH,
    WITHDRAW,
    BREAK
  };

  // -- Properties

  Q_ENUM ( Sequence );
  Q_PROPERTY ( int axisIndex READ axis_index NOTIFY axisIndexChanged )
  Q_PROPERTY ( bool alignable READ alignable NOTIFY alignableChanged )
  Q_PROPERTY ( bool aligning READ aligning NOTIFY aligningChanged )
  Q_PROPERTY ( double speedTarget READ speedTarget NOTIFY speedTargetChanged )

  Q_PROPERTY ( Sequence sequence READ sequence NOTIFY sequenceChanged )
  Q_PROPERTY ( bool sequenceIdle READ sequenceIdle NOTIFY sequenceChanged )
  Q_PROPERTY (
      bool sequenceApproach READ sequenceApproach NOTIFY sequenceChanged )
  Q_PROPERTY (
      bool sequenceWithdraw READ sequenceWithdraw NOTIFY sequenceChanged )
  Q_PROPERTY ( bool sequenceBreak READ sequenceBreak NOTIFY sequenceChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Axis index

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  Q_SIGNAL
  void
  axisIndexChanged ();

  // -- State

  const snc::svs::fac::axis_align::office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Alignable

  bool
  alignable () const
  {
    return state ().is_alignable ();
  }

  Q_SIGNAL
  void
  alignableChanged ();

  // -- Aligning

  bool
  aligning () const
  {
    return state ().is_aligning ();
  }

  Q_SIGNAL
  void
  aligningChanged ();

  // -- Speed target

  double
  speedTarget () const
  {
    return state ().speed_target ();
  }

  Q_SIGNAL
  void
  speedTargetChanged ();

  // -- Sequence state

  Sequence
  sequence () const
  {
    return _qtState.sequence;
  }

  bool
  sequenceIdle () const
  {
    return ( _qtState.sequence == Sequence::IDLE );
  }

  bool
  sequenceApproach () const
  {
    return ( _qtState.sequence == Sequence::APPROACH );
  }

  bool
  sequenceWithdraw () const
  {
    return ( _qtState.sequence == Sequence::WITHDRAW );
  }

  bool
  sequenceBreak () const
  {
    return ( _qtState.sequence == Sequence::BREAK );
  }

  Q_SIGNAL
  void
  sequenceChanged ();

  private:
  // -- Attributes
  const std::uint_fast32_t _axis_index = 0;
  snc::svs::fac::axis_align::office::State _state;
  struct
  {
    bool alignable = false;
    bool aligning = false;
    double speedTarget = 0.0;
    Sequence sequence = Sequence::IDLE;
  } _qtState;
};

} // namespace snc::svs::fac::axis_align::dash
