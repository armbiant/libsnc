/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/fac/axis_align/dash/events.hpp>
#include <snc/svs/fac/axis_align/office/clerk.hpp>

namespace snc::svs::fac::axis_align::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Pilot-Align-Axis[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::axis_align::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::State::etype:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.alignable, state ().is_alignable () ) ) {
    emit alignableChanged ();
  }
  if ( sev::change ( _qtState.aligning, state ().is_aligning () ) ) {
    emit aligningChanged ();
  }
  if ( sev::change ( _qtState.speedTarget, state ().speed_target () ) ) {
    emit speedTargetChanged ();
  }
  {
    Sequence c_sequence = Sequence::IDLE;
    {
      using Seq = snc::svs::fac::axis_align::Sequence;
      switch ( state ().sequence () ) {
      case Seq::IDLE:
        c_sequence = Sequence::IDLE;
        break;
      case Seq::APPROACH:
      case Seq::APPROACH_STOP:
        c_sequence = Sequence::APPROACH;
        break;
      case Seq::WITHDRAW:
      case Seq::WITHDRAW_STOP:
        c_sequence = Sequence::WITHDRAW;
        break;
      case Seq::BREAK:
        c_sequence = Sequence::BREAK;
        break;
      }
    }
    if ( sev::change ( _qtState.sequence, c_sequence ) ) {
      emit sequenceChanged ();
    }
  }
}

} // namespace snc::svs::fac::axis_align::dash
