/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::fac::axis_align
{

class Job
{
  public:
  // -- Types

  enum class Command : std::uint8_t
  {
    INVALID,
    ABORT,
    ALIGN,
    SET
  };

  // -- Construction and setup

  Job () = default;

  Job ( Command cmd_n )
  : _cmd ( cmd_n )
  {
  }

  void
  reset ()
  {
    _cmd = Command::INVALID;
  }

  // -- State

  bool
  is_valid () const
  {
    return _cmd != Command::INVALID;
  }

  // -- Command

  Command
  cmd () const
  {
    return _cmd;
  }

  void
  set_cmd ( Command cmd_n )
  {
    _cmd = cmd_n;
  }

  public:
  // -- Attributes
  Command _cmd = Command::INVALID;
};

} // namespace snc::svs::fac::axis_align
