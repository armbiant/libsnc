/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/control_unique.hpp>
#include <snc/svs/ctl/base/client_axis.hpp>
#include <snc/svs/fac/axis_align/job.hpp>
#include <snc/svs/fac/axis_align/office/state.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::ctl::base::Client_Axis
{
  // -- Types
  private:
  using Super = snc::svs::ctl::base::Client_Axis;

  public:
  // -- Construction

  Client ( User * user_n, std::uint_fast32_t axis_index_n );

  ~Client ();

  // -- Service state interface

  bool
  session_is_good () const;

  bool
  is_alignable () const;

  bool
  is_aligning () const;

  bool
  acquire_state ( State & state_n );

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client *
    client () const
    {
      return static_cast< Client * > ( client_abstract () );
    }

    // -- Control interface

    bool
    start_job ( const Job & job_n );

    bool
    begin ();

    bool
    set ();

    bool
    break_hard ();
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::axis_align::office
