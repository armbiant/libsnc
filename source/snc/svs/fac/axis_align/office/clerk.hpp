/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/axis_align/dash/events.hpp>
#include <snc/svs/fac/axis_align/job.hpp>
#include <snc/svs/fac/axis_align/office/service.hpp>
#include <snc/svs/fac/axis_align/office/state.hpp>
#include <snc/svs/fac/axis_align/ship/events.hpp>
#include <snc/svs/fac/axis_align/ship/state.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::office
{

// -- Forward declaration
class Client;

class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Clerk ();

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_begin_sailor () override;

  void
  bridge_session_abort () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_state ( Bridge_Event & event_n );

  // -- Bridge notification

  void
  bridge_notify_job ( const Job & job_n );

  // -- Central processing

  void
  process () override;

  private:
  // -- Service interface
  friend class snc::svs::fac::axis_align::office::Service;

  void
  state_changed ();

  private:
  // -- Resources
  Service _service;
  State _state_emitted;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;

  // -- Bridge event io
  sev::event::Pool< ship::event::in::Job > _event_pool_bridge_job;
};

} // namespace snc::svs::fac::axis_align::office
