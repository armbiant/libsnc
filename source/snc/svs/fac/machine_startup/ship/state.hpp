/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/machine_startup/startup_state.hpp>
#include <cstdint>

namespace snc::svs::fac::machine_startup::ship
{

class State
{
  public:
  // -- Setup

  void
  reset ()
  {
    *this = State ();
  }

  // -- Startup

  const Startup_State &
  startup () const
  {
    return _startup;
  }

  Startup_State &
  startup_ref ()
  {
    return _startup;
  }

  private:
  // -- Attributes
  Startup_State _startup;
};

} // namespace snc::svs::fac::machine_startup::ship
