/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/machine_startup/ship/events.hpp>
#include <snc/svs/fac/machine_startup/ship/state.hpp>

namespace snc::svs::fac::machine_startup::ship
{

class Pilot : public snc::svp::ship::Pilot_Emb
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Emb;

  struct Message_Send_Device
  {
    static constexpr std::uint8_t STARTUP = ( 1 << 0 );
    static constexpr std::uint8_t HEARTBEAT = ( 1 << 1 );
  };

  struct Office_Message
  {
    static constexpr std::uint8_t DEVICE_CONFIGURED = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot ( const Sailor_Init & init_n );

  ~Pilot ();

  // -- Bridge session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  // -- Office event processing

  void
  office_notify () override;

  // -- Embedded device interface

  void
  device_messages_acquire ( Emb_Writer stream_n ) override;

  void
  device_messages_acquire_startup ( Emb_Writer stream_n );

  void
  device_messages_acquire_config_outputs ( Emb_Writer stream_n );

  private:
  // -- Utility

  void
  request_device_message ( std::uint8_t message_n );

  private:
  // -- State
  bool _device_configured = false;
  State _pilot_state;

  // -- Requests
  sev::mem::Flags_Fast8 _message_send_device;

  // -- Startup
  sev::event::timer_queue::chrono::Client_Steady _startup_timer;

  // -- Heartbeat
  sev::event::timer_queue::chrono::Client_Steady _heartbeat_timer;

  // -- Office event io
  sev::event::Pool< ship::event::out::Device_Configured >
      _event_pool_device_configured;
};

} // namespace snc::svs::fac::machine_startup::ship
