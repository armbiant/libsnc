/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "startup_state.hpp"
#include <snc/emb/msg/out/all.hpp>
#include <snc/svs/fac/tracker/ship/sailor.hpp>

namespace snc::svs::fac::machine_startup
{

void
Startup_Step_Control::reset ()
{
  _enabled = false;
  _enable_duration_passed = false;
  _enable_time = Time_Point ();
}

void
Startup_Step_Control::set_enabled_now ()
{
  _enabled = true;
  _enable_time.now ();
}

bool
Startup_Step_Control::operator== ( const Startup_Step_Control & state_n ) const
{
  return ( _enabled == state_n._enabled ) &&                               //
         ( _enable_duration_passed == state_n._enable_duration_passed ) && //
         ( _enable_time == state_n._enable_time );
}

bool
Startup_Step_Control::operator!= ( const Startup_Step_Control & state_n ) const
{
  return ( _enabled != state_n._enabled ) ||                               //
         ( _enable_duration_passed != state_n._enable_duration_passed ) || //
         ( _enable_time != state_n._enable_time );
}

void
Startup_State::reset ()
{
  _outputs_configured = false;
  _power.reset ();
  _driver.reset ();
}

bool
Startup_State::operator== ( const Startup_State & state_n ) const
{
  return ( _outputs_configured == state_n._outputs_configured ) && //
         ( _power == state_n._power ) &&                           //
         ( _driver == state_n._driver );
}

bool
Startup_State::operator!= ( const Startup_State & state_n ) const
{
  return !operator== ( state_n );
}

} // namespace snc::svs::fac::machine_startup
