/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/chrono/steady_system.hpp>
#include <chrono>

namespace snc::svs::fac::machine_startup
{

class Startup_Step
{
  public:
  // -- Types

  using Time_Point = sev::chrono::Steady_System;
};

class Startup_Step_Control : public Startup_Step
{
  public:
  // -- Construction and setup

  Startup_Step_Control () = default;

  void
  reset ();

  // -- Enable

  void
  set_enabled_now ();

  bool
  is_enabled () const
  {
    return _enabled;
  }

  const Time_Point &
  enable_time () const
  {
    return _enable_time;
  }

  // -- Enable duration

  bool
  enable_duration_passed () const
  {
    return _enable_duration_passed;
  }

  void
  set_enable_duration_passed ()
  {
    _enable_duration_passed = true;
  }

  // -- Comparison operators

  bool
  operator== ( const Startup_Step_Control & state_n ) const;

  bool
  operator!= ( const Startup_Step_Control & state_n ) const;

  private:
  bool _enabled = false;
  bool _enable_duration_passed = false;
  Time_Point _enable_time;
};

class Startup_State
{
  public:
  // -- Types

  using Time_Point = sev::chrono::Steady_System;

  // -- Setup

  void
  reset ();

  // -- Output configuration

  bool
  outputs_configured () const
  {
    return _outputs_configured;
  }

  void
  set_outputs_configured ( bool flag_n )
  {
    _outputs_configured = flag_n;
  }

  // -- Power

  Startup_Step_Control &
  power ()
  {
    return _power;
  }

  const Startup_Step_Control &
  power () const
  {
    return _power;
  }

  // -- Driver

  Startup_Step_Control &
  driver ()
  {
    return _driver;
  }

  const Startup_Step_Control &
  driver () const
  {
    return _driver;
  }

  // -- Comparison operators

  bool
  operator== ( const Startup_State & state_n ) const;

  bool
  operator!= ( const Startup_State & state_n ) const;

  private:
  // -- Attributes
  bool _outputs_configured = false;

  Startup_Step_Control _power;
  Startup_Step_Control _driver;
};

} // namespace snc::svs::fac::machine_startup
