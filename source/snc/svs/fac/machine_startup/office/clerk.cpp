/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/svs/fac/machine_startup/ship/pilot.hpp>

namespace snc::svs::fac::machine_startup::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine_Startup" )
, _service ( *this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.init_connections ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
  }
}

void
Clerk::bridge_session_begin ()
{
  Super::bridge_session_begin ();

  _service.session_begin ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  pick_n (
      make_sailor_factory< snc::svs::fac::machine_startup::ship::Pilot > () );
}

void
Clerk::bridge_session_abort ()
{
  Super::bridge_session_abort ();

  _service.session_abort ();
}

void
Clerk::bridge_session_end ()
{
  _service.session_end ();

  Super::bridge_session_end ();
}

void
Clerk::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::Type::DEVICE_CONFIGURED:
    bridge_event_state ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::bridge_event_state ( Bridge_Event & event_n [[maybe_unused]] )
{
  _service.set_device_configured ();
}

void
Clerk::process ()
{
  _service.process ();
}

void
Clerk::state_changed ()
{
  if ( sev::change ( _state_emitted, _service.state () ) ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::fac::machine_startup::office
