/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "state.hpp"

namespace snc::svs::fac::machine_startup::office
{

void
State::reset ()
{
  _is_connected = false;
  _is_disconnecting = false;
  _startup.reset ();
}

bool
State::operator== ( const State & state_n ) const
{
  return ( _is_connected == state_n._is_connected ) &&
         ( _is_disconnecting == state_n._is_disconnecting ) &&
         ( _startup == state_n._startup );
}

bool
State::operator!= ( const State & state_n ) const
{
  return ( _is_connected != state_n._is_connected ) ||
         ( _is_disconnecting != state_n._is_disconnecting ) ||
         ( _startup != state_n._startup );
}

} // namespace snc::svs::fac::machine_startup::office
