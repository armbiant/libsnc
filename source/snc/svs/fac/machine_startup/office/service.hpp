/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/timer_queue/chrono/client.hpp>
#include <snc/svs/fac/base/service.hpp>
#include <snc/svs/fac/controls/office/client.hpp>
#include <snc/svs/fac/controls/office/i1/client.hpp>
#include <snc/svs/fac/machine_startup/office/state.hpp>
#include <chrono>
#include <cstdint>
#include <optional>

namespace snc::svs::fac::machine_startup::office
{

// -- Forward declaration
class Clerk;

/** @brief Service */
class Service : public snc::svs::fac::base::Service
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Service;
  using Provider = snc::svs::fac::machine_startup::office::Clerk;

  public:
  // -- Construction

  Service ( Provider & provider_n );

  ~Service ();

  void
  init_connections ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  const State &
  state () const
  {
    return _state;
  }

  // -- Provider session interface

  void
  session_begin () override;

  void
  session_abort ();

  void
  session_end () override;

  // -- Provider runtime interface

  void
  process ();

  void
  set_device_configured ();

  void
  controls_session_begin ();

  void
  controls_session_end ();

  void
  startup_next ();

  void
  state_updated ();

  private:
  // -- Resources
  State _state;
  svs::fac::controls::office::Client _controls;
  sev::event::timer_queue::chrono::Client_Steady _timer;
  struct Level
  {
    struct
    {
      std::optional< svs::fac::controls::office::i1::Client > control;
      bool enabled = false;
      bool warmed_up = false;
      std::chrono::steady_clock::time_point warm_up_begin;
      std::chrono::steady_clock::duration warm_up_duration;
    } power;
    struct
    {
      std::optional< svs::fac::controls::office::i1::Client > control;
      bool enabled = false;
      bool warmed_up = false;
      std::chrono::steady_clock::time_point warm_up_begin;
      std::chrono::steady_clock::duration warm_up_duration;
    } driver;
  };
  std::optional< Level > _level;
};

} // namespace snc::svs::fac::machine_startup::office
