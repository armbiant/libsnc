/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/machine_startup/dash/events.hpp>
#include <snc/svs/fac/machine_startup/office/service.hpp>
#include <snc/svs/fac/machine_startup/office/state.hpp>

namespace snc::svs::fac::machine_startup::office
{

// -- Forward declaration
class Client;

/** @brief Clerk */
class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  /// @brief Front message state
  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk () override;

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_abort () override;

  void
  bridge_session_end () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_state ( Bridge_Event & event_n );

  // -- Central processing

  void
  process () override;

  // -- Utility

  void
  state_changed ();

  private:
  // -- Service
  Service _service;
  // -- Dash event io
  State _state_emitted;
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::fac::machine_startup::office
