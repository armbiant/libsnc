/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/client.hpp>
#include <snc/svs/fac/machine_startup/office/state.hpp>

namespace snc::svs::fac::machine_startup::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  // -- Construction

  using Super::Super;

  // -- Service state

  bool
  session_is_good () const;

  const State &
  state () const;

  bool
  driver_ready () const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::machine_startup::office
