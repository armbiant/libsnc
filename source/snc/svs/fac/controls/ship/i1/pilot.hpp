/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/device/handle.hpp>
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/controls/ship/i1/events.hpp>
#include <snc/svs/fac/controls/ship/i1/state.hpp>

namespace snc::svs::fac::controls::ship::i1
{

/** @brief Pilot base */
class Pilot : public snc::svp::ship::Pilot_Emb
{
  // -- Types
  private:
  using Super = snc::svp::ship::Pilot_Emb;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  struct Request
  {
    void
    reset ()
    {
      is_valid = false;
      value = false;
    }

    // -- Attributes
    bool is_valid = false;
    bool value = false;
  };

  public:
  // -- Construction

  Pilot ( const Sailor_Init & init_n, std::uint_fast32_t control_index_n );

  ~Pilot ();

  // -- Attributes

  std::uint_fast32_t
  control_index () const
  {
    return _control_index;
  }

  const auto &
  control_state () const
  {
    return _control_state;
  }

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  void
  office_notify () override;

  // -- Device message processing

  void
  device_messages_acquire ( Emb_Writer stream_n ) override;

  protected:
  // -- Utility

  void
  request_value ( bool value_n );

  void
  set_run_state ( State::Run_State state_n );

  void
  device_message ( Emb_Writer stream_n, bool state_n );

  private:
  // -- Settings
  std::uint_fast32_t _control_index = 0;
  snc::device::state::control::handle::I1 _control_state;
  struct
  {
    std::uint_fast32_t index = 0;
    bool inverted = false;
  } _emb;

  // -- State
  State _state;
  Request _request;

  // -- Office event io
  sev::event::Pool< ship::i1::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::controls::ship::i1
