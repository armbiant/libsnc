/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svs/fac/controls/office/clerk.hpp>

namespace snc::svs::fac::controls::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Pilot-Controls" )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::controls::office::Clerk > ();
}

} // namespace snc::svs::fac::controls::dash
