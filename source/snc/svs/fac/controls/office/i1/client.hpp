/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/bat/ifc/control_unique.hpp>
#include <snc/device/handle.hpp>

namespace snc::svs::fac::controls::office::i1
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client ( User * user_n, std::uint_fast32_t control_index_n );

  ~Client () override final;

  // -- Accessors

  std::uint_fast32_t
  control_index () const
  {
    return _control_index;
  }

  /// @brief Only valid when is_connected()
  snc::device::statics::control::handle::I1
  control_statics () const;

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client *
    client () const
    {
      return static_cast< Client * > ( client_abstract () );
    }

    // -- Control interface

    void
    set_value ( bool value_n );

    void
    toggle ();
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  private:
  std::uint_fast32_t _control_index = 0;
};

} // namespace snc::svs::fac::controls::office::i1
