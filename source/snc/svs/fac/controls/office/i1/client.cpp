/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/controls/office/clerk.hpp>

namespace snc::svs::fac::controls::office::i1
{

Client::Client ( User * user_n, std::uint_fast32_t control_index_n )
: Super ( user_n )
, _control_index ( control_index_n )
{
}

Client::~Client ()
{
  Client::disconnect ();
}

snc::device::statics::control::handle::I1
Client::control_statics () const
{
  if ( is_connected () ) {
    return service ()->control_statics ();
  }
  return {};
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  if ( auto * ser = dynamic_cast< Service * > ( sb_n ) ) {
    if ( ser->control_index () == control_index () ) {
      return true;
    }
  }
  return false;
}

void
Client::Control::set_value ( bool value_n )
{
  if ( is_valid () ) {
    client ()->service ()->set_value ( value_n );
  }
}

void
Client::Control::toggle ()
{
  if ( is_valid () ) {
    client ()->service ()->toggle ();
  }
}

} // namespace snc::svs::fac::controls::office::i1
