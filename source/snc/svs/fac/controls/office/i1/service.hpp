/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/fac/base/service.hpp>
#include <snc/svs/fac/controls/ship/i1/events.hpp>
#include <snc/svs/frame/ship/office/client_ship_io.hpp>

// -- Forward declaration
namespace snc::svs::fac::controls::office
{
class Clerk;
}
namespace snc::svs::fac::controls::ship::i1
{
class Pilot;
}

namespace snc::svs::fac::controls::office::i1
{

// -- Forward declaration
class Client;

/** @brief Service */
class Service : public snc::svs::fac::base::Service
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Service;
  using Provider = Clerk;

  public:
  // -- Construction

  Service ( Provider & provider_n, ship::i1::Pilot * pilot_n );

  ~Service ();

  // -- Session

  void
  session_begin () override;

  void
  session_end () override;

  // -- Accessors

  std::uint_fast32_t
  control_index () const
  {
    return _control_index;
  }

  const auto &
  control_statics () const
  {
    return _control_statics;
  }

  auto &
  ship_io ()
  {
    return _ship_io;
  }

  // -- Central processing

  void
  process ();

  private:
  // -- Control interface
  friend snc::svs::fac::controls::office::i1::Client;

  void
  set_value ( bool value_n );

  void
  toggle ();

  private:
  // -- Attributes
  std::uint_fast32_t _control_index = 0;
  bool _control_was_available = false;
  snc::device::statics::control::handle::I1 _control_statics;
  ship::i1::Pilot * _pilot = nullptr;
  snc::svs::frame::ship::office::Client_Ship_IO & _ship_io;
  sev::event::Pool< ship::i1::event::in::Set > _event_pool_set;
  sev::event::Pool< ship::i1::event::in::Toggle > _event_pool_toggle;
};

} // namespace snc::svs::fac::controls::office::i1
