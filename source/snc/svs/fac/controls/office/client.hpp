/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/client.hpp>

namespace snc::svs::fac::controls::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::fac::base::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Client;

  public:
  struct Stats
  {
    std::size_t i1 = 0;
  };

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override final;

  // -- State interface

  bool
  session_is_good () const;

  Stats
  stats () const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::controls::office
