/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/svs/fac/controls/office/clerk.hpp>
#include <snc/svs/fac/controls/office/client.hpp>
#include <snc/svs/fac/controls/ship/i1/pilot.hpp>

namespace snc::svs::fac::controls::office
{

Service::Service ( Provider & provider_n )
: Super ( provider_n )
{
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin_pilots ()
{
  // Create services
  auto & prov = provider ();
  for ( auto * sailor : prov.ship_io ().sailors () ) {
    if ( auto * cs = dynamic_cast< ship::i1::Pilot * > ( sailor ) ) {
      _services.i1.push_back ( std::make_unique< i1::Service > ( prov, cs ) );
    }
  }

  // Start service sessions
  for ( auto & service : _services.i1 ) {
    service->session_begin ();
  }

  // Notify clients
  clients_session_begin ();
  // Notify state
  signal_state ().send ();
}

void
Service::session_end ()
{
  // End sessions
  {
    for ( auto & service : _services.i1 ) {
      service->session_end ();
    }
    clients_session_end ();
  }

  // Destroy services
  _services.i1.clear ();

  // Call super
  Super::session_end ();

  // Notify state
  signal_state ().send ();
}

void
Service::process ()
{
  for ( auto & service : _services.i1 ) {
    service->process ();
  }
}

} // namespace snc::svs::fac::controls::office
