/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_n.hpp"
#include <snc/svs/fac/mpath/office/service_n.hpp>

namespace snc::svs::fac::mpath::office
{

template < std::size_t DIM >
Client_N< DIM >::Client_N ( User * user_n )
: Super ( user_n )
{
}

template < std::size_t DIM >
Client_N< DIM >::~Client_N () = default;

template < std::size_t DIM >

bool
Client_N< DIM >::session_is_good () const
{
  return Super::is_connected () ? service ()->session_is_good () : false;
}

template < std::size_t DIM >
const snc::device::Info &
Client_N< DIM >::device_info () const
{
  return *device_info_handle ();
}

template < std::size_t DIM >
snc::device::handle::Info
Client_N< DIM >::device_info_handle () const
{
  return Super::is_connected () ? service ()->device_info_handle ()
                                : snc::device::handle::Info ();
}

template < std::size_t DIM >
const snc::device::statics::Statics &
Client_N< DIM >::device_statics () const
{
  return *device_info_handle ()->statics ();
}

template < std::size_t DIM >
const snc::device::state::State &
Client_N< DIM >::device_state () const
{
  return device_info_handle ()->state ();
}

template < std::size_t DIM >
void
Client_N< DIM >::Control::break_hard ()
{
  if ( !Super::is_valid () ) {
    return;
  }
  client ()->service ()->break_hard ();
}

template < std::size_t DIM >
bool
Client_N< DIM >::Control::route_begin (
    const std::shared_ptr< snc::mpath_stream::Read > & mpath_stream_n,
    sev::unicode::View name_n )
{
  if ( !Super::is_valid () ) {
    return false;
  }
  return client ()->service ()->control_route_begin ( mpath_stream_n, name_n );
}

template < std::size_t DIM >
Service_N< DIM > *
Client_N< DIM >::service () const
{
  return static_cast< Service_N< DIM > * > ( service_abstract () );
}

template < std::size_t DIM >
bool
Client_N< DIM >::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service_N< DIM > * > ( sb_n ) != nullptr );
}

template < std::size_t DIM >
void
Client_N< DIM >::connecting ()
{
  Super::connecting ();
  _state = &service ()->state ();
}

template < std::size_t DIM >
void
Client_N< DIM >::disconnecting ()
{
  _state = nullptr;
  Super::disconnecting ();
}

// -- Instantiation

template class Client_N< 1 >;
template class Client_N< 2 >;
template class Client_N< 3 >;
template class Client_N< 4 >;
template class Client_N< 5 >;
template class Client_N< 6 >;

} // namespace snc::svs::fac::mpath::office
