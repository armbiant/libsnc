/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/job_d.hpp>
#include <snc/mpath_feed_emb/axes_mapping_d.hpp>
#include <snc/mpath_stream/read.hpp>
#include <snc/svs/fac/base/service.hpp>
#include <snc/svs/fac/machine_startup/office/client.hpp>
#include <snc/svs/fac/mpath/office/state.hpp>
#include <snc/svs/fac/mpath/ship/state.hpp>

namespace snc::svs::fac::mpath::office
{

// -- Forward declaration
template < std::size_t DIM >
class Clerk_N;
template < std::size_t DIM >
class Client_N;

/** @brief Service */
template < std::size_t DIM >
class Service_N : public snc::svs::fac::base::Service
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::Service;
  using Provider = Clerk_N< DIM >;
  using Client_Abstract = typename Super::Client_Abstract;

  public:
  // -- Construction

  Service_N ( Provider & provider_n );

  ~Service_N ();

  void
  init_connections ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  using Super::log;

  const auto &
  axes_mapping () const
  {
    return _axes_mapping;
  }

  const State &
  state () const
  {
    return _state;
  }

  const std::shared_ptr< snc::mpath_feed::Job_D< DIM > > &
  mpath_job () const
  {
    return _mpath_job;
  }

  // -- Provider interface

  void
  session_begin () override;

  void
  session_end () override;

  void
  update_pilot_state ( const ship::State & pilot_state_n );

  // -- Central processing

  void
  process ();

  // -- Client control interface

  bool
  control_acquirable () const override;

  private:
  friend svs::fac::mpath::office::Client_N< DIM >;

  bool
  control_route_begin (
      const std::shared_ptr< snc::mpath_stream::Read > & mpath_stream_n,
      sev::unicode::View name_n );

  // -- Utility

  void
  break_hard ();

  void
  state_update ();

  private:
  // -- Attributes
  snc::mpath_feed_emb::Axes_Mapping_D< DIM > _axes_mapping;
  State _state;
  ship::State _pilot_state;
  std::shared_ptr< snc::mpath_feed::Job_D< DIM > > _mpath_job;
  snc::svs::fac::machine_startup::office::Client _machine_startup;
};

} // namespace snc::svs::fac::mpath::office
