/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <chrono>
#include <cstdint>

namespace snc::svs::fac::mpath::office
{

class State
{
  public:
  // -- Setup

  void
  reset ()
  {
    _is_running = false;
    _is_aborting = false;
  }

  // -- Run state

  bool
  is_running () const
  {
    return _is_running;
  }

  void
  set_is_running ( bool flag_n )
  {
    _is_running = flag_n;
  }

  // -- Abort state

  bool
  is_aborting () const
  {
    return _is_aborting;
  }

  void
  set_is_aborting ( bool flag_n )
  {
    _is_aborting = flag_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n )
  {
    return ( _is_running == state_n._is_running ) &&
           ( _is_aborting == state_n._is_aborting );
  }

  bool
  operator!= ( const State & state_n )
  {
    return !operator== ( state_n );
  }

  private:
  // -- Attributes
  bool _is_running = false;
  bool _is_aborting = false;
};

} // namespace snc::svs::fac::mpath::office
