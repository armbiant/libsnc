/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk_n.hpp"
#include <sev/assert.hpp>
#include <snc/svs/fac/mpath/ship/pilot_n.hpp>
#include <cstdint>

namespace snc::svs::fac::mpath::office
{

template < std::size_t DIM >
Clerk_N< DIM >::Clerk_N ( const Clerk_Init & init_n )
: Super ( init_n, "Pilot-MPath" )
, _service ( *this )
, _event_pool_bridge_job ( ship_io ().epool_tracker () )
, _event_pool_bridge_break ( ship_io ().epool_tracker () )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

template < std::size_t DIM >
Clerk_N< DIM >::~Clerk_N ()
{
}

template < std::size_t DIM >
void
Clerk_N< DIM >::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.init_connections ();
}

template < std::size_t DIM >
void
Clerk_N< DIM >::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
  }
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_session_begin ()
{
  Super::bridge_session_begin ();

  // Begin service session
  _service.session_begin ();
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  pick_n ( make_sailor_factory< snc::svs::fac::mpath::ship::Pilot_N< DIM >,
                                snc::mpath_feed_emb::Axes_Mapping_D< DIM > > (
      _service.axes_mapping () ) );
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_session_begin_sailor ()
{
  Super::bridge_session_begin_sailor ();

  // Allocate ship events
  ship_io ().allocate ( _event_pool_bridge_job, 2 );
  ship_io ().allocate ( _event_pool_bridge_break, 2 );
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_session_abort ()
{
  Super::bridge_session_abort ();

  // End service session
  _service.session_end ();
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::Type::STATE:
    bridge_event_state ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_event_state ( Bridge_Event & event_n )
{
  // Read event
  {
    auto & cevent = static_cast< const ship::event::out::State & > ( event_n );
    ship_io ().stats_out_sub ( cevent.office_event_count () );
    _service.update_pilot_state ( cevent.state () );
  }
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_notify_break_hard ()
{
  ship_io ().submit ( ship_io ().acquire ( _event_pool_bridge_break ) );
}

template < std::size_t DIM >
void
Clerk_N< DIM >::bridge_notify_route_begin (
    const std::shared_ptr< snc::mpath_feed::Job_D< DIM > > & mpath_job_n )
{
  auto * event = ship_io ().acquire ( _event_pool_bridge_job );
  event->set_mpath_job ( mpath_job_n );
  ship_io ().submit ( event );
}

template < std::size_t DIM >
void
Clerk_N< DIM >::process ()
{
  _service.process ();
}

// -- Instantiation

template class Clerk_N< 1 >;
template class Clerk_N< 2 >;
template class Clerk_N< 3 >;
template class Clerk_N< 4 >;
template class Clerk_N< 5 >;
template class Clerk_N< 6 >;

} // namespace snc::svs::fac::mpath::office
