/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/unicode/view.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/bat/ifc/control_unique.hpp>
#include <snc/device/handle.hpp>
#include <snc/mpath_stream/read.hpp>
#include <snc/settings/settings.hpp>
#include <snc/svs/fac/mpath/office/state.hpp>
#include <memory>

namespace snc::svs::fac::mpath::office
{

// -- Forward declaration
template < std::size_t DIM >
class Service_N;

/** @brief Client_N
 */
template < std::size_t DIM >
class Client_N : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  using User = typename Super::User;

  public:
  // -- Construction

  Client_N ( User * user_n );

  ~Client_N ();

  // -- Service state

  bool
  session_is_good () const;

  const snc::device::Info &
  device_info () const;

  snc::device::handle::Info
  device_info_handle () const;

  const snc::device::statics::Statics &
  device_statics () const;

  const snc::device::state::State &
  device_state () const;

  const State &
  state () const
  {
    return *_state;
  }

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client_N *
    client () const
    {
      return static_cast< Client_N * > ( client_abstract () );
    }

    // -- Control interface

    void
    break_hard ();

    bool
    route_begin (
        const std::shared_ptr< snc::mpath_stream::Read > & mpath_stream_n,
        sev::unicode::View name_n );
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service_N< DIM > *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  connecting () override;

  void
  disconnecting () override;

  private:
  const State * _state = nullptr;
};

} // namespace snc::svs::fac::mpath::office
