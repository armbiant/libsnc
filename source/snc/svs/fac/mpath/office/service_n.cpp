/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_n.hpp"
#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/fac/mpath/office/clerk_n.hpp>

namespace snc::svs::fac::mpath::office
{

template < std::size_t DIM >
Service_N< DIM >::Service_N ( Provider & provider_n )
: Super ( provider_n )
, _mpath_job ( std::make_shared< snc::mpath_feed::Job_D< DIM > > () )
, _machine_startup ( &provider_n )
{
  Super::actor ().set_break_hard_func ( [ this ] () {
    // Controller is responsible for breaking
    if ( !Super::controlled () ) {
      break_hard ();
    }
  } );
}

template < std::size_t DIM >
Service_N< DIM >::~Service_N () = default;

template < std::size_t DIM >
void
Service_N< DIM >::init_connections ()
{
  _machine_startup.connect ();
}

template < std::size_t DIM >
typename Service_N< DIM >::Provider &
Service_N< DIM >::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

template < std::size_t DIM >
const typename Service_N< DIM >::Provider &
Service_N< DIM >::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

template < std::size_t DIM >
void
Service_N< DIM >::state_update ()
{
  Super::signal_state ().send ();
  provider ().dash_io ().notification_set ( Provider::Dash_Message::STATE );
}

template < std::size_t DIM >
void
Service_N< DIM >::session_begin ()
{
  Super::session_begin ();

  // Register machine leases
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    Super::machine_res ().leases_ref ().register_axis (
        _axes_mapping.map ( ii ) );
  }

  // Notify clients
  state_update ();
  Super::clients_session_begin ();
}

template < std::size_t DIM >
void
Service_N< DIM >::session_end ()
{
  Super::session_end ();

  // Unregister machine leases
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    Super::machine_res ().leases_ref ().unregister_axis (
        _axes_mapping.map ( ii ) );
  }

  // Update state
  _state.reset ();
  _pilot_state.reset ();

  // Notify clients
  state_update ();
  Super::clients_session_end ();
}

template < std::size_t DIM >
void
Service_N< DIM >::update_pilot_state ( const ship::State & pilot_state_n )
{
  _pilot_state = pilot_state_n;

  if ( !_pilot_state.stream_finished () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "MPath stream routing finished." );

  // Pilot is idle now
  Super::actor ().go_idle ();

  // Clean up
  _mpath_job->reset ();

  // Update state
  _state.set_is_running ( false );
  _state.set_is_aborting ( false );
  state_update ();
}

template < std::size_t DIM >
void
Service_N< DIM >::process ()
{
  Super::control_acquirable_change_poll ();
}

template < std::size_t DIM >
void
Service_N< DIM >::break_hard ()
{
  if ( !_state.is_running () || _state.is_aborting () ) {
    return;
  }

  // Send event
  provider ().bridge_notify_break_hard ();

  // Update state
  _state.set_is_aborting ( true );
  state_update ();
}

template < std::size_t DIM >
bool
Service_N< DIM >::control_acquirable () const
{
  return Super::control_acquirable () && //
         _machine_startup.driver_ready ();
}

template < std::size_t DIM >
bool
Service_N< DIM >::control_route_begin (
    const std::shared_ptr< snc::mpath_stream::Read > & mpath_stream_n,
    sev::unicode::View name_n )
{
  // -- Checks
  if ( !mpath_stream_n ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied.  Invalid stream." );
    return false;
  }
  if ( _state.is_running () || !Super::actor ().is_idle () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied.  Pilot is active." );
    return false;
  }
  if ( !Super::actor ().go_active () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied.  Pilot activation failed." );
    return false;
  }

  // -- Request accepted
  log ().cat ( sev::logt::FL_DEBUG_0, "Route begin." );

  // -- Setup stream resources
  _mpath_job->set_name ( name_n.string () );
  _mpath_job->set_stream ( mpath_stream_n );

  // Request notification
  provider ().bridge_notify_route_begin ( _mpath_job );

  // -- Update state
  _state.set_is_running ( true );
  state_update ();

  return true;
}

// -- Instantiation

template class Service_N< 1 >;
template class Service_N< 2 >;
template class Service_N< 3 >;
template class Service_N< 4 >;
template class Service_N< 5 >;
template class Service_N< 6 >;

} // namespace snc::svs::fac::mpath::office
