/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/mpath_feed/job_d.hpp>
#include <snc/mpath_feed_emb/axes_mapping_d.hpp>
#include <snc/mpath_feed_emb/feed_d.hpp>
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/mpath/ship/events.hpp>

namespace snc::svs::fac::mpath::ship
{

template < std::size_t DIM >
class Pilot_N : public snc::svp::ship::Pilot_Emb
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Emb;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot_N ( const Sailor_Init & init_n,
            const snc::mpath_feed_emb::Axes_Mapping_D< DIM > & axes_mapping_n );

  ~Pilot_N ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_end () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  void
  office_event_job ( Office_Event & event_n );

  void
  office_event_break ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Device messates interface

  void
  device_messages_acquire ( Emb_Writer stream_n ) override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  private:
  // -- Utility

  void
  mpath_feed_break ( bool forced_n );

  void
  mpath_feed_poll_end ();

  private:
  // -- State
  State _state;

  // -- Resources
  snc::mpath_feed_emb::Feed_D< DIM > _mpath_feed;
  std::shared_ptr< snc::mpath_feed::Job_D< DIM > > _mpath_job;

  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::mpath::ship
