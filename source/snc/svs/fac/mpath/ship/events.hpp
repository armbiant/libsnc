/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/job_d.hpp>
#include <snc/svp/ship/events.hpp>
#include <snc/svs/fac/mpath/ship/state.hpp>
#include <cstdint>
#include <memory>

namespace snc::svs::fac::mpath::ship::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
  static constexpr std::uint_fast32_t BREAK = 1;
};

using In = snc::svp::ship::event::In;

template < std::size_t DIM >
class Job : public In
{
  public:
  static constexpr auto etype = Type::JOB;

  Job ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _mpath_job.reset ();
  }

  const std::shared_ptr< snc::mpath_feed::Job_D< DIM > > &
  mpath_job () const
  {
    return _mpath_job;
  }

  void
  set_mpath_job (
      const std::shared_ptr< snc::mpath_feed::Job_D< DIM > > & mpath_job_n )
  {
    _mpath_job = mpath_job_n;
  }

  private:
  std::shared_ptr< snc::mpath_feed::Job_D< DIM > > _mpath_job;
};

/// @brief Signalizes that the walking should be stopped
///
class Break : public In
{
  public:
  static constexpr auto etype = Type::BREAK;

  Break ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }
};

} // namespace snc::svs::fac::mpath::ship::event::in

namespace snc::svs::fac::mpath::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::ship::event::out;

/// @brief Pilot state event
using State = State_T< Type::STATE, State >;

} // namespace snc::svs::fac::mpath::ship::event::out
