/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_n.hpp"
#include <sev/lag/vector2.hpp>
#include <sev/math/numbers.hpp>
#include <snc/device/state/state.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>
#include <algorithm>
#include <cmath>

namespace snc::svs::fac::mpath::ship
{

template < std::size_t DIM >
Pilot_N< DIM >::Pilot_N (
    const Sailor_Init & init_n,
    const snc::mpath_feed_emb::Axes_Mapping_D< DIM > & axes_mapping_n )
: Super ( init_n, "Pilot-MPath" )
, _mpath_feed (
      log (),
      snc::mpath_feed_emb::Context_D< DIM > ( cell_context ().thread_tracker (),
                                              device_statics_handle (),
                                              axes_mapping_n ) )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 4 );
}

template < std::size_t DIM >
Pilot_N< DIM >::~Pilot_N ()
{
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_begin ()
{
  Super::cell_session_begin ();
  _mpath_feed.session_begin ( processing_request_async () );
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_end ()
{
  _mpath_feed.session_end ();
  Super::cell_session_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_abort ()
{
  Super::cell_session_abort ();
  mpath_feed_break ( true );
}

template < std::size_t DIM >
bool
Pilot_N< DIM >::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && !_mpath_feed.is_routing ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Type::JOB:
    office_event_job ( event_n );
    break;
  case ship::event::in::Type::BREAK:
    office_event_break ( event_n );
    break;
  default:
    break;
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event_job ( Office_Event & event_n )
{
  auto & cevent =
      static_cast< const ship::event::in::Job< DIM > & > ( event_n );

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming job event.";
  }

  // Reset state
  _state.set_stream_finished ( false );
  _mpath_job = cevent.mpath_job ();

  {
    // Acquire current position
    sev::lag::Vector< double, DIM > pos_cur ( sev::lag::init::zero );
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      auto axis_index = _mpath_feed.context ().axes_mapping ().map ( ii );
      pos_cur[ ii ] = device_state ()
                          .axes ()[ axis_index ]
                          .kinematics_latest ()
                          .position ();
    }
    _mpath_job->set_begin_position ( pos_cur );
  }
  _mpath_feed.route_begin ( _mpath_job );

  device_messages_register ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event_break ( Office_Event & event_n [[maybe_unused]] )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming break event.";
  }
  mpath_feed_break ( false );
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::mpath_feed_break ( bool forced_n )
{
  // Abort feeder
  _mpath_feed.route_abort ( forced_n );
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_acquire ( Emb_Writer stream_n )
{
  // Acquire messages from walker
  if ( _mpath_feed.is_routing () ) {
    snc::mpath_feed_emb::Stream_Request_D< DIM > request ( stream_n,
                                                           tracker () );
    _mpath_feed.acquire_messages ( request );
  }
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::process_async ()
{
  _mpath_feed.process_events ();
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::process ()
{
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::mpath_feed_poll_end ()
{
  // Finished
  if ( !_mpath_feed.is_finished () ) {
    return;
  }
  // Wait until all axes stopped before finishing
  if ( cell_session ().is_good ) {
    for ( auto ai : _mpath_feed.context ().axes_mapping ().maps () ) {
      if ( !device_state ().axes ().get ( ai ).steps_queue_empty () ) {
        return;
      }
    }
  }

  // MPath feed end
  _mpath_feed.route_end ();
  _mpath_job.reset ();

  // Unregister from device messages
  device_messages_unregister ();

  // Update state
  _state.set_stream_finished ( true );
  office_io ().notification_set ( Office_Message::STATE );
}

// -- Instantiation

template class Pilot_N< 1 >;
template class Pilot_N< 2 >;
template class Pilot_N< 3 >;
template class Pilot_N< 4 >;
template class Pilot_N< 5 >;
template class Pilot_N< 6 >;

} // namespace snc::svs::fac::mpath::ship
