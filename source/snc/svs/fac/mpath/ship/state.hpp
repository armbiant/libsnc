/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::svs::fac::mpath::ship
{

class State
{
  public:
  // -- Setup

  void
  reset ()
  {
    _stream_finished = false;
  }

  // -- Stream finished

  bool
  stream_finished () const
  {
    return _stream_finished;
  }

  void
  set_stream_finished ( bool flag_n )
  {
    _stream_finished = flag_n;
  }

  private:
  // -- Attributes
  bool _stream_finished = false;
};

} // namespace snc::svs::fac::mpath::ship
