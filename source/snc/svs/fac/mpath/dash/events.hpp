/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/fac/mpath/office/state.hpp>

namespace snc::svs::fac::mpath::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, snc::svs::fac::mpath::office::State >;

} // namespace snc::svs::fac::mpath::dash::event::in

namespace snc::svs::fac::mpath::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t REQUEST_TARGET_CURRENT = 0;
  static constexpr std::uint_fast32_t REQUEST_RUN = 1;
};

using Out = snc::svp::dash::event::Out;

class Request_Target_Current : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_TARGET_CURRENT;

  Request_Target_Current ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

/// @brief Routing begin / abort request
///
class Request_Run : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_RUN;

  enum class Request
  {
    NONE,
    START,
    ABORT
  };

  Request_Run ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _request = Request::NONE;
  }

  Request
  request () const
  {
    return _request;
  }

  void
  set_request ( Request request_n )
  {
    _request = request_n;
  }

  private:
  Request _request;
};

} // namespace snc::svs::fac::mpath::dash::event::out
