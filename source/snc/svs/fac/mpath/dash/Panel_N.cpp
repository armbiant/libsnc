/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel_N.hpp"
#include <snc/svs/fac/mpath/office/clerk_n.hpp>

namespace snc::svs::fac::mpath::dash
{

template < std::size_t DIM >
Panel_N< DIM >::Panel_N ( const Panel_Init & init_n )
: Super ( init_n, "Pilot-MPath" )
{
}

template < std::size_t DIM >
Panel_N< DIM >::~Panel_N () = default;

template < std::size_t DIM >
typename Panel_N< DIM >::Clerk_Factory_Handle
Panel_N< DIM >::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::mpath::office::Clerk_N< DIM > > ();
}

template < std::size_t DIM >
void
Panel_N< DIM >::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

// -- Instantiation

template class Panel_N< 1 >;
template class Panel_N< 2 >;
template class Panel_N< 3 >;
template class Panel_N< 4 >;
template class Panel_N< 5 >;
template class Panel_N< 6 >;

} // namespace snc::svs::fac::mpath::dash
