/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cmath>
#include <cstdint>

namespace snc::svs::fac::axis_move
{

class Dynamics
{
  public:
  // -- Construction and setup

  Dynamics () = default;

  Dynamics ( double speed_n, double accel_n )
  : _speed ( std::fabs ( speed_n ) )
  , _accel ( std::fabs ( accel_n ) )
  {
  }

  /// @brief Reset to default construction state
  void
  reset ()
  {
    _speed = 0.0;
    _accel = 0.0;
  }

  // -- Speed

  const double &
  speed () const
  {
    return _speed;
  }

  void
  set_speed ( double value_n )
  {
    _speed = std::fabs ( value_n );
  }

  // -- Acceleration

  const double &
  accel () const
  {
    return _accel;
  }

  void
  set_accel ( double value_n )
  {
    _accel = std::fabs ( value_n );
  }

  // -- Comparison operators

  bool
  operator== ( const Dynamics & dyn_n ) const
  {
    return ( _speed == dyn_n._speed ) && ( _accel == dyn_n._accel );
  }

  bool
  operator!= ( const Dynamics & dyn_n ) const
  {
    return ( _speed != dyn_n._speed ) || ( _accel != dyn_n._accel );
  }

  private:
  // -- Attributes
  double _speed = 0.0;
  double _accel = 0.0;
};

} // namespace snc::svs::fac::axis_move
