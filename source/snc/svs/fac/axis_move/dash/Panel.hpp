/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/fac/axis_move/office/state.hpp>

namespace snc::svs::fac::axis_move::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Construction

  Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n = 0 );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State interface

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  const office::State &
  state () const
  {
    return _state;
  }

  private:
  // -- State
  const std::uint_fast32_t _axis_index = 0;
  office::State _state;
};

} // namespace snc::svs::fac::axis_move::dash
