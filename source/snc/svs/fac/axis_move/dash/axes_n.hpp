/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/dash/Panel.hpp>
#include <array>
#include <cstdint>
#include <optional>

namespace snc::svs::fac::axis_move::dash
{

template < std::size_t N >
class Axes_N
{
  public:
  // -- Construction

  Axes_N ( snc::svp::dash::Dash & dash_n );

  // -- Accessors

  static constexpr std::size_t
  num_axes ()
  {
    return N;
  }

  Panel &
  axis ( std::size_t axis_index_n )
  {
    return *_panels.at ( axis_index_n );
  }

  const Panel &
  axis ( std::size_t axis_index_n ) const
  {
    return *_panels.at ( axis_index_n );
  }

  private:
  std::array< std::optional< Panel >, N > _panels;
};

template < std::size_t N >
Axes_N< N >::Axes_N ( snc::svp::dash::Dash & dash_n )
{
  for ( std::size_t ii = 0; ii != N; ++ii ) {
    _panels[ ii ].emplace ( dash_n, ii );
  }
}

} // namespace snc::svs::fac::axis_move::dash
