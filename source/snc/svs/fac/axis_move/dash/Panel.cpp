/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/fac/axis_move/dash/events.hpp>
#include <snc/svs/fac/axis_move/office/clerk.hpp>

namespace snc::svs::fac::axis_move::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Pilot-Move-Axis[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::fac::axis_move::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

} // namespace snc::svs::fac::axis_move::dash
