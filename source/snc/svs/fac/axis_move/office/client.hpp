/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/control_unique.hpp>
#include <snc/device/axis_info.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/ctl/base/client_axis.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_move::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::ctl::base::Client_Axis
{
  // -- Types
  private:
  using Super = snc::svs::ctl::base::Client_Axis;

  public:
  // -- Construction

  Client ( User * user_n, std::uint_fast32_t axis_index_n );

  ~Client ();

  // -- Service state interface

  bool
  session_is_good () const;

  snc::device::handle::Info
  device_info_handle () const;

  snc::device::Axis_Info
  axis_info () const;

  bool
  is_idle () const;

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client *
    client () const
    {
      return static_cast< Client * > ( client_abstract () );
    }

    // -- Control interface

    /// @brief Break feeding at the maximum acceleration
    bool
    break_hard ();

    /// @brief Set the speed and acceleration
    bool
    set_dynamics ( const Dynamics & dyn_n );

    /// @brief Move to target
    bool
    set_target ( std::int64_t pos_n );

    /// @brief Move by some steps
    bool
    set_target_delta ( std::int64_t delta_n );

    /// @brief Move at a given speed
    bool
    set_speeding ( const Speeding & speed_n );

    /// @brief Move at the given speed
    bool
    set_speeding ( double speed_n );
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::axis_move::office
