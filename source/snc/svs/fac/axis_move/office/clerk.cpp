/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/axis_move/ship/pilot.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_move::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Pilot-Move-Axis[", axis_index_n, "]" ) )
, _service ( *this, axis_index_n )
, _bridge_epools ( ship_io ().epool_tracker () )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::bridge_session_begin ()
{
  Super::bridge_session_begin ();
  _service.session_begin ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  // Axis available?
  if ( _service.axis_index () >= _service.device_statics ().axes ().size () ) {
    return;
  }

  pick_n (
      make_sailor_factory< snc::svs::fac::axis_move::ship::Pilot,
                           std::uint_fast32_t > ( _service.axis_index () ) );
}

void
Clerk::bridge_session_begin_sailor ()
{
  Super::bridge_session_begin_sailor ();

  // Allocate ship events
  if ( !ship_io ().sailors ().empty () ) {
    ship_io ().allocate ( _bridge_epools.pool< ship::event::in::Break > (), 4 );
    ship_io ().allocate ( _bridge_epools.pool< ship::event::in::Dynamics > (),
                          4 );
    ship_io ().allocate ( _bridge_epools.pool< ship::event::in::Target > (),
                          4 );
    ship_io ().allocate (
        _bridge_epools.pool< ship::event::in::Target_Delta > (), 4 );
    ship_io ().allocate ( _bridge_epools.pool< ship::event::in::Speed > (), 4 );
  }
}

void
Clerk::bridge_session_abort ()
{
  Super::bridge_session_abort ();
  _service.session_end ();
  request_processing ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
    _state_emitted = _service.state ();
  }
}

void
Clerk::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::Type::STATE:
    bridge_event_state ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::bridge_event_state ( Bridge_Event & event_n )
{
  auto & cevent = static_cast< const ship::event::out::State & > ( event_n );
  ship_io ().stats_out_sub ( cevent.office_event_count () );
  _service.update_pilot_state ( cevent.state () );
}

void
Clerk::bridge_notify_break ()
{
  ship_io ().submit (
      ship_io ().acquire ( _bridge_epools.pool< ship::event::in::Break > () ) );
}

void
Clerk::bridge_notify_dynamics ( const Dynamics & dynamics_n )
{
  auto * event = ship_io ().acquire (
      _bridge_epools.pool< ship::event::in::Dynamics > () );
  event->set_dynamics ( dynamics_n );
  ship_io ().submit ( event );
}

void
Clerk::bridge_notify_target ( std::int64_t pos_n )
{
  auto * event =
      ship_io ().acquire ( _bridge_epools.pool< ship::event::in::Target > () );
  event->set_position ( pos_n );
  ship_io ().submit ( event );
}

void
Clerk::bridge_notify_target_delta ( std::int64_t delta_n )
{
  auto * event = ship_io ().acquire (
      _bridge_epools.pool< ship::event::in::Target_Delta > () );
  event->set_delta ( delta_n );
  ship_io ().submit ( event );
}

void
Clerk::bridge_notify_speeding ( const Speeding & speed_n )
{
  auto * event =
      ship_io ().acquire ( _bridge_epools.pool< ship::event::in::Speed > () );
  event->set_speed ( speed_n );
  ship_io ().submit ( event );
}

void
Clerk::process ()
{
  _service.process ();
}

void
Clerk::state_changed ()
{
  if ( _state_emitted != _service.state () ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::fac::axis_move::office
