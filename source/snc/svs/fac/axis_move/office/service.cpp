/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/axis_move/office/clerk.hpp>
#include <snc/svs/fac/axis_move/office/client.hpp>

namespace snc::svs::fac::axis_move::office
{

Service::Service ( Provider & provider_n, std::uint_fast32_t axis_index_n )
: Super ( provider_n, axis_index_n )
{
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  Super::session_begin ();

  // Notify clients
  state_update ();
  clients_session_begin ();
}

void
Service::session_end ()
{
  Super::session_end ();

  // Reset state
  _state.reset ();
  _pilot_state.reset ();

  // Notify clients
  state_update ();
  clients_session_end ();
}

void
Service::update_pilot_state ( const ship::State & pilot_state_n )
{
  _pilot_state = pilot_state_n;
  state_update ();
}

void
Service::process ()
{
  control_acquirable_change_poll ();
}

bool
Service::is_idle () const
{
  return actor ().is_idle ();
}

bool
Service::control_break_hard ()
{
  if ( actor ().is_idle () ) {
    return true;
  }

  // Accept request
  provider ().bridge_notify_break ();
  // Update state
  state_update ();

  return true;
}

bool
Service::control_dynamics ( const Dynamics & dyn_n )
{
  if ( _state.dynamics () == dyn_n ) {
    return true;
  }

  // Send event
  provider ().bridge_notify_dynamics ( dyn_n );
  // Update state
  actor ().go_active ();
  _state.set_dynamics ( dyn_n );
  state_update ();

  return true;
}

bool
Service::control_target ( std::int64_t pos_n )
{
  // Send event
  provider ().bridge_notify_target ( pos_n );
  // Update state
  actor ().go_active ();
  state_update ();

  return true;
}

bool
Service::control_target_delta ( std::int64_t delta_n )
{
  if ( delta_n == 0 ) {
    return true;
  }

  // Send event
  provider ().bridge_notify_target_delta ( delta_n );
  // Update state
  actor ().go_active ();
  state_update ();

  return true;
}

bool
Service::control_speeding ( const Speeding & speed_n )
{
  // Send event
  provider ().bridge_notify_speeding ( speed_n );
  // Update state
  actor ().go_active ();
  _state.set_dynamics ( speed_n );
  state_update ();

  return true;
}

bool
Service::control_speeding ( double speed_n )
{
  Speeding speed;
  static_cast< Dynamics & > ( speed ) = _state.dynamics ();
  speed.set_speed ( speed_n );
  speed.set_forward ( speed_n >= 0.0 );

  return control_speeding ( speed );
}

void
Service::state_update ()
{
  // Set pilot idle on demand
  if ( !actor ().is_idle () ) {
    if ( _pilot_state.is_idle () && //
         provider ().ship_io ().output_satisfied () ) {
      actor ().go_idle ();
    }
  }

  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::fac::axis_move::office
