/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/dynamics.hpp>

namespace snc::svs::fac::axis_move
{

class Speeding : public Dynamics
{
  public:
  // -- Construction and setup

  Speeding () = default;

  Speeding ( const Dynamics & dynamics_n, bool forward_n )
  : Dynamics ( dynamics_n )
  , _forward ( forward_n )
  {
  }

  /// @brief Reset to default construction state
  void
  reset ()
  {
    Dynamics::reset ();
    _forward = false;
  }

  // -- Forward

  bool
  forward () const
  {
    return _forward;
  }

  void
  set_forward ( bool flag_n )
  {
    _forward = flag_n;
  }

  // -- Comparison operators

  bool
  operator== ( const Speeding & speed_n ) const
  {
    return Dynamics::operator== ( speed_n ) && ( _forward == speed_n._forward );
  }

  bool
  operator!= ( const Speeding & speed_n ) const
  {
    return Dynamics::operator!= ( speed_n ) || ( _forward != speed_n._forward );
  }

  private:
  // -- Attributes
  bool _forward = false;
};

} // namespace snc::svs::fac::axis_move
