/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/emb/step_splitter.hpp>
#include <snc/svs/fac/axis_move/ship/events.hpp>
#include <snc/svs/fac/axis_move/ship/pilot_bare.hpp>

namespace snc::svs::fac::axis_move::ship
{

class Pilot : public Pilot_Bare
{
  public:
  // -- Types

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Pilot ( const Sailor_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Pilot ();

  // -- Office event processing

  public:
  void
  office_event ( Office_Event & event_n ) override;

  private:
  void
  office_event_break ();

  void
  office_event_dynamics ( Office_Event & event_n );

  void
  office_event_target ( Office_Event & event_n );

  void
  office_event_target_delta ( Office_Event & event_n );

  void
  office_event_speed ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Axis speed pilot interface

  void
  pilot_state_changed () override;

  private:
  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::axis_move::ship
