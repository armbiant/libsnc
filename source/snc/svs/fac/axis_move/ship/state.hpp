/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::svs::fac::axis_move::ship
{

class State
{
  public:
  // -- Types

  enum class Run_State
  {
    IDLE,
    ACTIVE,
    BREAKING
  };

  // -- Setup

  void
  reset ()
  {
    _run_state = Run_State::IDLE;
  }

  // -- Accessors

  const Run_State &
  run_state () const
  {
    return _run_state;
  }

  void
  set_run_state ( Run_State state_n )
  {
    _run_state = state_n;
  }

  bool
  is_idle () const
  {
    return _run_state == Run_State::IDLE;
  }

  bool
  is_active () const
  {
    return _run_state == Run_State::ACTIVE;
  }

  bool
  is_breaking () const
  {
    return _run_state == Run_State::BREAKING;
  }

  private:
  // -- Attributes
  Run_State _run_state = Run_State::IDLE;
};

} // namespace snc::svs::fac::axis_move::ship
