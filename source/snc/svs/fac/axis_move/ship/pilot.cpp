/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot.hpp"

namespace snc::svs::fac::axis_move::ship
{

Pilot::Pilot ( const Sailor_Init & init_n, std::uint_fast32_t axis_index_n )
: Pilot_Bare ( init_n,
               sev::string::cat ( "Pilot-Move-Axis[", axis_index_n, "]" ),
               axis_index_n )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 2 );
}

Pilot::~Pilot () = default;

void
Pilot::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Type::BREAK:
    office_event_break ();
    break;
  case ship::event::in::Type::DYNAMICS:
    office_event_dynamics ( event_n );
    break;
  case ship::event::in::Type::TARGET:
    office_event_target ( event_n );
    break;
  case ship::event::in::Type::TARGET_DELTA:
    office_event_target_delta ( event_n );
    break;
  case ship::event::in::Type::SPEED:
    office_event_speed ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot::office_event_break ()
{
  break_motion ();
}

void
Pilot::office_event_dynamics ( Office_Event & event_n )
{
  load_dynamics ( static_cast< const ship::event::in::Dynamics & > ( event_n )
                      .dynamics () );
}

void
Pilot::office_event_target ( Office_Event & event_n )
{
  load_target (
      static_cast< const ship::event::in::Target & > ( event_n ).position () );
}

void
Pilot::office_event_target_delta ( Office_Event & event_n )
{
  load_target_delta (
      static_cast< const ship::event::in::Target_Delta & > ( event_n )
          .delta () );
}

void
Pilot::office_event_speed ( Office_Event & event_n )
{
  load_speeding (
      static_cast< const ship::event::in::Speed & > ( event_n ).speed () );
}

void
Pilot::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( pilot_state () );
    office_io ().submit ( event );
  }
}

void
Pilot::pilot_state_changed ()
{
  Pilot_Bare::pilot_state_changed ();
  // Request state event
  office_io ().notification_set ( Office_Message::STATE );
}

} // namespace snc::svs::fac::axis_move::ship
