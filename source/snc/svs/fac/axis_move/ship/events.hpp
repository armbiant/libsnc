/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>
#include <snc/svs/fac/axis_move/ship/state.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_move::ship::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t BREAK = 0;
  static constexpr std::uint_fast32_t DYNAMICS = 1;
  static constexpr std::uint_fast32_t TARGET = 2;
  static constexpr std::uint_fast32_t TARGET_DELTA = 3;
  static constexpr std::uint_fast32_t SPEED = 4;
};

using In = snc::svp::ship::event::In;

class Break : public In
{
  public:
  static constexpr auto etype = Type::BREAK;

  Break ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }
};

class Dynamics : public In
{
  public:
  static constexpr auto etype = Type::DYNAMICS;

  Dynamics ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _dynamics.reset ();
  }

  // -- Dynamics

  const snc::svs::fac::axis_move::Dynamics &
  dynamics () const
  {
    return _dynamics;
  }

  void
  set_dynamics ( const snc::svs::fac::axis_move::Dynamics & dyn_n )
  {
    _dynamics = dyn_n;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::Dynamics _dynamics;
};

class Target : public In
{
  public:
  static constexpr auto etype = Type::TARGET;

  Target ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _position = 0;
  }

  // -- Dynamics

  const std::int64_t &
  position () const
  {
    return _position;
  }

  void
  set_position ( std::int64_t pos_n )
  {
    _position = pos_n;
  }

  private:
  // -- Attributes
  std::int64_t _position;
};

class Target_Delta : public In
{
  public:
  static constexpr auto etype = Type::TARGET_DELTA;

  Target_Delta ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _delta = 0;
  }

  // -- Dynamics

  const std::int64_t &
  delta () const
  {
    return _delta;
  }

  void
  set_delta ( std::int64_t delta_n )
  {
    _delta = delta_n;
  }

  private:
  // -- Attributes
  std::int64_t _delta;
};

class Speed : public In
{
  public:
  static constexpr auto etype = Type::SPEED;

  Speed ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _speed.reset ();
  }

  // -- Speeding

  const snc::svs::fac::axis_move::Speeding &
  speed () const
  {
    return _speed;
  }

  void
  set_speed ( const snc::svs::fac::axis_move::Speeding & speed_n )
  {
    _speed = speed_n;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::Speeding _speed;
};

} // namespace snc::svs::fac::axis_move::ship::event::in

namespace snc::svs::fac::axis_move::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::ship::event::out;

/// @brief Pilot state event
using State = State_T< Type::STATE, State >;

} // namespace snc::svs::fac::axis_move::ship::event::out
