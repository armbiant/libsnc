/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_axis.hpp"

namespace snc::svs::ctl::base
{

Service_Axis::Service_Axis ( Provider_Cell & provider_n,
                             std::uint_fast32_t axis_index_n )
: Super ( provider_n )
, _axis_index ( axis_index_n )
{
}

Service_Axis::~Service_Axis () = default;

} // namespace snc::svs::ctl::base
