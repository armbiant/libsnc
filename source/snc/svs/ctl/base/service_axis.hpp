/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <cstdint>

namespace snc::svs::ctl::base
{

/** @brief Axis service base class */
class Service_Axis : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  public:
  // -- Construction

  Service_Axis ( Provider_Cell & provider_n, std::uint_fast32_t axis_index_n );

  ~Service_Axis ();

  // -- Accessors

  /// @brief Axis to control
  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  private:
  const std::uint_fast32_t _axis_index = 0;
};

} // namespace snc::svs::ctl::base
