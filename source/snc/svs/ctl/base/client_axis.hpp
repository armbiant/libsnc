/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <cstdint>

namespace snc::svs::ctl::base
{

/** @brief Axis client base class*/
class Client_Axis : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client_Axis ( User * user_n, std::uint_fast32_t axis_index_n );

  // -- Accessors

  /// @brief Axis to control
  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  // -- Service connection
  protected:
  template < typename SVS >
  inline bool
  connect_to_axis ( Service_Abstract * sb_n )
  {
    auto * svp = dynamic_cast< SVS * > ( sb_n );
    return ( ( svp != nullptr ) && ( svp->axis_index () == axis_index () ) );
  }

  private:
  std::uint_fast32_t _axis_index = 0;
};

} // namespace snc::svs::ctl::base
