/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_axis.hpp"

namespace snc::svs::ctl::base
{

Client_Axis::Client_Axis ( User * user_n, std::uint_fast32_t axis_index_n )
: Super ( user_n )
, _axis_index ( axis_index_n )
{
}

} // namespace snc::svs::ctl::base
