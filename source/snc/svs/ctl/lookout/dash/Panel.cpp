/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svs/ctl/lookout/office/clerk.hpp>

namespace snc::svs::ctl::lookout::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Lookout" )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::lookout::Clerk > ();
}

} // namespace snc::svs::ctl::lookout::dash
