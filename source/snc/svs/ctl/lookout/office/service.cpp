/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>

namespace snc::svs::ctl::lookout
{

Service::Service ( Provider_Cell & provider_n )
: Super ( provider_n )
, _lookout ( provider_cell ().cell_context ().thread_tracker (), log () )
{
  _lookout.set_event_callback ( provider_cell ().processing_request_async () );
  _lookout_callbacks.device_new = [ this ] () { this->device_new (); };
  _lookout_callbacks.device_remove = [ this ] () { this->device_remove (); };
  _lookout_callbacks.stopped = [ this ] () { this->lookout_stopped (); };
}

void
Service::set_enabled ( bool flag_n )
{
  if ( !provider_cell ().cell_session ().is_good ) {
    flag_n = false;
  }
  if ( !sev::change ( _is_enabled, flag_n ) ) {
    return;
  }
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Set enabled: " << is_enabled ();
  }
  process ();
  signal_state ().send ();
}

void
Service::add_usb_device_query (
    std::shared_ptr< const snc::usb::Device_Query > query_n )
{
  if ( !query_n ) {
    DEBUG_ASSERT ( false );
    return;
  }

  if ( !is_enabled () ) {
    log ().cat ( sev::logt::FL_WARNING,
                 "Ignoring usb device query add. Not enabled." );
    return;
  }

  // Debug
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Adding device query:\n";
    if ( query_n->manufacturer ) {
      logo << "  Manufacturer: " << query_n->manufacturer->regex_string << "\n";
    }
    if ( query_n->product ) {
      logo << "  Product: " << query_n->product->regex_string << "\n";
    }
    if ( query_n->serial_number ) {
      logo << "  Serial number: " << query_n->serial_number->regex_string
           << "\n";
    }
  }

  _lookout.add_usb_device_query ( std::move ( query_n ) );
}

void
Service::process ()
{
  // Enable / disable lookout
  if ( is_enabled () ) {
    if ( !_lookout.is_running () ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Starting lookout." );
      _lookout.start ();
    }
  } else {
    if ( _lookout.is_running () && !_lookout.is_aborting () ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Aborting lookout." );
      _lookout.abort ();
    }
  }

  if ( _lookout.is_running () ) {
    _lookout.events ( _lookout_callbacks );
  }
}

void
Service::device_new ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Device appeared." );

  _devices.emplace_back ( _lookout.event_device () );
  signal_state ().send ();
}

void
Service::device_remove ()
{
  DEBUG_ASSERT ( _devices.size () >= 1 );
  log ().cat ( sev::logt::FL_DEBUG_0, "Device removed." );

  if ( _devices.empty () ) {
    return;
  }

  // Find and remove device from list
  {
    auto itc = _devices.begin ();
    auto ite = _devices.end ();
    for ( ; itc != ite; ++itc ) {
      if ( *itc == _lookout.event_device () ) {
        _devices.erase ( itc );
        break;
      }
    }
  }
  signal_state ().send ();
}

void
Service::lookout_stopped ()
{
  this->log ().cat ( sev::logt::FL_DEBUG_0, "Lookout stopped." );
  // Clear devices
  _devices.clear ();
  // Notify
  signal_state ().send ();
  provider_cell ().request_processing ();
}

} // namespace snc::svs::ctl::lookout
