/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/serial_device/device.hpp>
#include <snc/serial_device_lookout/lookout.hpp>
#include <snc/svs/ctl/lookout/office/service.hpp>
#include <snc/usb/device_query.hpp>
#include <memory>

namespace snc::svs::ctl::lookout
{

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  public:
  // -- Construction

  Service ( Provider_Cell & provider_n );

  // -- Accessors

  bool
  is_enabled () const
  {
    return _is_enabled;
  }

  void
  set_enabled ( bool flag_n );

  bool
  is_running () const
  {
    return _lookout.is_running ();
  }

  const std::vector< std::shared_ptr< snc::serial_device::Letter > > &
  devices ()
  {
    return _devices;
  }

  // -- Central processing

  void
  process ();

  // -- Client interface

  void
  add_usb_device_query (
      std::shared_ptr< const snc::usb::Device_Query > query_n );

  private:
  // -- Utility

  void
  device_new ();

  void
  device_remove ();

  void
  lookout_stopped ();

  private:
  // -- Attributes
  bool _is_enabled = false;
  snc::serial_device_lookout::Lookout _lookout;
  snc::serial_device_lookout::Lookout::Callbacks _lookout_callbacks;
  std::vector< std::shared_ptr< snc::serial_device::Letter > > _devices;
};

} // namespace snc::svs::ctl::lookout
