/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"

namespace snc::svs::ctl::lookout
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Lookout" )
, _service ( *this )
{
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_abort ()
{
  Super::cell_session_abort ();
  _service.set_enabled ( false );
}

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && !_service.is_running ();
}

void
Clerk::process_async ()
{
  _service.process ();
}

void
Clerk::process ()
{
  _service.process ();
}

} // namespace snc::svs::ctl::lookout
