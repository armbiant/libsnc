/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "query.hpp"

namespace snc::svs::ctl::lookout
{

Query::Query ( std::shared_ptr< const snc::emb::config::Config > emb_config_n,
               std::shared_ptr< const snc::usb::Device_Query > usb_query_n )
: _emb_config ( std::move ( emb_config_n ) )
, _usb_query ( std::move ( usb_query_n ) )
{
}

} // namespace snc::svs::ctl::lookout
