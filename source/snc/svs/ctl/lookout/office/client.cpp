/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/ctl/lookout/office/service.hpp>

namespace snc::svs::ctl::lookout
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

bool
Client::is_enabled () const
{
  return is_connected () ? service ()->is_enabled () : false;
}

bool
Client::enable ()
{
  if ( !is_connected () ) {
    return false;
  }
  service ()->set_enabled ( true );
  return service ()->is_enabled ();
}

void
Client::disable ()
{
  if ( !is_connected () ) {
    return;
  }
  service ()->set_enabled ( false );
}

void
Client::add_usb_device_query (
    const std::shared_ptr< const snc::usb::Device_Query > & query_n )
{
  if ( !is_connected () ) {
    return;
  }
  service ()->add_usb_device_query ( query_n );
}

std::shared_ptr< snc::serial_device::Letter >
Client::device ()
{
  if ( is_connected ()              //
       && service ()->is_enabled () //
       && !service ()->devices ().empty () ) {
    return service ()->devices ().at ( 0 );
  }
  return std::shared_ptr< snc::serial_device::Letter > ();
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::ctl::lookout
