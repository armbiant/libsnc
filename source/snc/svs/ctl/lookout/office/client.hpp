/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/serial_device/letter.hpp>
#include <snc/usb/device_query.hpp>
#include <memory>

namespace snc::svs::ctl::lookout
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client ();

  // -- Interface

  bool
  is_enabled () const;

  bool
  enable ();

  void
  disable ();

  void
  add_usb_device_query (
      const std::shared_ptr< const snc::usb::Device_Query > & query_n );

  std::shared_ptr< snc::serial_device::Letter >
  device ();

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::ctl::lookout
