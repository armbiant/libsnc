/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/lookout/office/service.hpp>

namespace snc::svs::ctl::lookout
{

// -- Forward declaration
class Client;

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  private:
  // -- Attributes
  Service _service;
};

} // namespace snc::svs::ctl::lookout
