/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/axis_speed/office/state.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>

namespace snc::svs::ctl::axis_speed::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, office::State >;

} // namespace snc::svs::ctl::axis_speed::dash::event::in

namespace snc::svs::ctl::axis_speed::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t BREAK = 0;
  static constexpr std::uint_fast32_t SPEED = 1;
  static constexpr std::uint_fast32_t SPEED_ACCEL = 2;
};

using Out = snc::svp::dash::event::Out;

class Break : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::BREAK;

  // -- Construction and reset

  Break ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

class Speed : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::SPEED;

  // -- Construction and reset

  Speed ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _speed = 0.0;
  }

  // -- Speed

  const double &
  speed () const
  {
    return _speed;
  }

  void
  set_speed ( double speed_n )
  {
    _speed = speed_n;
  }

  private:
  // -- Attributes
  double _speed = 0.0;
};

class Speed_Accel : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::SPEED_ACCEL;

  // -- Construction and reset

  Speed_Accel ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _speed.reset ();
  }

  // -- Speed

  const snc::svs::fac::axis_move::Speeding &
  speed () const
  {
    return _speed;
  }

  void
  set_speed ( const snc::svs::fac::axis_move::Speeding & speed_n )
  {
    _speed = speed_n;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::Speeding _speed;
};

} // namespace snc::svs::ctl::axis_speed::dash::event::out
