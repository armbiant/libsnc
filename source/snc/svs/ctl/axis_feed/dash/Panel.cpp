/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/axis_feed/office/clerk.hpp>

namespace snc::svs::ctl::axis_feed::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Machine-Axis[", axis_index_n, "]-Feed" ) )
, _axis_index ( axis_index_n )
, _event_pool_direction ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::axis_feed::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_direction, 2 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.available, state ().available () ) ) {
    emit availableChanged ();
  }
  if ( sev::change ( _qtState.active, state ().is_active () ) ) {
    emit activeChanged ();
  }
  if ( sev::change ( _qtState.feeding, state ().is_feeding () ) ) {
    emit feedingChanged ();
  }
}

bool
Panel::set_direction ( Dir dir_n )
{
  // When do we not accept the request?
  if ( !_state.available () //
       || !office_session_is_good () ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_direction );
    event->set_dir ( dir_n );
    office_io ().submit ( event );
  }

  return true;
}

bool
Panel::feedNone ()
{
  return set_direction ( Dir::NONE );
}

bool
Panel::feedForward ()
{
  return set_direction ( Dir::FORWARD );
}

bool
Panel::feedBackward ()
{
  return set_direction ( Dir::BACKWARD );
}

} // namespace snc::svs::ctl::axis_feed::dash
