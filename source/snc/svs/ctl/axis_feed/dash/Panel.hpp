/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/axis_feed/dash/events.hpp>
#include <snc/svs/ctl/axis_feed/office/state.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>

namespace snc::svs::ctl::axis_feed::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( int axisIndex READ axis_index NOTIFY axisIndexChanged )
  Q_PROPERTY ( bool available READ available NOTIFY availableChanged )
  Q_PROPERTY ( bool active READ active NOTIFY activeChanged )
  Q_PROPERTY ( bool feeding READ feeding NOTIFY feedingChanged )

  using Dir = snc::svs::fac::axis_feed::Mode::Dir;

  // -- Construction

  Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n = 0 );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Axis index

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  Q_SIGNAL
  void
  axisIndexChanged ();

  // -- State

  const office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Available

  bool
  available () const
  {
    return _qtState.available;
  }

  Q_SIGNAL
  void
  availableChanged ();

  // -- Active

  bool
  active () const
  {
    return _qtState.active;
  }

  Q_SIGNAL
  void
  activeChanged ();

  // -- Feeding

  bool
  feeding () const
  {
    return _qtState.feeding;
  }

  Q_SIGNAL
  void
  feedingChanged ();

  // -- Request interface

  bool
  set_direction ( Dir dir_n );

  Q_INVOKABLE
  bool
  feedNone ();

  Q_INVOKABLE
  bool
  feedForward ();

  Q_INVOKABLE
  bool
  feedBackward ();

  private:
  // -- State
  std::uint_fast32_t _axis_index = 0;
  office::State _state;
  struct
  {
    bool available = false;
    bool active = false;
    bool feeding = false;
  } _qtState;

  // -- Office event io
  sev::event::Pool< dash::event::out::Direction > _event_pool_direction;
};

} // namespace snc::svs::ctl::axis_feed::dash
