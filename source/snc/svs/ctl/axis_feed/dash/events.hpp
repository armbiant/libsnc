/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/axis_feed/office/state.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_feed::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, office::State >;

} // namespace snc::svs::ctl::axis_feed::dash::event::in

namespace snc::svs::ctl::axis_feed::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t DIRECTION = 0;
};

using Out = snc::svp::dash::event::Out;

class Direction : public Out
{
  public:
  static constexpr auto etype = Type::DIRECTION;

  using Dir = snc::svs::fac::axis_feed::Mode::Dir;

  Direction ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _dir = Dir::NONE;
  }

  // -- Feed direction

  const Dir &
  dir () const
  {
    return _dir;
  }

  void
  set_dir ( Dir dir_n )
  {
    _dir = dir_n;
  }

  private:
  // -- Attributes
  Dir _dir = Dir::NONE;
};

} // namespace snc::svs::ctl::axis_feed::dash::event::out
