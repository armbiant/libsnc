/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/statics/statics.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_feed::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Machine-Axis[", axis_index_n, "]-Feed" ) )
, _service ( *this, axis_index_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.init_connections ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::DIRECTION:
    dash_event_direction ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_direction ( Dash_Event & event_n )
{
  auto & cevent =
      static_cast< const dash::event::out::Direction & > ( event_n );
  _service.request_direction ( cevent.dir () );
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
    _state_emitted = _service.state ();
  }
}

void
Clerk::process ()
{
  _service.process ();
}

void
Clerk::state_changed ()
{
  if ( _state_emitted != _service.state () ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::axis_feed::office
