/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/control_unique.hpp>
#include <snc/svs/ctl/base/client_axis.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>

namespace snc::svs::ctl::axis_feed::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::svs::ctl::base::Client_Axis
{
  // -- Types
  private:
  using Super = snc::svs::ctl::base::Client_Axis;

  public:
  // -- Construction

  Client ( User * user_n, std::uint_fast32_t axis_index_n );

  ~Client ();

  // -- Service control interface

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    // -- Client interface

    Client *
    client () const
    {
      return static_cast< Client * > ( client_abstract () );
    }

    // -- Control interface

    /// @brief Set the default mode for a feed direction
    bool
    set_default_mode ( const snc::svs::fac::axis_feed::Mode & mode_n );
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::ctl::axis_feed::office
