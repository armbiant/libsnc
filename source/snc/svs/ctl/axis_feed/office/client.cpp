/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/ctl/axis_feed/office/service.hpp>

namespace snc::svs::ctl::axis_feed::office
{

Client::Client ( User * user_n, std::uint_fast32_t axis_index_n )
: Super ( user_n, axis_index_n )
{
}

Client::~Client () = default;

bool
Client::Control::set_default_mode (
    const snc::svs::fac::axis_feed::Mode & mode_n )
{
  return is_valid ()
             ? client ()->service ()->control_set_default_mode ( mode_n )
             : false;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return connect_to_axis< Service > ( sb_n );
}

} // namespace snc::svs::ctl::axis_feed::office
