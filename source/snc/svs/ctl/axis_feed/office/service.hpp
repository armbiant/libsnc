/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/ctl/axis_feed/office/state.hpp>
#include <snc/svs/ctl/base/service_axis.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>
#include <snc/svs/fac/axis_feed/office/client.hpp>
#include <cstdint>
#include <optional>

namespace snc::svs::ctl::axis_feed::office
{

// -- Forward declaration
class Clerk;
class Client;

/** @brief Service */
class Service : public snc::svs::ctl::base::Service_Axis
{
  // -- Types
  private:
  using Super = snc::svs::ctl::base::Service_Axis;
  using Provider = Clerk;

  struct Request
  {
    static constexpr std::uint_fast32_t DIRECTION = ( 1 << 0 );
  };

  struct Requests
  {
    void
    reset ()
    {
      reg.clear ();
      dir = snc::svs::fac::axis_feed::Mode::Dir::NONE;
    }

    sev::mem::Flags_Fast32 reg;
    snc::svs::fac::axis_feed::Mode::Dir dir =
        snc::svs::fac::axis_feed::Mode::Dir::NONE;
  };

  struct Feed_Modes
  {
    void
    reset ()
    {
      forward.reset ();
      backward.reset ();
    }

    std::optional< snc::svs::fac::axis_feed::Mode > forward;
    std::optional< snc::svs::fac::axis_feed::Mode > backward;
  };

  public:
  // -- Construction and session

  Service ( Provider & provider_n, std::uint_fast32_t axis_index_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  const auto &
  feed_modes_user () const
  {
    return _feed_modes_user;
  }

  const auto &
  feed_modes_auto () const
  {
    return _feed_modes_auto;
  }

  const auto &
  state () const
  {
    return _state;
  }

  // -- Provider interface

  void
  init_connections ();

  void
  request_direction ( snc::svs::fac::axis_feed::Mode::Dir dir_n );

  void
  process ();

  // -- Processing utility
  private:
  void
  process_requests ();

  void
  state_update ();

  // -- Session interface
  private:
  void
  session_begin ();

  void
  session_end ();

  // -- Client control interface
  private:
  friend snc::svs::ctl::axis_feed::office::Client;

  bool
  control_acquirable () const override;

  bool
  control_set_default_mode ( const snc::svs::fac::axis_feed::Mode & mode_n );

  private:
  // -- Resources
  snc::svs::fac::axis_feed::office::Client _pilot;
  Feed_Modes _feed_modes_user;
  Feed_Modes _feed_modes_auto;
  Requests _requests;
  State _state;
};

} // namespace snc::svs::ctl::axis_feed::office
