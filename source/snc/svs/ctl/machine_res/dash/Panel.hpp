/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/machine_res/office/state.hpp>

namespace snc::svs::ctl::machine_res::dash
{

/** @brief Panel */
class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State

  const office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  Q_SIGNAL
  void
  stateChanged ();

  private:
  // -- Attributes
  office::State _state;
};

} // namespace snc::svs::ctl::machine_res::dash
