/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/machine_res/dash/events.hpp>
#include <snc/svs/ctl/machine_res/office/clerk.hpp>

namespace snc::svs::ctl::machine_res::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine" )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::machine_res::office::Clerk > ();
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();
  emit stateChanged ();
}

} // namespace snc::svs::ctl::machine_res::dash
