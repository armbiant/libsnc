/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/res/pool.hpp>
#include <snc/svs/ctl/machine_res/office/state.hpp>
#include <snc/svs/fac/machine_startup/office/client.hpp>
#include <cstdint>

namespace snc::svs::ctl::machine_res::office
{

// -- Forward declaration
class Clerk;

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Clerk;

  public:
  /// @brief Break lock types
  struct Break_Lock
  {
    /// @brief Session abort lock
    static constexpr std::uint8_t ABORT = ( 1 << 0 );
    /// @brief Hold the machine once and release the lock
    static constexpr std::uint8_t SINGLE_HOLD = ( 1 << 1 );
  };

  // -- Construction

  Service ( Provider & provider_n );

  ~Service ();

  void
  init_connections ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  const State &
  state () const
  {
    return _state;
  }

  // -- Provider interface

  void
  session_begin ();

  void
  session_abort ();

  void
  session_end ();

  void
  process ();

  // -- Machine resources

  /// @brief Probe if the resources are available
  bool
  resources_probe ( const snc::res::Lease & lease_n );

  /// @brief Lease the requested resources
  bool
  resources_acquire ( snc::res::Lease & lease_n );

  /// @brief Release the requested resources
  void
  resources_release ( snc::res::Lease & lease_n );

  bool
  resources_leased () const
  {
    return ( _resources.pool.leases () != 0 );
  }

  private:
  // -- Utility

  void
  state_updated ();

  private:
  // -- State
  State _state;
  // -- Machine resources
  struct Machine_Resources
  {
    snc::res::Pool pool;
  } _resources;
  // -- Machine startup
  snc::svs::fac::machine_startup::office::Client _machine_startup;
};

} // namespace snc::svs::ctl::machine_res::office
