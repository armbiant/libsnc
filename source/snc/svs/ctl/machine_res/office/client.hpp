/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/res/lease.hpp>

namespace snc::svp::office
{
class Clerk;
}

namespace snc::svs::ctl::machine_res::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client ( snc::svp::office::Clerk * user_n );

  ~Client ();

  // -- Accessors

  const snc::res::Lease &
  leases () const
  {
    return _leases;
  }

  snc::res::Lease &
  leases_ref ()
  {
    return _leases;
  }

  // -- Interface: Resources

  /// @brief Probe if the resources are available
  bool
  resources_probe () const;

  /// @brief Lease the requested resources
  bool
  resources_acquire ();

  /// @brief Release the requested resources
  void
  resources_release ();

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  private:
  // -- Resources
  snc::res::Lease _leases;
};

} // namespace snc::svs::ctl::machine_res::office
