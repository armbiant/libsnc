/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/machine_res/office/service.hpp>

namespace snc::svs::ctl::machine_res::office
{

Client::Client ( snc::svp::office::Clerk * user_n )
: Super ( user_n )
, _leases ( user_n )
{
}

Client::~Client () = default;

bool
Client::resources_probe () const
{
  return is_connected () ? service ()->resources_probe ( _leases ) : false;
}

bool
Client::resources_acquire ()
{
  return is_connected () ? service ()->resources_acquire ( _leases ) : false;
}

void
Client::resources_release ()
{
  if ( is_connected () ) {
    service ()->resources_release ( _leases );
  }
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::ctl::machine_res::office
