/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/res/lease.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/clerk.hpp>
#include <snc/svs/ctl/machine_res/office/clerk.hpp>

namespace snc::svs::ctl::machine_res::office
{

Service::Service ( Provider & provider_n )
: Super ( provider_n )
, _machine_startup ( &provider_n )
{
}

Service::~Service () = default;

void
Service::init_connections ()
{
  _machine_startup.connect_required ();
  _machine_startup.session ().set_begin_callback (
      [ this ] () { session_begin (); } );
  _machine_startup.session ().set_end_callback (
      [ this ] () { session_end (); } );
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  // Setup machine resources
  _resources.pool.fill ( _machine_startup.device_info_handle ()->statics () );
  signal_state ().send ();

  // Notify clients
  state_updated ();
  clients_session_begin ();
}

void
Service::session_end ()
{
  // Clean machine resoures
  _resources.pool.reset ();
  signal_state ().send ();

  // Update state
  _state.reset ();
  state_updated ();

  // Notify clients
  clients_session_end ();
}

void
Service::process ()
{
}

bool
Service::resources_probe ( const snc::res::Lease & lease_n )
{
  return _resources.pool.probe ( lease_n );
}

bool
Service::resources_acquire ( snc::res::Lease & lease_n )
{
  if ( !_resources.pool.acquire ( lease_n ) ) {
    return false;
  }
  signal_state ().send ();
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Machine resources acquired by " << lease_n.clerk ()->cell_type ()
         << ": " << lease_n.registrations_string ();
  }
  return true;
}

void
Service::resources_release ( snc::res::Lease & lease_n )
{
  if ( !lease_n.is_leasing () ) {
    return;
  }
  _resources.pool.release ( lease_n );
  signal_state ().send ();

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Machine resources released by " << lease_n.clerk ()->cell_type ()
         << ": " << lease_n.registrations_string ();
  }
}

void
Service::state_updated ()
{
  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::ctl::machine_res::office
