/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "state.hpp"

namespace snc::svs::ctl::machine_res::office
{

void
State::reset ()
{
}

bool
State::operator== ( const State & state_n [[maybe_unused]] ) const
{
  return true;
}

bool
State::operator!= ( const State & state_n [[maybe_unused]] ) const
{
  return false;
}

} // namespace snc::svs::ctl::machine_res::office
