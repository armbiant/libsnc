/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/align_axis/dash/events.hpp>
#include <snc/svs/ctl/align_axis/job.hpp>
#include <snc/svs/ctl/align_axis/office/state.hpp>
#include <snc/svs/fac/axis_align/office/client.hpp>
#include <cstdint>

namespace snc::svs::ctl::align_axis::office
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Clerk ();

  // -- Accessors

  std::uint_fast32_t
  axis_index () const
  {
    return _pilot.axis_index ();
  }

  const State &
  state () const
  {
    return _state;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_job ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  void
  process_request ();

  private:
  // -- Utility

  bool
  service_control_acquire ();

  void
  service_control_auto_release ();

  void
  state_update ();

  private:
  // -- Resources
  snc::svs::ctl::actor::office::Client_Actor _actor;
  snc::svs::fac::axis_align::office::Client _pilot;
  snc::svs::fac::axis_align::office::Client::Control _pilot_control;

  // -- State
  State _state;
  State _state_emitted;

  // -- Job request
  Job _job_request;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::align_axis::office
