/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svs/fac/axis_align/office/state.hpp>
#include <cstdint>

namespace snc::svs::ctl::align_axis::office
{

class State : public snc::svs::fac::axis_align::office::State
{
  public:
  // -- Types
  using Super = snc::svs::fac::axis_align::office::State;

  struct Block
  {
    /// @brief The state is invalid
    static constexpr std::uint_fast32_t INVALID = ( 1 << 0 );
    /// @brief Axis does not support aligning
    static constexpr std::uint_fast32_t NOT_ALIGNABLE = ( 1 << 1 );
    /// @brief Pilot can not be started
    static constexpr std::uint_fast32_t PILOT_NOT_AVAILABLE = ( 1 << 2 );
  };

  // -- Setup

  void
  reset ()
  {
    Super::reset ();
    _blocks = Block::INVALID;
  }

  // -- Availability

  bool
  available () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast32 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast32 &
  blocks () const
  {
    return _blocks;
  }

  // -- Startable / Stoppable

  bool
  startable () const
  {
    return available () && !is_aligning ();
  }

  bool
  stoppable () const
  {
    return available () && is_aligning ();
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return Super::operator== ( state_n ) && ( _blocks == state_n._blocks );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return Super::operator!= ( state_n ) || ( _blocks != state_n._blocks );
  }

  private:
  // -- Attributes
  sev::mem::Flags_Fast32 _blocks = Block::INVALID;
};

} // namespace snc::svs::ctl::align_axis::office
