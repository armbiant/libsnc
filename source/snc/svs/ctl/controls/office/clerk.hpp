/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pools.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/controls/dash/events.hpp>
#include <snc/svs/ctl/controls/office/state.hpp>
#include <snc/svs/fac/controls/office/client.hpp>
#include <snc/svs/fac/controls/office/i1/client.hpp>
#include <snc/svs/fac/machine_startup/office/client.hpp>
#include <memory>

namespace snc::svs::ctl::controls::office
{

/** @brief Clerk
 */
class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
    static constexpr std::uint_fast32_t SETUP = ( 1 << 1 );
    static constexpr std::uint_fast32_t AVAIL = ( 1 << 2 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_request ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  // -- Utility
  private:
  void
  controls_begin ();

  void
  controls_end ();

  void
  update_availability ();

  private:
  // -- Resources
  snc::svs::fac::controls::office::Client _factor;
  snc::svs::fac::machine_startup::office::Client _machine_startup;
  struct Control
  {
    bool available = false;
    bool dash_available = false;
  };
  struct Control_I1 : public Control
  {
    using Client_Type = snc::svs::fac::controls::office::i1::Client;
    std::unique_ptr< Client_Type > client;
  };
  struct
  {
    std::vector< Control_I1 > i1;
  } _controls;

  // -- Dash event io
  sev::event::Pools< dash::event::in::State,
                     dash::event::in::Setup,
                     dash::event::in::Avail >
      _event_pools_dash;
};

} // namespace snc::svs::ctl::controls::office
