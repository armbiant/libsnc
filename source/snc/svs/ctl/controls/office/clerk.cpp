/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/svs/ctl/controls/request.hpp>
#include <cstdint>
#include <stdexcept>
#include <type_traits>

namespace snc::svs::ctl::controls::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine-Controls" )
, _factor ( this )
, _machine_startup ( this )
, _event_pools_dash ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pools_dash.pool< dash::event::in::State > (),
                        2 );
  dash_io ().allocate ( _event_pools_dash.pool< dash::event::in::Setup > (),
                        1 );
  dash_io ().allocate ( _event_pools_dash.pool< dash::event::in::Avail > (),
                        2 );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Connect to controls service
  _factor.connect_required ();
  _factor.session ().set_begin_callback ( [ this ] () { controls_begin (); } );
  _factor.session ().set_end_callback ( [ this ] () { controls_end (); } );

  _machine_startup.connect_required ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::REQUEST:
    dash_event_request ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_request ( Dash_Event & event_n )
{
  auto & cevent = static_cast< const dash::event::out::Request & > ( event_n );
  auto & req = cevent.request ();

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming request: ";
    switch ( req.type () ) {
    case Request::Type::INVALID:
      logo << "type=INVALID; index=" << req.index ();
      break;
    case Request::Type::B1:
      logo << "type=B1; index=" << req.index ();
      break;
    case Request::Type::B8:
      logo << "type=B8; index=" << req.index ();
      break;
    case Request::Type::B16:
      logo << "type=B16; index=" << req.index ();
      break;
    case Request::Type::B32:
      logo << "type=B32; index=" << req.index ();
      break;
    case Request::Type::B64:
      logo << "type=B64; index=" << req.index ();
      break;
    }
    switch ( req.action () ) {
    case Request::Action::INVALID:
      logo << "; action=INVALID";
      break;
    case Request::Action::SET:
      logo << "; action=SET; value=" << req.value ();
      break;
    case Request::Action::INVERT:
      logo << "; action=INVERT";
      break;
    }
  }

  switch ( req.type () ) {
  case Request::Type::INVALID:
    break;
  case Request::Type::B1:
    if ( req.index () >= _controls.i1.size () ) {
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Invalid request control index " << req.index ();
      }
    } else {
      auto control = _controls.i1[ req.index () ].client->control ();
      switch ( req.action () ) {
      case Request::Action::INVALID:
        break;
      case Request::Action::SET:
        control.set_value ( req.value () );
        break;
      case Request::Action::INVERT:
        control.toggle ();
        break;
      }
    }
    break;

  case Request::Type::B8:
    break;
  case Request::Type::B16:
    break;
  case Request::Type::B32:
    break;
  case Request::Type::B64:
    break;
  }
}

void
Clerk::dash_notify ()
{
  if ( auto sender = dash_notifier (
           Dash_Message::STATE,
           _event_pools_dash.pool< dash::event::in::State > () ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
  }

  if ( auto sender = dash_notifier (
           Dash_Message::SETUP,
           _event_pools_dash.pool< dash::event::in::Setup > () ) ) {
    auto & control_setups = sender.event->i1_ref ();
    control_setups.reserve ( _controls.i1.size () );
    std::size_t index = 0;
    for ( auto & control : _controls.i1 ) {
      if ( auto statics = control.client->control_statics () ) {
        dash::Control_Setup setup;
        setup.index = index;
        setup.control_index = control.client->control_index ();
        setup.name = statics->name ();
        control_setups.push_back ( setup );
      }
      ++index;
    }
  }

  if ( dash_io ().notifications ().test_any ( Dash_Message::AVAIL ) ) {
    dash_io ().notification_unset ( Dash_Message::AVAIL );
    std::size_t index = 0;
    for ( auto & control : _controls.i1 ) {
      if ( sev::change ( control.dash_available, control.available ) ) {
        auto * event = dash_io ().acquire (
            _event_pools_dash.pool< dash::event::in::Avail > () );
        event->set_index ( index );
        event->set_avail ( control.dash_available );
        dash_io ().submit ( event );
      }
      ++index;
    }
  }
}

void
Clerk::controls_begin ()
{
  auto stats = _factor.stats ();

  auto build = [ this ] ( std::vector< auto > & vector_n, std::size_t num_n ) {
    using CTR = decltype ( vector_n.front () );
    using Control_Type = typename std::remove_reference< CTR >::type;
    using Client_Type = typename Control_Type::Client_Type;

    vector_n.reserve ( num_n );
    for ( std::size_t ii = 0; ii != num_n; ++ii ) {
      auto control = Control_Type ();
      control.client = std::make_unique< Client_Type > ( this, ii );
      if ( !control.client->connect () ) {
        continue;
      }
      auto statics = control.client->control_statics ();
      if ( !statics || !statics->user_settable () ) {
        continue;
      }
      vector_n.emplace_back ( std::move ( control ) );
    }
  };

  build ( _controls.i1, stats.i1 );

  // Notify
  dash_io ().notification_set ( Dash_Message::SETUP );
  update_availability ();
}

void
Clerk::controls_end ()
{
  _controls.i1.clear ();

  // Notify
  dash_io ().notification_set ( Dash_Message::SETUP );
}

void
Clerk::process ()
{
  update_availability ();
}

void
Clerk::update_availability ()
{
  for ( auto & control : _controls.i1 ) {
    bool available = control.client->control_available () &&
                     _machine_startup.driver_ready ();
    if ( sev::change ( control.available, available ) ) {
      dash_io ().notification_set ( Dash_Message::AVAIL );
    }
  }
}

} // namespace snc::svs::ctl::controls::office
