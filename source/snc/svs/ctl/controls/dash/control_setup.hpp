/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace snc::svs::ctl::controls::dash
{

class Control_Setup
{
  public:
  // -- Construction

  Control_Setup () = default;

  std::uint_fast32_t index = 0;
  std::uint_fast32_t control_index = 0;
  std::string name;
};

using Control_Setup_List = std::vector< Control_Setup >;

} // namespace snc::svs::ctl::controls::dash
