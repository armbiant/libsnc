/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/ctl/controls/dash/control_setup.hpp>
#include <snc/svs/ctl/controls/dash/controls_list_t.hpp>
#include <snc/svs/ctl/controls/dash/i1/Control.hpp>
#include <QAbstractListModel>
#include <memory>
#include <vector>

// -- Forward declaration
namespace snc::svs::ctl::controls
{
class Panel;
}

namespace snc::svs::ctl::controls::dash::i1
{

/*** Controls list */
class List : public QAbstractListModel
{
  Q_OBJECT

  // -- Types

  enum ExtraRole
  {
    listItem = Qt::UserRole
  };

  // -- Properties

  Q_PROPERTY ( int count READ count NOTIFY countChanged )

  public:
  // -- Construction

  List ( QObject * qparent_n );

  ~List ();

  // -- Number of controls

  std::size_t
  size ()
  {
    return _list.size ();
  }

  int
  count ()
  {
    return _list.size ();
  }

  Q_SIGNAL
  void
  countChanged ();

  // -- QAbstractListModel interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  // -- Interface

  void
  setup ( Panel * panel_n, const Control_Setup_List & control_setups_n );

  void
  set_available ( std::size_t index_n, bool flag_n );

  private:
  // -- Attributes
  Controls_List_T< Control > _list;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::svs::ctl::controls::dash::i1
