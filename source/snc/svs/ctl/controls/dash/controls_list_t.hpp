/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <QModelIndex>
#include <memory>
#include <vector>

namespace snc::svs::ctl::controls::dash
{

/*** Control list template with utility functions */
template < typename T >
class Controls_List_T
{
  public:
  // -- Types

  using Handle = std::shared_ptr< T >;
  using List = std::vector< Handle >;

  // -- Construction

  Controls_List_T () = default;

  ~Controls_List_T () = default;

  // -- Access

  std::size_t
  size () const
  {
    return _list.size ();
  }

  bool
  is_empty () const
  {
    return _list.empty ();
  }

  const List &
  list () const
  {
    return _list;
  }

  const Handle &
  at ( std::size_t index_n ) const
  {
    return _list.at ( index_n );
  }

  // -- Modify

  void
  clear ()
  {
    _list.clear ();
  }

  void
  push ( Handle handle_n )
  {
    _list.emplace_back ( std::move ( handle_n ) );
  }

  // -- Model indices

  bool
  validParentIndex ( const QModelIndex & index_n ) const
  {
    return !index_n.isValid ();
  }

  bool
  validIndex ( const QModelIndex & index_n ) const
  {
    return index_n.isValid () && //
           ( std::size_t ( index_n.row () ) < _list.size () ) &&
           validParentIndex ( index_n.parent () );
  }

  private:
  List _list;
};

} // namespace snc::svs::ctl::controls::dash
