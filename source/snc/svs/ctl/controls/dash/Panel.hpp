/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/controls/dash/Controls.hpp>
#include <snc/svs/ctl/controls/dash/events.hpp>
#include <snc/svs/ctl/controls/office/state.hpp>
#include <snc/svs/ctl/controls/request.hpp>

namespace snc::svs::ctl::controls::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( snc::svs::ctl::controls::dash::i1::List * i1 READ i1 CONSTANT )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  void
  office_event_setup ( Office_Event & event_n );

  void
  office_event_avail ( Office_Event & event_n );

  // -- Controls

  snc::svs::ctl::controls::dash::i1::List *
  i1 ()
  {
    return &_controls.i1 ();
  }

  // -- Request interface

  void
  submit_request ( const Request & req_n );

  private:
  // -- Resources
  office::State _state;
  dash::Controls _controls;

  // -- Office event io
  sev::event::Pool< dash::event::out::Request > _event_pool_request;
};

} // namespace snc::svs::ctl::controls::dash
