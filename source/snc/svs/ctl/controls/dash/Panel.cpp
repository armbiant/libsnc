/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/controls/office/clerk.hpp>

namespace snc::svs::ctl::controls::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine-Controls" )
, _controls ( this )
, _event_pool_request ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::controls::office::Clerk > ();
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_request, 4 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  case dash::event::in::Type::SETUP:
    office_event_setup ( event_n );
    break;
  case dash::event::in::Type::AVAIL:
    office_event_avail ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::office_event_setup ( Office_Event & event_n )
{
  auto & cevent = static_cast< const dash::event::in::Setup & > ( event_n );
  _controls.i1 ().setup ( this, cevent.i1 () );
}

void
Panel::office_event_avail ( Office_Event & event_n )
{
  auto & cevent = static_cast< const dash::event::in::Avail & > ( event_n );
  _controls.i1 ().set_available ( cevent.index (), cevent.avail () );
}

void
Panel::submit_request ( const Request & req_n )
{
  // Error checks
  if ( !office_session_is_good () ) {
    return;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_request );
    event->set_request ( req_n );
    office_io ().submit ( event );
  }
}

} // namespace snc::svs::ctl::controls::dash
