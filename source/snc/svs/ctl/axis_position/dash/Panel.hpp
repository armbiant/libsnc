/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/axis_position/dash/events.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>

namespace snc::svs::ctl::axis_position::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( int axisIndex READ axis_index NOTIFY axisIndexChanged )
  Q_PROPERTY ( bool available READ available NOTIFY availableChanged )

  using Unit = dash::event::out::Length::Unit;

  // -- Construction

  Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Axis index

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  Q_SIGNAL
  void
  axisIndexChanged ();

  // -- State

  const office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Available

  bool
  available () const
  {
    return state ().available ();
  }

  Q_SIGNAL
  void
  availableChanged ();

  // -- Request interface

  Q_INVOKABLE
  void
  break_motion ();

  bool
  move_dynamics ( const snc::svs::fac::axis_move::Dynamics & dyn_n );

  bool
  go_to_steps ( std::int64_t steps_n );

  bool
  go_to_length ( double length_n );

  bool
  go_to_radians ( double length_n );

  bool
  go_to_degrees ( double length_n );

  // -- Request interface: Qt wrapper

  Q_INVOKABLE
  void
  breakMotion ()
  {
    break_motion ();
  }

  Q_INVOKABLE
  void
  goToSteps ( int num_steps_n )
  {
    go_to_steps ( num_steps_n );
  }

  Q_INVOKABLE
  void
  goToLength ( double length_n )
  {
    go_to_length ( length_n );
  }

  Q_INVOKABLE
  void
  goToRadians ( double length_n )
  {
    go_to_radians ( length_n );
  }

  Q_INVOKABLE
  void
  goToDegrees ( double length_n )
  {
    go_to_degrees ( length_n );
  }

  private:
  // -- Utility

  bool
  go_to_unit_pos ( Unit unit_n, double length_n );

  private:
  // -- State
  const std::uint_fast32_t _axis_index = 0;
  office::State _state;
  struct
  {
    bool available = false;
  } _qtState;

  // -- Office event io
  sev::event::Pool< dash::event::out::Break > _event_pool_break;
  sev::event::Pool< dash::event::out::Dynamics > _event_pool_dynamics;
  sev::event::Pool< dash::event::out::Steps > _event_pool_steps;
  sev::event::Pool< dash::event::out::Length > _event_pool_length;
};

} // namespace snc::svs::ctl::axis_position::dash
