/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_position::office
{

class State
{
  public:
  // -- Types

  using Dynamics = snc::svs::fac::axis_move::Dynamics;

  struct Block
  {
    /// @brief State is not valid
    static constexpr std::uint_fast32_t INVALID = ( 1 << 0 );
    /// @brief Axis is unused
    static constexpr std::uint_fast32_t UNUSED = ( 1 << 1 );
    static constexpr std::uint_fast32_t PILOT_NOT_AVAILABLE = ( 1 << 2 );
    static constexpr std::uint_fast32_t NOT_USER_MOVABLE = ( 1 << 3 );
    static constexpr std::uint_fast32_t ALIGN_REQUIRED = ( 1 << 4 );
  };

  // -- Construction

  void
  reset ()
  {
    _blocks.assign ( Block::INVALID );
    _dynamics.reset ();
  }

  // -- Start blocks

  bool
  available () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast32 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast32 &
  blocks () const
  {
    return _blocks;
  }

  // -- Dynamics

  const Dynamics &
  dynamics () const
  {
    return _dynamics;
  }

  void
  set_dynamics ( const Dynamics & dyn_n )
  {
    _dynamics = dyn_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( ( _blocks == state_n._blocks ) &&
             ( _dynamics == state_n._dynamics ) );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return ( ( _blocks != state_n._blocks ) ||
             ( _dynamics != state_n._dynamics ) );
  }

  private:
  // -- Attributes
  sev::mem::Flags_Fast32 _blocks = Block::INVALID;
  Dynamics _dynamics;
};

} // namespace snc::svs::ctl::axis_position::office
