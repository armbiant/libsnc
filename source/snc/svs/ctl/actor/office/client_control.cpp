/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_control.hpp"
#include <snc/bat/cell.hpp>
#include <snc/svs/ctl/actor/office/service.hpp>

namespace snc::svs::ctl::actor::office
{

Client_Control::Client_Control ( User * user_n )
: Super ( user_n )
{
}

Client_Control::~Client_Control () = default;

void
Client_Control::break_locks_signal_connect () const
{
  if ( is_connected () ) {
    user ()->signal_connect ( &service ()->break_locks_signal () );
  }
}

void
Client_Control::break_locks_signal_disconnect () const
{
  if ( is_connected () ) {
    user ()->signal_disconnect ( &service ()->break_locks_signal () );
  }
}

bool
Client_Control::break_locked () const
{
  return is_connected () ? service ()->break_locked () : false;
}

void
Client_Control::break_lock_install_abort () const
{
  if ( is_connected () ) {
    service ()->break_lock_install ( Service::Break_Lock::ABORT );
  }
}

} // namespace snc::svs::ctl::actor::office
