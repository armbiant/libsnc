/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/svs/ctl/actor/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>

// -- Instantiation

namespace snc::svs::ctl::actor::office
{

Service::Service ( Provider & provider_n )
: Super ( provider_n )
, _break_locks_signal ( "break_locks" )
, _machine_startup ( &provider_n )
{
}

Service::~Service () = default;

void
Service::init_connections ()
{
  _machine_startup.connect ();
  _machine_startup.session ().set_end_callback (
      [ this ] () { break_lock_release ( _break_locks ); } );
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::stats_add ( Client_Actor::Activity act_n )
{
  switch ( act_n ) {
  case Client_Actor::Activity::IDLE:
    ++_counts.idle;
    break;
  case Client_Actor::Activity::BREAKING:
    ++_counts.breaking;
    break;
  case Client_Actor::Activity::ACTIVE:
    ++_counts.active;
    break;
  }
}

void
Service::stats_drop ( Client_Actor::Activity act_n )
{
  switch ( act_n ) {
  case Client_Actor::Activity::IDLE:
    --_counts.idle;
    break;
  case Client_Actor::Activity::BREAKING:
    --_counts.breaking;
    break;
  case Client_Actor::Activity::ACTIVE:
    --_counts.active;
    break;
  }
}

void
Service::client_activity_changed ( Client_Actor::Activity prev_n,
                                   Client_Actor::Activity now_n )
{
  // -- Update statistics
  stats_drop ( prev_n );
  stats_add ( now_n );
  // -- Update state
  state_update ();
}

void
Service::client_activity_add ( Client_Actor::Activity act_n )
{
  // Update statistics
  stats_add ( act_n );
  // Update state
  state_update ();
}

void
Service::client_activity_drop ( Client_Actor::Activity act_n )
{
  // Update statistics
  stats_drop ( act_n );
  // Update state
  state_update ();
}

bool
Service::all_idle () const
{
  return ( _counts.active == 0 ) && ( _counts.breaking == 0 );
}

void
Service::actors_break_hard ()
{
  if ( !_machine_startup.session ().is_good () ) {
    return;
  }

  // Break clients
  for ( auto * item : clients () ) {
    if ( auto * client =
             dynamic_cast< snc::svs::ctl::actor::office::Client_Actor * > (
                 item ) ) {
      client->break_hard ();
    }
  }
}

void
Service::break_lock_install ( std::uint_fast32_t locks_n )
{
  bool was_locked = break_locked ();
  _break_locks.set ( locks_n );
  if ( was_locked ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0,
               "Break lock installed.  Breaking actors." );
  // Break all actors
  actors_break_hard ();

  // Send signal
  _break_locks_signal.send ();
}

void
Service::break_lock_release ( std::uint_fast32_t locks_n )
{
  bool was_locked = break_locked ();
  _break_locks.unset ( locks_n );
  if ( break_locked () || !was_locked ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Break lock released." );
  // Send signal
  _break_locks_signal.send ();
}

void
Service::state_update ()
{
  // -Update state
  bool break_avail = ( _counts.active != 0 );
  if ( _state.break_hard_available () != break_avail ) {
    _state.set_break_hard_available ( break_avail );
    // Notify
    signal_state ().send ();
    provider ().state_update ();
  }
}

} // namespace snc::svs::ctl::actor::office
