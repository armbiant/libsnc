/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/ctl/actor/office/client_base.hpp>

namespace snc::svs::ctl::actor::office
{

/** Actor provides a simplified activity view into a machine controller.
 *
 *  It also provides spimplified control functions, especially the common
 *  break_hard() function.
 */
class Client_Actor : public Client_Base
{
  // -- Types
  private:
  using Super = Client_Base;

  public:
  enum class Activity : std::uint_fast8_t
  {
    IDLE,
    BREAKING,
    ACTIVE
  };

  // -- Construction

  Client_Actor ( User * user_n );

  ~Client_Actor ();

  // -- Activity state

  Activity
  activity () const
  {
    return _activity;
  }

  bool
  is_idle () const
  {
    return ( _activity == Activity::IDLE );
  }

  bool
  is_breaking () const
  {
    return ( _activity == Activity::BREAKING );
  }

  bool
  is_active () const
  {
    return ( _activity == Activity::ACTIVE );
  }

  bool
  is_busy () const
  {
    return !is_idle ();
  }

  // -- State change interface

  void
  go_idle ();

  bool
  go_active ();

  bool
  go_breaking ();

  // -- Breaking interface

  void
  break_hard ();

  const auto &
  break_hard_func () const
  {
    return _break_hard_func;
  }

  void
  set_break_hard_func ( std::function< void () > func_n );

  // -- Connection interface
  protected:
  void
  connecting () override;

  void
  disconnecting () override;

  private:
  // -- Attributes
  Activity _activity = Activity::IDLE;
  std::function< void () > _break_hard_func;
};

} // namespace snc::svs::ctl::actor::office
