/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/bat/signal.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/actor/office/state.hpp>
#include <snc/svs/fac/machine_startup/office/client.hpp>

namespace snc::svs::ctl::actor::office
{

// -- Forward declaration
class Clerk;
class Client;

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Clerk;

  public:
  /// @brief Break lock types
  struct Break_Lock
  {
    /// @brief Session abort lock
    static constexpr std::uint8_t ABORT = ( 1 << 0 );
  };

  // -- Construction and session

  Service ( Provider & provider_n );

  ~Service ();

  void
  init_connections ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors: State

  const auto &
  state () const
  {
    return _state;
  }

  // -- Accessors: Machine client

  const auto &
  machine_startup () const
  {
    return _machine_startup;
  }

  auto &
  machine_startup ()
  {
    return _machine_startup;
  }

  // -- Client interface

  void
  client_activity_changed ( Client_Actor::Activity prev_n,
                            Client_Actor::Activity now_n );

  void
  client_activity_add ( Client_Actor::Activity act_n );

  void
  client_activity_drop ( Client_Actor::Activity act_n );

  // -- Control client interface: Actors

  bool
  all_idle () const;

  void
  actors_break_hard ();

  // -- Control client interface: Break locks

  /// @brief Pilots can't enter ACTIVE state if a break lock is in place
  bool
  break_locked () const
  {
    return !_break_locks.is_empty ();
  }

  /// @brief Release a break lock
  void
  break_lock_release ( std::uint_fast32_t locks_n );

  /// @brief Install a break lock
  void
  break_lock_install ( std::uint_fast32_t locks_n );

  snc::bat::Signal &
  break_locks_signal ()
  {
    return _break_locks_signal;
  }

  // -- Session interface
  private:
  void
  session_begin ();

  void
  session_end ();

  // -- Utility

  void
  state_update ();

  void
  stats_add ( Client_Actor::Activity act_n );

  void
  stats_drop ( Client_Actor::Activity act_n );

  private:
  // -- Resources
  State _state;
  struct
  {
    std::uint32_t idle = 0;
    std::uint32_t breaking = 0;
    std::uint32_t active = 0;
  } _counts;
  // -- Break locks
  sev::mem::Flags_Fast32 _break_locks;
  snc::bat::Signal _break_locks_signal;
  snc::svs::fac::machine_startup::office::Client _machine_startup;
};

} // namespace snc::svs::ctl::actor::office
