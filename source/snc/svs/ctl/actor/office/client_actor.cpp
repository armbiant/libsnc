/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_actor.hpp"
#include <snc/svs/ctl/actor/office/service.hpp>

namespace snc::svs::ctl::actor::office
{

Client_Actor::Client_Actor ( User * user_n )
: Super ( user_n )
{
}

Client_Actor::~Client_Actor () = default;

void
Client_Actor::go_idle ()
{
  if ( !is_connected () ) {
    return;
  }
  if ( !is_idle () ) {
    Activity prev = _activity;
    _activity = Activity::IDLE;
    service ()->client_activity_changed ( prev, _activity );
  }
}

bool
Client_Actor::go_active ()
{
  if ( !is_connected () ) {
    return false;
  }
  if ( is_idle () ) {
    Activity prev = _activity;
    _activity = Activity::ACTIVE;
    service ()->client_activity_changed ( prev, _activity );
  }
  return ( _activity == Activity::ACTIVE );
}

bool
Client_Actor::go_breaking ()
{
  if ( !is_connected () ) {
    return false;
  }
  if ( is_active () ) {
    Activity prev = _activity;
    _activity = Activity::BREAKING;
    service ()->client_activity_changed ( prev, _activity );
  }
  return ( _activity == Activity::BREAKING );
}

void
Client_Actor::set_break_hard_func ( std::function< void () > func_n )
{
  _break_hard_func = std::move ( func_n );
}

void
Client_Actor::break_hard ()
{
  if ( go_breaking () ) {
    if ( _break_hard_func ) {
      _break_hard_func ();
    }
  }
}

void
Client_Actor::connecting ()
{
  Super::connecting ();
  service ()->client_activity_add ( _activity );
}

void
Client_Actor::disconnecting ()
{
  service ()->client_activity_drop ( _activity );
  Super::disconnecting ();
}

} // namespace snc::svs::ctl::actor::office
