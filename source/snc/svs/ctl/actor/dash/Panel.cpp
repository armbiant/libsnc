/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/actor/office/clerk.hpp>

namespace snc::svs::ctl::actor::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Actor" )
, _event_pool_break_hard ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::actor::office::Clerk > ();
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();
  office_io ().allocate ( _event_pool_break_hard, 4 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::State::etype:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.break_hard_available,
                     state ().break_hard_available () ) ) {
    emit breakHardAvailableChanged ();
  }
}

void
Panel::breakHard ()
{
  if ( !office_session_is_good () ) {
    return;
  }

  // Submit event
  office_io ().submit ( office_io ().acquire ( _event_pool_break_hard ) );
}

} // namespace snc::svs::ctl::actor::dash
