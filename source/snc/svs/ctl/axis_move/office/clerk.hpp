/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/mem/flags.hpp>
#include <snc/device/axis_info.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/axis_move/dash/events.hpp>
#include <snc/svs/ctl/axis_move/office/state.hpp>
#include <snc/svs/fac/axis_move/office/client.hpp>

namespace snc::svs::ctl::axis_move::office
{

/** Clerk */
class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;
  using Unit = dash::event::out::Length::Unit;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  struct Request
  {
    static constexpr std::uint_fast32_t BREAK = ( 1 << 0 );
    static constexpr std::uint_fast32_t DYNAMICS = ( 1 << 1 );
    static constexpr std::uint_fast32_t STEPS = ( 1 << 2 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Clerk ();

  // -- Accessors

  std::uint_fast32_t
  axis_index () const
  {
    return _pilot.axis_index ();
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_break ( Dash_Event & event_n );

  void
  dash_event_dynamics ( Dash_Event & event_n );

  void
  dash_event_steps ( Dash_Event & event_n );

  void
  dash_event_length ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  private:
  // -- Utility

  void
  process_requests ();

  void
  service_control_auto_release ();

  bool
  service_control_acquire ();

  void
  state_update ();

  private:
  // -- Resources
  snc::svs::ctl::actor::office::Client_Actor _actor;
  snc::svs::fac::axis_move::office::Client _pilot;
  snc::svs::fac::axis_move::office::Client::Control _pilot_control;
  struct Axis
  {
    snc::device::Axis_Info info;
    double length_per_step = 0.0;
    double speed_max = 0.0;
    double accel_max = 0.0;
  } _axis;

  // -- State
  State _state;
  State _state_emitted;

  // -- Request
  struct Requests
  {
    void
    clear ()
    {
      reg.clear ();
      steps = 0;
    }

    sev::mem::Flags_Fast32 reg;
    std::int_fast64_t steps = 0;
  } _requests;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::axis_move::office
