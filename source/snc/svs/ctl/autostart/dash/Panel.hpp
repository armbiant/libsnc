/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/autostart/dash/events.hpp>

namespace snc::svs::ctl::autostart::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Interface

  void
  set_default_device_statics_json ( std::string json_n );

  private:
  // -- Attributes
  sev::event::Pool< dash::event::out::Device_Statics_Json >
      _event_pool_device_statics_json;
};

} // namespace snc::svs::ctl::autostart::dash
