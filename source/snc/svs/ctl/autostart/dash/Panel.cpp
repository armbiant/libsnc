/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svs/ctl/autostart/office/clerk.hpp>

namespace snc::svs::ctl::autostart::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Autostart" )
, _event_pool_device_statics_json ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::autostart::office::Clerk > ();
}

void
Panel::set_default_device_statics_json ( std::string json_n )
{
  if ( !office_session_is_good () ) {
    throw std::runtime_error (
        "Bad office session when setting default device statics (json)." );
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Sending default device statics (json)" );
  {
    auto * event = office_io ().acquire ( _event_pool_device_statics_json );
    event->set_json ( std::move ( json_n ) );
    office_io ().submit ( event );
  }
}

} // namespace snc::svs::ctl::autostart::dash
