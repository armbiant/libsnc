/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_control.hpp>
#include <snc/svs/ctl/lookout/office/client.hpp>
#include <snc/svs/fac/serial_device/office/client.hpp>
#include <snc/svs/fac/tracker/office/client_setup.hpp>
#include <snc/svs/frame/ship/office/client_session.hpp>

namespace snc::svs::ctl::autostart::office
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_statics_json ( Dash_Event & event_n );

  // -- Central processing

  void
  process () override;

  void
  process_bridge_abort ();

  void
  process_bridge_start ();

  private:
  // -- Utility
  void
  set_device_statics_default (
      snc::device::statics::handle::Statics statics_n );

  private:
  // -- Attributes
  snc::svs::ctl::lookout::Client _lookout;
  snc::svs::fac::serial_device::office::Client _serial_device;
  snc::svs::frame::ship::office::Client_Session _ship_session;
  snc::svs::fac::tracker::office::Client_Setup _tracker;
  snc::svs::ctl::actor::office::Client_Control _actor_control;
  // - Device statics
  snc::device::statics::handle::Statics _device_statics_default;
  snc::device::statics::handle::Statics _device_statics_selected;
  bool _aborting = false;
};

} // namespace snc::svs::ctl::autostart::office
