/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/device/statics/json.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/ctl/autostart/dash/events.hpp>
#include <cstdint>

namespace snc::svs::ctl::autostart::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Autostart" )
, _lookout ( this )
, _serial_device ( this )
, _ship_session ( this )
, _tracker ( this )
, _actor_control ( this )
{
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Find services
  _lookout.connect_required ();
  _serial_device.connect_required ();
  _ship_session.connect_required ();
  _tracker.connect_required ();
  _actor_control.connect_required ();

  request_processing ();
}

void
Clerk::cell_session_abort ()
{
  Super::cell_session_abort ();
  request_processing ();
}

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && !_lookout.is_enabled () &&
         !_ship_session.is_running ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::DEVICE_STATICS_JSON:
    dash_event_statics_json ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::dash_event_statics_json ( Dash_Event & event_n )
{
  auto & cevent =
      static_cast< const dash::event::out::Device_Statics_Json & > ( event_n );

  log ().cat ( sev::logt::FL_DEBUG_0,
               "Incoming default device statics (json)." );

  {
    snc::device::statics::json::Reader reader;
    if ( !reader.parse ( cevent.json () ) ) {
      std::string message = sev::string::cat ( "Json device statics errors.\n",
                                               reader.error_string () );
      log ().cat ( sev::logt::FL_WARNING, message );
    }
    set_device_statics_default ( reader.statics () );
  }
}

void
Clerk::set_device_statics_default (
    snc::device::statics::handle::Statics statics_n )
{
  _device_statics_default = std::move ( statics_n );
  // Select statics
  if ( !_device_statics_selected ) {
    _device_statics_selected = _device_statics_default;
    _tracker.set_statics ( _device_statics_selected );
  }
  request_processing ();
}

void
Clerk::process ()
{
  const bool enabled = ( cell_session ().is_good && !_aborting );
  if ( enabled ) {
    process_bridge_start ();
  } else {
    process_bridge_abort ();
  }
}

void
Clerk::process_bridge_start ()
{
  // Already running?
  if ( _ship_session.is_running () ) {
    return;
  }
  // Any device statics selected?
  if ( !_device_statics_selected ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Bridge start blocked: No device statics selected." );
    return;
  }
  // USB device query check
  if ( !_device_statics_selected->usb_device_query () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Bridge start blocked: No usb device query." );
    return;
  }
  // emb device config check
  if ( !_device_statics_selected->emb_device_config () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Bridge start blocked: No emb device config." );
    return;
  }

  // Enable lookout
  if ( !_lookout.is_enabled () ) {
    _lookout.enable ();
    _lookout.add_usb_device_query (
        _device_statics_selected->usb_device_query () );
  }
  // Any device found by the lookout?
  auto device_letter = _lookout.device ();
  if ( !device_letter ) {
    return;
  }

  // Start the bridge
  log ().cat ( sev::logt::FL_DEBUG_0, "Starting the ship." );
  _serial_device.set_serial_device ( device_letter );
  _ship_session.start ();

  // Disable lookout
  _lookout.disable ();

  request_processing ();
}

void
Clerk::process_bridge_abort ()
{
  // Running?
  if ( !_ship_session.is_running () ) {
    // Finish the bridge abort
    if ( _aborting ) {
      _aborting = false;
      log ().cat ( sev::logt::FL_DEBUG_0, "Finishing the ship abort process." );
      _tracker.reset_statics ();
      _serial_device.clear_serial_device ();
      request_processing ();
    }
    return;
  }
  // Already aborting?
  if ( _ship_session.is_aborting () ) {
    return;
  }
  // Should we abort?
  if ( !cell_session ().is_aborting ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Aborting the ship." );
  _aborting = true;
  // Enable "abort" break lock
  _actor_control.break_lock_install_abort ();
  // Abort ship
  _ship_session.abort ();
  request_processing ();
}

} // namespace snc::svs::ctl::autostart::office
