/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <algorithm>
#include <cstdint>
#include <utility>

namespace snc::svs::ctl::align_multi
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine-Align-Multi" )
, _actor ( this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () { break_hard (); } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();

  // Find axis align services
  for ( std::uint_fast32_t ai = 0; ai != 6; ++ai ) {
    auto axis = std::make_shared< Axis > ( this, ai );
    if ( !axis->client.connect () ) {
      break;
    }
    axis->client.session ().set_begin_callback (
        [ this ] () { session_begin (); } );
    axis->client.session ().set_end_callback (
        [ this ] () { session_end (); } );
    _axes.emplace_back ( std::move ( axis ) );
  }
}

void
Clerk::session_begin ()
{
  // Wait until all service sessions are good
  for ( auto & plt : _axes ) {
    if ( !plt->client.session_is_good () ) {
      return;
    }
  }

  // -- Gather program pilots
  _program.pilots.reserve ( _axes.size () );
  for ( auto & plt : _axes ) {
    if ( !plt->client.is_alignable () ) {
      continue;
    }
    _program.pilots.push_back ( plt );
  }
  std::reverse ( _program.pilots.begin (), _program.pilots.end () );
}

void
Clerk::session_end ()
{
  // Wait until all service sessions are done
  for ( auto & plt : _axes ) {
    if ( plt->client.session_is_good () ) {
      return;
    }
  }

  // Reset sequence state
  _program.clear ();
  // Reset state
  _state.reset ();
  // Update state
  state_update ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Job::etype:
    dash_event_job ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_job ( Dash_Event & event_n )
{
  const auto & job =
      static_cast< const dash::event::out::Job & > ( event_n ).job ();

  switch ( job.cmd () ) {
  case Job::Command::INVALID:
    break;
  case Job::Command::SEQUENCE:
    align_start_sequence ();
    break;
  case Job::Command::PARALLEL:
    align_start_parallel ();
    break;
  case Job::Command::ASSUME_ALIGNED:
    align_start_set ();
    break;
  case Job::Command::ABORT:
    break_hard ();
    break;
  }
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
  }
}

void
Clerk::process ()
{
  state_update ();
  process_program ();
}

void
Clerk::process_program ()
{
  switch ( _state.program () ) {
  case State::Program::SEQUENCE:
    process_program_sequence_iterate ();
    process_program_sequence_end ();
    break;
  case State::Program::PARALLEL:
    process_program_parallel ();
    break;
  case State::Program::SET:
    process_program_set ();
    break;
  case State::Program::BREAK:
    process_program_break ();
    break;
  default:
    break;
  }
}

void
Clerk::process_program_sequence_iterate ()
{
  while ( _program.sequence_index < _program.pilots.size () ) {
    // Process current axis task
    switch ( _program.sequence_task ) {

    case Axis_Task::BEGIN:
      if ( _program.pilots[ _program.sequence_index ]->control.begin () ) {
        _program.sequence_task = Axis_Task::WAIT_UNTIL_ALIGNED;
      } else {
        // Abort sequence
        _program.sequence_index = _program.pilots.size ();
        _program.sequence_task = Axis_Task::BEGIN;
        log ().cat ( sev::logt::FL_DEBUG_0, "Sequence axis not alignable." );
      }
      break;

    case Axis_Task::WAIT_UNTIL_ALIGNED:
      // Leave is axis isn't aligned, yet
      if ( _program.pilots[ _program.sequence_index ]->client.is_aligning () ) {
        return;
      }
      // Next axis
      ++_program.sequence_index;
      _program.sequence_task = Axis_Task::BEGIN;
      break;

    default:
      return;
    }
  }
}

void
Clerk::process_program_sequence_end ()
{
  if ( _program.sequence_index < _program.pilots.size () ) {
    return;
  }
  if ( !process_program_finished () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Align sequence finished." );
}

void
Clerk::process_program_parallel ()
{
  if ( !process_program_finished () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Parallel alignment finished." );
}

void
Clerk::process_program_set ()
{
  if ( !process_program_finished () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Setting alignment finished." );
}

void
Clerk::process_program_break ()
{
  if ( !process_program_finished () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Break alignment finished." );
}

bool
Clerk::process_program_finished ()
{
  // Wait until all pilots are aligned
  if ( program_pilots_aligning () ) {
    return false;
  }
  // Release pilot locks
  program_pilots_unlock ();
  // Update state
  state_set_program ( State::Program::NONE );
  // Set part idle
  _actor.go_idle ();

  return true;
}

void
Clerk::state_update ()
{
  // -- Update availability blocks
  _state.blocks ().clear ();
  if ( !_state.is_running () ) {
    if ( !program_pilots_available () ) {
      _state.blocks ().set ( State::Block::AXIS_UNAVAILABLE );
    }
  }

  if ( _state_emitted == _state ) {
    return;
  }
  // New state
  _state_emitted = _state;
  // Notify
  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::state_set_program ( std::uint8_t prog_n )
{
  _state.set_program ( prog_n );
  state_update ();
  request_processing ();
}

bool
Clerk::program_pilots_available () const
{
  if ( _program.pilots.empty () ) {
    return false;
  }

  for ( auto & plt : _program.pilots ) {
    if ( !plt->control && !plt->client.control_available () ) {
      return false;
    }
  }
  return true;
}

bool
Clerk::program_pilots_aligning () const
{
  for ( auto & plt : _program.pilots ) {
    if ( plt->client.is_aligning () ) {
      return true;
    }
  }
  return false;
}

bool
Clerk::program_pilots_lock ()
{
  for ( auto & plt : _program.pilots ) {
    plt->control = plt->client.control ();
    if ( !plt->control ) {
      program_pilots_unlock ();
      return false;
    }
  }
  return true;
}

void
Clerk::program_pilots_unlock ()
{
  for ( auto & plt : _program.pilots ) {
    plt->control.release ();
  }
}

void
Clerk::break_hard ()
{
  if ( !_state.is_running () || _state.is_running_break () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Break hard." );

  // Abort pilots
  for ( auto & plt : _program.pilots ) {
    plt->control.break_hard ();
  }

  // Update state
  state_set_program ( State::Program::BREAK );
}

bool
Clerk::align_start_common ()
{
  if ( _state.is_running () ) {
    return false;
  }
  if ( !program_pilots_lock () ) {
    return false;
  }
  _actor.go_active ();

  return true;
}

bool
Clerk::align_start_sequence ()
{
  if ( !align_start_common () ) {
    return false;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Align sequence begin." );

  // Reset sequence
  _program.reset ();
  // Update state
  state_set_program ( State::Program::SEQUENCE );
  return true;
}

bool
Clerk::align_start_parallel ()
{
  if ( !align_start_common () ) {
    return false;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Parallel alignment begin." );

  // Start pilots
  for ( auto & plt : _program.pilots ) {
    plt->control.begin ();
  }

  // Update state
  state_set_program ( State::Program::PARALLEL );
  return true;
}

bool
Clerk::align_start_set ()
{
  if ( !align_start_common () ) {
    return false;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Setting alignment begin." );

  // Set pilots aligned
  for ( auto & plt : _program.pilots ) {
    plt->control.set ();
  }

  // Update state
  state_set_program ( State::Program::SET );
  return true;
}

} // namespace snc::svs::ctl::align_multi
