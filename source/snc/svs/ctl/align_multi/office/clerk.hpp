/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/signal.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/align_multi/dash/events.hpp>
#include <snc/svs/ctl/align_multi/job.hpp>
#include <snc/svs/ctl/align_multi/office/state.hpp>
#include <snc/svs/fac/axis_align/office/client.hpp>
#include <cstdint>
#include <utility>

namespace snc::svs::ctl::align_multi
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;
  using Axis_Client = snc::svs::fac::axis_align::office::Client;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  enum class Axis_Task
  {
    BEGIN,
    WAIT_UNTIL_ALIGNED
  };

  struct Axis
  {
    Axis ( Clerk * clerk_n, std::uint_fast32_t axis_index_n )
    : client ( clerk_n, axis_index_n )
    {
    }

    Axis_Client client;
    Axis_Client::Control control;
  };

  using Axis_Handle = std::shared_ptr< Axis >;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Accessors

  const State &
  state () const
  {
    return _state;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Bridge session interface

  void
  session_begin ();

  void
  session_end ();

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_job ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  void
  process_program ();

  void
  process_program_sequence_iterate ();

  void
  process_program_sequence_end ();

  void
  process_program_parallel ();

  void
  process_program_set ();

  void
  process_program_break ();

  bool
  process_program_finished ();

  private:
  // -- Utility

  void
  state_update ();

  void
  state_set_program ( std::uint8_t prog_n );

  bool
  program_pilots_available () const;

  bool
  program_pilots_aligning () const;

  bool
  program_pilots_lock ();

  void
  program_pilots_unlock ();

  void
  break_hard ();

  bool
  align_start_common ();

  bool
  align_start_sequence ();

  bool
  align_start_parallel ();

  bool
  align_start_set ();

  private:
  // -- State
  State _state;
  State _state_emitted;

  // -- Resources
  snc::svs::ctl::actor::office::Client_Actor _actor;
  std::vector< Axis_Handle > _axes;

  struct Program
  {
    void
    reset ()
    {
      sequence_index = 0;
      sequence_task = Axis_Task::BEGIN;
    }

    void
    clear ()
    {
      reset ();
      pilots.clear ();
    }

    std::vector< Axis_Handle > pilots;
    std::size_t sequence_index = 0;
    Axis_Task sequence_task = Axis_Task::BEGIN;
  } _program;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::align_multi
