/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::ctl::align_multi
{

class State
{
  public:
  // -- Types

  struct Block
  {
    /// @brief The state is invalid
    static constexpr std::uint_fast32_t INVALID = ( 1 << 0 );
    /// @brief An axis is busy
    static constexpr std::uint_fast32_t AXIS_UNAVAILABLE = ( 1 << 1 );
  };

  struct Program
  {
    /// @brief Not aligning
    static constexpr std::uint8_t NONE = 0;
    /// @brief Alignment sequence is running
    static constexpr std::uint8_t SEQUENCE = 1;
    /// @brief Parallel alignment is running
    static constexpr std::uint8_t PARALLEL = 2;
    /// @brief Setting alignment is running
    static constexpr std::uint8_t SET = 3;
    /// @brief Axes are breaking
    static constexpr std::uint8_t BREAK = 4;
  };

  // -- Setup

  void
  reset ()
  {
    _blocks = Block::INVALID;
    _program = Program::NONE;
  }

  // -- Availability blocks

  bool
  available () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast32 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast32 &
  blocks () const
  {
    return _blocks;
  }

  // -- Program state

  std::uint8_t
  program () const
  {
    return _program;
  }

  void
  set_program ( std::uint8_t prog_n )
  {
    _program = prog_n;
  }

  bool
  is_running () const
  {
    return ( _program != Program::NONE );
  }

  bool
  is_running_sequence () const
  {
    return ( _program == Program::SEQUENCE );
  }

  bool
  is_running_parallel () const
  {
    return ( _program == Program::PARALLEL );
  }

  bool
  is_running_break () const
  {
    return ( _program == Program::BREAK );
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( _blocks == state_n._blocks ) && ( _program == state_n._program );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return !operator== ( state_n );
  }

  private:
  // -- Attributes
  sev::mem::Flags_Fast32 _blocks = Block::INVALID;
  std::uint8_t _program = Program::NONE;
};

} // namespace snc::svs::ctl::align_multi
