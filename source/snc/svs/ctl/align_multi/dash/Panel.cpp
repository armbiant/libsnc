/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/align_multi/office/clerk.hpp>

namespace snc::svs::ctl::align_multi::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine-Align-Multi" )
, _event_pool_job ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::align_multi::Clerk > ();
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_job, 4 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::State::etype:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.available, state ().available () ) ) {
    emit availableChanged ();
  }
  if ( sev::change ( _qtState.running, state ().is_running () ) ) {
    emit runningChanged ();
  }
  if ( sev::change ( _qtState.startable,
                     state ().available () && !state ().is_running () ) ) {
    emit startableChanged ();
  }
  if ( sev::change ( _qtState.stoppable,
                     state ().available () && state ().is_running () ) ) {
    emit stoppableChanged ();
  }
}

void
Panel::alignSerial ()
{
  Job job;
  job.set_cmd ( Job::Command::SEQUENCE );
  request_job ( job );
}

void
Panel::alignParallel ()
{
  Job job;
  job.set_cmd ( Job::Command::PARALLEL );
  request_job ( job );
}

void
Panel::alignSet ()
{
  Job job;
  job.set_cmd ( Job::Command::ASSUME_ALIGNED );
  request_job ( job );
}

void
Panel::alignAbort ()
{
  Job job;
  job.set_cmd ( Job::Command::ABORT );
  request_job ( job );
}

void
Panel::request_job ( const Job & job_n )
{
  if ( !office_session_is_good () ) {
    return;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_job );
    event->job () = job_n;
    office_io ().submit ( event );
  }
}

} // namespace snc::svs::ctl::align_multi::dash
