/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/align_multi/dash/events.hpp>
#include <snc/svs/ctl/align_multi/job.hpp>
#include <snc/svs/ctl/align_multi/office/state.hpp>
#include <cstdint>

namespace snc::svs::ctl::align_multi::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( bool available READ available NOTIFY availableChanged )
  Q_PROPERTY ( bool running READ running NOTIFY runningChanged )
  Q_PROPERTY ( bool startable READ startable NOTIFY startableChanged )
  Q_PROPERTY ( bool stoppable READ stoppable NOTIFY stoppableChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State

  const State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Available

  bool
  available () const
  {
    return _qtState.available;
  }

  Q_SIGNAL
  void
  availableChanged ();

  // -- Running

  bool
  running () const
  {
    return _qtState.running;
  }

  Q_SIGNAL
  void
  runningChanged ();

  // -- Startable

  bool
  startable () const
  {
    return _qtState.startable;
  }

  Q_SIGNAL
  void
  startableChanged ();

  // -- Stoppable

  bool
  stoppable () const
  {
    return _qtState.stoppable;
  }

  Q_SIGNAL
  void
  stoppableChanged ();

  // -- Request interface

  Q_INVOKABLE
  void
  alignSerial ();

  Q_INVOKABLE
  void
  alignParallel ();

  Q_INVOKABLE
  void
  alignSet ();

  Q_INVOKABLE
  void
  alignAbort ();

  private:
  // -- Utility

  void
  request_job ( const Job & job_n );

  private:
  // -- Attributes
  std::uint_fast32_t _axis_index = 0;
  State _state;
  struct
  {
    bool available = false;
    bool running = false;
    bool startable = false;
    bool stoppable = false;
  } _qtState;

  // -- Office event io
  sev::event::Pool< dash::event::out::Job > _event_pool_job;
};

} // namespace snc::svs::ctl::align_multi::dash
