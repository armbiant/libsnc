/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/align_multi/job.hpp>
#include <snc/svs/ctl/align_multi/office/state.hpp>

namespace snc::svs::ctl::align_multi::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, snc::svs::ctl::align_multi::State >;

} // namespace snc::svs::ctl::align_multi::dash::event::in

namespace snc::svs::ctl::align_multi::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
};

using Out = snc::svp::dash::event::Out;

class Job : public Out
{
  public:
  static constexpr auto etype = Type::JOB;

  Job ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  snc::svs::ctl::align_multi::Job &
  job ()
  {
    return _job;
  }

  const snc::svs::ctl::align_multi::Job &
  job () const
  {
    return _job;
  }

  private:
  snc::svs::ctl::align_multi::Job _job;
};

} // namespace snc::svs::ctl::align_multi::dash::event::out
