/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::ctl::align_multi
{

class Job
{
  public:
  // -- Types

  enum class Command : std::uint8_t
  {
    INVALID,
    SEQUENCE,
    PARALLEL,
    ASSUME_ALIGNED,
    ABORT
  };

  // -- Interface

  Command
  cmd () const
  {
    return _cmd;
  }

  void
  set_cmd ( Command cmd_n )
  {
    _cmd = cmd_n;
  }

  bool
  is_valid () const
  {
    return _cmd != Command::INVALID;
  }

  void
  invalidate ()
  {
    _cmd = Command::INVALID;
  }

  public:
  // -- Attributes
  Command _cmd = Command::INVALID;
};

} // namespace snc::svs::ctl::align_multi
