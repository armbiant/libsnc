/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_session.hpp"
#include <snc/svs/frame/ship/office/service.hpp>

namespace snc::svs::frame::ship::office
{

Client_Session::Client_Session ( User * user_n )
: Super ( user_n )
{
}

Client_Session::~Client_Session () = default;

bool
Client_Session::is_running () const
{
  return is_connected () ? service ()->is_running () : false;
}

bool
Client_Session::is_aborting () const
{
  return is_connected () ? service ()->is_aborting () : false;
}

bool
Client_Session::start ()
{
  return is_connected () ? service ()->start () : false;
}

void
Client_Session::abort ( bool forced_n ) const
{
  if ( is_connected () ) {
    service ()->abort ( forced_n );
  }
}

} // namespace snc::svs::frame::ship::office
