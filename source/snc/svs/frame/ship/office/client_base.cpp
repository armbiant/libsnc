/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_base.hpp"
#include <snc/svs/frame/ship/office/service.hpp>

namespace snc::svs::frame::ship::office
{

Client_Base::Client_Base ( User * user_n )
: Super ( user_n )
{
}

Client_Base::~Client_Base () = default;

Service *
Client_Base::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client_Base::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::frame::ship::office
