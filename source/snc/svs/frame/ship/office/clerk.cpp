/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/frame/ship/ship/sailor.hpp>

namespace snc::svs::frame::ship::office
{

Clerk::Clerk ( const Clerk_Init & init_n,
               sev::logt::Reference const & bridge_log_parent_n )
: Super ( init_n, "Ship" )
, _service ( *this, bridge_log_parent_n )
{
}

Clerk::~Clerk () = default;

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && !_service.is_running ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  pick_n ( make_sailor_factory< snc::svs::frame::ship::ship::Sailor,
                                sev::event::queue_io::Link_2 > (
      _service.ship_link () ) );
}

void
Clerk::process_async ()
{
  _service.process_async ();
}

void
Clerk::process ()
{
  _service.process ();
}

} // namespace snc::svs::frame::ship::office
