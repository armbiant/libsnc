/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/ring_resizing.hpp>
#include <snc/bat/battery.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/svp/office/factor.hpp>
#include <cstdint>
#include <memory>

namespace snc::svs::frame::ship::office
{

// -- Forward declaration
class Clerk;

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Clerk;

  struct LEvent
  {
    static constexpr std::uint_fast32_t BRIDGE_EVENT_GENERATION = ( 1 << 0 );
    static constexpr std::uint_fast32_t BRIDGE_IN = ( 1 << 1 );
    static constexpr std::uint_fast32_t BRIDGE_OUT = ( 1 << 2 );
    static constexpr std::uint_fast32_t PILOTS_READY_TO_END = ( 1 << 3 );
  };

  struct Ship_Abort_State
  {
    static constexpr std::uint8_t NONE = 0;

    static constexpr std::uint8_t ABORT = ( 1 << 0 );
    static constexpr std::uint8_t FORCED = ( 1 << 1 );

    static constexpr std::uint8_t PILOTS_ABORTED = ( 1 << 2 );
    static constexpr std::uint8_t CONTROLLERS_READY_TO_END = ( 1 << 3 );
    static constexpr std::uint8_t PILOTS_READY_TO_END = ( 1 << 4 );
  };

  public:
  using Bridge_Notification = snc::svp::ship::event::In;
  using Bridge_Event = const snc::svp::ship::event::Out;

  // -- Construction

  Service ( Provider & provider_n,
            sev::logt::Reference const & bridge_log_parent_n );

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  bool
  is_running () const
  {
    return _ship_battery.is_running ();
  }

  bool
  is_aborting () const
  {
    return !_ship_abort_state.is_empty ();
  }

  sev::event::queue_io::Link_2
  ship_link () const
  {
    return _event_queue.link_b ();
  }

  // -- Central processing

  void
  process_async ();

  void
  process ();

  private:
  void
  process_bridge_notification ();

  void
  process_ship_abort ();

  public:
  // -- Client interface: Ship control

  bool
  start ();

  void
  abort ( bool forced_n = false );

  // -- Client interface: Bridge io

  void
  request_pick_up ( snc::svp::office::Factor * factor_n );

  /// @brief Submit a pilot controller event
  void
  submit ( Bridge_Notification * event_n )
  {
    _ship_connection.push ( event_n );
  }

  private:
  // -- Utility

  /// @brief Find all clerks with the given type
  template < typename T >
  void
  find_clerks_by_type ( std::vector< T * > & list_n ) const
  {
    auto & cells = provider_cell ().cell_context ().cells ();
    list_n.reserve ( cells.size () );
    for ( auto * cell : cells ) {
      if ( T * clerk = dynamic_cast< T * > ( cell ) ) {
        list_n.push_back ( clerk );
      }
    }
    list_n.shrink_to_fit ();
  }

  private:
  // -- State flags
  sev::mem::Flags_Fast8 _ship_abort_state;

  // -- Event registers
  sev::event::bit_accus_async::Callback _event_accu_async;
  sev::event::Bit_Accu_Fast32 _event_accu;

  // -- Bridge event generation registers
  sev::mem::Ring_Resizing< snc::svp::office::Factor * > _notification_factors;

  /// @brief Bridge processing battery
  snc::bat::Battery _ship_battery;

  // -- Bridge connection
  bool _ship_event_io = false;
  sev::event::queue_io::Connection_2 _ship_connection;
  sev::event::queue_io::Reference_2 _event_queue;

  // -- Pilot controllers
  std::vector< snc::svp::office::Factor * > _factors;
};

} // namespace snc::svs::frame::ship::office
