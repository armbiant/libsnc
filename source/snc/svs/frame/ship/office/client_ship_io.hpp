/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <sev/mem/flags.hpp>
#include <snc/svp/ship/events.hpp>
#include <snc/svs/frame/ship/office/client_base.hpp>
#include <cstdint>
#include <vector>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
}
namespace snc::svp::ship
{
class Sailor;
}

namespace snc::svs::frame::ship::office
{

/** @brief Event io client */
class Client_Ship_IO : public Client_Base
{
  // -- Types
  private:
  using Super = Client_Base;
  using Factor = snc::svp::office::Factor;
  using Sailor = snc::svp::ship::Sailor;
  using Bridge_Notification = snc::svp::ship::event::In;
  using Bridge_Event = const snc::svp::ship::event::Out;

  public:
  // -- Construction

  Client_Ship_IO ( Factor * user_n );

  ~Client_Ship_IO ();

  // -- Factor

  Factor *
  factor ();

  const Factor *
  factor () const;

  // -- Sailors

  const std::vector< Sailor * > &
  sailors () const
  {
    return _sailors;
  }

  void
  sailors_push ( Sailor * sailor_n )
  {
    _sailors.push_back ( sailor_n );
  }

  // -- Event pool tracker

  sev::event::Pool_Tracker &
  epool_tracker ()
  {
    return _epool_tracker;
  }

  const sev::event::Pool_Tracker &
  epool_tracker () const
  {
    return _epool_tracker;
  }

  // -- Event allocation

  template < typename T, typename... Args >
  void
  allocate_for ( Sailor * sailor_n,
                 sev::event::Pool< T > & pool_n,
                 std::size_t num_n,
                 Args &&... args_n )
  {
    pool_n.set_size ( num_n,
                      Bridge_Notification::Pair{ factor (), sailor_n },
                      std::forward< Args > ( args_n )... );
  }

  template < typename T, typename... Args >
  void
  allocate ( sev::event::Pool< T > & pool_n,
             std::size_t num_n,
             Args &&... args_n )
  {
    allocate_for ( sailors ().at ( 0 ),
                   pool_n,
                   num_n,
                   std::forward< Args > ( args_n )... );
  }

  template < typename T, typename... Args >
  T *
  acquire_for ( Sailor * sailor_n,
                sev::event::Pool< T > & pool_n,
                Args &&... args_n )
  {
    if ( !pool_n.is_empty () ) {
      return pool_n.pop_not_empty ();
    }
    return pool_n.create ( Bridge_Notification::Pair{ factor (), sailor_n },
                           std::forward< Args > ( args_n )... );
  }

  template < typename T, typename... Args >
  T *
  acquire ( sev::event::Pool< T > & pool_n, Args &&... args_n )
  {
    return acquire_for (
        sailors ().at ( 0 ), pool_n, std::forward< Args > ( args_n )... );
  }

  // -- Outgoing event statistics

  bool
  events_unacknowledged () const
  {
    return ( _stats_out != 0 );
  }

  std::int_fast32_t
  stats_out () const
  {
    return _stats_out;
  }

  void
  stats_out_increment ()
  {
    ++_stats_out;
  }

  void
  stats_out_sub ( std::int_fast32_t delta_n )
  {
    DEBUG_ASSERT ( delta_n <= _stats_out );
    _stats_out -= delta_n;
  }

  // -- Bridge notification bit register

  const sev::mem::Flags_Fast32 &
  notifications () const
  {
    return _notifications;
  }

  void
  notifications_assign ( std::uint_fast32_t messages_n );

  void
  notification_set ( std::uint_fast32_t messages_n );

  void
  notification_unset ( std::uint_fast32_t messages_n );

  /// @brief Returns a valid event pointer if the message was requested
  ///        and the pool is not empty
  template < class T >
  T *
  notify_get ( std::uint_fast32_t message_n, sev::event::Pool< T > & pool_n )
  {
    if ( !_notifications.test_any ( message_n ) || pool_n.is_empty () ) {
      return nullptr;
    }
    _notifications.unset ( message_n );
    return pool_n.pop_not_empty ();
  }

  // -- Event io

  void
  submit ( Bridge_Notification * event_n );

  void
  release ( Bridge_Notification * event_n );

  void
  request_pick_up ();

  void
  pick_up ();

  bool
  output_satisfied () const
  {
    return ( _notifications.is_empty () && ( _stats_out == 0 ) );
  }

  private:
  sev::mem::Flags_Fast32 _notifications;
  bool _pick_up_requested = false;
  std::int_fast32_t _stats_out = 0;
  std::vector< Sailor * > _sailors;
  sev::event::Pool_Tracker _epool_tracker;
};

} // namespace snc::svs::frame::ship::office
