/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/frame/ship/office/client_base.hpp>

namespace snc::svs::frame::ship::office
{

/** @brief Session control client */
class Client_Session : public Client_Base
{
  // -- Types
  private:
  using Super = Client_Base;

  public:
  // -- Construction

  Client_Session ( User * user_n );

  ~Client_Session ();

  // -- Interface

  bool
  is_running () const;

  bool
  is_aborting () const;

  bool
  start ();

  void
  abort ( bool forced_n = false ) const;
};

} // namespace snc::svs::frame::ship::office
