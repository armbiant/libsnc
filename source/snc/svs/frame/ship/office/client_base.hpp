/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>

namespace snc::svs::frame::ship::office
{

// -- Forward declaration
class Service;

class Client_Base : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client_Base ( User * user_n );

  ~Client_Base ();

  // -- Service connection
  protected:
  /// @brief Service instance
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::frame::ship::office
