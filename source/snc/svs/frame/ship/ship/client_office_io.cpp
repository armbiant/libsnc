/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_office_io.hpp"
#include <sev/utility.hpp>
#include <snc/svp/ship/sailor.hpp>
#include <snc/svs/frame/ship/ship/service.hpp>

namespace snc::svs::frame::ship::ship
{

Client_Office_IO::Client_Office_IO ( User * user_n, Factor * factor_n )
: Super ( user_n )
, _factor ( factor_n )
{
  session ().set_end_callback ( [ this ] () {
    _notifications.clear ();
    _stats_in = 0;
  } );
}

Client_Office_IO::~Client_Office_IO () = default;

Client_Office_IO::Sailor *
Client_Office_IO::sailor ()
{
  return static_cast< Sailor * > ( user () );
}

const Client_Office_IO::Sailor *
Client_Office_IO::sailor () const
{
  return static_cast< const Sailor * > ( user () );
}

void
Client_Office_IO::notifications_clear ()
{
  _notifications.clear ();
}

void
Client_Office_IO::notification_set ( std::uint_fast32_t messages_n )
{
  if ( !session ().is_good () ) {
    return;
  }

  _notifications.set ( messages_n );
  request_pick_up ();
}

void
Client_Office_IO::notification_unset ( std::uint_fast32_t messages_n )
{
  _notifications.unset ( messages_n );
}

void
Client_Office_IO::request_pick_up ()
{
  if ( sev::change ( _pick_up_requested, true ) ) {
    service ()->office_notify_register ( static_cast< Sailor * > ( user () ) );
  }
}

void
Client_Office_IO::pick_up ()
{
  if ( sev::change ( _pick_up_requested, false ) ) {
    static_cast< Sailor * > ( user () )->office_notify ();
  }
}

void
Client_Office_IO::submit ( Office_Notification * event_n )
{
  if ( session ().is_good () ) {
    service ()->office_notify_submit ( event_n );
  } else {
    release ( event_n );
  }
}

void
Client_Office_IO::release ( Office_Notification * event_n )
{
  // Test that we own this event
  DEBUG_ASSERT ( epool_tracker ().owned ( event_n ) );
  // Reset release the event
  event_n->pool ()->casted_reset_release_virtual ( event_n );
  // We might be waiting for the event to return
  if ( !notifications ().is_empty () ) {
    request_pick_up ();
  }
}

Service *
Client_Office_IO::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client_Office_IO::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::frame::ship::ship
