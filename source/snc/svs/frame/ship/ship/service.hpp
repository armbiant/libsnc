/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <snc/bat/detail/cell_ring.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/svp/ship/events.hpp>

namespace snc::svs::frame::ship::ship
{

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  struct LEvent
  {
    static constexpr std::uint_fast32_t OFFICE_EVENT_GENERATION = ( 1 << 0 );
    static constexpr std::uint_fast32_t OFFICE_IN = ( 1 << 1 );
    static constexpr std::uint_fast32_t OFFICE_OUT = ( 1 << 2 );
  };
  using Office_Notification = snc::svp::ship::event::Out;
  using Office_Event = const snc::svp::ship::event::In;
  using Sailor = snc::svp::ship::Sailor;

  public:
  // -- Construction and session

  Service ( Provider_Cell & provider_n,
            const sev::event::queue_io::Link_2 & event_queue_link_n );

  ~Service ();

  // -- Session interface

  void
  session_begin ();

  bool
  session_is_ready_to_end () const;

  void
  session_end ();

  // -- Central processing

  void
  process_async ();

  void
  process ();

  // -- Client interface

  void
  office_notify_register ( Sailor * sailor_n );

  void
  office_notify_submit ( Office_Notification * event_n )
  {
    _office_connection.push ( event_n );
  }

  private:
  void
  process_office_event_generation ();

  void
  process_office_event_io ();

  private:
  // -- Event bits
  sev::event::bit_accus_async::Callback _event_accu_async;
  sev::event::Bit_Accu_Fast32 _event_accu;

  // -- Office event queue connection
  sev::event::queue_io::Connection_2 _office_connection;

  // -- Office pilots event io
  std::vector< snc::bat::Cell * > _office_egen_pilots_buffer;
  snc::bat::detail::Cell_Ring _office_egen_pilots;
};

} // namespace snc::svs::frame::ship::ship
