/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <sev/mem/flags.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/svp/ship/events.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
}
namespace snc::svp::ship
{
class Sailor;
}

namespace snc::svs::frame::ship::ship
{

// -- Forward declaration
class Service;

/** @brief Client for event IO */
class Client_Office_IO : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  using Factor = snc::svp::office::Factor;
  using Sailor = snc::svp::ship::Sailor;
  using Office_Event = const snc::svp::ship::event::In;
  using Office_Notification = snc::svp::ship::event::Out;

  // -- Construction

  Client_Office_IO ( User * user_n, Factor * factor_n );

  ~Client_Office_IO ();

  // -- Sailor

  Sailor *
  sailor ();

  const Sailor *
  sailor () const;

  // -- Event pool tracker

  sev::event::Pool_Tracker &
  epool_tracker ()
  {
    return _epool_tracker;
  }

  const sev::event::Pool_Tracker &
  epool_tracker () const
  {
    return _epool_tracker;
  }

  // -- Event allocation

  template < typename T, typename... Args >
  void
  allocate ( sev::event::Pool< T > & pool_n,
             std::size_t num_n,
             Args &&... args_n )
  {
    pool_n.set_size ( num_n,
                      Office_Notification::Pair ( _factor, sailor () ),
                      std::forward< Args > ( args_n )... );
  }

  // -- Incoming office event statistics

  /// @brief Number of events incoming from the office
  std::int_fast32_t
  stats_in ()
  {
    return _stats_in;
  }

  void
  stats_in_increment ()
  {
    ++_stats_in;
  }

  std::int_fast32_t
  stats_in_fetch_clear ()
  {
    std::int_fast32_t res = _stats_in;
    _stats_in = 0;
    return res;
  }

  // -- Event notification bits

  /// @brief Notification bits
  const sev::mem::Flags_Fast32 &
  notifications () const
  {
    return _notifications;
  }

  /// @brief Set the notification bits and clear all others
  void
  notifications_clear ();

  /// @brief Set the notification bits
  void
  notification_set ( std::uint_fast32_t messages_n );

  /// @brief Unset the notification bits
  void
  notification_unset ( std::uint_fast32_t messages_n );

  // -- Event pick up request

  /// @brief Returns a valid event pointer if the message was requested
  ///        and the pool is not empty
  template < class T >
  T *
  notify_get ( std::uint_fast32_t message_n, sev::event::Pool< T > & pool_n )
  {
    if ( !_notifications.test_any ( message_n ) || pool_n.is_empty () ) {
      return nullptr;
    }
    _notifications.unset ( message_n );
    return pool_n.pop_not_empty ();
  }

  // -- Event io

  /// @brief Office event submission
  void
  submit ( Office_Notification * event_n );

  void
  release ( Office_Notification * event_n );

  /// @brief Office event generation interface method for the bridge pilot
  void
  pick_up ();

  private:
  /// @brief Request office event generation at the next convenient time
  void
  request_pick_up ();

  // -- Service connection
  protected:
  /// @brief Service instance
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  private:
  Factor * _factor = nullptr;
  sev::mem::Flags_Fast32 _notifications;
  bool _pick_up_requested = false;
  std::int_fast32_t _stats_in = 0;
  sev::event::Pool_Tracker _epool_tracker;
};

} // namespace snc::svs::frame::ship::ship
