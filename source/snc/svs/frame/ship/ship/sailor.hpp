/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/sailor.hpp>
#include <snc/svs/frame/ship/ship/service.hpp>

namespace snc::svs::frame::ship::ship
{

class Sailor : public snc::svp::ship::Sailor
{
  // -- Types
  private:
  using Super = snc::svp::ship::Sailor;

  public:
  // -- Construction

  Sailor ( const Sailor_Init & init_n,
           sev::event::queue_io::Link_2 eql_link_n );

  ~Sailor ();

  // -- Service

  auto &
  service ()
  {
    return _service;
  }

  // -- Bridge session interface

  void
  cell_session_begin () override;

  bool
  cell_session_is_ready_to_end () const override;

  void
  cell_session_end () override;

  // -- Central processing

  public:
  void
  process_async () override;

  void
  process () override;

  private:
  Service _service;
};

} // namespace snc::svs::frame::ship::ship
