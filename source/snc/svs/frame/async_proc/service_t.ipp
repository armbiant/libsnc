/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "service_t.hpp"

namespace snc::svs::frame::async_proc
{

template < typename WRK >
Service_T< WRK >::Service_T ( Provider_Cell & provider_n,
                              sev::unicode::View log_name_n )
: Super ( provider_n, &typeid ( Request ), log_name_n )
{
  reset_vars ();
}

template < typename WRK >
Service_T< WRK >::~Service_T ()
{
}

template < typename WRK >
void
Service_T< WRK >::reset_vars_virtual ( Variables & vars_n ) const
{
  vars_n.request = std::make_shared< const Request > ();
  vars_n.progress = std::make_unique< Progress > ();
  vars_n.result = std::make_unique< Result > ();
}

template < typename WRK >
std::unique_ptr< Worker_Base >
Service_T< WRK >::make_worker (
    const sev::event::queue_io::Link_2 & link_n,
    const std::shared_ptr< const Request_Base > & request_n )
{
  vars_ref ().request = request_n;
  return std::make_unique< Worker > ( Super::log (), link_n, request_n );
}

template < typename WRK >
void
Service_T< WRK >::update_progress ( const Progress_Base & prog_n )
{
  dynamic_cast< Progress & > ( *vars ().progress ) =
      dynamic_cast< const Progress & > ( prog_n );
}

template < typename WRK >
void
Service_T< WRK >::update_result ( const Result_Base & result_n )
{
  dynamic_cast< Result & > ( *vars ().result ) =
      dynamic_cast< const Result & > ( result_n );
}

} // namespace snc::svs::frame::async_proc
