/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "client_t.hpp"
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/frame/async_proc/service_async.hpp>
#include <stdexcept>

namespace snc::svs::frame::async_proc
{

template < typename TPS >
Client_T< TPS >::Client_T ( User * user_n )
: Super ( user_n )
{
}

template < typename TPS >
Client_T< TPS >::~Client_T ()
{
}

template < typename TPS >
Client_T< TPS >::Control::~Control ()
{
  abort ();
}

template < typename TPS >
bool
Client_T< TPS >::Control::start (
    const std::shared_ptr< const Request > & request_n )
{
  if ( Super::is_valid () ) {
    return client ()->service_async ()->client_start ( request_n );
  }
  return false;
}

template < typename TPS >
bool
Client_T< TPS >::Control::poll_updates ()
{
  if ( Super::is_valid () ) {
    return client ()->service_async ()->client_poll_updates ();
  }
  return false;
}

template < typename TPS >
void
Client_T< TPS >::Control::abort ()
{
  if ( Super::is_valid () ) {
    client ()->service_async ()->client_abort ();
  }
}

template < typename TPS >
const typename Client_T< TPS >::Request &
Client_T< TPS >::Control::request () const
{
  if ( !Super::is_valid () ) {
    throw std::runtime_error ( "Invalid control." );
  }
  return static_cast< const Request & > (
      *client ()->service_async ()->vars ().request );
}

template < typename TPS >
const typename Client_T< TPS >::Progress &
Client_T< TPS >::Control::progress () const
{
  if ( !Super::is_valid () ) {
    throw std::runtime_error ( "Invalid control." );
  }
  return static_cast< const Progress & > (
      *client ()->service_async ()->vars ().progress );
}

template < typename TPS >
const typename Client_T< TPS >::Result &
Client_T< TPS >::Control::result () const
{
  if ( !Super::is_valid () ) {
    throw std::runtime_error ( "Invalid control." );
  }
  return static_cast< const Result & > (
      *client ()->service_async ()->vars ().result );
}

template < typename TPS >
bool
Client_T< TPS >::connect_to ( Service_Abstract * sb_n )
{
  auto * service_base = dynamic_cast< Service_Async * > ( sb_n );
  if ( service_base == nullptr ) {
    return false;
  }
  if ( *service_base->request_type_info () != typeid ( Request ) ) {
    return false;
  }
  _service_async = service_base;
  return true;
}

template < typename TPS >
void
Client_T< TPS >::disconnecting ()
{
  Super::disconnecting ();
  _service_async = nullptr;
}

} // namespace snc::svs::frame::async_proc
