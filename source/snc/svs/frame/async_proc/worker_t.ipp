/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "worker_t.hpp"
#include <sev/assert.hpp>

namespace snc::svs::frame::async_proc
{

template < typename TPS >
Worker_T< TPS >::Worker_T ( sev::logt::Context log_context_n,
                            const sev::event::queue_io::Link_2 & link_n,
                            std::shared_ptr< const Request_Base > request_n )
: Super ( std::move ( log_context_n ), link_n )
, _request ( std::move ( request_n ) )
, _event_pool_progress ( epool_tracker () )
, _event_pool_result ( epool_tracker () )
{
  _event_pool_progress.set_size ( 1, std::make_unique< Progress > () );
  _event_pool_result.set_size ( 1, std::make_unique< Result > () );
}

template < typename TPS >
Worker_T< TPS >::~Worker_T () = default;

template < typename TPS >
bool
Worker_T< TPS >::send_progress ()
{
  if ( _event_pool_progress.is_empty () ) {
    return false;
  }
  auto * event = _event_pool_progress.pop_not_empty ();
  static_cast< Progress & > ( *event->progress ) = _progress;
  connection ().push ( event );
  return true;
}

template < typename TPS >
void
Worker_T< TPS >::send_result ()
{
  if ( _event_pool_result.is_empty () ) {
    throw std::runtime_error ( "Unexpected empty event pool" );
  }
  auto * event = _event_pool_result.pop_not_empty ();
  static_cast< Result & > ( *event->result ) = _result;
  connection ().push ( event );
}

} // namespace snc::svs::frame::async_proc
