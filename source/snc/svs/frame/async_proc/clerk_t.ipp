/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "clerk_t.hpp"

namespace snc::svs::frame::async_proc
{

template < typename SVS >
Clerk_T< SVS >::Clerk_T ( const Clerk_Init & init_n )
: Super ( init_n, "ASync-Processor" )
, _service ( *this )
{
}

template < typename SVS >
Clerk_T< SVS >::~Clerk_T () = default;

template < typename SVS >
bool
Clerk_T< SVS >::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && !_service.is_active ();
}

template < typename SVS >
void
Clerk_T< SVS >::process_async ()
{
  _service.process_async ();
}

template < typename SVS >
void
Clerk_T< SVS >::process ()
{
  _service.process ();
}

} // namespace snc::svs::frame::async_proc
