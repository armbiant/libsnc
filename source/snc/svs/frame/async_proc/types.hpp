/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <string>

namespace snc::svs::frame::async_proc
{

class Request_Base
{
  public:
  // -- Construction

  Request_Base ();

  Request_Base ( const Request_Base & val_n ) = delete;

  virtual ~Request_Base ();
};

class Progress_Base
{
  public:
  // -- Construction

  Progress_Base ();

  virtual ~Progress_Base ();

  // -- Interface

  virtual void
  reset ()
  {
  }
};

class Result_Base
{
  public:
  // -- Types
  enum class End
  {
    NONE,

    GOOD,
    ABORTED,
    ERROR
  };

  // -- Construction

  Result_Base ();

  virtual ~Result_Base ();

  // -- Interface

  bool
  finished () const
  {
    return ( end != End::NONE );
  }

  bool
  good () const
  {
    return ( end == End::GOOD );
  }

  /// @brief Reset to construction state
  virtual void
  reset ();

  // -- Attributes
  End end = End::NONE;
  std::string error_message;
};

} // namespace snc::svs::frame::async_proc
