/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/svs/frame/async_proc/service_async.hpp>

namespace snc::svs::frame::async_proc
{

template < typename WRK >
class Service_T : public Service_Async
{
  // -- Types
  private:
  using Super = Service_Async;

  public:
  using Worker = WRK;
  using Types = typename WRK::Types;

  using Request = typename Types::Request;
  using Progress = typename Types::Progress;
  using Result = typename Types::Result;

  public:
  // -- Construction

  Service_T ( Provider_Cell & provider_n,
              sev::unicode::View log_name_n = "Async_Service" );

  ~Service_T ();

  protected:
  // -- Utility

  void
  reset_vars_virtual ( Variables & vars_n ) const override;

  std::unique_ptr< Worker_Base >
  make_worker (
      const sev::event::queue_io::Link_2 & link_n,
      const std::shared_ptr< const Request_Base > & request_n ) override;

  void
  update_progress ( const Progress_Base & prog_n ) override;

  void
  update_result ( const Result_Base & result_n ) override;
};

} // namespace snc::svs::frame::async_proc
