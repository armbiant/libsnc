/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "service_async.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>
#include <stdexcept>

namespace snc::svs::frame::async_proc
{

Service_Async::Service_Async ( Provider_Cell & provider_n,
                               const std::type_info * request_type_info_n,
                               sev::unicode::View log_name_n )
: Super ( provider_n )
, _request_type_info ( request_type_info_n )
, _worker_thread ( log_name_n, provider_n.cell_context ().thread_tracker () )
{
  _event_accu.set_callback ( provider_cell ().processing_request () );
  _event_accu_async.set_callback (
      provider_cell ().processing_request_async () );

  _epools.pool< events::out::Abort > ().set_capacity ( 1 );

  _connection.set_push_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::EVENTS_OUT ); } );
  _connection.set_incoming_notifier (
      [ &ec = _event_accu_async ] () { ec.set ( LEvent::EVENTS_IN ); } );
}

Service_Async::~Service_Async () = default;

bool
Service_Async::is_active () const
{
  return _worker.operator bool ();
}

bool
Service_Async::control_acquirable () const
{
  return Super::control_acquirable () && !is_active ();
}

void
Service_Async::process_async ()
{
  _event_accu.set_silent ( _event_accu_async.fetch_and_clear () );
  process ();
}

void
Service_Async::process ()
{
  // Check of there are pending events
  if ( _event_accu.test_any ( LEvent::EVENTS_IN | LEvent::EVENTS_OUT ) ) {
    // Feed event queue
    _connection.feed_queue (
        _event_accu.flags (), LEvent::EVENTS_IN, LEvent::EVENTS_OUT );

    // Release returned events
    _connection.out ().release_all_with_reset ();

    // Deliver new events
    _connection.in ().process_all ( [ this ] (
                                        const sev::event::Event & event_n ) {
      switch ( event_n.type () ) {
      case events::in::Type::PROGRESS: {
        auto & cevent = static_cast< const events::in::Progress & > ( event_n );
        this->update_progress ( *cevent.progress );
      }
        set_updated ();
        break;

      case events::in::Type::RESULT: {
        auto & cevent = static_cast< const events::in::Result & > ( event_n );
        this->update_result ( *cevent.result );
      }
        set_updated ();
        // Always the last event
        _worker_join = true;
        break;

      default:
        DEBUG_ASSERT ( false );
      }
    } );
  }

  // Destroy worker in the end
  if ( _worker_join && _epools.tracker ().all_home () &&
       _connection.all_empty () ) {
    log ().cat ( sev::logt::FL_DEBUG, "Join worker thread begin." );
    _worker_thread.join ();
    log ().cat ( sev::logt::FL_DEBUG, "Join worker thread done." );
    _worker.reset ();
    _worker_join = false;
    _connection.disconnect ();

    // Reset variables on demand
    if ( !controlled () ) {
      reset_vars ();
    }
    // Availability has changed
    set_updated ();
  }
}

bool
Service_Async::client_start (
    const std::shared_ptr< const Request_Base > & request_n )
{
  if ( !request_n ) {
    return false;
  }

  // Create worker
  log ().cat ( sev::logt::FL_DEBUG, "Starting worker." );
  {
    auto queue_ref = sev::event::queue_io::Reference_2::created ();
    _connection.connect ( queue_ref.link_a () );
    _worker = this->make_worker ( queue_ref.link_b (), request_n );
    _worker_thread.start (
        [ wrk = _worker.get () ] () { wrk->operator() (); } );
  }

  // Availability has changed
  signal_state ().send ();

  return true;
}

bool
Service_Async::client_poll_updates ()
{
  return sev::change ( _updated, false );
}

void
Service_Async::client_abort ()
{
  // Abort worker on demand
  if ( _worker && !_worker_join ) {
    _connection.push ( _epools.pool< events::out::Abort > ().acquire () );
  }
}

void
Service_Async::reset_vars ()
{
  this->reset_vars_virtual ( vars_ref () );
}

void
Service_Async::set_updated ()
{
  _updated = true;
  signal_state ().send ();
}

void
Service_Async::control_released ( Client_Abstract * client_n )
{
  Super::control_released ( client_n );
  reset_vars ();
}

} // namespace snc::svs::frame::async_proc
