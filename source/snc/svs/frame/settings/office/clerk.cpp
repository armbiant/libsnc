/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/string/utility.hpp>
#include <snc/svs/frame/settings/utility.hpp>
#include <QDir>
#include <utility>

namespace snc::svs::frame::settings::office
{

Clerk::Clerk ( const Clerk_Init & init_n,
               const std::vector< std::string > & directories_n )
: Super ( init_n, "Settings" )
, _service ( *this, directories_n )
{
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.read ();
}

void
Clerk::cell_session_end ()
{
  _service.write ();
  Super::cell_session_end ();
}

} // namespace snc::svs::frame::settings::office
