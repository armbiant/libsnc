/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/settings/settings.hpp>

namespace snc::svs::frame::settings::office
{

// -- Forward declaration
class Service;

/** @brief Client */
class Client : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client ();

  // -- Interface

  snc::settings::Settings *
  settings () const;

  // -- Service connection
  protected:
  /// @brief Service instance
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::frame::settings::office
