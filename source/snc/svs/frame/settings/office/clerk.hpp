/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/clerk.hpp>
#include <snc/svs/frame/settings/office/service.hpp>
#include <string>
#include <vector>

namespace snc::svs::frame::settings::office
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n,
          const std::vector< std::string > & directories_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_end () override;

  private:
  // -- Attributes
  Service _service;
};

} // namespace snc::svs::frame::settings::office
