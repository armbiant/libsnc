/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "utility.hpp"
#include <snc/settings/settings.hpp>
#include <QDir>
#include <fstream>

namespace snc::svs::frame::settings::utility
{

bool
json_import ( snc::settings::Database_Reference db_n,
              const std::string & file_n )
{
  if ( file_n.empty () ) {
    return false;
  }

  bool good = false;
  {
    std::ifstream ifs ( file_n, std::ios_base::in | std::ios_base::binary );
    if ( ifs ) {
      good = snc::settings::Settings ( std::move ( db_n ) )
                 .json_import_stream ( ifs )
                 .first;
    }
  }
  return good;
}

void
json_export ( snc::settings::Database_Reference db_n,
              const std::string & file_n )
{
  if ( file_n.empty () ) {
    return;
  }

  std::ofstream ofs ( file_n,
                      std::ios_base::out | std::ios_base::trunc |
                          std::ios_base::binary );
  if ( ofs ) {
    snc::settings::Settings ( std::move ( db_n ) ).json_export_stream ( ofs );
  }
}

} // namespace snc::svs::frame::settings::utility
