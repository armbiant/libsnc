/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svs/frame/settings/office/clerk.hpp>
#include <snc/svs/frame/settings/utility.hpp>
#include <QDir>
#include <QStandardPaths>
#include <utility>

namespace snc::svs::frame::settings::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Settings" )
, _settings_file_name ( "dash.json" )
, _directories (
      QStandardPaths::standardLocations ( QStandardPaths::AppConfigLocation ) )
{
  if ( !_directories.isEmpty () ) {
    _settings_dir = _directories.front ();
    _settings_file = settings_file_abs ( _settings_dir );
  }
}

Panel::~Panel () = default;

void
Panel::session_begin ()
{
  Super::session_begin ();
  read_settings ();
}

void
Panel::session_end ()
{
  write_settings ();
  Super::session_end ();
}

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  std::vector< std::string > dirs;
  dirs.reserve ( _directories.size () );
  for ( const auto & dir : _directories ) {
    dirs.push_back ( dir.toStdString () );
  }

  return make_clerk_factory< snc::svs::frame::settings::office::Clerk > (
      std::move ( dirs ) );
}

void
Panel::read_settings ()
{
  // Import from back to front
  auto & dirs = _directories;
  for ( auto itc = dirs.crbegin (), ite = dirs.crend (); itc != ite; ++itc ) {
    auto file = settings_file_abs ( *itc ).toStdString ();
    log ().cat ( sev::logt::FL_DEBUG, "Reading dash settings file ", file );
    utility::json_import ( _settings.database (), file );
  }
}

void
Panel::write_settings ()
{
  if ( _settings_file.isEmpty () ) {
    return;
  }

  // Create settings directory
  {
    QDir dir ( _settings_dir );
    if ( !dir.exists () ) {
      auto dir_name = _settings_dir.toStdString ();
      log ().cat (
          sev::logt::FL_DEBUG, "Creating settings directory ", dir_name );
      if ( !dir.mkpath ( "." ) ) {
        log ().cat (
            sev::logt::FL_DEBUG, "Can't create settings directory ", dir_name );
        return;
      }
    }
  }

  {
    auto file = _settings_file.toStdString ();
    log ().cat ( sev::logt::FL_DEBUG, "Writing dash settings file ", file );
    utility::json_export ( _settings.database (), file );
  }
}

QString
Panel::settings_file_abs ( QString dir_n )
{
  if ( dir_n.isEmpty () ) {
    return QString ();
  }
  return QDir ( dir_n ).absoluteFilePath ( _settings_file_name );
}

} // namespace snc::svs::frame::settings::dash
