/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <snc/bat/battery.hpp>
#include <snc/svp/dash/Panel.hpp>

// -- Forward declaration
namespace snc::svp::dash
{
class Office_IO;
}

namespace snc::svs::frame::office::dash
{

class Panel : public snc::svp::dash::Panel
{
  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENTS_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENTS_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t ABORT_DONE = ( 1 << 2 );
  };

  // -- Construction

  Panel ( const Panel_Init & init_n, sev::thread::Tracker * thread_tracker_n );

  ~Panel ();

  // -- Session interface

  void
  session_abort () override;

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office session state interface

  bool
  office_session_is_ready_to_end () const override;

  // -- Central processing

  void
  process () override;

  void
  process_office_event_io ();

  void
  process_office_abort ();

  bool
  event ( QEvent * event_n ) override;

  // -- Office session control

  bool
  start_office ();

  void
  abort_office ();

  // -- Event io interface
  private:
  friend snc::svp::dash::Office_IO;

  void
  submit_office_event ( sev::event::Event * event_n )
  {
    _office_connection.push ( event_n );
  }

  private:
  // -- Office event queue
  sev::event::queue_io::Reference_2 _queue_io_ref;
  // -- Office event queue connection
  sev::event::queue_io::Connection_2 _office_connection;
  // -- Office thread
  snc::bat::Battery _office_battery;
};

} // namespace snc::svs::frame::office::dash
