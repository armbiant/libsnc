/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"

namespace snc::svs::frame::office::office
{

Clerk::Clerk ( const Clerk_Init & init_n,
               const sev::event::queue_io::Link_2 & event_queue_link_n )
: Super ( init_n, "Office" )
, _service ( *this, event_queue_link_n )
{
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.session_begin ();
}

void
Clerk::cell_session_end ()
{
  _service.session_end ();
  Super::cell_session_end ();
}

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () && _service.buffers_empty ();
}

void
Clerk::process_async ()
{
  _service.process_async ();
}

void
Clerk::process ()
{
  _service.process ();
}

} // namespace snc::svs::frame::office::office
