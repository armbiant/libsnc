/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/ring_resizing.hpp>
#include <snc/bat/ifc/service_abstract.hpp>

// -- Forward declaration
namespace snc::svp::office
{
class Clerk;
}

namespace snc::svs::frame::office::office
{

/** @brief Service */
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENTS_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENTS_OUT = ( 1 << 1 );
  };

  public:
  // -- Construction and session

  Service ( Provider_Cell & provider_n,
            const sev::event::queue_io::Link_2 & event_queue_link_n );

  ~Service ();

  void
  session_begin ();

  void
  session_end ();

  // -- Provider interface

  bool
  buffers_empty () const;

  void
  process_async ();

  void
  process ();

  // -- Client interface

  void
  request_pick_up ( snc::svp::office::Clerk * clerk_n );

  void
  submit ( sev::event::Event * event_n )
  {
    _connection.push ( event_n );
  }

  private:
  // -- Utility

  void
  generate_dash_events ();

  private:
  // -- Attributes
  sev::event::bit_accus_async::Callback _event_accu_async;
  sev::event::Bit_Accu_Fast32 _event_accu;
  // -- Dash event queue connection
  sev::event::queue_io::Connection_2 _connection;
  // -- Dash event generation registers
  sev::mem::Ring_Resizing< snc::svp::office::Clerk * > _notification_clerks;
  // -- Dash event generation
  std::chrono::steady_clock::duration _pick_up_interval;
  sev::event::timer_queue::chrono::Client_Steady _pick_up_timer;
};

} // namespace snc::svs::frame::office::office
