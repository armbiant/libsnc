/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <sev/lag/vector_math.hpp>
#include <array>
#include <cmath>

namespace snc::mpath_feed
{

/// @brief Integer step forward(+), hold(0) or backward(-)
///
template < std::size_t DIM >
using Grid_Step = std::array< std::int8_t, DIM >;

/// @brief Basic step
///
template < std::size_t DIM >
class BStep
{
  public:
  // -- Construction

  BStep () = default;

  BStep ( const BStep< DIM > & bstep_n )
  : grid_step ( bstep_n.grid_step )
  , pos ( bstep_n.pos )
  , tangent ( bstep_n.tangent )
  {
  }

  // -- Assignment operators

  void
  operator= ( const BStep< DIM > & bstep_n )
  {
    grid_step = bstep_n.grid_step;
    pos = bstep_n.pos;
    tangent = bstep_n.tangent;
  }

  public:
  // -- Attributes
  Grid_Step< DIM > grid_step;
  sev::lag::Vector< double, DIM > pos;
  sev::lag::Vector< double, DIM > tangent;
};

/// @brief Celerated step with speed information
///
template < std::size_t DIM >
class CStep : public BStep< DIM >
{
  public:
  // -- Construction

  CStep () = default;

  CStep ( const CStep< DIM > & cstep_n )
  : BStep< DIM > ( cstep_n )
  , speed ( cstep_n.speed )
  , speed_max ( cstep_n.speed_max )
  {
  }

  CStep ( const BStep< DIM > & bstep_n )
  : BStep< DIM > ( bstep_n )
  {
  }

  // -- Assignment operators

  void
  operator= ( const CStep< DIM > & cstep_n )
  {
    BStep< DIM >::operator= ( cstep_n );
    speed = cstep_n.speed;
    speed_max = cstep_n.speed_max;
  }

  void
  operator= ( const BStep< DIM > & bstep_n )
  {
    BStep< DIM >::operator= ( bstep_n );
  }

  // -- Utility

  void
  acquire_vspeed ( sev::lag::Vector< double, DIM > & vspeed_n ) const
  {
    vspeed_n = BStep< DIM >::tangent * speed;
  }

  void
  acquire_vspeed_max ( sev::lag::Vector< double, DIM > & vspeed_n ) const
  {
    vspeed_n = BStep< DIM >::tangent * speed_max;
  }

  public:
  // -- Attributes
  double speed;
  double speed_max;
};

// -- Types

using CStep_D1 = CStep< 1 >;
using CStep_D2 = CStep< 2 >;
using CStep_D3 = CStep< 3 >;
using CStep_D4 = CStep< 4 >;
using CStep_D5 = CStep< 5 >;
using CStep_D6 = CStep< 6 >;

} // namespace snc::mpath_feed
