/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/event/event.hpp>
#include <sev/event/list.hpp>
#include <snc/mpath_feed/cargo.hpp>

namespace snc::mpath_feed
{

/// @brief Cargo pool entry stack
///
class Cargo_Pool_Stack : protected sev::event::List
{
  public:
  // -- Construction

  Cargo_Pool_Stack ();

  // -- Interface

  using sev::event::List::is_empty;

  bool
  all_home () const
  {
    return ( _num_home == _num_allocated );
  }

  std::size_t
  num_home () const
  {
    return _num_home;
  }

  std::size_t
  num_allocated () const
  {
    return _num_allocated;
  }

  sev::event::Event *
  pop_not_empty ()
  {
    auto * res = front ();
    sev::event::List::pop_front_not_empty ();
    --_num_home;
    return res;
  }

  void
  release ( Cargo * cargo_n )
  {
    sev::event::List::push_front ( cargo_n );
    DEBUG_ASSERT ( _num_home < _num_allocated );
    ++_num_home;
  }

  void
  push_allocated ( Cargo * cargo_n )
  {
    sev::event::List::push_front ( cargo_n );
    ++_num_allocated;
    ++_num_home;
  }

  sev::event::Event *
  pop_allocated ()
  {
    auto * res = front ();
    sev::event::List::pop_front_not_empty ();
    --_num_allocated;
    --_num_home;
    return res;
  }

  private:
  // -- Attributes
  std::size_t _num_home = 0;
  std::size_t _num_allocated = 0;
};

} // namespace snc::mpath_feed
