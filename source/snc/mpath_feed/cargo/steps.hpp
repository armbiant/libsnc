/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <snc/mpath_feed/cargo.hpp>
#include <snc/mpath_feed/step.hpp>

namespace snc::mpath_feed::cargo
{

template < class T >
class Steps : public snc::mpath_feed::Cargo
{
  public:
  // -- Types
  static const std::size_t _num_steps_max = 256;

  // -- Construction

  Steps () = delete;

  protected:
  Steps ( Cargo_Type cargo_type_n, Cargo_Pool * pool_n = nullptr )
  : snc::mpath_feed::Cargo ( cargo_type_n, pool_n )
  , _num_steps ( 0 )
  {
  }

  // -- Setup
  public:
  /// @brief Clears the step count
  void
  reset ()
  {
    _num_steps = 0;
  }

  // -- Interface

  std::size_t
  num_steps () const
  {
    return _num_steps;
  }

  void
  num_steps_increment ()
  {
    DEBUG_ASSERT ( _num_steps < _num_steps_max );
    ++_num_steps;
  }

  void
  num_steps_increment ( std::size_t num_delta_n )
  {
    _num_steps += num_delta_n;
    DEBUG_ASSERT ( _num_steps <= num_steps_max () );
  }

  void
  set_num_steps ( std::size_t num_n )
  {
    DEBUG_ASSERT ( num_n <= num_steps_max () );
    _num_steps = num_n;
  }

  static std::size_t
  num_steps_max ()
  {
    return _num_steps_max;
  }

  std::size_t
  num_steps_free () const
  {
    return ( num_steps_max () - num_steps () );
  }

  bool
  is_empty () const
  {
    return ( num_steps () == 0 );
  }

  bool
  is_full () const
  {
    return ( num_steps () == num_steps_max () );
  }

  T &
  step ( std::size_t index_n )
  {
    return _steps[ index_n ];
  }

  const T &
  step ( std::size_t index_n ) const
  {
    return _steps[ index_n ];
  }

  T &
  step_end ()
  {
    return _steps[ _num_steps ];
  }

  T *
  begin ()
  {
    return &_steps[ 0 ];
  }

  const T *
  begin () const
  {
    return &_steps[ 0 ];
  }

  T *
  end ()
  {
    return &_steps[ _num_steps ];
  }

  const T *
  end () const
  {
    return &_steps[ _num_steps ];
  }

  T &
  back ()
  {
    return _steps[ ( _num_steps - 1 ) ];
  }

  const T &
  back () const
  {
    return _steps[ ( _num_steps - 1 ) ];
  }

  void
  push_back ( const T & step_n )
  {
    DEBUG_ASSERT ( !is_full () );
    _steps[ num_steps () ] = step_n;
    ++_num_steps;
  }

  private:
  // -- Attributes
  /// @brief Number of steps
  std::size_t _num_steps = 0;
  /// @brief Steps array
  std::array< T, _num_steps_max > _steps;
};

/// @brief Cargo BStep
///
template < std::size_t DIM >
class BSteps : public Steps< BStep< DIM > >
{
  public:
  BSteps ( Cargo_Pool * pool_n = nullptr )
  : Steps< BStep< DIM > > ( snc::mpath_feed::Cargo_Type::BSTEPS, pool_n )
  {
  }
};

/// @brief Cargo CStep
///
template < std::size_t DIM >
class CSteps : public Steps< CStep< DIM > >
{
  public:
  CSteps ( Cargo_Pool * pool_n = nullptr )
  : Steps< CStep< DIM > > ( snc::mpath_feed::Cargo_Type::CSTEPS, pool_n )
  {
  }
};

} // namespace snc::mpath_feed::cargo
