/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_feed/cargo.hpp>
#include <cstdint>

namespace snc::mpath_feed::cargo
{

template < std::size_t DIM >
class Element_D : public snc::mpath_feed::Cargo
{
  public:
  // -- Types
  static const Cargo_Type class_cargo_type = Cargo_Type::ELEMENT;

  // -- Construction

  Element_D ( Cargo_Pool * pool_n = 0 )
  : Cargo ( class_cargo_type, pool_n )
  {
  }

  void
  reset ()
  {
    _ebuffer.reset ();
  }

  snc::mpath::Element_Buffer_D< DIM > &
  ebuffer ()
  {
    return _ebuffer;
  }

  const snc::mpath::Element_Buffer_D< DIM > &
  ebuffer () const
  {
    return _ebuffer;
  }

  snc::mpath::Element &
  elem ()
  {
    return ebuffer ().elem ();
  }

  const snc::mpath::Element &
  elem () const
  {
    return ebuffer ().elem ();
  }

  std::uint_fast32_t
  elem_type () const
  {
    return elem ().elem_type ();
  }

  private:
  // -- Attributes
  /// @brief Machine path element
  snc::mpath::Element_Buffer_D< DIM > _ebuffer;
};

// -- Types

using Element_D1 = Element_D< 1 >;
using Element_D2 = Element_D< 2 >;
using Element_D3 = Element_D< 3 >;
using Element_D4 = Element_D< 4 >;
using Element_D5 = Element_D< 5 >;
using Element_D6 = Element_D< 6 >;

} // namespace snc::mpath_feed::cargo
