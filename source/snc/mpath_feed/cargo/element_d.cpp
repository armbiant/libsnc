/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "element_d.hpp"

namespace snc::mpath_feed::cargo
{

// -- Instantiation

template class Element_D< 1 >;
template class Element_D< 2 >;
template class Element_D< 3 >;
template class Element_D< 4 >;
template class Element_D< 5 >;
template class Element_D< 6 >;

} // namespace snc::mpath_feed::cargo
