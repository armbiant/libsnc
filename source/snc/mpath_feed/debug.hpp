/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

#define MPATH_WALKER_DEBUG_TO_FILE_CARGO 0
#define MPATH_WALKER_DEBUG_TO_FILE_SPEEDER_STEPS 0
#define MPATH_WALKER_DEBUG_MUXER_STEPS 0
#define MPATH_WALKER_DEBUG_MUXER_USECS 0

// -- Forward declaration
namespace snc::utility
{
class Debug_File;
}

namespace snc::mpath_feed
{

// -- Forward declaration
class Cargo;
template < std::size_t DIM >
class Step;

template < std::size_t DIM >
void
debug_write_cargo ( snc::utility::Debug_File & debug_file_n,
                    const Cargo & tile_n );

} // namespace snc::mpath_feed
