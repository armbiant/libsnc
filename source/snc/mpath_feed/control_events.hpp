/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/mpath_feed/job.hpp>
#include <cstdint>

namespace snc::mpath_feed::control_event::in
{

struct Type
{
  static constexpr std::uint_fast32_t JOB_ABORT_DONE = 0;
};

/// @brief Job abort done signal
///
class Job_Abort_Done : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::JOB_ABORT_DONE;

  Job_Abort_Done ()
  : sev::event::Event ( etype )
  {
  }
};

} // namespace snc::mpath_feed::control_event::in

namespace snc::mpath_feed::control_event::out
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
  static constexpr std::uint_fast32_t JOB_ABORT = 1;
};

/// @brief New job request
///
class Job : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::JOB;

  Job ()
  : sev::event::Event ( etype )
  {
  }

  void
  reset ()
  {
    sev::event::Event::reset ();
    _job.reset ();
  }

  Job_Handle
  job () const
  {
    return _job;
  }

  void
  set_job ( Job_Handle job_n )
  {
    _job = job_n;
  }

  private:
  Job_Handle _job;
};

/// @brief Job abort signal
///
class Job_Abort : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::JOB_ABORT;

  Job_Abort ()
  : sev::event::Event ( etype )
  {
  }
};

} // namespace snc::mpath_feed::control_event::out
