/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/condition.hpp>
#include <sev/event/queue/connection_in.hpp>
#include <sev/event/queue/connection_out.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/thread.hpp>
#include <snc/mpath_feed/cargos.hpp>
#include <snc/mpath_feed/events.hpp>
#include <snc/mpath_feed/station_constructor.hpp>

namespace snc::mpath_feed
{

/// @brief Cargo processing station thread
///
class Station_Processor
{
  public:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t NONE = 0;
    static constexpr std::uint_fast32_t CARGO_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t CARGO_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t CONTROL_IN = ( 1 << 2 );
    static constexpr std::uint_fast32_t CONTROL_OUT = ( 1 << 3 );
    static constexpr std::uint_fast32_t STATION = ( 1 << 4 );
    static constexpr std::uint_fast32_t LOOP_AGAIN = ( 1 << 5 );
  };

  struct Station_Done
  {
    static constexpr std::uint8_t DONE = ( 1 << 0 );
    static constexpr std::uint8_t DONE_ACKED = ( 1 << 1 );
  };

  struct Abort_State
  {
    static constexpr std::uint8_t ABORT = ( 1 << 0 );
    static constexpr std::uint8_t ABORT_ACKED = ( 1 << 1 );
  };

  // -- Construction

  Station_Processor ( const sev::logt::Reference & log_parent_n,
                      sev::thread::Tracker * thread_tracker_n,
                      const Station_Contructor_Handle & station_constructor_n,
                      sev::event::queue_io::Link_2 front_link_n );

  ~Station_Processor ();

  // -- Interface

  void
  set_source_queue ( const sev::event::queue::Reference & queue_n );

  void
  set_sink_queue ( const sev::event::queue::Reference & queue_n );

  void
  start ( const Job_Handle & job_n );

  void
  join ();

  /// @brief Thread main loop method
  void
  operator() ();

  private:
  // -- Utility

  void
  thread_run ();

  bool
  process_shut_down ();

  void
  process_station ();

  bool
  station_front_feed ();

  void
  station_back_read ();

  void
  process_control_events ();

  void
  control_signal_send ( event::Signal_Type signal_n );

  void
  request_processing ();

  private:
  // -- Attributes
  sev::logt::Context _log;

  sev::mem::Flags_Fast8 _abort_state;
  sev::mem::Flags_Fast8 _station_done;

  std::unique_ptr< Station > _station;
  Job_Handle _job;

  // -- Event registers
  sev::mem::Flags_Fast32 _loop_events;
  sev::event::bit_accus_async::Condition _event_waiter;

  // -- Cargo queues connections
  sev::event::queue::Connection_In _source_connection;
  sev::event::queue::Connection_Out _sink_connection;

  // -- Control thread events
  sev::event::Tracker_Pools< event::Signal > _epools;
  // -- Control thread connection
  sev::event::queue_io::Connection_2 _control_connection;

  /// @brief Constructs an destructs the station
  Station_Contructor_Handle _station_constructor;

  sev::thread::Thread _thread;
};

} // namespace snc::mpath_feed
