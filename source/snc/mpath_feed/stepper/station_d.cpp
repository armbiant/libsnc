/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_d.hpp"
#include <snc/mpath/elem/type_test.hpp>
#include <snc/mpath_feed/job_d.hpp>

namespace snc::mpath_feed::stepper
{

template < std::size_t DIM >
Station_D< DIM >::Station_D (
    const Station_Context & context_n,
    const sev::lag::Vector< double, DIM > & step_lengths_n )
: Station ( context_n )
, _step_lengths ( step_lengths_n )
{
  // Memory limits
  _cargo_pool.allocate_bsteps ( snc::mpath_feed::Station::num_cargos_max );
}

template < std::size_t DIM >
Station_D< DIM >::~Station_D ()
{
}

template < std::size_t DIM >
void
Station_D< DIM >::open ( const Job_Handle & job_n,
                         std::function< void () > const & notifier_n )
{
  if ( _is_open ) {
    return;
  }

  _is_open = true;
  _is_aborting = false;
  _stream_end_reached = false;
  _pmode = PMode::NONE;
  _plevel = 0;

  _cargo_pool.set_notifier ( notifier_n );

  auto & job_dim = static_cast< const Job_D< DIM > & > ( *job_n );
  _curve_stepper.begin_curve ( _step_lengths, job_dim.begin_position () );
}

template < std::size_t DIM >
void
Station_D< DIM >::abort ()
{
  if ( !_is_open ) {
    return;
  }
  if ( _is_aborting ) {
    return;
  }

  _is_aborting = true;
  _stream_end_reached = true;

  // Release front cargo
  front_release ();
  _pmode = PMode::NONE;
  _plevel = 0;

  if ( _cargo_back_bsteps != nullptr ) {
    _cargo_pool.cargo_release ( _cargo_back_bsteps );
    _cargo_back_bsteps = nullptr;
  }
  _curve_stepper.reset ();
}

template < std::size_t DIM >
void
Station_D< DIM >::close ()
{
  if ( !_is_open ) {
    return;
  }

  _is_open = false;
  _is_aborting = false;
  // Clear process mode
  _pmode = PMode::NONE;
  _plevel = 0;
  DEBUG_ASSERT ( _cargo_front == nullptr );
  DEBUG_ASSERT ( _cargo_back_bsteps == nullptr );
  // Reset curve stepper
  _curve_stepper.reset ();

  _cargo_pool.clear_notifier ();
}

template < std::size_t DIM >
snc::mpath_feed::Station::Token
Station_D< DIM >::read_next ()
{
  if ( !_is_open ) {
    return Token::NONE;
  }

  Token res = Token::NONE;
  while ( true ) {
    if ( _cargo_front == nullptr ) {
      if ( _stream_end_reached ) {
        if ( _cargo_pool.all_home () ) {
          res = Token::DONE;
        } else {
          res = Token::NONE;
        }
      } else {
        res = Token::FRONT_FEED;
      }
    } else {
      // Evaluate processing mode
      if ( read_next_pmode ( res ) ) {
        continue;
      }
    }

    // Leave loop by default
    break;
  }
  return res;
}

template < std::size_t DIM >
inline bool
Station_D< DIM >::read_next_pmode ( Station::Token & res_n )
{
  switch ( _pmode ) {

  case PMode::STEP:
    if ( _plevel == (uint32_t)Step_Level::CALC_STEPS ) {
      // Curve stepping level
      curve_step ();
      // Analyze stepping result
      if ( _cargo_back_bsteps != nullptr ) {
        // A step cargo item was acquired
        if ( _cargo_back_bsteps->is_full () ) {
          // Cargo tile is full.
          // Switch to step forwarding level
          _plevel = (uint32_t)Step_Level::BACK_READ_STEPS;
        } else {
          // Step cargo is not full. Wait for another curve element.
          // This curve element is done. Switch to front release
          // level.
          front_release ();
          // Return
          return true;
        }
      } else {
        // Out of step cargo
        res_n = Token::NONE;
        break;
      }
    }
    switch ( (Step_Level)_plevel ) {
    case Step_Level::BACK_READ_STEPS:
      // Step forwarding
      res_n = Token::BACK_READ;
      break;
    default:
      DEBUG_ASSERT ( false );
    }
    break;

  case PMode::FORWARD:
    if ( _plevel == (uint32_t)Forward_Level::EVAL ) {
      // Switch to back read level by default
      _plevel = (uint32_t)Forward_Level::BACK_READ_ELEM;
      // Analyze step cargo
      if ( _cargo_back_bsteps != nullptr ) {
        // A step cargo item is still around
        if ( _cargo_back_bsteps->is_empty () ) {
          // Cargo tile is empty. Release it.
          _cargo_pool.cargo_release ( _cargo_back_bsteps );
          _cargo_back_bsteps = nullptr;
        } else {
          // Cargo tile is not empty. Forward remaining steps.
          // Switch to forward steps level
          _plevel = (uint32_t)Forward_Level::BACK_READ_STEPS;
        }
      }
    }
    switch ( (Forward_Level)_plevel ) {
    case Forward_Level::BACK_READ_STEPS:
    case Forward_Level::BACK_READ_ELEM:
      // Step forwarding
      res_n = Token::BACK_READ;
      break;
    default:
      DEBUG_ASSERT ( false );
    }
    break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }

  return false;
}

template < std::size_t DIM >
void
Station_D< DIM >::front_release ()
{
  if ( _cargo_front != nullptr ) {
    _cargo_front->release ();
    _cargo_front = nullptr;
    _pmode = PMode::NONE;
    _plevel = 0;
  }
}

template < std::size_t DIM >
bool
Station_D< DIM >::front_feed ( Cargo * cargo_n )
{
  if ( _is_open && !_is_aborting ) {
    if ( cargo_n != nullptr ) {
      DEBUG_ASSERT ( _cargo_front == nullptr );
      if ( _cargo_front == nullptr ) {
        _cargo_front = cargo_n;
        front_feed_eval ();
        return true;
      }
    }
  }
  return false;
}

template < std::size_t DIM >
inline void
Station_D< DIM >::front_feed_eval ()
{
  // Analyze cargo type
  switch ( _cargo_front->cargo_type () ) {
  case Cargo_Type::ELEMENT:
    front_feed_elem ();
    break;
  default:
    // Release immediately
    front_release ();
    break;
  }
}

template < std::size_t DIM >
inline void
Station_D< DIM >::front_feed_elem ()
{
  snc::mpath::Element & elem (
      static_cast< cargo::Element_D< DIM > * > ( _cargo_front )->elem () );

  if ( snc::mpath::elem::is_curve< DIM > ( elem ) ) {
    // Curve element
    DEBUG_ASSERT ( _curve_stepper.token () ==
                   Curve_Stepper_D< DIM >::Token::FEED_CURVE );
    // Feed curve stepper
    if ( _curve_stepper.token () ==
         Curve_Stepper_D< DIM >::Token::FEED_CURVE ) {
      typedef const snc::mpath::elem::Curve_D< DIM > & ETYPE;
      ETYPE ecurve ( static_cast< ETYPE > ( elem ) );
      _curve_stepper.curve_segment_feed ( ecurve );
    }
    // Update mode and level
    _pmode = PMode::STEP;
    _plevel = (uint32_t)Step_Level::CALC_STEPS;
  } else {
    // Non curve element
    switch ( elem.elem_type () ) {
    case snc::mpath::elem::Type::STREAM_END:
      _stream_end_reached = true;
      log ().cat ( sev::logt::FL_DEBUG_0, "End of stream element." );
      break;
    default:
      break;
    }
    // Update mode and level
    _pmode = PMode::FORWARD;
    _plevel = (uint32_t)Forward_Level::EVAL;
  }
}

template < std::size_t DIM >
Cargo *
Station_D< DIM >::back_read ()
{
  Cargo * res = nullptr;
  switch ( _pmode ) {

  case PMode::STEP:
    switch ( (Step_Level)_plevel ) {
    case Step_Level::BACK_READ_STEPS:
      // Step reading
      if ( back_read_forward_steps ( res ) ) {
        // Switch to step generation level
        _plevel = (uint32_t)Step_Level::CALC_STEPS;
      }
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  case PMode::FORWARD:
    switch ( (Forward_Level)_plevel ) {
    case Forward_Level::BACK_READ_STEPS:
      // Step reading
      if ( back_read_forward_steps ( res ) ) {
        _plevel = (uint32_t)Forward_Level::BACK_READ_ELEM;
      }
      break;
    case Forward_Level::BACK_READ_ELEM:
      // Element reading
      back_read_forward_elem ( res );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  default:
    break;
  }
  return res;
}

template < std::size_t DIM >
void
Station_D< DIM >::back_read_forward_elem ( Cargo *& res_n )
{
  // Forward front element
  if ( _cargo_front != nullptr ) {
    res_n = _cargo_front;
    _cargo_front = nullptr;
    // Clear processing state
    _pmode = PMode::NONE;
    _plevel = 0;
  }
}

template < std::size_t DIM >
bool
Station_D< DIM >::back_read_forward_steps ( Cargo *& res_n )
{
  // Back read steps level
  if ( _cargo_back_bsteps != nullptr ) {
    res_n = _cargo_back_bsteps;
    _cargo_back_bsteps = nullptr;
    return true;
  }
  return false;
}

template < std::size_t DIM >
void
Station_D< DIM >::curve_step ()
{
  // Acquire a new step tile on demand
  if ( _cargo_back_bsteps == nullptr ) {
    _cargo_back_bsteps = _cargo_pool.acquire_bsteps ();
    if ( _cargo_back_bsteps != nullptr ) {
      // Ensure clean state
      _cargo_back_bsteps->reset ();
    }
  }
  if ( _cargo_back_bsteps != nullptr ) {
    // -- Acquire steps
    const auto num_steps ( _curve_stepper.acquire_steps (
        &_cargo_back_bsteps->step_end (),
        _cargo_back_bsteps->num_steps_free () ) );

    _cargo_back_bsteps->num_steps_increment ( num_steps );
  }
}

// -- Instantiation

template class Station_D< 1 >;
template class Station_D< 2 >;
template class Station_D< 3 >;
template class Station_D< 4 >;
template class Station_D< 5 >;
template class Station_D< 6 >;

} // namespace snc::mpath_feed::stepper
