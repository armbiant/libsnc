/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "curve_stepper_d.hpp"
#include <sev/assert.hpp>
#include <algorithm>

namespace snc::mpath_feed::stepper
{

template < std::size_t DIM >
Curve_Stepper_D< DIM >::Curve_Stepper_D ()
: _token ( Token::NONE )
{
  reset ();
}

template < std::size_t DIM >
Curve_Stepper_D< DIM >::~Curve_Stepper_D ()
{
}

template < std::size_t DIM >
void
Curve_Stepper_D< DIM >::reset ()
{
  _token = Token::NONE;
  _tval_latest = 0.0;
}

template < std::size_t DIM >
void
Curve_Stepper_D< DIM >::begin_curve (
    const sev::lag::Vector< double, DIM > & step_length_n,
    const sev::lag::Vector< double, DIM > & current_position_n )
{
  reset ();

  // Axes step lengths
  _axis_step_length = step_length_n;
  _begin_pos = current_position_n;

  // calculate the current grid position
  calc_grid_pos ( _latest_grid_pos, current_position_n );

  // Step search length hint
  _tval_delta_length = *std::min_element ( _axis_step_length.begin (),
                                           _axis_step_length.end () );

  // Update token
  _token = Token::FEED_CURVE;
}

template < std::size_t DIM >
void
Curve_Stepper_D< DIM >::curve_segment_feed (
    const snc::mpath::elem::Curve_D< DIM > & curve_n )
{
  DEBUG_ASSERT ( _token == Token::FEED_CURVE );

  // Load curve evaluator
  _eval_buffer.new_from_curve ( _begin_pos, curve_n );

  // Approximate tval delta
  const double lapprox ( _eval_buffer.evaluator ().length_approx () );
  DEBUG_ASSERT ( lapprox >= 0.0 );
  if ( _tval_delta_length < lapprox ) {
    _tval_delta = ( _tval_delta_length / lapprox );
  } else {
    _tval_delta = 1.0;
  }
  _tval_latest = 0.0;

  // Update token
  _token = Token::READ_STEPS;
}

template < std::size_t DIM >
inline void
Curve_Stepper_D< DIM >::calc_grid_pos (
    sev::lag::Vector< double, DIM > & grid_pos_n,
    const sev::lag::Vector< double, DIM > & pos_n )
{
  // Calculate step position from float position
  grid_pos_n = pos_n;
  grid_pos_n /= _axis_step_length;
  grid_pos_n += 0.5;
  for ( double & item : grid_pos_n ) {
    item = std::floor ( item );
  }
}

template < std::size_t DIM >
inline void
Curve_Stepper_D< DIM >::calc_grid_step (
    VGrid_Step & step_n, const sev::lag::Vector< double, DIM > & grid_pos_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    DEBUG_ASSERT ( std::abs ( grid_pos_n[ ii ] - _latest_grid_pos[ ii ] ) < 2 );
    step_n[ ii ] = int8_t ( grid_pos_n[ ii ] - _latest_grid_pos[ ii ] );
  }
}

template < std::size_t DIM >
inline bool
Curve_Stepper_D< DIM >::test_tval_grid_step ( double tval_n )
{
  _eval_buffer.evaluator ().calc_pos ( _current_pos, tval_n );

  calc_grid_pos ( _current_grid_pos, _current_pos );

  const double dlimit ( 1.0e-6 );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( std::abs ( _current_grid_pos[ ii ] - _latest_grid_pos[ ii ] ) >
         dlimit ) {
      return true;
    }
  }
  return false;
}

template < std::size_t DIM >
std::size_t
Curve_Stepper_D< DIM >::acquire_steps ( BStep< DIM > * steps_n,
                                        std::size_t num_max_n )
{
  std::size_t res ( 0 );
  if ( _token == Token::READ_STEPS ) {
    while ( res != num_max_n ) {
      if ( acquire_step ( *steps_n ) ) {
        ++steps_n;
        ++res;
      } else {
        // End of curve reached
        _eval_buffer.evaluator ().calc_end_pos ( _begin_pos );
        // Update token
        _token = Token::FEED_CURVE;
        break;
      }
    }
  }
  return res;
}

template < std::size_t DIM >
inline bool
Curve_Stepper_D< DIM >::acquire_step ( BStep< DIM > & step_n )
{
  double tval_near ( _tval_latest );
  double tval_far ( _tval_latest );

  // -- Go forward until over requested distance
  {
    const double tval_max ( 1.0 );
    do {
      // Calculate next tval
      {
        double tval_next ( tval_far );
        tval_next += _tval_delta;
        if ( tval_next > tval_max ) {
          // Check if we were at the end already
          if ( tval_far != tval_max ) {
            // Don't go beyond maximum value
            tval_next = tval_max;
          } else {
            // End of curve reached
            return false;
          }
        }
        tval_far = tval_next;
      }

      // Check if there was a grid step
      if ( test_tval_grid_step ( tval_far ) ) {
        break;
      }
    } while ( true );
  }
  // -- Bisect tval_near and tval_far
  for ( unsigned int ii = 0; ii != num_bisect; ++ii ) {
    // Calculate middle tval
    double tval_cur ( tval_far - tval_near );
    tval_cur /= 2.0;
    tval_cur += tval_near;

    // Check if there was a grid step
    if ( test_tval_grid_step ( tval_cur ) ) {
      tval_far = tval_cur;
    } else {
      tval_near = tval_cur;
    }
  }

  // -- Compile step using tval_far
  {
    _eval_buffer.evaluator ().calc_pos ( step_n.pos, tval_far );
    _eval_buffer.evaluator ().calc_tangent ( step_n.tangent, tval_far );
    calc_grid_pos ( _current_grid_pos, step_n.pos );
    calc_grid_step ( step_n.grid_step, _current_grid_pos );
  }
  DEBUG_ASSERT ( sev::lag::magnitude ( step_n.tangent ) > ( 1.0 - 1.0e-6 ) );
  DEBUG_ASSERT ( sev::lag::magnitude ( step_n.tangent ) < ( 1.0 + 1.0e-6 ) );

  // -- Update state
  _tval_latest = tval_far;
  _latest_grid_pos = _current_grid_pos;

  return true;
}

// -- Instantiation

template class Curve_Stepper_D< 1 >;
template class Curve_Stepper_D< 2 >;
template class Curve_Stepper_D< 3 >;
template class Curve_Stepper_D< 4 >;
template class Curve_Stepper_D< 5 >;
template class Curve_Stepper_D< 6 >;

} // namespace snc::mpath_feed::stepper
