/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath_feed/station_constructor.hpp>

namespace snc::mpath_feed::stepper
{

/// @brief Specialized station constructor
///
template < std::size_t DIM >
class Station_Constructor_D : public Station_Constructor
{
  // Public methods
  public:
  Station_Constructor_D ();

  const sev::lag::Vector< double, DIM > &
  step_lengths () const
  {
    return _step_lengths;
  }

  void
  set_step_lengths ( sev::lag::Vector< double, DIM > & length_n );

  // --- Virtual interface

  std::unique_ptr< Station >
  make ( const Station_Context & context_n ) override;

  private:
  // -- Attributes
  sev::lag::Vector< double, DIM > _step_lengths;
};

} // namespace snc::mpath_feed::stepper
