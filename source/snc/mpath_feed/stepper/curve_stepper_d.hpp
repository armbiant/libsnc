/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath/elem/curve.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_feed/step.hpp>

namespace snc::mpath_feed::stepper
{

template < std::size_t DIM >
class Curve_Stepper_D
{
  public:
  // -- Types

  using VGrid_Step = std::array< std::int8_t, DIM >;
  static const unsigned int num_bisect = 12;

  enum class Token
  {
    NONE,
    FEED_CURVE,
    READ_STEPS
  };

  // -- Construction and setup

  Curve_Stepper_D ();

  ~Curve_Stepper_D ();

  void
  reset ();

  // -- Interface

  void
  begin_curve ( const sev::lag::Vector< double, DIM > & step_length_n,
                const sev::lag::Vector< double, DIM > & current_position_n );

  Token
  token () const
  {
    return _token;
  }

  void
  curve_segment_feed ( const snc::mpath::elem::Curve_D< DIM > & curve_n );

  /// @return Number of steps generated
  std::size_t
  acquire_steps ( BStep< DIM > * steps_n, std::size_t num_max_n );

  private:
  // -- Utility

  bool
  acquire_step ( BStep< DIM > & step_n );

  void
  calc_grid_pos ( sev::lag::Vector< double, DIM > & grid_pos_n,
                  const sev::lag::Vector< double, DIM > & pos_n );

  bool
  test_tval_grid_step ( double tval );

  void
  calc_grid_step ( VGrid_Step & step_n,
                   const sev::lag::Vector< double, DIM > & grid_pos_n );

  private:
  // -- Attributes
  Token _token;

  double _tval_delta_length;
  double _tval_delta;
  double _tval_latest;
  sev::lag::Vector< double, DIM > _current_pos;
  sev::lag::Vector< double, DIM > _current_grid_pos;
  sev::lag::Vector< double, DIM > _latest_grid_pos;
  sev::lag::Vector< double, DIM > _axis_step_length;
  sev::lag::Vector< double, DIM > _begin_pos;

  snc::mpath::Curve_Evaluator_Buffer_D< DIM > _eval_buffer;
};

// -- Types

using Curve_Stepper_D1 = Curve_Stepper_D< 1 >;
using Curve_Stepper_D2 = Curve_Stepper_D< 2 >;
using Curve_Stepper_D3 = Curve_Stepper_D< 3 >;
using Curve_Stepper_D4 = Curve_Stepper_D< 4 >;
using Curve_Stepper_D5 = Curve_Stepper_D< 5 >;
using Curve_Stepper_D6 = Curve_Stepper_D< 6 >;

} // namespace snc::mpath_feed::stepper
