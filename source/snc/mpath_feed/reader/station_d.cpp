/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_d.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/mpath/element.hpp>
#include <snc/mpath_feed/debug.hpp>
#include <cmath>

namespace snc::mpath_feed::reader
{

template < std::size_t DIM >
Station_D< DIM >::Station_D ( const Station_Context & context_n )
: Station ( context_n )
{
  _cargo_pool.allocate_elem ( snc::mpath_feed::Station::num_cargos_max );
}

template < std::size_t DIM >
Station_D< DIM >::~Station_D ()
{
}

template < std::size_t DIM >
void
Station_D< DIM >::open ( const Job_Handle & job_n,
                         std::function< void () > const & notifier_n )
{
  if ( _is_open ) {
    return;
  }

  // stream reference
  _stream_read = job_n->stream ();
  // Init state
  _is_open = true;
  if ( _stream_read ) {
    _stream_state = Stream_State::OPEN;
  } else {
    _stream_state = Stream_State::NONE;
    log ().cat ( sev::logt::FL_DEBUG, "No stream given." );
  }

  _cargo_pool.set_notifier ( notifier_n );

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  _debug_file_cargo_out.open ( job_n->name () + "_reader_cargo_out.txt" );
#endif
}

template < std::size_t DIM >
void
Station_D< DIM >::abort ()
{
  if ( !_is_open ) {
    return;
  }

  if ( _stream_state != Stream_State::NONE ) {
    if ( _stream_state == Stream_State::GOOD ) {
      // Log
      log ().cat ( sev::logt::FL_DEBUG_0, "Stream aborted." );
      // Close stream
      _stream_read->close ();
    }
    _stream_state = Stream_State::NONE;
  }
}

template < std::size_t DIM >
void
Station_D< DIM >::close ()
{
  if ( !_is_open ) {
    return;
  }

  _is_open = false;
  _stream_read.reset ();
  _cargo_pool.clear_notifier ();

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  _debug_file_cargo_out.close ();
#endif
}

template < std::size_t DIM >
Station::Token
Station_D< DIM >::read_next ()
{
  if ( !_is_open ) {
    return Token::NONE;
  }

  switch ( _stream_state ) {
  case Stream_State::NONE:
    if ( _cargo_pool.all_home () ) {
      return Token::DONE;
    }
    return Token::NONE;

  case Stream_State::OPEN:
  case Stream_State::GOOD:
    if ( _cargo_pool.num_elem () != 0 ) {
      return Token::BACK_READ;
    }
    return Token::NONE;
  }

  return Token::NONE;
}

template < std::size_t DIM >
bool
Station_D< DIM >::front_feed ( Cargo * cargo_n )
{
  DEBUG_ASSERT ( false );
  (void)cargo_n;
  return false;
}

template < std::size_t DIM >
Cargo *
Station_D< DIM >::back_read ()
{
  Cargo * res = nullptr;
  do {
    switch ( _stream_state ) {
    case Stream_State::NONE:
      break;

    case Stream_State::OPEN:
      if ( _stream_read->open () ) {
        _stream_state = Stream_State::GOOD;
        log ().cat ( sev::logt::FL_DEBUG_0, "Stream opened." );
      } else {
        _stream_state = Stream_State::NONE;
        log ().cat ( sev::logt::FL_DEBUG_0, "Stream open failed." );
      }
      break;

    case Stream_State::GOOD:

      if ( _stream_read->is_good () ) {
        // Try to read an element from the stream
        cargo::Element_D< DIM > * cargo_e = _cargo_pool.acquire_elem ();
        if ( cargo_e != nullptr ) {
          auto * elem = _stream_read->read ();
          if ( elem != nullptr ) {
            cargo_e->ebuffer ().copy ( *elem );
            res = cargo_e;
          } else {
            // Bad stream behavior fallback
            DEBUG_ASSERT ( elem != nullptr );
            _cargo_pool.cargo_release ( cargo_e );
          }
        }
      } else {
        // Close the stream
        // Log
        log ().cat ( sev::logt::FL_DEBUG_0,
                     "End of stream reached. Closing stream." );
        // Close stream
        _stream_read->close ();
        _stream_state = Stream_State::NONE;
      }
      break;
    }
  } while ( ( res == nullptr ) && ( _stream_state != Stream_State::NONE ) );

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  if ( res != nullptr ) {
    debug_write_cargo< DIM > ( _debug_file_cargo_out, *res );
    _debug_file_cargo_out.flush ();
  }
#endif

  return res;
}

// -- Instantiation

template class Station_D< 1 >;
template class Station_D< 2 >;
template class Station_D< 3 >;
template class Station_D< 4 >;
template class Station_D< 5 >;
template class Station_D< 6 >;

} // namespace snc::mpath_feed::reader
