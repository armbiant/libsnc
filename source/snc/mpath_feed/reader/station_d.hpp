/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_feed/cargo_pool_d.hpp>
#include <snc/mpath_feed/station.hpp>
#include <snc/mpath_stream/read.hpp>
#include <snc/utility/debug_file.hpp>
#include <memory>

namespace snc::mpath_feed::reader
{

/// @brief MPath stream reader station
///
template < std::size_t DIM >
class Station_D : public Station
{
  public:
  // -- Types

  enum class Stream_State : uint8_t
  {
    NONE,
    OPEN,
    GOOD
  };

  // -- Construction

  Station_D ( const Station_Context & context_n );

  ~Station_D ();

  // -- Abstract interface

  void
  open ( const Job_Handle & job_n,
         std::function< void () > const & notifier_n ) override;

  void
  abort () override;

  void
  close () override;

  Token
  read_next () override;

  bool
  front_feed ( Cargo * cargo_n ) override;

  Cargo *
  back_read () override;

  private:
  // -- Attributes
  // States
  bool _is_open = false;
  Stream_State _stream_state = Stream_State::NONE;
  std::shared_ptr< snc::mpath_stream::Read > _stream_read;
  snc::mpath_feed::Cargo_Pool_D< DIM > _cargo_pool;
  snc::utility::Debug_File _debug_file_cargo_out;
};

} // namespace snc::mpath_feed::reader
