/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cargo.hpp"
#include <snc/mpath_feed/cargo_pool.hpp>

namespace snc::mpath_feed
{

Cargo::Cargo ( Cargo_Type cargo_type_n, Cargo_Pool * pool_n )
: sev::event::Event ( 0 )
, _cargo_type ( cargo_type_n )
, _pool ( pool_n )
{
}

void
Cargo::release ()
{
  DEBUG_ASSERT ( _pool != nullptr );
  _pool->cargo_release ( this );
}

} // namespace snc::mpath_feed
