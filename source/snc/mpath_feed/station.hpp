/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <sev/unicode/view.hpp>
#include <snc/mpath_feed/cargo.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed/station_context.hpp>
#include <cstdint>
#include <functional>
#include <string>

namespace snc::mpath_feed
{

/// @brief Cargo processing station abstract base class
///
class Station
{
  public:
  // -- Types

  /// @brief Processing request token
  enum class Token
  {
    NONE,
    FRONT_FEED,
    BACK_READ,
    DONE
  };

  /// @brief Maximum number of cargos to preallocate
  static const std::size_t num_cargos_max = 16;

  // -- Construction

  Station ( const Station_Context & context_n );

  virtual ~Station ();

  // -- Accessors

  const Station_Context &
  context () const
  {
    return _context;
  }

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  // -- Station name

  const std::string &
  name () const
  {
    return _name;
  }

  void
  set_name ( sev::unicode::View span_n );

  // -- Abstract interface

  virtual void
  open ( const Job_Handle & job_n,
         std::function< void () > const & notifier_n ) = 0;

  virtual void
  abort () = 0;

  virtual void
  close () = 0;

  virtual Token
  read_next () = 0;

  /// @return True if the item was accepted
  virtual bool
  front_feed ( Cargo * cargo_n ) = 0;

  virtual Cargo *
  back_read () = 0;

  private:
  // -- Attributes
  Station_Context _context;
  sev::logt::Context _log;
  std::string _name;
};

} // namespace snc::mpath_feed
