/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_ring_d.hpp"
#include <sev/math/numbers.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/math/vector.hpp>
#include <algorithm>

namespace snc::mpath_feed::speeder
{

template < std::size_t DIM >
Stepper_Ring_D< DIM >::Stepper_Ring_D (
    const sev::logt::Reference & log_parent_n )
: _curve_finish ( false )
, _curve_finish_done ( false )
, _steps_rbuffer_length_min ( 40 * 1024 )
, _step_speed_max ( 0.0 )
, _axis_stop_speed ( sev::lag::init::zero )
, _axis_step_length ( sev::lag::init::zero )
, _debug_file_steps ( 0 )
, _log ( log_parent_n, "Stepper_Ring_D" )
{
}

template < std::size_t DIM >
Stepper_Ring_D< DIM >::~Stepper_Ring_D ()
{
}

template < std::size_t DIM >
void
Stepper_Ring_D< DIM >::reset ()
{
  _curve_finish = false;
  _curve_finish_done = false;

  _steps_rbuffer.set_capacity ( 0 );
}

template < std::size_t DIM >
void
Stepper_Ring_D< DIM >::set_limits (
    const Axes_Limits_D< DIM > & limits_n,
    const sev::lag::Vector< double, DIM > & axis_step_length_n )
{
  _celerator.set_axes_limits ( limits_n );

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    _axis_stop_speed[ ii ] = limits_n[ ii ].speed_reversible;
  }
  _axis_step_length = axis_step_length_n;
}

template < std::size_t DIM >
void
Stepper_Ring_D< DIM >::begin (
    const sev::lag::Vector< double, DIM > & current_position_n,
    const sev::lag::Vector< double, DIM > & current_speed_n )
{
  _curve_finish = false;
  _curve_finish_done = false;

  _steps_rbuffer.set_capacity ( _steps_rbuffer_length_min +
                                cargo::CSteps< DIM >::num_steps_max () );

  _step_begin.pos = current_position_n;
  {
    _step_begin.speed = sev::lag::magnitude ( current_speed_n );
    if ( _step_begin.speed > 1.0e-6 ) {
      // Current speed is not zero
      _step_begin.tangent = current_speed_n / _step_begin.speed;
    } else {
      _step_begin.tangent.fill ( 0.0 );
    }
  }
}

template < std::size_t DIM >
void
Stepper_Ring_D< DIM >::enable_curve_finishing ()
{
  if ( !_steps_rbuffer.is_empty () ) {
    _curve_finish = true;
    _curve_finish_done = false;
    {
      CStep< DIM > & step_last ( _steps_rbuffer.back () );
      // Set last step speed to the stop speed and iterate again
      {
        double tangent_scale ( snc::fit_vector_into_maximum_limits_scale (
            step_last.tangent, _axis_stop_speed ) );
        // Maybe the requested speed was even slower
        // than the stoppable speed
        sev::math::assign_smaller< double > ( tangent_scale,
                                              step_last.speed_max );
        // To be sure it is larger than 0.0
        sev::math::assign_larger< double > ( tangent_scale, 0.001 );
        step_last.speed_max = tangent_scale;
      }
      if ( _debug_file_steps == 0 ) {
        iterate_latest_steps< false > ( 1 );
      } else {
        iterate_latest_steps< true > ( 1 );
      }
    }
  } else {
    _curve_finish = false;
    _curve_finish_done = true;
  }
}

template < std::size_t DIM >
bool
Stepper_Ring_D< DIM >::curve_steps_feed ( const BStep< DIM > * steps_n,
                                          size_t num_steps_n )
{
  if ( _steps_rbuffer.capacity_free () >= num_steps_n ) {
    // Disable curve finishing
    _curve_finish = false;
    _curve_finish_done = false;

    // Copy steps
    {
      const BStep< DIM > * it_cur ( steps_n );
      const BStep< DIM > * it_end ( steps_n + num_steps_n );
      for ( ; it_cur != it_end; ++it_cur ) {
        _steps_rbuffer.emplace_back_not_full ();
        CStep< DIM > & step ( _steps_rbuffer.back () );
        // Copy step
        step = *it_cur;
        // Use stop speed as minimum speed
        {
          double speed_max ( snc::fit_vector_into_maximum_limits_scale (
              step.tangent, _axis_stop_speed ) );
          sev::math::assign_larger ( speed_max, _step_speed_max );
          step.speed_max = speed_max;
        }
      }
    }
    // Iterate steps
    if ( _debug_file_steps == 0 ) {
      iterate_latest_steps< false > ( num_steps_n );
    } else {
      iterate_latest_steps< true > ( num_steps_n );
    }

    return true;
  }
  return false;
}

template < std::size_t DIM >
size_t
Stepper_Ring_D< DIM >::acquire_steps ( CStep< DIM > * steps_n,
                                       size_t num_max_n )
{
  {
    size_t limit ( _steps_rbuffer.size () );
    if ( !curve_finishing_enabled () ) {
      sev::math::subtract_minimum_zero ( limit, _steps_rbuffer_length_min );
    }
    sev::math::assign_smaller ( num_max_n, limit );
  }
  if ( num_max_n != 0 ) {
    for ( std::size_t ii = 0; ii != num_max_n; ++ii ) {
      _steps_rbuffer.pop_front_not_empty ( ( steps_n + ii ) );
    }
    // Keep a copy of the last step
    _step_begin = *( steps_n + ( num_max_n - 1 ) );
  }
  if ( curve_finishing_enabled () ) {
    if ( _steps_rbuffer.is_empty () ) {
      _curve_finish = false;
      _curve_finish_done = true;
      // Debug
      _log.cat ( sev::logt::FL_DEBUG_0, "Curve finishing complete" );
    }
  }
  return num_max_n;
}

template < std::size_t DIM >
template < bool STEPS_DEBUG >
void
Stepper_Ring_D< DIM >::iterate_latest_steps ( size_t num_steps_n )
{
  const size_t index_end ( _steps_rbuffer.size () );
  size_t index ( index_end - num_steps_n );
  for ( ; index != index_end; ++index ) {
    // Try to accelerate
    if ( !iterate_accelerate< STEPS_DEBUG > ( index ) ) {
      // Acceleration failed, decelerate instead
      iterate_speed_decelerate_bisect< STEPS_DEBUG > ( index );
      // iterate_decelerate < STEPS_DEBUG > ( index );
    }
  }
}

template < std::size_t DIM >
template < bool STEPS_DEBUG >
inline bool
Stepper_Ring_D< DIM >::iterate_accelerate ( size_t step_index_n )
{
  CStep< DIM > & step ( _steps_rbuffer.item ( step_index_n ) );
  // Load celerator
  if ( step_index_n == 0 ) {
    // First step
    _celerator.load_step ( _step_begin, step );
  } else {
    _celerator.load_step ( _steps_rbuffer.item ( step_index_n - 1 ), step );
  }
  if ( _celerator.loaded_step_calc_vmin_vmax () ) {
    if ( _celerator.vmin () <= step.speed_max ) {
      step.speed = sev::math::min ( step.speed_max, _celerator.vmax () );

      // Debug
      if ( STEPS_DEBUG ) {
        _debug_file_steps->write ( "--- Accelerated step " );
        _debug_file_steps->write ( step_index_n );
        _debug_file_steps->write ( "\n" );
        _celerator.print_debug ( *_debug_file_steps );
      }

      return true;
    }
  }
  return false;
}

///
/// Step 1: Bisect good and bad speeds
/// Step 2: Iterate backwards
///
template < std::size_t DIM >
template < bool STEPS_DEBUG >
void
Stepper_Ring_D< DIM >::iterate_decelerate ( size_t step_index_n )
{
  if ( step_index_n == 0 ) {
    // First step: Use minimum speed
    CStep< DIM > & step ( _steps_rbuffer.item ( 0 ) );
    step.speed = snc::fit_vector_into_maximum_limits_scale ( step.tangent,
                                                             _axis_stop_speed );
    return;
  }

  // -- Bisect until a valid speed was found
  _log.cat ( sev::logt::FL_DEBUG_0, "Bisection step_index=", step_index_n );

  {
    CStep< DIM > & step ( _steps_rbuffer.item ( step_index_n ) );
    const CStep< DIM > & step_prev ( _steps_rbuffer.item ( step_index_n - 1 ) );
    // Load celerator
    _celerator.load_step ( step_prev, step );

    double pspeed_good ( 0.0 );
    double pspeed_bad ( step_prev.speed );
    for ( std::size_t ii = 0; ii != reverse_speed_bisect_count; ++ii ) {
      // Use middle average speed for next test
      const double speed_middle ( ( pspeed_good + pspeed_bad ) / 2.0 );
      _celerator.load_step_prev_speed ( speed_middle );
      if ( _celerator.loaded_step_calc_vmin_vmax () &&
           ( _celerator.vmin () <= step.speed_max ) &&
           ( pspeed_good <= step.speed_max ) ) {
        // Step speed should only increase
        pspeed_good = speed_middle;
      } else {
        pspeed_bad = speed_middle;
      }
    }

    step.speed = pspeed_good;

    _log.cat ( sev::logt::FL_DEBUG_0,
               "Speed bisection: prev_speed=",
               step_prev.speed,
               " pspeed_bad=",
               pspeed_bad,
               " pspeed_good=",
               pspeed_good,
               " step_speed=",
               step.speed );
  }

  // -- Iterate backwards
  _log.cat ( sev::logt::FL_DEBUG_0, "Backward iteration" );
  {
    size_t index ( step_index_n );
    while ( index != 0 ) {
      const CStep< DIM > & step ( _steps_rbuffer.item ( index ) );
      CStep< DIM > & step_prev ( _steps_rbuffer.item ( index - 1 ) );
      _celerator.load_step_reverse ( step, step_prev );
      if ( _celerator.loaded_step_calc_vmin_vmax () ) {
        // Store maximum speed. The _celerator may be used meanwhile.
        const double vmax ( _celerator.vmax () );
        // Check if we catched up to the forward high speed
        if ( vmax >= step_prev.speed ) {
          // Catched up!
          break;
        } else {
          // We really should be below the previous step maximum speed
          // since we're catching up to a higher speed
          if ( _celerator.vmin () <= step_prev.speed_max ) {
            // If we are below just by a marginal amount
            // try forward decelerating the previous steps.
            //
            // This handles cases where the backward acceleration
            // fails to catch up to the forward speed because of
            // limited acceleration options. E.g in tight circles.
            if ( ( vmax / step_prev.speed ) > 0.99 ) {
              // Debug
              _log.cat (
                  sev::logt::FL_DEBUG_0, "Close ", ( vmax / step_prev.speed ) );
              /*
              if ( iterate_decelerate_forward ( index ) ) {
                // Catched up!
                break;
              }
               */
            }
            // We're still here so use the step an loop again
            step_prev.speed = sev::math::min ( step_prev.speed_max, vmax );
          } else {
            // Invalid calculated speed. Interrupt here.
            DEBUG_ASSERT ( false );
            break;
          }
        }
      } else {
        if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
          logo << "Reverse iteration failed:";
          logo << " step_index_n=" << step_index_n;
          logo << " index=" << index;
          logo << " step.speed=" << step.speed;
          logo << " step_prev.speed=" << step_prev.speed;
          logo << "\nPrev step pos " << _celerator.prev_step_pos ();
          logo << "\nPrev step tangent " << _celerator.prev_step_tangent ();
          logo << "\nPrev step tangent magnitude "
               << sev::lag::magnitude ( _celerator.prev_step_tangent () );
          logo << "\nPrev step speed " << _celerator.prev_step_speed ();
          logo << "\nStep pos " << _celerator.step_pos ();
          logo << "\nStep tangent " << _celerator.step_tangent ();
        }
        // Calculation failed. Interrupt here.
        DEBUG_ASSERT ( false );
      }
      --index;
    }
  }
}

template < std::size_t DIM >
bool
Stepper_Ring_D< DIM >::iterate_decelerate_forward ( size_t step_index_n )
{
  if ( step_index_n == 0 ) {
    DEBUG_ASSERT ( false );
    return false;
  }

  const size_t index_end ( step_index_n + 1 );
  size_t index_good ( step_index_n );

  // -- Lower index_good until deceleration succeeds
  {
    const size_t step_delta_max ( 1024 );
    size_t step_delta ( 1 );
    while ( true ) {
      // Probe index
      if ( iterate_decelerate_forward_probe ( index_good, index_end ) ) {
        // Success
        break;
      }

      // Lower index_good
      if ( index_good > step_delta ) {
        index_good -= step_delta;
        // Increase step delta until the upper limit
        if ( step_delta != step_delta_max ) {
          step_delta *= 2;
          sev::math::assign_smaller ( step_delta, step_delta_max );
        }
      } else {
        if ( index_good != 1 ) {
          index_good = 1;
        } else {
          // We could not slow down enough
          return false;
        }
      }
    }
    // Debug
    _log.cat ( sev::logt::FL_DEBUG_0,
               "Approach count ",
               ( step_index_n - index_good ) );
  }

  // -- Bisect to exact deceleration
  if ( index_good != step_index_n ) {
    size_t index_bad ( step_index_n );
    while ( true ) {
      // Span between good and bad step
      size_t index_middle ( ( index_good + index_bad ) / 2 );
      if ( index_middle == index_good ) {
        break;
      }
      if ( iterate_decelerate_forward_probe ( index_middle, index_end ) ) {
        index_good = index_middle;
      } else {
        index_bad = index_middle;
      }
    }
  }
  // Decelerate now for real
  iterate_decelerate_forward_real ( index_good, step_index_n );
  return true;
}

template < std::size_t DIM >
bool
Stepper_Ring_D< DIM >::iterate_decelerate_forward_probe ( size_t index_begin_n,
                                                          size_t index_end_n )
{
  DEBUG_ASSERT ( index_begin_n < index_end_n );
  double speed;
  const CStep< DIM > * step_prev ( &_steps_rbuffer.item ( index_begin_n - 1 ) );
  speed = step_prev->speed;
  for ( std::size_t iii = index_begin_n; iii != index_end_n; ++iii ) {
    const CStep< DIM > * step ( &_steps_rbuffer.item ( iii ) );
    _celerator.load_step ( step_prev->pos, step_prev->tangent, speed, *step );
    if ( _celerator.loaded_step_calc_vmin_vmax () ) {
      speed = _celerator.vmin ();
      // Debug
      _log.cat ( sev::logt::FL_DEBUG_0, "Probe: iii=", iii, " speed=", speed );
      step_prev = step;
    } else {
      // Debug
      if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Probe celerator failed: iii=" << iii << " ib=" << index_begin_n
             << " ie=" << index_end_n
             << " id=" << ( index_end_n - index_begin_n ) << " speed=" << speed;
      }
      return false;
    }
  }

  // Debug
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Probe: vp=" << step_prev->speed << " vc=" << speed
         << " vd=" << ( step_prev->speed - speed )
         << " id=" << ( index_end_n - index_begin_n );
  }

  return ( step_prev->speed >= speed );
}

template < std::size_t DIM >
void
Stepper_Ring_D< DIM >::iterate_decelerate_forward_real ( size_t index_begin_n,
                                                         size_t index_end_n )
{
  const CStep< DIM > * step_prev ( &_steps_rbuffer.item ( index_begin_n - 1 ) );
  for ( std::size_t iii = index_begin_n; iii != index_end_n; ++iii ) {
    CStep< DIM > * step ( &_steps_rbuffer.item ( iii ) );
    _celerator.load_step ( *step_prev, *step );
    if ( !_celerator.loaded_step_calc_vmin_vmax () ) {
      break;
    }
    if ( step->speed > _celerator.vmin () ) {
      step->speed = _celerator.vmin ();
    } else {
      break;
    }
    step_prev = step;
  }
}

template < std::size_t DIM >
template < bool STEPS_DEBUG >
void
Stepper_Ring_D< DIM >::iterate_speed_decelerate_bisect ( size_t step_index_n )
{
  bool all_good ( false );

  size_t step_index_good ( step_index_n );
  size_t step_index_bad ( step_index_n );
  // Move towards the buffer begin until
  // deceleration ends in a valid speed
  if ( step_index_good != 0 ) {

    size_t num ( 0 );

    size_t step_delta_max ( 4096 );
    size_t step_delta ( 4 );
    while ( true ) {
      ++num;
      if ( iterate_speed_decelerate_probe ( step_index_good ) ) {
        all_good = true;
        break;
      }
      if ( step_index_good == 0 ) {
        // Can't slow down enough :(
        break;
      }
      // This was a bad deceleration step
      step_index_bad = step_index_good;
      // Increase step delta until an upper limit
      if ( step_delta != step_delta_max ) {
        step_delta *= 2;
        sev::math::assign_smaller ( step_delta, step_delta_max );
      }
      if ( step_index_good > step_delta ) {
        step_index_good -= step_delta;
      } else {
        step_index_good = 0;
      }
    }
  }
  // Try deceleration in bisection style
  if ( all_good ) {
    while ( true ) {
      // Span between good and bad step
      size_t step_index_cur ( step_index_bad - step_index_good );
      step_index_cur /= 2;
      if ( step_index_cur == 0 ) {
        // Bisection target found, leave
        break;
      }
      step_index_cur += step_index_good;
      if ( iterate_speed_decelerate_probe ( step_index_cur ) ) {
        step_index_good = step_index_cur;
      } else {
        step_index_bad = step_index_cur;
      }
    }
  }
  // Decelerate now for real
  iterate_speed_decelerate_real< STEPS_DEBUG > ( step_index_good );

  if ( !all_good ) {
    // Deceleration failed as well
    _log.cat ( sev::logt::FL_DEBUG_0,
               "Could not decelerate fast enough: Hard cut to ",
               _steps_rbuffer.item ( step_index_n ).speed );
  }
}

template < std::size_t DIM >
bool
Stepper_Ring_D< DIM >::iterate_speed_decelerate_probe ( size_t index_begin_n )
{
  bool all_good ( true );

  // Load first step
  const CStep< DIM > * step ( &_steps_rbuffer.item ( index_begin_n ) );
  if ( index_begin_n == 0 ) {
    // First step
    _celerator.load_step ( _step_begin, *step );
  } else {
    _celerator.load_step ( _steps_rbuffer.item ( index_begin_n - 1 ), *step );
  }

  // Slow down over the following steps
  const size_t index_end ( _steps_rbuffer.size () );
  size_t index ( index_begin_n );
  do {
    double speed;
    {
      if ( _celerator.loaded_step_calc_vmin_vmax () ) {
        speed = _celerator.vmin ();
        if ( speed <= step->speed_max ) {
          {
            // Maximum speed to fit into reversible limits
            double speed_scale ( snc::fit_vector_into_maximum_limits_scale (
                _celerator.step_tangent (), _axis_stop_speed ) );
            // Increase unnecessary low speeds
            sev::math::assign_larger ( speed, speed_scale );
          }
          // Stay below requested speed
          sev::math::assign_smaller ( speed, step->speed_max );
        } else {
          all_good = false;
          break;
        }
      } else {
        all_good = false;
        break;
      }
    }
    ++index;
    if ( index == index_end ) {
      break;
    }
    // Load next step
    const CStep< DIM > * step_next ( &_steps_rbuffer.item ( index ) );
    _celerator.load_step ( step->pos, step->tangent, speed, *step_next );
    step = step_next;
  } while ( true );

  return all_good;
}

template < std::size_t DIM >
template < bool STEPS_DEBUG >
void
Stepper_Ring_D< DIM >::iterate_speed_decelerate_real ( size_t index_begin_n )
{
  // Load first step
  CStep< DIM > * step ( &_steps_rbuffer.item ( index_begin_n ) );
  if ( index_begin_n == 0 ) {
    // First step
    _celerator.load_step ( _step_begin, *step );
  } else {
    _celerator.load_step ( _steps_rbuffer.item ( index_begin_n - 1 ), *step );
  }

  // Slow down over the following steps
  const size_t index_end ( _steps_rbuffer.size () );
  size_t index ( index_begin_n );
  do {
    _celerator.loaded_step_calc_vmin_vmax ();
    {
      double speed ( _celerator.vmin () );
      // Maximum speed to fit into reversible limits
      {
        double speed_scale ( snc::fit_vector_into_maximum_limits_scale (
            _celerator.step_tangent (), _axis_stop_speed ) );
        // Increase unnecessary low speeds
        sev::math::assign_larger ( speed, speed_scale );
      }
      // Stay below requested speed
      if ( speed > step->speed_max ) {
        step->speed = step->speed_max;
      } else {
        step->speed = speed;
      }
    }

    // Debug
    if ( STEPS_DEBUG ) {
      _debug_file_steps->write ( "--- Decelerated step " );
      _debug_file_steps->write ( index );
      _debug_file_steps->write ( "\n" );
      _celerator.print_debug ( *_debug_file_steps );
    }

    ++index;
    if ( index == index_end ) {
      break;
    }
    // Load next step
    CStep< DIM > * step_next ( &_steps_rbuffer.item ( index ) );
    _celerator.load_step ( *step, *step_next );
    step = step_next;
  } while ( true );
}

// -- Instantiation

template class Stepper_Ring_D< 1 >;
template class Stepper_Ring_D< 2 >;
template class Stepper_Ring_D< 3 >;
template class Stepper_Ring_D< 4 >;
template class Stepper_Ring_D< 5 >;
template class Stepper_Ring_D< 6 >;

} // namespace snc::mpath_feed::speeder
