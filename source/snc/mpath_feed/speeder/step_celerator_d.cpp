/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "step_celerator_d.hpp"
#include <snc/device/statics/statics.hpp>
#include <snc/math/stepping.hpp>
#include <snc/math/vector.hpp>
#include <algorithm>
#include <iostream>

namespace snc::mpath_feed::speeder
{

template < std::size_t DIM >
Step_Celerator_D< DIM >::Step_Celerator_D ()
{
  static_assert ( DIM != 0, "Zero dimensions not allowed" );
}

template < std::size_t DIM >
void
Step_Celerator_D< DIM >::set_axes_limits (
    const Axes_Limits_D< DIM > & limits_n )
{
  _axes_limits = limits_n;

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    _axis_stop_speed[ ii ] = limits_n[ ii ].speed_reversible;
  }
  _axis_stop_speed_max =
      *std::max_element ( _axis_stop_speed.begin (), _axis_stop_speed.end () );
}

template < std::size_t DIM >
inline bool
Step_Celerator_D< DIM >::a_calc_speed_limits ( double & vmin_n,
                                               double & vmax_n,
                                               size_t aindex_n )
{
  // -- Calculate minimal and maximal achievable speeds
  const Axis_Limits & axis_lim ( _axes_limits[ aindex_n ] );
  const double speed_prev ( _prev_step_tangent[ aindex_n ] * _prev_step_speed );
  const double accel_len ( _step_pos[ aindex_n ] - _prev_step_pos[ aindex_n ] );
  if ( accel_len >= 0.0 ) {
    // Acceleration span is positive
    if ( speed_prev >= 0.0 ) {
      // Previous speed is positive as well. Direction match :)
      vmax_n = speed_accelerate_over_length (
          speed_prev, axis_lim.accel_max, accel_len );
      if ( speed_prev > axis_lim.speed_reversible ) {
        // Previous speed is not reversible
        vmin_n = speed_accelerate_over_length (
            speed_prev, -axis_lim.accel_max, accel_len );
      } else {
        // Previous speed is reversible.
        // Allow reversible speed at least in both directions
        sev::math::assign_larger ( vmax_n, axis_lim.speed_reversible );
        vmin_n = -axis_lim.speed_reversible;
      }

      // Limit speed to axis limit
      {
        const double limit ( axis_lim.speed_max );
        sev::math::assign_smaller ( vmin_n, limit );
        sev::math::assign_smaller ( vmax_n, limit );
      }
    } else {
      // Previous speed is negative. Direction missmatch!
      if ( speed_prev >= -axis_lim.speed_reversible ) {
        // Speed is reversible
        vmax_n = axis_lim.speed_reversible;
        vmin_n = -axis_lim.speed_reversible;
      } else {
        // Speed is too high to be reversible
        return false;
      }
    }
  } else {
    // Acceleration span is negative
    if ( speed_prev >= 0.0 ) {
      // Previous speed is positive. Direction missmatch!
      if ( speed_prev <= axis_lim.speed_reversible ) {
        // Speed is reversible
        vmax_n = axis_lim.speed_reversible;
        vmin_n = -axis_lim.speed_reversible;
      } else {
        // Speed is too high to be reversible
        return false;
      }
    } else {
      // Previous speed is negative as well. Direction match :)
      vmin_n = -speed_accelerate_over_length (
          speed_prev, -axis_lim.accel_max, accel_len );
      if ( speed_prev < -axis_lim.speed_reversible ) {
        // Previous speed is not reversible
        // This decelerates the absolute speed because ( accel_len < 0.0
        // )
        vmax_n = -speed_accelerate_over_length (
            speed_prev, axis_lim.accel_max, accel_len );
      } else {
        // Previous speed is reversible
        // Allow reversible speed at least in both directions
        vmax_n = axis_lim.speed_reversible;
        sev::math::assign_smaller ( vmin_n, -axis_lim.speed_reversible );
      }

      // Limit speed to axis limit
      {
        const double limit ( -axis_lim.speed_max );
        sev::math::assign_larger ( vmin_n, limit );
        sev::math::assign_larger ( vmax_n, limit );
      }
    }
  }

  DEBUG_ASSERT ( vmin_n <= vmax_n );

  return true;
}

/// @brief Calculate the tangent scales (aka speed) for an axis
///
/// Tangent scale calculation fails if the directions of minimum and maximum
/// speed
/// achievable from the previous step are both against the step tangent
/// direction.
/// In this case the speed of the previous step was too high to be reversible.
///
/// @return True if valid tangent scales were calculated
template < std::size_t DIM >
inline bool
Step_Celerator_D< DIM >::a_calc_tangent_scales ( double & tscale_min_n,
                                                 double & tscale_max_n,
                                                 size_t aindex_n )
{
  double vmin;
  double vmax;
  if ( !a_calc_speed_limits ( vmin, vmax, aindex_n ) ) {
    return false;
  }
  // -- Calculate tangent scale values
  {
    const double tang_min ( 1.0e-6 );
    double tang ( _step_tangent[ aindex_n ] );
    if ( tang >= 0.0 ) {
      // Tangent direction is positive
      // Avoid divion by zero or very small values
      sev::math::assign_larger ( tang, tang_min );
      if ( vmin >= 0.0 ) {
        // Minimum speed is positive - maximum speed must be as well
        tscale_min_n = ( vmin / tang );
        tscale_max_n = ( vmax / tang );
      } else {
        // Minimum speed is negative - against tangent direction
        if ( vmax >= 0.0 ) {
          tscale_min_n = 0.0;
          tscale_max_n = ( vmax / tang );
        } else {
          // Minimum and maximum speed are negative - against the
          // tangent direction There can't be a valid (non negative)
          // tangent scale
          return false;
        }
      }
    } else {
      // Tangent direction is negative
      // Avoid divion by zero or very small values
      sev::math::assign_smaller ( tang, -tang_min );
      if ( vmax >= 0.0 ) {
        // Maximum speed is positive - against tangent direction
        if ( vmin >= 0.0 ) {
          // Minimum and maximum speed are positive - against the
          // tangent direction There can't be a valid (non negative)
          // tangent scale
          return false;
        } else {
          // At least the minimum speed matches the tangent direction
          // abs(vmin) > abs(vmax) => tangent scales min/max swapped
          tscale_min_n = 0.0;
          tscale_max_n = ( vmin / tang );
        }
      } else {
        // Maximum speed is negative - minimum speed must be as well
        // abs(vmin) > abs(vmax) => tangent scales min/max swapped
        tscale_min_n = ( vmax / tang );
        tscale_max_n = ( vmin / tang );
      }
    }
  }

  DEBUG_ASSERT ( tscale_min_n <= tscale_max_n );

  return true;
}

template < std::size_t DIM >
inline bool
Step_Celerator_D< DIM >::a_calc_speeds ()
{
  // First axis
  if ( !a_calc_tangent_scales ( _vmin, _vmax, 0 ) ) {
    return false;
  }
  // Remaining axes
  for ( std::size_t ii = 1; ii != DIM; ++ii ) {
    double vmin;
    double vmax;
    if ( !a_calc_tangent_scales ( vmin, vmax, ii ) ) {
      return false;
    }
    // Crop the allowed speed range
    sev::math::assign_larger ( _vmin, vmin );
    sev::math::assign_smaller ( _vmax, vmax );
  }

  // Check if the speed range is valid
  // std::cout << "vmin " << _vmin << " vmax " << _vmax << "\n";
  return ( _vmin <= _vmax );
}

template < std::size_t DIM >
inline void
Step_Celerator_D< DIM >::b_calc_speed_limits ( double & vmin_n,
                                               double & vmax_n,
                                               double dtval_n,
                                               size_t aindex_n )
{
  const Axis_Limits & alimits ( _axes_limits[ aindex_n ] );

  vmin_n = -alimits.accel_max;
  vmax_n = alimits.accel_max;

  vmin_n *= dtval_n;
  vmax_n *= dtval_n;

  vmin_n += _prev_step_tangent[ aindex_n ] * _prev_step_speed;
  vmax_n += _prev_step_tangent[ aindex_n ] * _prev_step_speed;

  DEBUG_ASSERT ( vmin_n <= vmax_n );
}

/// @brief Calculate the tangent scales (aka speed) for an axis
///
/// Tangent scale calculation fails if the directions of minimum and maximum
/// speed
/// achievable from the previous step are both against the step tangent
/// direction.
/// In this case the speed of the previous step was too high to be reversible.
///
/// @return True if valid tangent scales were calculated
template < std::size_t DIM >
inline bool
Step_Celerator_D< DIM >::b_calc_tangent_scales ( double & tscale_min_n,
                                                 double & tscale_max_n,
                                                 double dtval_n,
                                                 size_t aindex_n )
{
  double vmin;
  double vmax;
  b_calc_speed_limits ( vmin, vmax, dtval_n, aindex_n );
  // -- Calculate tangent scale values
  {
    const double tang_min ( 1.0e-6 );
    double tang ( _step_tangent[ aindex_n ] );
    if ( tang >= 0.0 ) {
      // Tangent direction is positive
      // Avoid divion by zero or very small values
      sev::math::assign_larger ( tang, tang_min );
      if ( vmin >= 0.0 ) {
        // Minimum speed is positive - maximum speed must be as well
        tscale_min_n = ( vmin / tang );
        tscale_max_n = ( vmax / tang );
      } else {
        // Minimum speed is negative - against tangent direction
        if ( vmax >= 0.0 ) {
          tscale_min_n = 0.0;
          tscale_max_n = ( vmax / tang );
        } else {
          // Minimum and maximum speed are negative - against the
          // tangent direction There can't be a valid (non negative)
          // tangent scale
          std::cout << "axis " << aindex_n
                    << " tangent positive vmin vmax negative"
                    << "\n";
          std::cout << "vmin " << vmin << " vmax " << vmax << "\n";
          return false;
        }
      }
    } else {
      // Tangent direction is negative
      // Avoid divion by zero or very small values
      sev::math::assign_smaller ( tang, -tang_min );
      if ( vmax >= 0.0 ) {
        // Maximum speed is positive - against tangent direction
        if ( vmin >= 0.0 ) {
          // Minimum and maximum speed are positive - against the
          // tangent direction There can't be a valid (non negative)
          // tangent scale
          std::cout << "axis " << aindex_n
                    << " tangent negative vmin vmax positive"
                    << "\n";
          std::cout << "vmin " << vmin << " vmax " << vmax << "\n";
          return false;
        } else {
          // At least the minimum speed matches the tangent direction
          // abs(vmin) > abs(vmax) => tangent scales min/max swapped
          tscale_min_n = 0.0;
          tscale_max_n = ( vmin / tang );
        }
      } else {
        // Maximum speed is negative - minimum speed must be as well
        // abs(vmin) > abs(vmax) => tangent scales min/max swapped
        tscale_min_n = ( vmax / tang );
        tscale_max_n = ( vmin / tang );
      }
    }
  }

  DEBUG_ASSERT ( tscale_min_n <= tscale_max_n );

  return true;
}

template < std::size_t DIM >
inline bool
Step_Celerator_D< DIM >::b_calc_speeds ()
{
  double dtval;

  {
    // Distance vector with normalized version
    sev::lag::Vector< double, DIM > vdist_norm ( _step_pos - _prev_step_pos );
    const double dist = sev::lag::magnitude ( vdist_norm );
    vdist_norm /= dist;

    // Projected speed
    double pspeed = sev::lag::dot_prod ( _prev_step_tangent, vdist_norm );
    pspeed *= _prev_step_speed;
    // For small or negative values increase to minimum speed
    if ( pspeed <= _axis_stop_speed_max ) {
      // Accelerate along the distance vector
      const double accel ( snc::fit_vector_into_maximum_limits_scale (
          vdist_norm, _axis_stop_speed ) );
      double lvmin ( speed_accelerate_over_length ( 0.0, accel, dist ) );
      sev::math::assign_larger ( pspeed, lvmin );
    }
    dtval = ( dist / pspeed );
  }

  // First axis
  if ( !b_calc_tangent_scales ( _vmin, _vmax, dtval, 0 ) ) {
    return false;
  }
  // Remaining axes
  for ( std::size_t ii = 1; ii != DIM; ++ii ) {
    double vmin;
    double vmax;
    if ( !b_calc_tangent_scales ( vmin, vmax, dtval, ii ) ) {
      return false;
    }
    // Crop the allowed speed range
    sev::math::assign_larger ( _vmin, vmin );
    sev::math::assign_smaller ( _vmax, vmax );
  }

  // Check if the speed range is valid
  // std::cout << "vmin " << _vmin << " vmax " << _vmax << "\n";
  return ( _vmin <= _vmax );
}

template < std::size_t DIM >
bool
Step_Celerator_D< DIM >::loaded_step_calc_vmin_vmax ()
{
  return a_calc_speeds ();
}

template < std::size_t DIM >
void
Step_Celerator_D< DIM >::print_debug ( snc::utility::Debug_File & debug_file_n )
{
  debug_file_n.write ( "_prev_step_pos:   " );
  debug_file_n.write ( _prev_step_pos );
  debug_file_n.write ( "\n" );

  debug_file_n.write ( "_prev_step_speed:   " );
  debug_file_n.write ( _prev_step_speed );
  debug_file_n.write ( "\n" );

  debug_file_n.write ( "_step_pos:   " );
  debug_file_n.write ( _step_pos );
  debug_file_n.write ( "\n" );

  debug_file_n.write ( "_step_tangent:   " );
  debug_file_n.write ( _step_tangent );
  debug_file_n.write ( "\n" );

  debug_file_n.write ( "_vspeed_min:   " );
  debug_file_n.write ( _step_tangent * _vmin );
  debug_file_n.write ( "\n" );

  debug_file_n.write ( "_vspeed_max:   " );
  debug_file_n.write ( _step_tangent * _vmax );
  debug_file_n.write ( "\n" );

  for ( std::size_t ii = 0; ii != _speed_limits.size () / 2; ++ii ) {
    debug_file_n.write ( "_speed_limit[" );
    debug_file_n.write ( ii );
    debug_file_n.write ( "]:  " );
    debug_file_n.write ( _speed_limits[ ii * 2 ] );
    debug_file_n.write ( " : " );
    debug_file_n.write ( _speed_limits[ ii * 2 + 1 ] );
    debug_file_n.write ( "\n" );
  }

  for ( std::size_t ii = 0; ii != _speed_scales.size () / 2; ++ii ) {
    debug_file_n.write ( "_speed_scales[" );
    debug_file_n.write ( ii );
    debug_file_n.write ( "]:  " );
    debug_file_n.write ( _speed_scales[ ii * 2 ] );
    debug_file_n.write ( " : " );
    debug_file_n.write ( _speed_scales[ ii * 2 + 1 ] );
    debug_file_n.write ( "\n" );
  }
}

// -- Instantiation

template class Step_Celerator_D< 1 >;
template class Step_Celerator_D< 2 >;
template class Step_Celerator_D< 3 >;
template class Step_Celerator_D< 4 >;
template class Step_Celerator_D< 5 >;
template class Step_Celerator_D< 6 >;

} // namespace snc::mpath_feed::speeder
