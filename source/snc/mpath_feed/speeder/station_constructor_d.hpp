/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath_feed/axes_limits.hpp>
#include <snc/mpath_feed/station_constructor.hpp>

namespace snc::mpath_feed::speeder
{

/// @brief Specialized station constructor
///
template < std::size_t DIM >
class Station_Constructor_D : public snc::mpath_feed::Station_Constructor
{
  // Public methods
  public:
  Station_Constructor_D ();

  const Axes_Limits_D< DIM > &
  axes_limits () const
  {
    return _axes_limits;
  }

  Axes_Limits_D< DIM > &
  axes_limits_ref ()
  {
    return _axes_limits;
  }

  const sev::lag::Vector< double, DIM > &
  step_lengths () const
  {
    return _step_lengths;
  }

  void
  set_step_lengths ( const sev::lag::Vector< double, DIM > & length_n )
  {
    _step_lengths = length_n;
  }

  // --- Virtual interface

  std::unique_ptr< Station >
  make ( const Station_Context & context_n ) override;

  private:
  // -- Attributes
  Axes_Limits_D< DIM > _axes_limits;
  sev::lag::Vector< double, DIM > _step_lengths;
};

} // namespace snc::mpath_feed::speeder
