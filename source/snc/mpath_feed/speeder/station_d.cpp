/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_d.hpp"
#include <snc/mpath/elem/speed.hpp>
#include <snc/mpath_feed/job_d.hpp>

namespace snc::mpath_feed::speeder
{

template < std::size_t DIM >
Station_D< DIM >::Station_D (
    const Station_Context & context_n,
    const Axes_Limits_D< DIM > & axes_limits_n,
    const sev::lag::Vector< double, DIM > & axis_step_length_n )
: snc::mpath_feed::Station ( context_n )
, _step_speeder ( log () )
{
  // Memory limits
  _cargo_pool.allocate_csteps ( snc::mpath_feed::Station::num_cargos_max );

  _step_speeder.set_limits ( axes_limits_n, axis_step_length_n );
}

template < std::size_t DIM >
Station_D< DIM >::~Station_D ()
{
}

template < std::size_t DIM >
void
Station_D< DIM >::open ( const Job_Handle & job_n,
                         std::function< void () > const & notifier_n )
{
  if ( !_is_open ) {
    _is_open = true;
    _is_aborting = false;
    _stream_end_reached = false;
    _pmode = PMode::NONE;
    _plevel = 0;

    _cargo_pool.set_notifier ( notifier_n );

    auto & job_dim = static_cast< const Job_D< DIM > & > ( *job_n );

    _step_speeder.begin (
        job_dim.begin_position (),
        sev::lag::Vector< double, DIM > ( sev::lag::init::zero ) );

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
    _debug_file_cargo_out.open ( job_n->name () + "_speeder_cargo_out.txt" );
#endif
#if ( MPATH_WALKER_DEBUG_TO_FILE_SPEEDER_STEPS )
    _debug_file_steps.open ( job_n->name () + "_speeder_steps.txt" );
    _step_speeder.set_debug_file_steps ( &_debug_file_steps );
#endif
  }
}

template < std::size_t DIM >
void
Station_D< DIM >::abort ()
{
  if ( _is_open ) {
    if ( !_is_aborting ) {
      _is_aborting = true;
      _stream_end_reached = true;

      // Clear state and resources
      front_release ();
      _pmode = PMode::NONE;
      _plevel = 0;

      if ( _cargo_back_csteps != 0 ) {
        _cargo_pool.cargo_release ( _cargo_back_csteps );
        _cargo_back_csteps = 0;
      }
      _step_speeder.reset ();
    }
  }
}

template < std::size_t DIM >
void
Station_D< DIM >::close ()
{
  if ( _is_open ) {
    _is_open = false;
    _is_aborting = false;
    // Clear process mode
    _pmode = PMode::NONE;
    _plevel = 0;
    DEBUG_ASSERT ( _cargo_front == 0 );
    DEBUG_ASSERT ( _cargo_back_csteps == 0 );
    // Reset curve speeder
    _step_speeder.reset ();

    _cargo_pool.clear_notifier ();

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
    _debug_file_cargo_out.close ();
#endif
#if ( MPATH_WALKER_DEBUG_TO_FILE_SPEEDER_STEPS )
    _debug_file_steps.close ();
#endif
  }
}

template < std::size_t DIM >
snc::mpath_feed::Station::Token
Station_D< DIM >::read_next ()
{
  Token res ( Token::NONE );
  if ( _is_open ) {
    while ( true ) {
      if ( _cargo_front == 0 ) {
        if ( _stream_end_reached ) {
          if ( _cargo_pool.all_home () ) {
            res = Token::DONE;
          } else {
            res = Token::NONE;
          }
        } else {
          res = Token::FRONT_FEED;
        }
      } else {
        // Evaluate processing mode
        if ( read_next_pmode ( res ) ) {
          continue;
        }
      }

      // Leave loop by default
      break;
    }
  }
  return res;
}

template < std::size_t DIM >
inline bool
Station_D< DIM >::read_next_pmode ( Station_D::Token & res_n )
{
  switch ( _pmode ) {

  case PMode::FORWARD:
    switch ( (Forward_Level)_plevel ) {
    case Forward_Level::FORWARD:
      res_n = Token::BACK_READ;
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  case PMode::STEP:
    if ( _plevel == (uint32_t)Step_Level::FEED ) {
      speeder_feed ();
      _plevel = (uint32_t)Step_Level::CALC_STEPS;
    }
    if ( _plevel == (uint32_t)Step_Level::CALC_STEPS ) {
      // Curve speeding
      speeder_run ();
      if ( _cargo_back_csteps != 0 ) {
        // A step cargo item was acquired
        if ( _cargo_back_csteps->is_full () ) {
          // Switch to back read steps level.
          _plevel = (uint32_t)Step_Level::BACK_READ_STEPS;
        } else {
          if ( _cargo_back_csteps->is_empty () ) {
            // No steps were generated. Release empty cargo.
            _cargo_pool.cargo_release ( _cargo_back_csteps );
            _cargo_back_csteps = 0;
          }

          // Front step cargo is done
          front_release ();

          return true;
        }
      } else {
        // Out of step cargo
        res_n = Token::NONE;
        break;
      }
    }
    if ( _plevel == (uint32_t)Step_Level::BACK_READ_STEPS ) {
      // Step reading
      res_n = Token::BACK_READ;
      break;
    } else {
      DEBUG_ASSERT ( false );
    }
    break;

  case PMode::FINISH_FORWARD:
    if ( _plevel == (uint32_t)FF_Level::BEGIN ) {
      // Switch stepper to curve finishing mode
      _step_speeder.enable_curve_finishing ();
      _plevel = (uint32_t)FF_Level::CALC_STEPS;
    }
    if ( _plevel == (uint32_t)FF_Level::CALC_STEPS ) {
      // Curve speeding
      speeder_run ();
      if ( _cargo_back_csteps != 0 ) {
        // A step cargo item was acquired
        if ( !_cargo_back_csteps->is_empty () ) {
          // Some steps were generated
          // Switch to back read steps level.
          _plevel = (uint32_t)FF_Level::BACK_READ_STEPS;
        } else {
          // No steps were generated. Release empty cargo.
          _cargo_pool.cargo_release ( _cargo_back_csteps );
          _cargo_back_csteps = 0;
          // This curve element is done.
          // Switch back read element level.
          _plevel = (uint32_t)FF_Level::BACK_READ_ELEM;
        }
      } else {
        // Out of step cargo
        res_n = Token::NONE;
        break;
      }
    }
    switch ( (FF_Level)_plevel ) {
    case FF_Level::BACK_READ_STEPS:
    case FF_Level::BACK_READ_ELEM:
      res_n = Token::BACK_READ;
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }

  return false;
}

template < std::size_t DIM >
void
Station_D< DIM >::front_release ()
{
  if ( _cargo_front != 0 ) {
    _cargo_front->release ();
    _cargo_front = 0;
    _pmode = PMode::NONE;
    _plevel = 0;
  }
}

template < std::size_t DIM >
bool
Station_D< DIM >::front_feed ( Cargo * cargo_n )
{
  if ( _is_open && !_is_aborting ) {
    if ( cargo_n != 0 ) {
      DEBUG_ASSERT ( _cargo_front == 0 );
      if ( _cargo_front == 0 ) {
        _cargo_front = cargo_n;
        front_feed_eval ();
        return true;
      }
    }
  }
  return false;
}

template < std::size_t DIM >
inline void
Station_D< DIM >::front_feed_eval ()
{
  // Analyze cargo type
  switch ( _cargo_front->cargo_type () ) {
  case Cargo_Type::ELEMENT:
    front_feed_elem ();
    break;
  case Cargo_Type::BSTEPS:
    // Process steps
    _pmode = PMode::STEP;
    _plevel = (uint32_t)Step_Level::FEED;
    break;
  default:
    // Release unknown cargo immediately
    front_release ();
    break;
  }
}

template < std::size_t DIM >
inline void
Station_D< DIM >::front_feed_elem ()
{
  const snc::mpath::Element & elem ( cargo_front_elem ()->elem () );
  // Non curve element
  switch ( elem.elem_type () ) {
  case snc::mpath::elem::Type::STREAM_END:
    _stream_end_reached = true;
    _pmode = PMode::FINISH_FORWARD;
    _plevel = (uint32_t)FF_Level::BEGIN;
    break;
  case snc::mpath::elem::Type::STREAM_CURVE_FINISH:
    _pmode = PMode::FINISH_FORWARD;
    _plevel = (uint32_t)FF_Level::BEGIN;
    break;
  default:
    // Enable immediate element forward
    _pmode = PMode::FORWARD;
    _plevel = (uint32_t)Forward_Level::FORWARD;
    break;
  }

  if ( elem.elem_type () == snc::mpath::elem::Type::SPEED ) {
    typedef const snc::mpath::elem::Speed & ETYPE;
    ETYPE espeed ( static_cast< ETYPE > ( elem ) );

    // Acquire curve speed
    if ( espeed.speed ().is_symbolic () ) {
      DEBUG_ASSERT ( !espeed.speed ().is_symbolic () );
    } else {
      _step_speeder.set_step_speed_max ( espeed.speed ().value () );
    }
  }
}

template < std::size_t DIM >
Cargo *
Station_D< DIM >::back_read ()
{
  Cargo * res ( 0 );

  switch ( _pmode ) {

  case PMode::FORWARD:
    switch ( (Forward_Level)_plevel ) {
    case Forward_Level::FORWARD:
      back_read_forward_elem ( res );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  case PMode::STEP:
    switch ( (Step_Level)_plevel ) {
    case Step_Level::BACK_READ_STEPS:
      if ( back_read_forward_steps ( res ) ) {
        // Switch to step generation level
        _plevel = (uint32_t)Step_Level::CALC_STEPS;
      }
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  case PMode::FINISH_FORWARD:
    switch ( (FF_Level)_plevel ) {
    case FF_Level::BACK_READ_STEPS:
      if ( back_read_forward_steps ( res ) ) {
        // Switch to step generation level
        _plevel = (uint32_t)FF_Level::CALC_STEPS;
      }
      break;
    case FF_Level::BACK_READ_ELEM:
      back_read_forward_elem ( res );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }
    break;

  default:
    break;
  }

#if ( MPATH_WALKER_DEBUG_TO_FILE_CARGO )
  if ( res != 0 ) {
    debug_write_cargo ( _debug_file_cargo_out, *res_n );
    _debug_file_cargo_out.flush ();
  }
#endif

  return res;
}

template < std::size_t DIM >
void
Station_D< DIM >::back_read_forward_elem ( Cargo *& res_n )
{
  // Forward front element
  if ( _cargo_front != 0 ) {
    res_n = _cargo_front;
    _cargo_front = 0;
    // Clear processing state
    _pmode = PMode::NONE;
    _plevel = 0;
  }
}

template < std::size_t DIM >
bool
Station_D< DIM >::back_read_forward_steps ( Cargo *& res_n )
{
  // Back read steps level
  if ( _cargo_back_csteps != 0 ) {
    DEBUG_ASSERT ( _cargo_back_csteps->num_steps () != 0 );
    DEBUG_ASSERT ( _cargo_back_csteps->num_steps () <=
                   _cargo_back_csteps->num_steps_max () );
    res_n = _cargo_back_csteps;
    _cargo_back_csteps = 0;
    return true;
  }
  return false;
}

template < std::size_t DIM >
void
Station_D< DIM >::speeder_feed ()
{
  // Try to acquire a step tile
  if ( _cargo_front != 0 ) {
    if ( _cargo_front->cargo_type () == Cargo_Type::BSTEPS ) {
      typedef cargo::BSteps< DIM > CType;
      CType * csteps ( static_cast< CType * > ( _cargo_front ) );
      _step_speeder.curve_steps_feed ( csteps->begin (), csteps->num_steps () );
    }
  }
}

template < std::size_t DIM >
void
Station_D< DIM >::speeder_run ()
{
  // Acquire a new step tile on demand
  if ( _cargo_back_csteps == 0 ) {
    _cargo_back_csteps = _cargo_pool.acquire_csteps ();
    if ( _cargo_back_csteps != 0 ) {
      // Ensure clean state
      _cargo_back_csteps->reset ();
    }
  }
  if ( _cargo_back_csteps != 0 ) {
    if ( !_cargo_back_csteps->is_full () ) {
      const unsigned int num ( _step_speeder.acquire_steps (
          &_cargo_back_csteps->step_end (),
          _cargo_back_csteps->num_steps_free () ) );
      if ( num != 0 ) {
        _cargo_back_csteps->num_steps_increment ( num );
      }
    }
  }
}

// -- Instantiation

template class Station_D< 1 >;
template class Station_D< 2 >;
template class Station_D< 3 >;
template class Station_D< 4 >;
template class Station_D< 5 >;
template class Station_D< 6 >;

} // namespace snc::mpath_feed::speeder
