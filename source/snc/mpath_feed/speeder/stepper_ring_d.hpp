/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <snc/mpath_feed/cargos.hpp>
#include <snc/mpath_feed/speeder/step_celerator_d.hpp>
#include <snc/mpath_feed/step.hpp>
#include <snc/utility/debug_file.hpp>

namespace snc::mpath_feed::speeder
{

template < std::size_t DIM >
class Stepper_Ring_D
{
  public:
  // -- Types
  using Ring_Buffer_Step = sev::mem::Ring_Fixed< CStep< DIM > >;
  static const std::size_t reverse_speed_bisect_count = 16;

  // -- Construction and setup

  Stepper_Ring_D ( const sev::logt::Reference & log_parent_n );

  ~Stepper_Ring_D ();

  void
  reset ();

  void
  set_debug_file_steps ( snc::utility::Debug_File * file_n )
  {
    _debug_file_steps = file_n;
  }

  void
  set_limits ( const Axes_Limits_D< DIM > & limits_n,
               const sev::lag::Vector< double, DIM > & axis_step_length_n );

  // -- Interface

  void
  begin ( const sev::lag::Vector< double, DIM > & current_position_n,
          const sev::lag::Vector< double, DIM > & current_speed_n );

  /// @brief Maximum speed for all new steps
  double
  step_speed_max () const
  {
    return _step_speed_max;
  }

  void
  set_step_speed_max ( double speed_n )
  {
    _step_speed_max = speed_n;
  }

  bool
  curve_steps_needed () const
  {
    return ( !curve_finishing_enabled () &&
             ( _steps_rbuffer.size () <= _steps_rbuffer_length_min ) );
  }

  bool
  curve_steps_feed ( const BStep< DIM > * steps_n, size_t num_steps_n );

  void
  enable_curve_finishing ();

  bool
  curve_finishing_enabled () const
  {
    return _curve_finish;
  }

  bool
  curve_finishing_complete () const
  {
    return _curve_finish_done;
  }

  bool
  steps_available () const
  {
    return ( ( _steps_rbuffer.size () != 0 ) &&
             ( ( _steps_rbuffer.size () > _steps_rbuffer_length_min ) ||
               curve_finishing_enabled () ) );
  }

  size_t
  acquire_steps ( CStep< DIM > * steps_n, size_t num_max_n );

  private:
  // -- Utility

  template < bool STEPS_DEBUG >
  void
  iterate_latest_steps ( size_t num_steps_n );

  template < bool STEPS_DEBUG >
  bool
  iterate_accelerate ( size_t step_index_n );

  template < bool STEPS_DEBUG >
  void
  iterate_decelerate ( size_t step_index_n );

  bool
  iterate_decelerate_forward ( size_t step_index_n );

  bool
  iterate_decelerate_forward_probe ( size_t index_begin_n,
                                     size_t index_last_n );

  void
  iterate_decelerate_forward_real ( size_t index_begin_n, size_t index_last_n );

  template < bool STEPS_DEBUG >
  void
  iterate_speed_decelerate_bisect ( size_t step_index_n );

  /// @return True if all calculated step speeds are in the valid range
  bool
  iterate_speed_decelerate_probe ( size_t index_begin_n );

  /// @return True if all calculated step speeds are in the valid range
  template < bool STEPS_DEBUG >
  void
  iterate_speed_decelerate_real ( size_t index_begin_n );

  private:
  // -- Attributes
  bool _curve_finish;
  bool _curve_finish_done;

  size_t _steps_rbuffer_length_min;
  Ring_Buffer_Step _steps_rbuffer;
  Step_Celerator_D< DIM > _celerator;

  CStep< DIM > _step_begin;

  double _step_speed_max;
  sev::lag::Vector< double, DIM > _axis_stop_speed;
  sev::lag::Vector< double, DIM > _axis_step_length;

  snc::utility::Debug_File * _debug_file_steps;
  sev::logt::Context _log;
};

// -- Types

using Stepper_Bisection_D1 = Stepper_Ring_D< 1 >;
using Stepper_Bisection_D2 = Stepper_Ring_D< 2 >;
using Stepper_Bisection_D3 = Stepper_Ring_D< 3 >;
using Stepper_Bisection_D4 = Stepper_Ring_D< 4 >;
using Stepper_Bisection_D5 = Stepper_Ring_D< 5 >;
using Stepper_Bisection_D6 = Stepper_Ring_D< 6 >;

} // namespace snc::mpath_feed::speeder
