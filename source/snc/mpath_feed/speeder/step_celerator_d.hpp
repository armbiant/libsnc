/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/mpath_feed/axes_limits.hpp>
#include <snc/mpath_feed/step.hpp>
#include <snc/utility/debug_file.hpp>

namespace snc::mpath_feed::speeder
{

/// @brief Accelerates or decelerates a step
///
template < std::size_t DIM >
class Step_Celerator_D
{
  public:
  // -- Construction

  Step_Celerator_D ();

  // -- Setup

  void
  set_axes_limits ( const Axes_Limits_D< DIM > & limits_n );

  const Axes_Limits_D< DIM > &
  axes_limits () const
  {
    return _axes_limits;
  }

  // -- Interface

  /// @brief Forward iteration. Speeds are calculated for step_n
  void
  load_step ( const sev::lag::Vector< double, DIM > & prev_pos_n,
              const sev::lag::Vector< double, DIM > & prev_tangent_n,
              double prev_speed_n,
              const CStep< DIM > & step_n )
  {
    // Previous step values
    _prev_step_pos = prev_pos_n;
    _prev_step_tangent = prev_tangent_n;
    _prev_step_speed = prev_speed_n;
    // Current step values
    _step_tangent = step_n.tangent;
    _step_pos = step_n.pos;
  }

  /// @brief Forward iteration. Speeds are calculated for step_n
  void
  load_step ( const CStep< DIM > & step_prev_n, const CStep< DIM > & step_n )
  {
    // Previous step values
    _prev_step_pos = step_prev_n.pos;
    _prev_step_tangent = step_prev_n.tangent;
    _prev_step_speed = step_prev_n.speed;
    // Current step values
    _step_tangent = step_n.tangent;
    _step_pos = step_n.pos;
  }

  void
  load_step_prev_speed ( double prev_speed_n )
  {
    _prev_step_speed = prev_speed_n;
  }

  /// @brief Reverse iteration. Speeds are calculated for step_n
  void
  load_step_reverse ( const CStep< DIM > & step_prev_n,
                      const CStep< DIM > & step_n )
  {
    // Previous step values
    _prev_step_pos = step_prev_n.pos;
    _prev_step_tangent = -step_prev_n.tangent;
    _prev_step_speed = step_prev_n.speed;
    // Current step values
    _step_tangent = -step_n.tangent;
    _step_pos = step_n.pos;
  }

  bool
  loaded_step_calc_vmin_vmax ();

  double
  vmin () const
  {
    return _vmin;
  }

  double
  vmax () const
  {
    return _vmax;
  }

  // -- Readers

  const sev::lag::Vector< double, DIM > &
  prev_step_pos () const
  {
    return _prev_step_pos;
  }

  const sev::lag::Vector< double, DIM > &
  prev_step_tangent () const
  {
    return _prev_step_tangent;
  }

  double
  prev_step_speed () const
  {
    return _prev_step_speed;
  }

  const sev::lag::Vector< double, DIM > &
  step_pos () const
  {
    return _step_pos;
  }

  const sev::lag::Vector< double, DIM > &
  step_tangent () const
  {
    return _step_tangent;
  }

  /// @brief For debugging
  void
  print_debug ( snc::utility::Debug_File & debug_file_n );

  private:
  // -- Utility

  bool
  a_calc_speed_limits ( double & vmin_n, double & vmax_n, size_t aindex_n );

  bool
  a_calc_tangent_scales ( double & tscale_min_n,
                          double & tscale_max_n,
                          size_t aindex_n );

  bool
  a_calc_speeds ();

  void
  b_calc_speed_limits ( double & vmin_n,
                        double & vmax_n,
                        double dtval_n,
                        size_t aindex_n );

  bool
  b_calc_tangent_scales ( double & tscale_min_n,
                          double & tscale_max_n,
                          double dtval_n,
                          size_t aindex_n );

  bool
  b_calc_speeds ();

  private:
  // -- Attributes
  std::array< double, DIM * 2 > _speed_limits;
  std::array< double, DIM * 2 > _speed_scales;
  double _vmin;
  double _vmax;
  sev::lag::Vector< double, DIM > _step_tangent;
  sev::lag::Vector< double, DIM > _step_pos;
  sev::lag::Vector< double, DIM > _prev_step_pos;
  sev::lag::Vector< double, DIM > _prev_step_tangent;
  double _prev_step_speed;

  double _axis_stop_speed_max;
  sev::lag::Vector< double, DIM > _axis_stop_speed;
  Axes_Limits_D< DIM > _axes_limits;
};

// -- Types

using Celerator1 = Step_Celerator_D< 1 >;
using Celerator2 = Step_Celerator_D< 2 >;
using Celerator3 = Step_Celerator_D< 3 >;
using Celerator4 = Step_Celerator_D< 4 >;
using Celerator5 = Step_Celerator_D< 5 >;
using Celerator6 = Step_Celerator_D< 6 >;

} // namespace snc::mpath_feed::speeder
