/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/cargo_pool.hpp>
#include <snc/mpath_feed/cargos.hpp>
#include <cstddef>

namespace snc::mpath_feed
{

/// @brief Cargo pool
///
template < std::size_t DIM >
class Cargo_Pool_D : public Cargo_Pool
{
  // Public methods
  public:
  Cargo_Pool_D ();

  ~Cargo_Pool_D ();

  void
  clear ();

  bool
  allocate_bsteps ( std::size_t num_n );

  bool
  allocate_csteps ( std::size_t num_n );

  bool
  allocate_elem ( std::size_t num_n );

  cargo::BSteps< DIM > *
  acquire_bsteps ();

  cargo::CSteps< DIM > *
  acquire_csteps ();

  cargo::Element_D< DIM > *
  acquire_elem ();

  // Private methods
  private:
  template < class T >
  bool
  allocate_cargo ( Cargo_Pool_Stack & stack_n, std::size_t num_n );

  template < class T >
  void
  clear_stack ( Cargo_Pool_Stack & stack_n );
};

} // namespace snc::mpath_feed
