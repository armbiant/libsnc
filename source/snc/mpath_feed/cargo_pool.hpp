/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/cargo_pool_stack.hpp>
#include <functional>
#include <mutex>

namespace snc::mpath_feed
{

/// @brief Cargo pool
///
class Cargo_Pool
{
  public:
  // -- Construction

  Cargo_Pool ();

  // -- Interface

  void
  cargo_release ( Cargo * cargo_n );

  bool
  all_home ();

  std::size_t
  num_bsteps ();

  std::size_t
  num_csteps ();

  std::size_t
  num_elem ();

  const std::function< void () > &
  notifier () const
  {
    return _notifier;
  }

  // -- Notifier

  void
  set_notifier ( const std::function< void () > & notifier_n );

  void
  clear_notifier ();

  protected:
  // -- Protected attributes
  std::mutex _mutex;

  Cargo_Pool_Stack _stack_bsteps;
  Cargo_Pool_Stack _stack_csteps;
  Cargo_Pool_Stack _stack_elem;
  std::function< void () > _notifier;
};

} // namespace snc::mpath_feed
