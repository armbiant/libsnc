/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/mpath_feed/job.hpp>
#include <cstdint>

namespace snc::mpath_feed::event
{

struct Type
{
  static constexpr std::uint_fast32_t SIGNAL = 0;
};

enum class Signal_Type : std::uint8_t
{
  INVALID,
  STREAM_END,
  STREAM_ABORT,

  THREAD_ABORT,
  THREAD_ABORT_DONE
};

/// @brief Transports a signal
///
class Signal : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::SIGNAL;

  Signal ()
  : sev::event::Event ( etype )
  , _signal ( Signal_Type::INVALID )
  {
  }

  Signal_Type
  signal () const
  {
    return _signal;
  }

  void
  set_signal ( Signal_Type signal_n )
  {
    _signal = signal_n;
  }

  private:
  Signal_Type _signal;
};

} // namespace snc::mpath_feed::event
