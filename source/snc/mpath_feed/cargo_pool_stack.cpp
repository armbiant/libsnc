/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cargo_pool_stack.hpp"

namespace snc::mpath_feed
{

Cargo_Pool_Stack::Cargo_Pool_Stack () = default;

} // namespace snc::mpath_feed
