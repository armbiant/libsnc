/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/read.hpp>
#include <memory>
#include <string>

namespace snc::mpath_feed
{

/// @brief Feed job
///
class Job
{
  public:
  // -- Types
  using Stream_Handle = std::shared_ptr< snc::mpath_stream::Read >;

  // -- Construction and setup

  Job ();

  virtual ~Job ();

  virtual void
  reset ();

  // -- Job name

  const std::string &
  name () const
  {
    return _name;
  }

  void
  set_name ( std::string name_n )
  {
    _name = std::move ( name_n );
  }

  // -- MPath stream

  const Stream_Handle &
  stream () const
  {
    return _stream;
  }

  void
  set_stream ( const Stream_Handle & stream_n )
  {
    _stream = stream_n;
  }

  private:
  // -- Attributes
  std::string _name;
  Stream_Handle _stream;
};

/// @brief Job handle type
using Job_Handle = std::shared_ptr< Job >;

} // namespace snc::mpath_feed
