/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath_feed/job.hpp>

namespace snc::mpath_feed
{

/// @brief Job for an DIM dimensional stream
///
template < std::size_t DIM >
class Job_D : public Job
{
  public:
  // -- Construction and setup

  Job_D ();

  explicit Job_D ( const Job & job_n );

  ~Job_D () override;

  void
  reset () override;

  // -- Begin position

  const sev::lag::Vector< double, DIM > &
  begin_position () const
  {
    return _begin_position;
  }

  void
  set_begin_position ( const sev::lag::Vector< double, DIM > & pos_n )
  {
    _begin_position = pos_n;
  }

  private:
  // -- Attributes
  sev::lag::Vector< double, DIM > _begin_position;
};

// -- Types

using Job_D1 = Job_D< 1 >;
using Job_D2 = Job_D< 2 >;
using Job_D3 = Job_D< 3 >;
using Job_D4 = Job_D< 4 >;
using Job_D5 = Job_D< 5 >;
using Job_D6 = Job_D< 6 >;

} // namespace snc::mpath_feed
