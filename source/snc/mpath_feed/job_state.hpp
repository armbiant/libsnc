/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <array>
#include <chrono>

namespace snc::mpath_feed
{

/// @brief Progress state of a job
///
class Job_State
{
  public:
  // -- Types

  /// @brief State flags
  struct State
  {
    // Processing running
    static constexpr std::uint8_t RUNNING = ( 1 << 0 );
    static constexpr std::uint8_t ABORT = ( 1 << 1 );
    // Processing finished
    static constexpr std::uint8_t FINISHED = ( 1 << 2 );
  };

  using Clock = std::chrono::steady_clock;
  using Time_Point = Clock::time_point;
  using Duration = Clock::duration;

  // -- Construction and setup

  Job_State () = default;

  virtual ~Job_State ();

  void
  reset ()
  {
    _state.clear ();
    _time_update = Time_Point::min ();
    _time_begin = Time_Point::min ();
    _time_finish = Time_Point::min ();
  }

  // -- State

  sev::mem::Flags_Fast8 &
  state ()
  {
    return _state;
  }

  const sev::mem::Flags_Fast8 &
  state () const
  {
    return _state;
  }

  // -- Times

  /// @brief Time point when this state was updated
  const Time_Point &
  time_update () const
  {
    return _time_update;
  }

  void
  set_time_update ( const Time_Point & time_n )
  {
    _time_update = time_n;
  }

  /// @brief Time point when this job was started
  const Time_Point &
  time_begin () const
  {
    return _time_begin;
  }

  void
  set_time_begin ( const Time_Point & time_n )
  {
    _time_begin = time_n;
  }

  /// @brief Time point when this job finished
  const Time_Point &
  time_finish () const
  {
    return _time_finish;
  }

  void
  set_time_finish ( const Time_Point & time_n )
  {
    _time_finish = time_n;
  }

  private:
  // -- Attributes
  sev::mem::Flags_Fast8 _state;
  Time_Point _time_update = Time_Point::min ();
  Time_Point _time_begin = Time_Point::min ();
  Time_Point _time_finish = Time_Point::min ();
};

} // namespace snc::mpath_feed
