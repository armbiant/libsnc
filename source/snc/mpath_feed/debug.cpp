/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include <snc/mpath/elements_common.hpp>
#include <snc/mpath_feed/cargos.hpp>
#include <snc/mpath_feed/debug.hpp>
#include <snc/mpath_feed/step.hpp>
#include <snc/utility/debug_file.hpp>

namespace snc::mpath_feed
{

template < std::size_t DIM >
void
debug_write_cargo ( snc::utility::Debug_File & debug_file_n,
                    const Cargo & tile_n )
{
  debug_file_n.write ( "Cargo:" );
  debug_file_n.write ( " cargo_type=\"" );
  switch ( tile_n.cargo_type () ) {
  case Cargo_Type::BSTEPS:
    debug_file_n.write ( "BSTEPS" );
    debug_file_n.write ( "\"" );
    {
      typedef const cargo::BSteps< DIM > & TTYPE;
      TTYPE ctile ( static_cast< TTYPE > ( tile_n ) );
      debug_file_n.write ( " num_steps=\"" );
      debug_file_n.write ( ctile.num_steps () );
      debug_file_n.write ( "\"" );
    }
    break;
  case Cargo_Type::CSTEPS:
    debug_file_n.write ( "CSTEPS" );
    debug_file_n.write ( "\"" );
    {
      typedef const cargo::CSteps< DIM > & TTYPE;
      TTYPE ctile ( static_cast< TTYPE > ( tile_n ) );
      debug_file_n.write ( " num_steps=\"" );
      debug_file_n.write ( ctile.num_steps () );
      debug_file_n.write ( "\"" );
    }
    break;
  case Cargo_Type::ELEMENT:
    debug_file_n.write ( "ELEMENT" );
    debug_file_n.write ( "\"" );
    {
      typedef const cargo::Element_D< DIM > & TTYPE;
      TTYPE ctile ( static_cast< TTYPE > ( tile_n ) );
      debug_file_n.write ( " elem_type=\"" );
      switch ( ctile.ebuffer ().elem ().elem_type () ) {

      case snc::mpath::elem::Type::STREAM_END:
        debug_file_n.write ( "STREAM_END" );
        break;

      case snc::mpath::elem::Type::STREAM_CURVE_FINISH:
        debug_file_n.write ( "STREAM_CURVE_FINISH" );
        break;

      case snc::mpath::elem::Type::SLEEP:
        debug_file_n.write ( "SLEEP" );
        {
          typedef const snc::mpath::elem::Sleep & EType;
          EType celem ( static_cast< EType > ( ctile.ebuffer ().elem () ) );
          debug_file_n.write ( " usecs=\"" );
          debug_file_n.write ( celem.usecs () );
          debug_file_n.write ( "\"" );
        }
        break;
      case snc::mpath::elem::Type::FADER:
        debug_file_n.write ( "FADER" );
        {
          typedef const snc::mpath::elem::Fader & EType;
          EType celem ( static_cast< EType > ( ctile.ebuffer ().elem () ) );
          debug_file_n.write ( " fader_index=\"" );
          debug_file_n.write ( (uint32_t)celem.fader_index () );
          debug_file_n.write ( "\"" );
          debug_file_n.write ( " fader_bits=\"" );
          debug_file_n.write ( (uint32_t)celem.fader_bits () );
          debug_file_n.write ( "\"" );
          debug_file_n.write ( " fader_state=\"" );
          debug_file_n.write ( (uint32_t)celem.fader_value ().get_uint64 () );
          debug_file_n.write ( "\"" );
        }
        break;
      case snc::mpath::elem::Type::SPEED:
        debug_file_n.write ( "SPEED" );
        {
          auto & celem = static_cast< const snc::mpath::elem::Speed & > (
              ctile.ebuffer ().elem () );
          if ( celem.speed ().is_symbolic () ) {
            debug_file_n.write ( " speed_regime=\"" );
            debug_file_n.write ( static_cast< std::uint_fast32_t > (
                celem.speed ().regime () ) );
            debug_file_n.write ( "\"" );
          } else {
            debug_file_n.write ( " speed_value=\"" );
            debug_file_n.write ( celem.speed ().value () );
            debug_file_n.write ( "\"" );
          }
        }
        break;

      default:
        debug_file_n.write ( "UNKNOWN/DIMENSIONAL" );
        break;
      }
    }
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
  debug_file_n.write ( "\n" );
}

// -- Instantiation

template void
debug_write_cargo< 1 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );
template void
debug_write_cargo< 2 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );
template void
debug_write_cargo< 3 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );
template void
debug_write_cargo< 4 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );
template void
debug_write_cargo< 5 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );
template void
debug_write_cargo< 6 > ( snc::utility::Debug_File & debug_file_n,
                         const Cargo & tile_n );

} // namespace snc::mpath_feed
