/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cargo_pool_d.hpp"
#include <sev/assert.hpp>

namespace snc::mpath_feed
{

template < std::size_t DIM >
Cargo_Pool_D< DIM >::Cargo_Pool_D ()
{
}

template < std::size_t DIM >
Cargo_Pool_D< DIM >::~Cargo_Pool_D ()
{
  clear ();
}

template < std::size_t DIM >
void
Cargo_Pool_D< DIM >::clear ()
{
  clear_stack< cargo::BSteps< DIM > > ( _stack_bsteps );
  clear_stack< cargo::CSteps< DIM > > ( _stack_csteps );
  clear_stack< cargo::Element_D< DIM > > ( _stack_elem );
}

template < std::size_t DIM >
bool
Cargo_Pool_D< DIM >::allocate_bsteps ( std::size_t num_n )
{
  return allocate_cargo< cargo::BSteps< DIM > > ( _stack_bsteps, num_n );
}

template < std::size_t DIM >
bool
Cargo_Pool_D< DIM >::allocate_csteps ( std::size_t num_n )
{
  return allocate_cargo< cargo::CSteps< DIM > > ( _stack_csteps, num_n );
}

template < std::size_t DIM >
bool
Cargo_Pool_D< DIM >::allocate_elem ( std::size_t num_n )
{
  return allocate_cargo< cargo::Element_D< DIM > > ( _stack_elem, num_n );
}

template < std::size_t DIM >
template < class T >
inline bool
Cargo_Pool_D< DIM >::allocate_cargo ( Cargo_Pool_Stack & stack_n,
                                      std::size_t num_n )
{
  if ( stack_n.num_allocated () < num_n ) {
    std::size_t num ( num_n - stack_n.num_allocated () );
    for ( ; num != 0; --num ) {
      stack_n.push_allocated ( new T ( this ) );
    }
    return true;
  }
  return false;
}

template < std::size_t DIM >
template < class T >
void
Cargo_Pool_D< DIM >::clear_stack ( Cargo_Pool_Stack & stack_n )
{
  while ( !stack_n.is_empty () ) {
    T * cargo ( static_cast< T * > ( stack_n.pop_allocated () ) );
    delete cargo;
  }
}

template < std::size_t DIM >
cargo::BSteps< DIM > *
Cargo_Pool_D< DIM >::acquire_bsteps ()
{
  cargo::BSteps< DIM > * res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    auto & stack ( _stack_bsteps );
    if ( !stack.is_empty () ) {
      res = static_cast< cargo::BSteps< DIM > * > ( stack.pop_not_empty () );
    }
  }
  return res;
}

template < std::size_t DIM >
cargo::CSteps< DIM > *
Cargo_Pool_D< DIM >::acquire_csteps ()
{
  cargo::CSteps< DIM > * res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    auto & stack ( _stack_csteps );
    if ( !stack.is_empty () ) {
      res = static_cast< cargo::CSteps< DIM > * > ( stack.pop_not_empty () );
    }
  }
  return res;
}

template < std::size_t DIM >
cargo::Element_D< DIM > *
Cargo_Pool_D< DIM >::acquire_elem ()
{
  cargo::Element_D< DIM > * res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    auto & stack ( _stack_elem );
    if ( !stack.is_empty () ) {
      res = static_cast< cargo::Element_D< DIM > * > ( stack.pop_not_empty () );
    }
  }
  return res;
}

// -- Instantiation

template class Cargo_Pool_D< 1 >;
template class Cargo_Pool_D< 2 >;
template class Cargo_Pool_D< 3 >;
template class Cargo_Pool_D< 4 >;
template class Cargo_Pool_D< 5 >;
template class Cargo_Pool_D< 6 >;

} // namespace snc::mpath_feed
