/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cargo_pool.hpp"
#include <sev/assert.hpp>

namespace snc::mpath_feed
{

Cargo_Pool::Cargo_Pool ()
: _notifier ( [] () {} )
{
}

bool
Cargo_Pool::all_home ()
{
  bool res ( false );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = ( _stack_bsteps.all_home () && _stack_csteps.all_home () &&
            _stack_elem.all_home () );
  }
  return res;
}

std::size_t
Cargo_Pool::num_bsteps ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _stack_bsteps.num_home ();
  }
  return res;
}

std::size_t
Cargo_Pool::num_csteps ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _stack_csteps.num_home ();
  }
  return res;
}

std::size_t
Cargo_Pool::num_elem ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _stack_elem.num_home ();
  }
  return res;
}

void
Cargo_Pool::cargo_release ( Cargo * cargo_n )
{
  if ( cargo_n == nullptr ) {
    return;
  }

  Cargo_Pool_Stack * stack = nullptr;
  switch ( cargo_n->cargo_type () ) {
  case Cargo_Type::BSTEPS:
    stack = &_stack_bsteps;
    break;
  case Cargo_Type::CSTEPS:
    stack = &_stack_csteps;
    break;
  case Cargo_Type::ELEMENT:
    stack = &_stack_elem;
    break;
  default:
    break;
  }

  DEBUG_ASSERT ( stack != nullptr );
  // Append to stack
  if ( stack != nullptr ) {
    std::lock_guard< std::mutex > lock ( _mutex );

    DEBUG_ASSERT ( stack->num_home () < stack->num_allocated () );
    stack->release ( cargo_n );
    // Notify bit notifier if the stack was empty
    if ( ( stack->num_home () == 1 ) || ( stack->all_home () ) ) {
      _notifier ();
    }
  }
}

void
Cargo_Pool::set_notifier ( const std::function< void () > & notifier_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _notifier = notifier_n;
}

void
Cargo_Pool::clear_notifier ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _notifier = [] () {};
}

} // namespace snc::mpath_feed
