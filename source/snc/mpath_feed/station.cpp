/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station.hpp"

namespace snc::mpath_feed
{

Station::Station ( const Station_Context & context_n )
: _context ( context_n )
, _log ( context ().log_parent (), "Station" )
{
}

Station::~Station () {}

void
Station::set_name ( sev::unicode::View span_n )
{
  span_n.get ( _name );
}

} // namespace snc::mpath_feed
