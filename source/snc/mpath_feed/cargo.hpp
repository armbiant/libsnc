/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <cstdint>

namespace snc::mpath_feed
{

// -- Forward declaration
class Cargo_Pool;

/// @brief Cargo types
enum class Cargo_Type
{
  BSTEPS,
  CSTEPS,
  ELEMENT,
  LIST_END
};

/// @brief Abstract cargo object
///
/// Cargo objects get passed from station to station.
///
/// They feature Cargo previous and next pointers that allow
/// chainings of a number of Cargo objects.
///
class Cargo : public sev::event::Event
{
  public:
  // -- Construction

  /// @brief Cargo type
  Cargo_Type
  cargo_type () const
  {
    return _cargo_type;
  }

  // -- Interface

  Cargo_Pool *
  pool () const
  {
    return _pool;
  }

  void
  release ();

  protected:
  // -- Protected methods
  Cargo ( Cargo_Type cargo_type_n, Cargo_Pool * pool_n );

  private:
  // -- Private attributes
  Cargo_Type _cargo_type;
  Cargo_Pool * _pool;
};

} // namespace snc::mpath_feed
