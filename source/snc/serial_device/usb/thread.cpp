/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "thread.hpp"
#include <sev/assert.hpp>
#include <sev/math/exponents.hpp>
#include <sev/system.hpp>
#include <snc/usb/error_string.hpp>
#include <chrono>

namespace snc::serial_device::usb
{

Thread::Transfer_Bind::Transfer_Bind ()
: usb_transfer ( libusb_alloc_transfer ( 0 ) )
{
}

Thread::Transfer_Bind::~Transfer_Bind ()
{
  libusb_free_transfer ( usb_transfer );
}

Thread::Thread ( Context const & context_n,
                 snc::usb::Device_Port_Id const & usb_port_id_n,
                 sev::logt::Reference const & log_parent_n )
: _log ( log_parent_n, "T" )
, _usb_port_id ( usb_port_id_n )
, _thread ( "Emb-Device-USB", context_n.thread_tracker )
{
  _epools.pool< event::out::Data_In > ().ensure_minimum_size ( read_tiles_max );
  _epools.pool< event::out::Data_Sent > ().ensure_minimum_size ( 4 );
  _epools.pool< event::out::Device_Error > ().ensure_minimum_size ( 1 );
  _epools.pool< event::out::Abort_Stop_IO_Done > ().ensure_minimum_size ( 1 );
  _epools.pool< event::out::Abort_Shutdown_Done > ().ensure_minimum_size ( 1 );

  // Allocate pollfds
  {
    ::pollfd base;
    std::fill_n ( (unsigned char *)&base, sizeof ( base ), 0 );
    _pollfds.resize ( Socket_Index::SERIAL_DEVICE_BEGIN, base );
  }
  // Event collector event io
  {
    pollfd & pfd ( _pollfds[ Socket_Index::EVENTS_HUB ] );
    pfd.fd = _event_accu_eio.socket_fd ();
    pfd.events = _event_accu_eio.socket_events ();
  }

  // Event queue connections
  _connection.set_incoming_notifier (
      [ &ec = _event_accu_eio ] () { ec.set ( LEvent::EVENTS_IN ); } );
  _connection.set_push_notifier (
      [ &le = _loop_events ] () { le.set ( LEvent::EVENTS_OUT ); } );
  _connection.connect ( context_n.link );

  DEBUG_ASSERT ( _connection.is_connected () );

  // Start thread
  _thread.start ( [ this ] () { this->operator() (); } );
}

Thread::~Thread ()
{
  if ( _thread.is_running () ) {
    _thread.join ();
  }
}

void
Thread::usb_context_init ()
{
  // Reading structures
  _read.owner_tiles = std::make_unique< Read_Tile[] > ( read_tiles_max );
  for ( std::size_t ii = 0; ii != read_tiles_max; ++ii ) {
    _read.tiles_free.push_back ( &_read.owner_tiles[ ii ] );
  }

  _read.owner_binds = std::make_unique< Transfer_Bind[] > ( Read::ut_in_max );
  for ( std::size_t ii = 0; ii != Read::ut_in_max; ++ii ) {
    _read.binds_idle.push_back ( &_read.owner_binds[ ii ] );
  }

  // Writing structures
  _write.owner_binds =
      std::make_unique< Transfer_Bind[] > ( Write::ut_out_max );

  for ( std::size_t ii = 0; ii != Write::ut_out_max; ++ii ) {
    auto * tbind = &_write.owner_binds[ ii ];
    _write.binds_idle.push_back ( tbind );
  }

  // Create usb context
  int ret = libusb_init ( &_usb_context );
  if ( ret != 0 ) {
    _usb_context = nullptr;
    _log.cat ( sev::logt::FL_ERROR, "libusb context creation failed" );
    return;
  }

  // Set libusb debug level
  libusb_set_option (
      _usb_context, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_NONE );

  // Determine if timeout calls are required
  _libusb_context_timeouts =
      ( libusb_pollfds_handle_timeouts ( _usb_context ) == 0 );

  // Acquire pollfds
  {
    const libusb_pollfd ** usb_pollfds = libusb_get_pollfds ( _usb_context );
    for ( const libusb_pollfd ** pit = usb_pollfds; ( *pit ) != nullptr;
          ++pit ) {
      ::pollfd pfd;
      pfd.fd = ( *pit )->fd;
      pfd.events = ( *pit )->events;
      _pollfds.push_back ( pfd );
    }
    libusb_free_pollfds ( usb_pollfds );
  }

  // Install libusb pollfd changes callbacks
  libusb_set_pollfd_notifiers ( _usb_context,
                                &Thread::lusb_cb_pollfd_added,
                                &Thread::lusb_cb_pollfd_removed,
                                this // Pass this as user data
  );
}

void
Thread::usb_context_destroy ()
{
  if ( _usb_context == nullptr ) {
    return;
  }

  DEBUG_ASSERT ( _read.binds_submitted.is_empty () );
  _read.binds_idle.clear ();
  _read.owner_binds.reset ();
  _read.owner_tiles.reset ();

  DEBUG_ASSERT ( _write.binds_submitted.is_empty () );
  _write.binds_idle.clear ();
  _write.owner_binds.reset ();

  libusb_exit ( _usb_context );
  _usb_context = nullptr;
}

bool
Thread::usb_device_open ()
{
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Opening usb device:"
         << "\n  Bus number:  " << _usb_port_id.bus_number ()
         << "\n  Bus address: " << _usb_port_id.bus_address ()
         << "\n  Port number: " << _usb_port_id.port_number ();
  }

  // Find device in list
  {
    libusb_device ** usb_dev_list;
    size_t num_dev = libusb_get_device_list ( _usb_context, &usb_dev_list );
    for ( std::size_t ii = 0; ii != num_dev; ++ii ) {
      libusb_device * usb_dev = usb_dev_list[ ii ];
      if ( snc::usb::Device_Port_Id ( usb_dev ) == _usb_port_id ) {
        _usb_device = usb_dev;
        break;
      }
    }
    libusb_free_device_list ( usb_dev_list, 1 );
  }

  if ( _usb_device == nullptr ) {
    _log.cat ( sev::logt::FL_WARNING, "USB device not found" );
    usb_device_close ();
    return false;
  }

  // Open device
  if ( libusb_open ( _usb_device, &_usb_device_handle ) == 0 ) {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB device opened" );
  } else {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB device opening failed" );
    usb_device_close ();
    return false;
  }

  // Claim default interface
  if ( libusb_claim_interface ( _usb_device_handle, 0 ) == 0 ) {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB device interface claimed" );
  } else {
    _log.cat ( sev::logt::FL_DEBUG_0, "USB device interface claiming failed" );
    usb_device_close ();
    return false;
  }

  // Good
  _libusb_device_good = true;
  return true;
}

void
Thread::usb_device_close ()
{
  _libusb_device_good = false;
  _libusb_process_events = false;
  _libusb_context_timeouts = false;
  _libusb_error.clear ();

  if ( _usb_device_handle != nullptr ) {
    libusb_release_interface ( _usb_device_handle, 0 );
    libusb_close ( _usb_device_handle );
    _usb_device_handle = nullptr;
    _usb_device = nullptr;
  }

  _pollfds.resize ( Socket_Index::SERIAL_DEVICE_BEGIN );
}

void
Thread::usb_abort ()
{
  if ( !_libusb_device_good ) {
    return;
  }
  _libusb_device_good = false;

  // Release pending write tiles
  while ( !_write.tiles_pending.is_empty () ) {
    _write.tiles_done.push_back ( _write.tiles_pending.front () );
    _write.tiles_pending.pop_front_not_empty ();
  }

  // Cancel running transfers
  if ( !_read.binds_submitted.is_empty () ||
       !_write.binds_submitted.is_empty () ) {
    auto cancel_transfers =
        [] ( sev::mem::list::se::List< Transfer_Bind > & list_n ) {
          for ( auto & tbind : list_n ) {
            libusb_cancel_transfer ( tbind.usb_transfer );
          }
        };

    // Cancel read transfers
    if ( !_read.binds_submitted.is_empty () ) {
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Canceling submitted USB IN transfers." );
      cancel_transfers ( _read.binds_submitted );
    }

    // Cancel write transfers
    if ( !_write.binds_submitted.is_empty () ) {
      _log.cat ( sev::logt::FL_DEBUG_0,
                 "Canceling submitted USB OUT transfers." );
      cancel_transfers ( _write.binds_submitted );
    }
  }
}

void
Thread::usb_error ()
{
  if ( _libusb_error.test_any_set ( LibUSB_Error::ERROR ) ) {
    return;
  }

  // Send device error event
  if ( !_libusb_error.test_any_set ( LibUSB_Error::ERROR_ACKED ) &&
       !_abort_state.test_any ( Abort_Event::STOP_IO ) ) {
    _log.cat ( sev::logt::FL_DEBUG_0, "Sending device error event." );
    _connection.push ( _epools.pool< event::out::Device_Error > ().acquire () );
  }

  usb_abort ();
}

void
Thread::operator() ()
{
  usb_context_init ();
  usb_device_open ();

  while ( true ) {
    // Wait for events
    wait_for_events ();

    // Process event io
    if ( _loop_events.test_any ( LEvent::EVENTS_IN | LEvent::EVENTS_OUT ) ) {
      // Feed event queues
      _connection.feed_queue (
          _loop_events.flags (), LEvent::EVENTS_IN, LEvent::EVENTS_OUT );

      // Release returned events
      _connection.out ().release_all (
          [ this ] ( sev::event::Event * event_n ) {
            if ( event_n->type () == event::out::Type::DATA_IN ) {
              auto * cevent = static_cast< event::out::Data_In * > ( event_n );
              _read.tiles_free.push_front ( cevent->data_tile () );
            }
            event_n->pool ()->casted_reset_release_virtual ( event_n );
          } );

      // Process new events
      _connection.in ().process_all (
          [ this ] ( const sev::event::Event & event_n ) {
            switch ( event_n.type () ) {
            case event::in::Type::DATA:
              process_event_data ( event_n );
              break;
            case event::in::Type::ABORT_STOP_IO:
              process_event_abort_stop_io ( event_n );
              break;
            case event::in::Type::ABORT_SHUT_DOWN:
              process_event_abort_shut_down ( event_n );
              break;
            default:
              DEBUG_ASSERT ( false );
              break;
            }
          } );
    }

    // Process USB
    process_libusb ();
    process_write_return ();

    // Shut down handling
    if ( !_abort_state.is_empty () && process_shut_down () ) {
      break;
    }
  }

  usb_device_close ();
  usb_context_destroy ();

  _log.cat ( sev::logt::FL_DEBUG_0, "Thread main loop done." );
}

bool
Thread::process_shut_down ()
{
  if ( !_epools.tracker ().all_home () ||     //
       !_connection.all_empty () ||           //
       !_read.binds_submitted.is_empty () ||  //
       !_write.binds_submitted.is_empty () || //
       !_write.tiles_pending.is_empty () ||   //
       !_write.tiles_done.is_empty () ) {
    return false;
  }

  if ( !_abort_state.test_any ( Abort_Event::STOP_IO_DONE ) ) {
    if ( _abort_state.test_any ( Abort_Event::STOP_IO ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Sending IO stopped event." );
      _connection.push (
          _epools.pool< event::out::Abort_Stop_IO_Done > ().acquire () );
      _abort_state.set ( Abort_Event::STOP_IO_DONE );
    }
    return false;
  }

  if ( !_abort_state.test_any ( Abort_Event::SHUT_DOWN_SENT ) ) {
    if ( _abort_state.test_any ( Abort_Event::SHUT_DOWN ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Sending shut down done event." );
      _connection.push (
          _epools.pool< event::out::Abort_Shutdown_Done > ().acquire () );
      _abort_state.set ( Abort_Event::SHUT_DOWN_SENT );
    }
    return false;
  }

  return true;
}

void
Thread::wait_for_events ()
{
  if ( !_loop_events.is_empty () ) {
    return;
  }

  // Poll timeout in milli seconds
  int timeout_ms ( -1 );
  // Check if libusb requires timeouts
  if ( _libusb_context_timeouts ) {
    timeval tval;
    if ( libusb_get_next_timeout ( _usb_context, &tval ) != 0 ) {
      int usb_ms = ( tval.tv_sec * 1000 );
      usb_ms += ( tval.tv_usec + ( 1000 - 1 ) ) / 1000;
      sev::math::assign_larger ( usb_ms, 0 );
      _libusb_process_events = true;
    }
  }
  // Poll file descriptors
  int poll_ret = poll ( &_pollfds.front (), _pollfds.size (), timeout_ms );

  if ( poll_ret == 0 ) {
    // No socket events. Timed out.
  } else if ( poll_ret > 0 ) {
    // Socket events.
    auto it_end = _pollfds.end ();
    auto it_c = _pollfds.begin ();

    // Event io collector
    if ( it_c->revents != 0 ) {
      _loop_events.set ( _event_accu_eio.fetch_and_clear () );
    }
    ++it_c;
    // libusb sockets
    for ( ; it_c != it_end; ++it_c ) {
      if ( it_c->revents != 0 ) {
        _libusb_process_events = true;
        break;
      }
    }
  } else {
    // Poll error
    if ( errno == EINTR ) {
      // Call was interrupted, do nothing
    } else {
      _log.cat ( sev::logt::FL_ERROR,
                 "poll error:\n",
                 sev::strerror_string ( errno ) );
    }
  }
}

void
Thread::process_event_data ( const sev::event::Event & event_n )
{
  auto & cevent = static_cast< const event::in::Data & > ( event_n );
  if ( _libusb_device_good ) {
    _write.tiles_pending.push_back ( cevent.data_tile () );
  } else {
    _write.tiles_done.push_back ( cevent.data_tile () );
  }
}

void
Thread::process_event_abort_stop_io ( const sev::event::Event & event_n )
{
  (void)event_n;

  _log.cat ( sev::logt::FL_DEBUG_0, "Incoming stop IO event." );
  if ( !_abort_state.test_any ( Abort_Event::STOP_IO ) ) {
    _abort_state.set ( Abort_Event::STOP_IO );
    usb_abort ();
  }
}

void
Thread::process_event_abort_shut_down ( const sev::event::Event & event_n )
{
  (void)event_n;

  _log.cat ( sev::logt::FL_DEBUG_0, "Incoming shut down event." );
  // Stop IO must happen before shut down
  DEBUG_ASSERT ( _abort_state.test_any ( Abort_Event::STOP_IO ) );
  _abort_state.set ( Abort_Event::SHUT_DOWN );
}

void
Thread::process_write_return ()
{
  // Return sent (or canceled) data tiles
  auto & epool = _epools.pool< event::out::Data_Sent > ();
  while ( !_write.tiles_done.is_empty () && !epool.is_empty () ) {
    auto * event = epool.pop_not_empty ();
    event->set_data_tile ( _write.tiles_done.front () );
    _write.tiles_done.pop_front ();
    _connection.push ( event );
  }
}

void
Thread::process_libusb ()
{
  // Process USB events
  if ( _libusb_process_events ) {
    _libusb_process_events = false;
    {
      timeval tval;
      tval.tv_sec = 0;
      tval.tv_usec = 0;
      int ret =
          libusb_handle_events_timeout_completed ( _usb_context, &tval, 0 );
      if ( ret != 0 ) {
        usb_error ();
      }
    }
  }

  // Generate USB transfers
  if ( _libusb_device_good ) {
    process_libusb_read_transfers ();
    process_libusb_write_transfers ();
  }
}

void
Thread::process_libusb_read_transfers ()
{
  // Feed read transfers
  while ( !_read.tiles_free.is_empty () && !_read.binds_idle.is_empty () ) {
    Transfer_Bind * tbind = _read.binds_idle.front ();
    _read.binds_idle.pop_front_not_empty ();

    DEBUG_ASSERT ( tbind->data_tile == nullptr );
    tbind->data_tile = _read.tiles_free.front ();
    _read.tiles_free.pop_front_not_empty ();

    // Fill transfer
    libusb_fill_bulk_transfer (
        tbind->usb_transfer,
        _usb_device_handle,
        0x82, // Endpoint address
        reinterpret_cast< unsigned char * > ( tbind->data_tile->data () ),
        tbind->data_tile->capacity (),
        &Thread::lusb_cb_transfer_in_anon,
        this,
        0 );

    // Submit transfer
    int const ret = libusb_submit_transfer ( tbind->usb_transfer );
    if ( ret == 0 ) {
      // Submit success, keep bind
      _read.binds_submitted.push_back ( tbind );
    } else {
      // Submit failed, release bind
      _read.tiles_free.push_front ( tbind->data_tile );
      tbind->data_tile = nullptr;
      _read.binds_idle.push_front ( tbind );
      if ( auto logo = _log.ostr ( sev::logt::FL_WARNING ) ) {
        logo << "USB IN transfer submit error: " << libusb_error_name ( ret );
      }
      usb_error ();
      break;
    }
  }
}

void
Thread::process_libusb_write_transfers ()
{
  // Feed write transfers
  while ( !_write.binds_idle.is_empty () &&
          !_write.tiles_pending.is_empty () ) {
    Transfer_Bind * tbind = _write.binds_idle.front ();
    _write.binds_idle.pop_front_not_empty ();
    auto * tile = _write.tiles_pending.front ();
    _write.tiles_pending.pop_front_not_empty ();

    // Install tile in bind
    tbind->data_tile = tile;

    // Fill usb transfer
    libusb_fill_bulk_transfer (
        tbind->usb_transfer,
        _usb_device_handle,
        0x01, // Endpoint address
        reinterpret_cast< unsigned char * > ( tile->data () ),
        tile->size (),
        &Thread::lusb_cb_transfer_out_anon,
        this, // transfer user data
        0 );

    // Submit usb transfer
    int const ret = libusb_submit_transfer ( tbind->usb_transfer );
    if ( ret == 0 ) {
      // Submit success, keep bind
      _write.binds_submitted.push_back ( tbind );
    } else {
      // Submit error, drop bind
      _write.tiles_done.push_back ( tile );
      _write.binds_idle.push_front ( tbind );
      if ( auto logo = _log.ostr ( sev::logt::FL_WARNING ) ) {
        logo << "USB OUT transfer submit error: " << libusb_error_name ( ret );
      }
      usb_error ();
      break;
    }
  }
}

void
Thread::lusb_cb_transfer_in_anon ( libusb_transfer * transfer_n )
{
  static_cast< Thread * > ( transfer_n->user_data )
      ->lusb_cb_transfer_in ( transfer_n );
}

void
Thread::lusb_cb_transfer_in ( libusb_transfer * transfer_n )
{
  // Get transfer bind
  DEBUG_ASSERT ( !_read.binds_submitted.is_empty () );
  Transfer_Bind * tbind = _read.binds_submitted.extract_if (
      [ transfer_n ] ( const Transfer_Bind & tb_n ) {
        return tb_n.usb_transfer == transfer_n;
      } );
  DEBUG_ASSERT ( tbind != nullptr );

  // Keep data tile and release transfr ind
  auto * data_tile = tbind->data_tile;
  tbind->data_tile = nullptr;
  _read.binds_idle.push_front ( tbind );

  if ( transfer_n->status == LIBUSB_TRANSFER_COMPLETED ) {
    // -- Transfer success
    data_tile->set_size ( transfer_n->actual_length );
    // Submit data event
    {
      auto * event = _epools.pool< event::out::Data_In > ().pop_not_empty ();
      event->set_data_tile ( data_tile );
      _connection.push ( event );
    }
  } else {
    // -- Transfer error
    data_tile->set_size ( 0 );
    _read.tiles_free.push_front ( data_tile );
    log_transfer_error ( "USB IN transfer error", transfer_n );
    usb_error ();
  }
}

void
Thread::lusb_cb_transfer_out_anon ( libusb_transfer * transfer_n )
{
  static_cast< Thread * > ( transfer_n->user_data )
      ->lusb_cb_transfer_out ( transfer_n );
}

void
Thread::lusb_cb_transfer_out ( libusb_transfer * transfer_n )
{
  DEBUG_ASSERT ( !_write.binds_submitted.is_empty () );
  Transfer_Bind * tbind = _write.binds_submitted.extract_if (
      [ transfer_n ] ( const Transfer_Bind & tb_n ) {
        return tb_n.usb_transfer == transfer_n;
      } );
  DEBUG_ASSERT ( tbind != nullptr );

  // Keep data tile and release transfer binf
  auto * data_tile = tbind->data_tile;
  tbind->data_tile = nullptr;
  _write.binds_idle.push_front ( tbind );
  _write.tiles_done.push_back ( data_tile );

  if ( transfer_n->status == LIBUSB_TRANSFER_COMPLETED ) {
    // -- Transfer success
  } else {
    // -- Transfer error
    log_transfer_error ( "USB OUT transfer error", transfer_n );
    usb_error ();
  }
}

void
Thread::lusb_cb_pollfd_added ( int fd_n, short events_n, void * user_data_n )
{
  Thread & cinst = *static_cast< Thread * > ( user_data_n );

  for ( auto & item : cinst._pollfds ) {
    // Check if we know about this fd already
    if ( item.fd == fd_n ) {
      item.events = events_n;
      return;
    }
  }

  // Add fd
  pollfd pfd;
  pfd.fd = fd_n;
  pfd.events = events_n;
  cinst._pollfds.push_back ( pfd );
}

void
Thread::lusb_cb_pollfd_removed ( int fd_n, void * user_data_n )
{
  Thread & cinst = *static_cast< Thread * > ( user_data_n );

  auto it = cinst._pollfds.begin ();
  while ( it != cinst._pollfds.end () ) {
    if ( it->fd == fd_n ) {
      it = cinst._pollfds.erase ( it );
    } else {
      ++it;
    }
  }
}

void
Thread::log_transfer_error ( std::string_view prefix_n,
                             libusb_transfer * transfer_n )
{
  if ( transfer_n->status == LIBUSB_TRANSFER_CANCELLED ) {
    return;
  }
  if ( auto logo = _log.ostr ( sev::logt::FL_WARNING ) ) {
    logo << prefix_n;
    logo << "\n  Status:   "
         << snc::usb::transfer_status_name ( transfer_n->status )
         << "\n  Endpoint: "
         << static_cast< std::uint_fast32_t > ( transfer_n->endpoint )
         << "\n  Length:   "
         << static_cast< std::uint_fast32_t > ( transfer_n->length )
         << "\n  Actual length: "
         << static_cast< std::uint_fast32_t > ( transfer_n->actual_length );
  }
}

} // namespace snc::serial_device::usb
