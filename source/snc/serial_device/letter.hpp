/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::serial_device
{

class Letter
{
  public:
  Letter ();

  virtual ~Letter ();
};

} // namespace snc::serial_device
