/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "letter.hpp"

namespace snc::serial_device
{

Letter::Letter () = default;

Letter::~Letter () = default;

} // namespace snc::serial_device
