/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/serial_device/stats.hpp>

namespace snc::serial_device
{

/** @brief Serial data transfer statistics in two directions.
 */
class Stats_IO
{
  public:
  // -- Types

  using Clock = Stats::Clock;
  using Time_Point = Stats::Time_Point;
  using Duration = Stats::Duration;

  // -- Construction

  Stats_IO () = default;

  ~Stats_IO () = default;

  void
  reset ();

  // -- In

  Stats &
  in ()
  {
    return _in;
  }

  const Stats &
  in () const
  {
    return _in;
  }

  // -- Out

  Stats &
  out ()
  {
    return _out;
  }

  const Stats &
  out () const
  {
    return _out;
  }

  // -- Frame

  void
  begin_frame ( Time_Point tnow_n );

  void
  begin_frame ();

  void
  end_frame ( Time_Point tnow_n );

  void
  end_frame ();

  void
  begin_next_frame ();

  private:
  // -- Attributes
  Stats _in;
  Stats _out;
};

} // namespace snc::serial_device
