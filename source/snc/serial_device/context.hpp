/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/queue_io/link.hpp>
#include <sev/logt/reference.hpp>
#include <sev/thread/tracker.hpp>

namespace snc::serial_device
{

class Context
{
  public:
  // -- Construction
  Context ();

  // -- Attributes
  sev::event::queue_io::Link_2 link;
  sev::thread::Tracker * thread_tracker = nullptr;
  sev::logt::Reference log_parent;
};

} // namespace snc::serial_device
