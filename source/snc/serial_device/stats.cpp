/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stats.hpp"
#include <sev/assert.hpp>

namespace snc::serial_device
{

void
Stats::reset ()
{
  _size_total = 0;
  _size_frame = 0;
  _frame_time_begin = Time_Point ();
  _frame_time_end = Time_Point ();
}

double
Stats::frame_bytes_per_second () const
{
  auto duration = frame_duration ();
  if ( duration.count () == 0 ) {
    return 0.0;
  }
  return ( double ( _size_frame ) / duration_seconds ( duration ) );
}

} // namespace snc::serial_device
