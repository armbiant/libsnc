/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "letter_usb.hpp"

namespace snc::serial_device
{

Letter_USB::Letter_USB () = default;

Letter_USB::~Letter_USB () = default;

} // namespace snc::serial_device
