/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <chrono>
#include <cstdint>

namespace snc::serial_device
{

/** @brief Serial data transfer statistics for a single direction.
 */
class Stats
{
  public:
  // -- Types

  using Clock = std::chrono::steady_clock;
  using Time_Point = Clock::time_point;
  using Duration = Clock::duration;

  // -- Construction

  Stats () = default;

  ~Stats () = default;

  // -- Setup

  /// @brief Reset to construction state
  void
  reset ();

  // -- Total number of bytes

  std::uint_fast64_t
  size_total () const
  {
    return _size_total;
  }

  void
  set_size_total ( std::uint_fast64_t num_n )
  {
    _size_total = num_n;
  }

  void
  increment_size_total ( std::uint_fast64_t delta_n )
  {
    _size_total += delta_n;
  }

  // -- Frame: Number of bytes

  std::uint_fast64_t
  size_frame () const
  {
    return _size_frame;
  }

  void
  set_size_frame ( std::uint_fast64_t num_n )
  {
    _size_frame = num_n;
  }

  void
  increment_size_frame ( std::uint_fast64_t delta_n )
  {
    _size_frame += delta_n;
  }

  // -- Frame times

  const Time_Point &
  frame_time_begin () const
  {
    return _frame_time_begin;
  }

  const Time_Point &
  frame_time_end () const
  {
    return _frame_time_end;
  }

  // -- Frame duration

  Duration
  frame_duration () const
  {
    return ( _frame_time_end - _frame_time_begin );
  }

  double
  frame_duration_seconds () const
  {
    return duration_seconds ( frame_duration () );
  }

  double
  frame_bytes_per_second () const;

  // -- Frame control

  void
  begin_frame ( Time_Point tnow_n )
  {
    _size_frame = 0;
    _frame_time_begin = tnow_n;
  }

  void
  begin_frame ()
  {
    begin_frame ( Clock::now () );
  }

  void
  end_frame ( Time_Point tnow_n )
  {
    _size_total += _size_frame;
    _frame_time_end = tnow_n;
  }

  void
  end_frame ()
  {
    end_frame ( Clock::now () );
  }

  void
  begin_next_frame ()
  {
    _size_frame = 0;
    _frame_time_begin = _frame_time_end;
  }

  // -- Utility

  static double
  duration_seconds ( const Duration & duration_n )
  {
    return std::chrono::duration_cast< std::chrono::duration< double > > (
               duration_n )
        .count ();
  }

  private:
  // -- Attributes
  std::uint_fast64_t _size_total = 0;
  std::uint_fast64_t _size_frame = 0;
  Time_Point _frame_time_begin;
  Time_Point _frame_time_end;
};

} // namespace snc::serial_device
