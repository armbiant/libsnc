/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/read.hpp>

namespace snc::mpath_stream::read
{

template < std::size_t DIM >
class Translate_D : public snc::mpath_stream::Read
{
  public:
  // -- Construction and setup

  Translate_D ();

  Translate_D ( snc::mpath_stream::Read * stream_source_n );

  /// @brief Clears the stream source and translation
  void
  reset ();

  // -- Stream source

  snc::mpath_stream::Read *
  stream_source () const
  {
    return _stream_source;
  }

  void
  set_stream_source ( snc::mpath_stream::Read * stream_n );

  void
  clear_stream_source ();

  // -- Translation

  const sev::lag::Vector< double, DIM > &
  translation () const
  {
    return _translation;
  }

  void
  set_translation ( const sev::lag::Vector< double, DIM > & pos_n );

  void
  reset_translation ();

  // -- Stream interface

  bool
  open () override;

  void
  close () override;

  const snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  snc::mpath_stream::Read * _stream_source = nullptr;
  sev::lag::Vector< double, DIM > _translation;
  snc::mpath::Element_Buffer_D< DIM > _elem_buffer;
};

// -- Types

using Translate_D1 = Translate_D< 1 >;
using Translate_D2 = Translate_D< 2 >;
using Translate_D3 = Translate_D< 3 >;
using Translate_D4 = Translate_D< 4 >;
using Translate_D5 = Translate_D< 5 >;
using Translate_D6 = Translate_D< 6 >;

} // namespace snc::mpath_stream::read
