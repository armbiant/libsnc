/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "translate_d.hpp"
#include <sev/assert.hpp>
#include <snc/mpath/elem/curve_d.hpp>
#include <snc/mpath/elem/type_test.hpp>

namespace snc::mpath_stream::read
{

template < std::size_t DIM >
Translate_D< DIM >::Translate_D ()
: _translation ( sev::lag::init::zero )
{
}

template < std::size_t DIM >
Translate_D< DIM >::Translate_D ( snc::mpath_stream::Read * stream_source_n )
: _stream_source ( stream_source_n )
, _translation ( sev::lag::init::zero )
{
}

template < std::size_t DIM >
void
Translate_D< DIM >::set_stream_source (
    snc::mpath_stream::Read * stream_source_n )
{
  close ();
  _stream_source = stream_source_n;
}

template < std::size_t DIM >
void
Translate_D< DIM >::clear_stream_source ()
{
  set_stream_source ( nullptr );
}

template < std::size_t DIM >
void
Translate_D< DIM >::set_translation (
    const sev::lag::Vector< double, DIM > & pos_n )
{
  _translation = pos_n;
}

template < std::size_t DIM >
void
Translate_D< DIM >::reset_translation ()
{
  _translation.fill ( 0.0 );
}

template < std::size_t DIM >
bool
Translate_D< DIM >::open ()
{
  if ( is_open () ) {
    return true;
  }
  if ( _stream_source == nullptr ) {
    return false;
  }

  _stream_source->open ();
  set_state ( _stream_source->state () );
  return is_open ();
}

template < std::size_t DIM >
void
Translate_D< DIM >::close ()
{
  if ( !is_open () ) {
    return;
  }

  _stream_source->close ();
  set_state_closed ();
}

template < std::size_t DIM >
const snc::mpath::Element *
Translate_D< DIM >::read ()
{
  DEBUG_ASSERT ( is_open () );

  const snc::mpath::Element * elem_src = _stream_source->read ();
  DEBUG_ASSERT ( elem_src != nullptr );
  set_state ( _stream_source->state () );

  // Not a curve?
  if ( !snc::mpath::elem::is_curve< DIM > ( *elem_src ) ) {
    return elem_src;
  }

  // Create local copy, translate and return it
  auto & curve_copy = static_cast< snc::mpath::elem::Curve_D< DIM > & > (
      _elem_buffer.copy ( *elem_src ) );
  snc::mpath::elem::curve_translate_d< DIM > ( curve_copy, _translation );
  return &curve_copy;
}

// -- Instantiation

template class Translate_D< 1 >;
template class Translate_D< 2 >;
template class Translate_D< 3 >;
template class Translate_D< 4 >;
template class Translate_D< 5 >;
template class Translate_D< 6 >;

} // namespace snc::mpath_stream::read
