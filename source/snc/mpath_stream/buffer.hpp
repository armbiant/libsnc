/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/rails/type_rails.hpp>
#include <snc/mpath/element.hpp>
#include <snc/mpath/element_buffer_d.hpp>

namespace snc::mpath_stream
{

/// @brief Stores a stream of snc::mpath::Element items
///
class Buffer
{
  public:
  // -- Types
  using Storage = sev::mem::rails::Type_Rails< snc::mpath::Element >;
  using iterator = Storage::iterator;
  using const_Iterator = Storage::const_iterator;
  static const std::uint_fast32_t default_rail_capacity = 1024 * 1024;
  static const std::uint_fast32_t default_pool_capacity = 0;

  // -- Construction

  Buffer ();

  // -- Interface

  /// @brief Clears all elements from the buffer
  ///
  void
  clear ();

  /// @brief Number of stored elements
  sev::mem::rails::Rails::size_type
  size () const
  {
    return _rails.size ();
  }

  bool
  is_empty () const
  {
    return _rails.is_empty ();
  }

  void
  push_back ( const snc::mpath::Element & elem_n )
  {
    _rails.push_back ( { reinterpret_cast< const std::byte * > ( &elem_n ),
                         elem_n.elem_size () } );
  }

  void
  pop_front ()
  {
    _rails.pop_front ();
  }

  void
  pop_back ()
  {
    _rails.pop_back ();
  }

  // -- Iteration

  const_Iterator
  begin () const
  {
    return _rails.begin ();
  }

  const_Iterator
  end () const
  {
    return _rails.end ();
  }

  private:
  // -- Attributes
  Storage _rails;
};

} // namespace snc::mpath_stream
