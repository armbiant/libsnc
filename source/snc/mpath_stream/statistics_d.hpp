/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <array>

namespace snc::mpath_stream
{

template < std::size_t DIM >
class Statistics_D
{
  public:
  // -- Construction and setup
  Statistics_D ();

  void
  reset ();

  public:
  // -- Attributes
  std::uint_fast64_t num_elements = 0;
  std::uint_fast64_t num_elements_curve = 0;
  double length = 0.0;
  sev::lag::Vector< double, DIM > curve_pos_begin;
  sev::lag::Vector< double, DIM > curve_pos_end;
  /// @brief Curve bounding box
  std::array< sev::lag::Vector< double, DIM >, 2 > curve_bbox;
};

// -- Types

using Statistics_D1 = Statistics_D< 1 >;
using Statistics_D2 = Statistics_D< 2 >;
using Statistics_D3 = Statistics_D< 3 >;
using Statistics_D4 = Statistics_D< 4 >;
using Statistics_D5 = Statistics_D< 5 >;
using Statistics_D6 = Statistics_D< 6 >;

} // namespace snc::mpath_stream
