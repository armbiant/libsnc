/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>

namespace snc::mpath_stream
{

/// @brief An output stream for ::mpath::Element objects
///
class Write
{
  public:
  // -- Construction

  Write ();

  virtual ~Write ();

  // -- Interface

  /// @brief Open the stream
  /// @return True on success
  virtual bool
  open () = 0;

  virtual bool
  is_open () = 0;

  /// @brief Writes an element to the stream
  /// @return True on success
  virtual bool
  write ( const snc::mpath::Element & elem_n ) = 0;

  virtual void
  close () = 0;
};

} // namespace snc::mpath_stream
