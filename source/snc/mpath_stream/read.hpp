/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <cstdint>

namespace snc::mpath_stream
{

/// @brief An input stream for ::mpath::Element objects
///
class Read
{
  public:
  // -- Types

  enum class State : std::uint8_t
  {
    /// @brief Stream is closed
    CLOSED,
    /// @brief Stream is open but has no more elements
    FINISHED,
    /// @brief Stream is open and more elements are available
    GOOD
  };

  // -- Construction

  Read ();

  virtual ~Read ();

  // -- Interface

  /// @brief Open stream
  /// @return True on success
  virtual bool
  open () = 0;

  /// @brief Close stream
  /// @return True on success
  virtual void
  close () = 0;

  /// @brief Iterates to next element and updates token()
  virtual const snc::mpath::Element *
  read () = 0;

  // -- Static interface

  /// @brief Current state token
  const State &
  state () const
  {
    return _state;
  }

  /// @return True if the stream is open
  bool
  is_open () const
  {
    return ( _state != State::CLOSED );
  }

  /// @return True if the stream is open and more elements are available
  bool
  is_good () const
  {
    return ( _state == State::GOOD );
  }

  /// @return True if the stream is open but mo more elements are available
  bool
  is_finished () const
  {
    return ( _state == State::FINISHED );
  }

  protected:
  // -- Utility

  void
  set_state ( State state_n )
  {
    _state = state_n;
  }

  void
  set_state_closed ()
  {
    set_state ( State::CLOSED );
  }

  void
  set_state_finished ()
  {
    set_state ( State::FINISHED );
  }

  void
  set_state_good ()
  {
    set_state ( State::GOOD );
  }

  private:
  // -- Attributes
  State _state = State::CLOSED;
};

} // namespace snc::mpath_stream
