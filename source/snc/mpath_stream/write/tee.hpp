/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/write.hpp>
#include <array>

namespace snc::mpath_stream::write
{

template < std::size_t TNUM >
class Tee : public snc::mpath_stream::Write
{
  public:
  // -- Construction and setup

  Tee ();

  void
  reset ();

  // -- ostream

  snc::mpath_stream::Write *
  ostream ( std::size_t index_n ) const
  {
    return _ostreams[ index_n ];
  }

  void
  set_ostream ( std::size_t index_n, snc::mpath_stream::Write * ostream_n )
  {
    _ostreams[ index_n ] = ostream_n;
  }

  // --- Abstract interface

  bool
  open () override;

  bool
  is_open () override;

  bool
  write ( const snc::mpath::Element & elem_n ) override;

  void
  close () override;

  private:
  // -- Attributes
  std::array< snc::mpath_stream::Write *, TNUM > _ostreams;
};

// -- Types

using Tee1 = Tee< 1 >;
using Tee2 = Tee< 2 >;
using Tee3 = Tee< 3 >;
using Tee4 = Tee< 4 >;
using Tee5 = Tee< 5 >;
using Tee6 = Tee< 6 >;

} // namespace snc::mpath_stream::write
