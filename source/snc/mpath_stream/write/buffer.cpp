/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "buffer.hpp"

namespace snc::mpath_stream::write
{

Buffer::Buffer () {}

Buffer::Buffer ( snc::mpath_stream::Buffer * buffer_n )
: _buffer ( buffer_n )
{
}

void
Buffer::set_buffer ( snc::mpath_stream::Buffer * buffer_n )
{
  close ();
  _buffer = buffer_n;
}

bool
Buffer::open ()
{
  if ( _is_open ) {
    return true;
  }
  if ( _buffer != nullptr ) {
    _is_open = true;
  }
  return _is_open;
}

bool
Buffer::is_open ()
{
  return _is_open;
}

void
Buffer::close ()
{
  if ( !_is_open ) {
    return;
  }
  _is_open = false;
}

bool
Buffer::write ( const snc::mpath::Element & elem_n )
{
  if ( !_is_open ) {
    return false;
  }
  _buffer->push_back ( elem_n );
  return true;
}

} // namespace snc::mpath_stream::write
