/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "translate_d.hpp"
#include <snc/mpath/elem/curve_d.hpp>
#include <snc/mpath/elem/type_test.hpp>

namespace snc::mpath_stream::write
{

template < std::size_t DIM >
Translate_D< DIM >::Translate_D ()
: _translation ( sev::lag::init::zero )
{
}

template < std::size_t DIM >
void
Translate_D< DIM >::set_stream_target (
    snc::mpath_stream::Write * stream_target_n )
{
  close ();
  _stream_target = stream_target_n;
}

template < std::size_t DIM >
void
Translate_D< DIM >::clear_stream_target ()
{
  set_stream_target ( nullptr );
}

template < std::size_t DIM >
void
Translate_D< DIM >::set_translation (
    const sev::lag::Vector< double, DIM > & pos_n )
{
  _translation = pos_n;
}

template < std::size_t DIM >
void
Translate_D< DIM >::reset_translation ()
{
  _translation.fill ( 0.0 );
}

template < std::size_t DIM >
bool
Translate_D< DIM >::open ()
{
  if ( !_is_open ) {
    if ( _stream_target != nullptr ) {
      _is_open = _stream_target->open ();
    }
  }
  return _is_open;
}

template < std::size_t DIM >
bool
Translate_D< DIM >::is_open ()
{
  return _is_open;
}

template < std::size_t DIM >
void
Translate_D< DIM >::close ()
{
  if ( _is_open ) {
    _stream_target->close ();
    _is_open = false;
  }
}

template < std::size_t DIM >
bool
Translate_D< DIM >::write ( const snc::mpath::Element & elem_n )
{
  if ( !_is_open ) {
    return false;
  }

  if ( !snc::mpath::elem::is_curve< DIM > ( elem_n ) ) {
    // Write source element
    return _stream_target->write ( elem_n );
  }

  // Create local copy, translate it and write to sink
  _elem_buffer.copy ( elem_n );
  {
    auto & curve = static_cast< snc::mpath::elem::Curve_D< DIM > & > (
        _elem_buffer.elem () );
    snc::mpath::elem::curve_translate_d< DIM > ( curve, _translation );
  }
  return _stream_target->write ( _elem_buffer.elem () );
}

// -- Instantiation

template class Translate_D< 1 >;
template class Translate_D< 2 >;
template class Translate_D< 3 >;
template class Translate_D< 4 >;
template class Translate_D< 5 >;
template class Translate_D< 6 >;

} // namespace snc::mpath_stream::write
