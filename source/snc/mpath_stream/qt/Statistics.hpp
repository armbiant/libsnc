/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/qt/StatisticsAxes.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <QObject>
#include <cstdint>
#include <memory>

namespace snc::mpath_stream::qt
{

class Statistics : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( QAbstractListModel * axes READ axes NOTIFY axesChanged )
  Q_PROPERTY ( double length READ length NOTIFY lengthChanged )

  public:
  // -- Construction

  Statistics ( QObject * qparent_n = nullptr );

  ~Statistics ();

  // -- Setup

  Q_INVOKABLE
  void
  clear ();

  Q_INVOKABLE
  void
  reset ();

  void
  update ( const snc::mpath_stream::Statistics_D2 & stats_n );

  void
  update ( const snc::mpath_stream::Statistics_D3 & stats_n );

  void
  update ( const snc::mpath_stream::Statistics_D4 & stats_n );

  // -- Axes

  snc::mpath_stream::qt::StatisticsAxes *
  axes ()
  {
    return &_axes;
  }

  Q_SIGNAL
  void
  axesChanged ();

  // -- Length

  double
  length () const
  {
    return _length;
  }

  Q_SIGNAL
  void
  lengthChanged ();

  private:
  // -- Utility

  template < std::size_t DIM >
  void
  updateImpl ( const snc::mpath_stream::Statistics_D< DIM > & stats_n );

  void
  set_length ( double length_n );

  private:
  // -- Attributes
  StatisticsAxes _axes;
  double _length = 0.0;
};

} // namespace snc::mpath_stream::qt
