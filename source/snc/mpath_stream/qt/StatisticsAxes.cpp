/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "StatisticsAxes.hpp"

namespace snc::mpath_stream::qt
{

StatisticsAxes::StatisticsAxes ( QObject * qparent_n )
: QAbstractListModel ( qparent_n )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::axisObject, "axisObject" );
}

StatisticsAxes::~StatisticsAxes () = default;

void
StatisticsAxes::clear ()
{
  ensure_dimensions_max ( 0 );
}

void
StatisticsAxes::reset ()
{
  for ( auto & axis : _axes ) {
    axis->reset ();
  }
}

void
StatisticsAxes::ensure_dimensions_min ( std::size_t dimensions_n )
{
  if ( _axes.size () >= dimensions_n ) {
    return;
  }
  // Create items
  beginInsertRows ( QModelIndex (), _axes.size (), dimensions_n - 1 );
  _axes.reserve ( dimensions_n );
  while ( _axes.size () != dimensions_n ) {
    _axes.emplace_back ( std::make_unique< Axis > ( this ) );
  }
  endInsertRows ();
}

void
StatisticsAxes::ensureDimensionsMin ( int dimensions_n )
{
  if ( dimensions_n >= 0 ) {
    ensure_dimensions_min ( dimensions_n );
  }
}

void
StatisticsAxes::ensure_dimensions_max ( std::size_t dimensions_n )
{
  if ( _axes.size () <= dimensions_n ) {
    return;
  }
  // Remove items
  beginRemoveRows ( QModelIndex (), dimensions_n, _axes.size () - 1 );
  _axes.resize ( dimensions_n );
  endRemoveRows ();
}

void
StatisticsAxes::ensureDimensionsMax ( int dimensions_n )
{
  if ( dimensions_n >= 0 ) {
    ensure_dimensions_max ( dimensions_n );
  }
}

void
StatisticsAxes::setDimensions ( int dimensions_n )
{
  ensureDimensionsMin ( dimensions_n );
  ensureDimensionsMax ( dimensions_n );
}

snc::mpath_stream::qt::StatisticsAxis *
StatisticsAxes::get ( int index_n ) const
{
  if ( index_n < 0 ) {
    return nullptr;
  }
  const std::size_t idx = index_n;
  if ( idx >= _axes.size () ) {
    return nullptr;
  }
  return _axes[ idx ].get ();
}

snc::mpath_stream::qt::StatisticsAxis *
StatisticsAxes::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    return nullptr;
  }
  const std::size_t idx = index_n;
  ensure_dimensions_min ( idx + 1 );
  return _axes[ idx ].get ();
}

QHash< int, QByteArray >
StatisticsAxes::roleNames () const
{
  return _roleNames;
}

int
StatisticsAxes::rowCount ( const QModelIndex & parent_n ) const
{
  if ( parent_n.isValid () ) {
    return 0;
  }
  return _axes.size ();
}

QVariant
StatisticsAxes::data ( const QModelIndex & index_n,
                       int role_n [[maybe_unused]] ) const
{
  if ( index_n.parent ().isValid () || ( index_n.column () != 0 ) ||
       ( index_n.row () < 0 ) ||
       ( static_cast< std::size_t > ( index_n.row () ) >= _axes.size () ) ) {
    return QVariant ();
  }

  switch ( role_n ) {
  case ExtraRole::axisObject:
    return QVariant::fromValue (
        static_cast< QObject * > ( _axes[ index_n.row () ].get () ) );
  default:
    break;
  }

  return QVariant ();
}

} // namespace snc::mpath_stream::qt
