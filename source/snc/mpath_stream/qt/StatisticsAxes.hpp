/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/qt/StatisticsAxis.hpp>
#include <QAbstractListModel>
#include <memory>
#include <vector>

namespace snc::mpath_stream::qt
{

class StatisticsAxes : public QAbstractListModel
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int dimensions READ dimensions WRITE setDimensions NOTIFY
                   dimensionsChanged )

  // -- Types

  using Axis = snc::mpath_stream::qt::StatisticsAxis;

  enum ExtraRole
  {
    axisObject = Qt::UserRole
  };

  public:
  // -- Construction

  StatisticsAxes ( QObject * qparent_n = nullptr );

  ~StatisticsAxes ();

  // -- Setup

  Q_INVOKABLE
  void
  clear ();

  Q_INVOKABLE
  void
  reset ();

  // -- Dimensions

  int
  dimensions () const
  {
    return _axes.size ();
  }

  Q_SIGNAL void
  dimensionsChanged ();

  void
  ensure_dimensions_min ( std::size_t size_n );

  Q_INVOKABLE
  void
  ensureDimensionsMin ( int dimensions_n );

  void
  ensure_dimensions_max ( std::size_t size_n );

  Q_INVOKABLE
  void
  ensureDimensionsMax ( int dimensions_n );

  void
  setDimensions ( int dimensions_n );

  // -- Axis get or create

  Q_INVOKABLE
  snc::mpath_stream::qt::StatisticsAxis *
  get ( int index_n ) const;

  Q_INVOKABLE
  snc::mpath_stream::qt::StatisticsAxis *
  getOrCreate ( int index_n );

  // -- Axes

  const std::vector< std::unique_ptr< Axis > > &
  axes () const
  {
    return _axes;
  }

  const Axis &
  operator[] ( std::size_t index_n ) const
  {
    return *_axes[ index_n ];
  }

  Axis &
  operator[] ( std::size_t index_n )
  {
    return *_axes[ index_n ];
  }

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n, int role_n ) const override;

  private:
  // -- Attributes
  std::vector< std::unique_ptr< Axis > > _axes;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::mpath_stream::qt
