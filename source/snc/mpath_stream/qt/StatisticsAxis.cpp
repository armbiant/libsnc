/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "StatisticsAxis.hpp"

namespace snc::mpath_stream::qt
{

StatisticsAxis::StatisticsAxis ( QObject * qparent_n )
: QObject ( qparent_n )
{
}

StatisticsAxis::~StatisticsAxis () = default;

void
StatisticsAxis::reset ()
{
  setMinMax ( 0.0, 0.0 );
}

void
StatisticsAxis::setMinMax ( double min_n, double max_n )
{
  double length = ( max_n - min_n );
  bool length_change = ( _length != length );
  bool min_change = ( _min != min_n );
  bool max_change = ( _max != max_n );

  _min = min_n;
  _max = max_n;
  _length = length;

  if ( min_change ) {
    emit minChanged ();
  }
  if ( max_change ) {
    emit maxChanged ();
  }
  if ( length_change ) {
    emit lengthChanged ();
  }
}

} // namespace snc::mpath_stream::qt
