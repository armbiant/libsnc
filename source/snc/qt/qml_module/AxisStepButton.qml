import QtQuick 2.11

ArrowButton {
  property var axis
  property real value: 0.0
  property bool forward: false
  property bool reverse: false
  property int pxLength: 32

  implicitWidth: pxLength
  implicitHeight: pxLength

  arrowAngle: forward ? 0.0 : 180.0
  enabled: axis && axis.move.available

  function step(steps) {
    if ( !axis ) {
      return
    }
    if ( axis.info.isRotational ) {
      axis.move.moveDegrees(value * steps);
    } else {
      axis.move.moveLength(value * steps);
    }
  }

  onPressed: step(reverse ? -1 : 1)

  MouseArea {
    anchors.fill: parent
    onWheel: {
        var ticks = ( wheel.angleDelta.y / 120.0 )
        step(ticks)
    }
    // propogate
    onPressed: { mouse.accepted = false; }
    onReleased: { mouse.accepted = false; }
  }
}
