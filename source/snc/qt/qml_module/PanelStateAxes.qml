import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

GridLayout {
  id: root
  property var model: []

  columns: 1 + model.length
  columnSpacing: 16

  HeadLabelV {
    text: qsTr("Axis")
  }
  Repeater {
    model: root.model
    Label {
      text: modelData.info.name
      Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
    }
  }

  HeadLabelV {
    text: qsTr("Position")
  }
  Repeater {
    model: root.model
    NumberLabel {
      precision: 3
      value: modelData.info.state.position
      valueMax: modelData.info.length
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }
  }

  HeadLabelV {
    text: qsTr("Speed")
  }
  Repeater {
    model: root.model
    NumberLabel {
      precision: 3
      value: modelData.info.state.speed
      valueMin: -modelData.info.speedMax
      valueMax: modelData.info.speedMax
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }
  }
}
