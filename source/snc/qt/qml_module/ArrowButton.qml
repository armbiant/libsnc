import QtQuick 2.11
import QtQuick.Controls 2.4

Button {
  property real arrowAngle: 0.0
  icon.source: {
    if (( arrowAngle >= 315.0 ) || ( arrowAngle <= 45.0 )) {
      return "qrc:///graphics/icons/StepArrowUp.svg"
    }
    if (( arrowAngle > 45.0 ) && ( arrowAngle < 135.0 )) {
      return "qrc:///graphics/icons/StepArrowRight.svg"
    }
    if (( arrowAngle > 225.0 ) && ( arrowAngle < 315.0 )) {
      return "qrc:///graphics/icons/StepArrowLeft.svg"
    }
    return "qrc:///graphics/icons/StepArrowDown.svg"
  }
  icon.color: enabled ? "transparent" : palette.buttonText
}
