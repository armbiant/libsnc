import QtQuick 2.11
import QtQuick.Controls 2.4

ListView {
  id: view
  model: threadsListModel

  Component.onCompleted: threadsListModel.update()

  Timer {
    interval: 100; running: view.visible; repeat: true
    onTriggered: threadsListModel.update()
  }

  Component {
    id: contentDelegate
    Column {
      property var trd: thread

      Label {
        id: label
        text: trd ? trd.name : "?"
        enabled: trd ? trd.running : false
      }

      Item {
        id: loadBar
        implicitHeight: 2
        implicitWidth: view.width
        Rectangle {
          color: label.color
          anchors.top: loadBar.top
          anchors.left: loadBar.left
          height: 2
          width: (trd ? trd.load : 0.0)*loadBar.width
        }
      }
    }
  }

  delegate: contentDelegate
  clip: true
}
