import QtQml 2.11
import QtQuick.Extras 1.4

StatusIndicator {
  property int size: SampleLabel.implicitHeight

  implicitWidth: size
  implicitHeight: size
}
