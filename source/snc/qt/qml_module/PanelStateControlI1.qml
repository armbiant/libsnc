StatusLight {
  property var control

  color: "darkOrange"
  active: control && control.state.value
}
