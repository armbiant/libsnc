import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

GridLayout {
  id: root
  property var model

  columns: 3
  columnSpacing: 8

  Layout.fillWidth: false

  Repeater {
    model: root.model
    Label {
      Layout.row: index
      Layout.column: 0
      text: listItem.name
    }
  }

  Repeater {
    model: root.model
    PanelStateControlI1 {
      Layout.row: index
      Layout.column: 1
      control: listItem
    }
  }
}
