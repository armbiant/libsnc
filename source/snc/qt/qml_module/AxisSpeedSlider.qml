import QtQuick.Controls 2.4

Slider {
  property var axis
  orientation: Qt.Vertical
  from: 0.0
  to: 0.0
  value: 0.0
  enabled: axis.speed.available

  onPressedChanged: {
    if ( !pressed ) {
      value = from
      axis.speed.breakMotion()
    }
  }

  onValueChanged: {
    if ( pressed ) {
      axis.speed.moveAtSpeed(value);
    }
  }
}
