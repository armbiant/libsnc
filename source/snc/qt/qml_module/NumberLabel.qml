import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Item {
  property var locale: Qt.locale()
  property var format: "f"
  property int precision: 2

  property real value: 0.0
  property real valueMin: 0.0
  property real valueMax: 0.0

  implicitWidth: Math.max(
    displayLabel.implicitWidth,
    hiddenMin.implicitWidth,
    hiddenMax.implicitWidth )
  implicitHeight: Math.max(
    displayLabel.implicitHeight,
    hiddenMin.implicitHeight,
    hiddenMax.implicitHeight)

  function valueString( value ) {
    return value.toLocaleString( locale, format, precision )
  }

  TextInput {
    id: displayLabel
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    font.family: "monospace"
    readOnly: true
    selectByMouse: true
    text: valueString(value)
  }
  Label {
    id: hiddenMin
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    font.family: "monospace"
    text: valueString(valueMin)
    opacity: 0.0
  }
  Label {
    id: hiddenMax
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    font.family: "monospace"
    text: valueString(valueMax)
    opacity: 0.0
  }
}
