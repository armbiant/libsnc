/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <QString>
#include <vector>

namespace snc::qt
{

void
qml_register_types ();

std::vector< QString >
qml_import_paths ();

} // namespace snc::qt
