/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector.hpp>
#include <sev/mem/view.hpp>
#include <QAbstractListModel>
#include <memory>
#include <vector>

namespace snc::qt
{

class VectorModelItem : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( double value READ value NOTIFY valueChanged )

  public:
  // -- Construction

  VectorModelItem ( QObject * qparent_n = nullptr, double value_n = 0.0 );

  ~VectorModelItem ();

  // -- Value accessor

  double
  value () const
  {
    return _value;
  }

  void
  setValue ( double value_n );

  Q_SIGNAL
  void
  valueChanged ();

  private:
  // -- Attributes
  double _value = 0.0;
};

class VectorModel : public QAbstractListModel
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int dimensions READ dimensions WRITE setDimensions NOTIFY
                   dimensionsChanged )

  // -- Types

  enum ExtraRole
  {
    axisObject = Qt::UserRole
  };

  public:
  // -- Construction

  VectorModel ( QObject * qparent_n = nullptr );

  ~VectorModel ();

  // -- Dimensions

  int
  dimensions () const
  {
    return _items.size ();
  }

  Q_SIGNAL void
  dimensionsChanged ();

  void
  ensure_dimensions_min ( std::size_t size_n );

  Q_INVOKABLE
  void
  ensureDimensionsMin ( int dimensions_n );

  void
  ensure_dimensions_max ( std::size_t size_n );

  Q_INVOKABLE
  void
  ensureDimensionsMax ( int dimensions_n );

  void
  setDimensions ( int dimensions_n );

  // -- Get or create

  Q_INVOKABLE
  snc::qt::VectorModelItem *
  get ( int index_n ) const;

  Q_INVOKABLE
  snc::qt::VectorModelItem *
  getOrCreate ( int index_n );

  Q_INVOKABLE
  double
  getValue ( int index_n ) const;

  Q_INVOKABLE
  void
  setValue ( int index_n, double value_n );

  // -- Setting

  template < std::size_t DIM >
  void
  update ( const sev::lag::Vector< double, DIM > & vec_n )
  {
    update ( sev::mem::View< const double > ( vec_n.data (), DIM ) );
  }

  void
  update ( sev::mem::View< const double > data_n );

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n, int role_n ) const override;

  private:
  // -- Attributes
  std::vector< std::unique_ptr< VectorModelItem > > _items;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::qt
