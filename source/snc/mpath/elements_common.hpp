/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

// -- Elements common for all dimensions
#include <snc/mpath/elem/curve_cubic_d.hpp>
#include <snc/mpath/elem/curve_linear_d.hpp>
#include <snc/mpath/elem/fader.hpp>
#include <snc/mpath/elem/sleep.hpp>
#include <snc/mpath/elem/speed.hpp>
#include <snc/mpath/elem/speed_db_value.hpp>
#include <snc/mpath/elem/stream_curve_finish.hpp>
#include <snc/mpath/elem/stream_end.hpp>
