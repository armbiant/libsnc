/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "speed_database.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>

namespace snc::mpath
{

Speed_Database::Speed_Database ()
{
  reset ();
}

void
Speed_Database::reset ()
{
  _speeds.fill ( 0.0 );
}

void
Speed_Database::set_speed ( Speed_Db_Id id_n, double speed_n )
{
  auto index = static_cast< std::size_t > ( id_n );
  if ( index >= _speeds.size () ) {
    return;
  }

  sev::math::assign_larger< double > ( speed_n, 0.0 );
  _speeds[ index ] = speed_n;
}

bool
Speed_Database::change_speed ( Speed_Db_Id id_n, double speed_n )
{
  auto index = static_cast< std::size_t > ( id_n );
  if ( index >= _speeds.size () ) {
    return false;
  }

  sev::math::assign_larger< double > ( speed_n, 0.0 );
  return sev::change ( _speeds[ index ], speed_n );
}

void
Speed_Database::sanitized_maximum_speeds ()
{
  sev::math::assign_smaller< double > ( speed_ref ( Speed_Db_Id::MATERIAL ),
                                        speed ( Speed_Db_Id::RAPID ) );
  sev::math::assign_smaller< double > (
      speed_ref ( Speed_Db_Id::MATERIAL_PLANAR ),
      speed ( Speed_Db_Id::MATERIAL ) );
  sev::math::assign_smaller< double > (
      speed_ref ( Speed_Db_Id::MATERIAL_NORMAL ),
      speed ( Speed_Db_Id::MATERIAL ) );
}

void
Speed_Database::crop_speeds ( double speed_min_n, double speed_max_n )
{
  for ( auto & speed : _speeds ) {
    sev::math::range_crop< double > ( speed, speed_min_n, speed_max_n );
  }
}

void
Speed_Database::crop_planar_speeds ( double speed_min_n, double speed_max_n )
{
  sev::math::range_crop< double > (
      speed_ref ( Speed_Db_Id::MATERIAL_PLANAR ), speed_min_n, speed_max_n );
}

void
Speed_Database::crop_normal_speeds ( double speed_min_n, double speed_max_n )
{
  sev::math::range_crop< double > (
      speed_ref ( Speed_Db_Id::MATERIAL_NORMAL ), speed_min_n, speed_max_n );
}

bool
Speed_Database::operator== ( const Speed_Database & speed_db_n ) const
{
  for ( std::size_t ii = 0; ii != _speeds.size (); ++ii ) {
    if ( _speeds[ ii ] != speed_db_n._speeds[ ii ] ) {
      return false;
    }
  }
  return true;
}

bool
Speed_Database::operator!= ( const Speed_Database & speed_db_n ) const
{
  for ( std::size_t ii = 0; ii != _speeds.size (); ++ii ) {
    if ( _speeds[ ii ] != speed_db_n._speeds[ ii ] ) {
      return true;
    }
  }
  return false;
}

} // namespace snc::mpath
