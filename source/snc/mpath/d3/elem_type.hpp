/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/elem/type.hpp>

namespace snc::mpath::d3::elem
{

/// @brief Element type namespace struct
struct Type
{
  static constexpr std::uint_fast32_t LIST_BEGIN =
      snc::mpath::elem::Type::LIST_END;

  // -- Curves
  static constexpr std::uint_fast32_t CURVE_LINEAR = LIST_BEGIN + 0;
  static constexpr std::uint_fast32_t CURVE_CUBIC = LIST_BEGIN + 1;
  static constexpr std::uint_fast32_t CURVE_CIRCLE = LIST_BEGIN + 2;
  static constexpr std::uint_fast32_t CURVE_HELIX = LIST_BEGIN + 3;

  // -- Element list end
  static constexpr std::uint_fast32_t LIST_END = LIST_BEGIN + 4;

  // -- Range marker: Curves
  static constexpr std::uint_fast32_t CURVES_BEGIN = CURVE_LINEAR;
  static constexpr std::uint_fast32_t CURVES_LAST = CURVE_HELIX;
};

inline bool
type_is_valid ( std::uint_fast32_t elem_type_n )
{
  return ( ( elem_type_n > snc::mpath::elem::Type::INVALID ) &&
           ( elem_type_n < Type::LIST_END ) );
}

inline bool
type_is_curve ( std::uint_fast32_t elem_type_n )
{
  return ( ( elem_type_n >= Type::CURVES_BEGIN ) &&
           ( elem_type_n <= Type::CURVES_LAST ) );
}

} // namespace snc::mpath::d3::elem
