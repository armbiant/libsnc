/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector2.hpp>
#include <sev/lag/vector3.hpp>
#include <snc/mpath/d3/elem_type.hpp>
#include <snc/mpath/elem/curve_d.hpp>

namespace snc::mpath::d3::elem
{

class Curve_Circle : public snc::mpath::elem::Curve_D< 3 >
{
  public:
  // -- Types

  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::d3::elem::Type::CURVE_CIRCLE;

  // -- Construction

  Curve_Circle ();

  // -- Center

  const sev::lag::Vector2d &
  pos_center () const
  {
    return _pos_center;
  }

  void
  set_pos_center ( const sev::lag::Vector2d & pos_n )
  {
    _pos_center = pos_n;
  }

  // -- Angle

  double
  angle_radians () const
  {
    return _angle_radians;
  }

  void
  set_angle_radians ( double angle_n )
  {
    _angle_radians = angle_n;
  }

  // -- Abstract interface

  void
  translate ( const sev::lag::Vector3d & delta_n );

  private:
  // -- Attributes
  sev::lag::Vector2d _pos_center;
  double _angle_radians;
};

} // namespace snc::mpath::d3::elem
