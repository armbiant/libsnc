/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "curve_helix.hpp"
#include <sev/math/numbers.hpp>

namespace snc::mpath::d3::elem
{

Curve_Helix::Curve_Helix ()
: snc::mpath::elem::Curve_D< 3 > ( class_elem_type, sizeof ( Curve_Helix ) )
, _angle_radians ( 0.0 )
, _height ( 0.0 )
, _pos_center ( sev::lag::init::zero )
{
}

void
Curve_Helix::translate ( const sev::lag::Vector3d & delta_n )
{
  _pos_center[ 0 ] += delta_n[ 0 ];
  _pos_center[ 1 ] += delta_n[ 1 ];
}

} // namespace snc::mpath::d3::elem
