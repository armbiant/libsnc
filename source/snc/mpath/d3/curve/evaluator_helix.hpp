/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath/curve_evaluator_d.hpp>
#include <snc/mpath/d3/elem/curve_helix.hpp>

namespace snc::mpath::d3::curve
{

/// @brief Abstract base class for curve evaluators
///
class Evaluator_Helix : public snc::mpath::Curve_Evaluator_D< 3 >
{
  public:
  // -- Construction

  Evaluator_Helix ( const sev::lag::Vector3d & pos_begin_n,
                    const snc::mpath::d3::elem::Curve_Helix & curve_n );

  // -- Accessors

  const sev::lag::Vector2d &
  pos_center () const
  {
    return _pos_center;
  }

  double
  angle_radians_offset () const
  {
    return _angle_radians_offset;
  }

  double
  angle_radians () const
  {
    return _angle_radians;
  }

  double
  radius () const
  {
    return _radius;
  }

  double
  height () const
  {
    return _height;
  }

  // -- Abstract interface

  void
  calc_pos ( sev::lag::Vector3d & res_n, double tval_n ) override;

  void
  calc_end_pos ( sev::lag::Vector3d & res_n ) override;

  void
  calc_tangent ( sev::lag::Vector3d & res_n, double tval_n ) override;

  double
  calc_length () override;

  void
  calc_extents ( sev::lag::Vector3d & min_n,
                 sev::lag::Vector3d & max_n ) override;

  private:
  // -- Attributes
  using snc::mpath::Curve_Evaluator_D< 3 >::_length_approx;
  using snc::mpath::Curve_Evaluator_D< 3 >::_pos_begin;

  sev::lag::Vector2d _pos_center;
  double _angle_radians_offset;
  double _angle_radians;
  double _radius;
  double _height;
};

} // namespace snc::mpath::d3::curve
