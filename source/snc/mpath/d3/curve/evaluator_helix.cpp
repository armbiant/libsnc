/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "evaluator_helix.hpp"
#include <sev/math/numbers.hpp>

namespace snc::mpath::d3::curve
{

Evaluator_Helix::Evaluator_Helix (
    const sev::lag::Vector3d & pos_begin_n,
    const snc::mpath::d3::elem::Curve_Helix & curve_n )
{
  _pos_begin = pos_begin_n;
  _pos_center = curve_n.pos_center ();
  _height = curve_n.height ();

  // -- Radius and angles
  {
    sev::lag::Vector2d vdir;
    vdir[ 0 ] = _pos_begin[ 0 ];
    vdir[ 1 ] = _pos_begin[ 1 ];
    vdir -= _pos_center;

    _radius = sev::lag::magnitude ( vdir );
    if ( _radius > 1.0e-6 ) {
      vdir /= _radius;
      _angle_radians_offset = std::atan2 ( vdir[ 1 ], vdir[ 0 ] );
      _angle_radians = curve_n.angle_radians ();
    } else {
      _radius = 0.0;
      _angle_radians_offset = 0.0;
      _angle_radians = 0.0;
    }
  }

  // -- Length
  _length_approx = sev::lag::square ( _radius * _angle_radians );
  _length_approx += sev::lag::square ( _height );
  _length_approx = sev::lag::square_root ( _length_approx );
}

void
Evaluator_Helix::calc_pos ( sev::lag::Vector3d & res_n, double tval_n )
{
  sev::lag::Vector2d vpos;
  {
    double angle ( ( _angle_radians * tval_n ) + _angle_radians_offset );
    vpos[ 0 ] = std::cos ( angle );
    vpos[ 1 ] = std::sin ( angle );
  }
  vpos *= _radius;
  vpos += _pos_center;

  res_n[ 0 ] = vpos[ 0 ];
  res_n[ 1 ] = vpos[ 1 ];
  res_n[ 2 ] = ( _height * tval_n ) + _pos_begin[ 2 ];
}

void
Evaluator_Helix::calc_end_pos ( sev::lag::Vector3d & res_n )
{
  calc_pos ( res_n, 1.0 );
}

void
Evaluator_Helix::calc_tangent ( sev::lag::Vector3d & res_n, double tval_n )
{
  sev::lag::Vector3d vres;
  {
    const double scale ( _angle_radians * _radius );
    {
      double angle ( ( _angle_radians * tval_n ) + _angle_radians_offset );
      vres[ 0 ] = -std::sin ( angle );
      vres[ 1 ] = std::cos ( angle );
    }
    vres[ 0 ] *= scale;
    vres[ 1 ] *= scale;
  }
  vres[ 2 ] = _height;
  res_n = sev::lag::normalized ( vres );
}

double
Evaluator_Helix::calc_length ()
{
  // Approximated length is as precise as possible
  return _length_approx;
}

void
Evaluator_Helix::calc_extents ( sev::lag::Vector3d & min_n,
                                sev::lag::Vector3d & max_n )
{
  // Most simple, full circle bbox
  sev::lag::Vector2d pos_min ( sev::lag::init::Fill< double > ( 1.0 ) );
  sev::lag::Vector2d pos_max ( sev::lag::init::Fill< double > ( -1.0 ) );
  {
    double angle_begin ( 0.0 );
    double angle_end ( 0.0 );
    if ( _angle_radians > 0.0 ) {
      // Positive angle
      const double angle ( std::min ( _angle_radians, sev::lag::two_pi_d ) );
      angle_begin = sev::lag::radians_in_two_pi ( _angle_radians_offset );
      angle_end = angle_begin + angle;
    } else {
      // Negative angle
      const double angle ( std::min ( -_angle_radians, sev::lag::two_pi_d ) );
      angle_begin =
          sev::lag::radians_in_two_pi ( _angle_radians_offset - angle );
      angle_end = angle_begin + angle;
    }

    double quad_angle_min ( std::floor ( angle_begin / sev::lag::pi_half_d ) *
                            sev::lag::pi_half_d );
    double quad_angle_max ( quad_angle_min + sev::lag::pi_half_d );

    const unsigned int num_quads ( 4 );
    for ( unsigned int ii = 0; ii != num_quads; ++ii ) {
      const double angle_min ( std::max ( quad_angle_min, angle_begin ) );
      const double angle_max ( std::min ( quad_angle_max, angle_end ) );
      {
        double cos_val[ 2 ] = { std::cos ( angle_min ),
                                std::cos ( angle_max ) };
        if ( cos_val[ 0 ] > cos_val[ 1 ] ) {
          std::swap ( cos_val[ 0 ], cos_val[ 1 ] );
        }
        sev::math::assign_smaller< double > ( pos_min[ 0 ], cos_val[ 0 ] );
        sev::math::assign_larger< double > ( pos_max[ 0 ], cos_val[ 1 ] );
      }
      {
        double sin_val[ 2 ] = { std::sin ( angle_min ),
                                std::sin ( angle_max ) };
        if ( sin_val[ 0 ] > sin_val[ 1 ] ) {
          std::swap ( sin_val[ 0 ], sin_val[ 1 ] );
        }
        sev::math::assign_smaller< double > ( pos_min[ 1 ], sin_val[ 0 ] );
        sev::math::assign_larger< double > ( pos_max[ 1 ], sin_val[ 1 ] );
      }
      if ( quad_angle_max >= angle_end ) {
        break;
      }
      quad_angle_min = quad_angle_max;
      quad_angle_max += sev::lag::pi_half_d;
    }
  }
  pos_min *= _radius;
  pos_max *= _radius;
  pos_min += _pos_center;
  pos_max += _pos_center;

  // Min
  min_n[ 0 ] = pos_min[ 0 ];
  min_n[ 1 ] = pos_min[ 1 ];
  min_n[ 2 ] = _pos_begin[ 2 ];
  if ( _height < 0.0 ) {
    min_n[ 2 ] += _height;
  }
  // Max
  max_n[ 0 ] = pos_max[ 0 ];
  max_n[ 1 ] = pos_max[ 1 ];
  max_n[ 2 ] = _pos_begin[ 2 ];
  if ( _height > 0.0 ) {
    max_n[ 2 ] += _height;
  }
}

} // namespace snc::mpath::d3::curve
