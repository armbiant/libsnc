/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::mpath
{

/// @brief Speed type identifier
enum class Speed_Regime : std::uint_fast8_t
{
  RAPID,
  MATERIAL,
  LIST_END
};

static constexpr std::size_t num_speed_regimes =
    static_cast< std::size_t > ( Speed_Regime::LIST_END );

/// @brief Absolute or symbolic speed value
class Speed
{
  public:
  // -- Construction and setup

  Speed () = default;

  explicit Speed ( double value_n )
  : _is_symbolic ( false )
  , _value ( value_n )
  {
  }

  explicit Speed ( Speed_Regime regime_n )
  : _is_symbolic ( true )
  , _regime ( regime_n )
  {
  }

  void
  reset ()
  {
    _is_symbolic = false;
    _regime = Speed_Regime::RAPID;
    _value = 0.0;
  }

  // -- Interface

  bool
  is_symbolic () const
  {
    return _is_symbolic;
  }

  bool
  is_absolute () const
  {
    return !_is_symbolic;
  }

  Speed_Regime
  regime () const
  {
    return _regime;
  }

  void
  set_regime ( Speed_Regime regime_n )
  {
    _is_symbolic = true;
    _regime = regime_n;
  }

  double
  value () const
  {
    return _value;
  }

  void
  set_value ( double value_n )
  {
    _is_symbolic = false;
    _value = value_n;
  }

  // -- Comparison operators

  bool
  operator== ( const snc::mpath::Speed & speed_n ) const
  {
    if ( is_symbolic () != speed_n.is_symbolic () ) {
      return false;
    }
    if ( is_symbolic () ) {
      return ( regime () == speed_n.regime () );
    }
    return ( value () == speed_n.value () );
  }

  bool
  operator!= ( const snc::mpath::Speed & speed_n ) const
  {
    return !operator== ( speed_n );
  }

  private:
  // -- Attributes
  bool _is_symbolic = false;
  Speed_Regime _regime = Speed_Regime::RAPID;
  double _value = 0.0;
};

} // namespace snc::mpath
