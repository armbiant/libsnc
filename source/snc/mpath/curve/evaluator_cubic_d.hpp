/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/curve_evaluator_d.hpp>
#include <snc/mpath/elem/curve_cubic_d.hpp>
#include <array>

namespace snc::mpath::curve
{

/// @brief Abstract base class for curve evaluators
///
template < std::size_t DIM >
class Evaluator_Cubic_D : public Curve_Evaluator_D< DIM >
{
  public:
  // -- Types
  static constexpr std::size_t num_legs_approx = 5;
  static constexpr std::size_t num_legs_max = 4096;

  // -- Construction

  Evaluator_Cubic_D ( const sev::lag::Vector< double, DIM > & pos_begin_n,
                      const snc::mpath::elem::Curve_Cubic_D< DIM > & curve_n );

  // -- Interface

  const std::array< sev::lag::Vector< double, DIM >, 3 > &
  points () const
  {
    return _points;
  }

  double
  calc_length_approx ( std::size_t num_legs_n );

  // -- Abstract interface

  void
  calc_pos ( sev::lag::Vector< double, DIM > & res_n, double tval_n ) override;

  void
  calc_end_pos ( sev::lag::Vector< double, DIM > & res_n ) override;

  void
  calc_tangent ( sev::lag::Vector< double, DIM > & res_n,
                 double tval_n ) override;

  double
  calc_length () override;

  void
  calc_extents ( sev::lag::Vector< double, DIM > & min_n,
                 sev::lag::Vector< double, DIM > & max_n ) override;

  private:
  // -- Attributes
  using Curve_Evaluator_D< DIM >::_length_approx;
  using Curve_Evaluator_D< DIM >::_pos_begin;

  std::array< sev::lag::Vector< double, DIM >, 3 > _deltas;
  std::array< sev::lag::Vector< double, DIM >, 3 > _points;
};

} // namespace snc::mpath::curve
