/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "evaluator_linear_d.hpp"
#include <sev/lag/vector_math.hpp>

namespace snc::mpath::curve
{

template < std::size_t DIM >
Evaluator_Linear_D< DIM >::Evaluator_Linear_D ()
{
  _length_approx = 0.0;
  _pos_begin.fill ( 0.0 );
  _pos_end.fill ( 0.0 );
  _delta.fill ( 0.0 );
  _tangent.fill ( 0.0 );
}

template < std::size_t DIM >
Evaluator_Linear_D< DIM >::Evaluator_Linear_D (
    const sev::lag::Vector< double, DIM > & pos_begin_n,
    const snc::mpath::elem::Curve_Linear_D< DIM > & curve_n )
{
  _pos_begin = pos_begin_n;
  _pos_end = curve_n.pos_end ();
  _delta = _pos_end - _pos_begin;
  _length_approx = sev::lag::magnitude ( _delta );
  _tangent = _delta;
  _tangent /= _length_approx;
}

template < std::size_t DIM >
void
Evaluator_Linear_D< DIM >::calc_pos ( sev::lag::Vector< double, DIM > & res_n,
                                      double tval_n )
{
  res_n = _delta * tval_n + _pos_begin;
}

template < std::size_t DIM >
void
Evaluator_Linear_D< DIM >::calc_end_pos (
    sev::lag::Vector< double, DIM > & res_n )
{
  res_n = _pos_end;
}

template < std::size_t DIM >
void
Evaluator_Linear_D< DIM >::calc_tangent (
    sev::lag::Vector< double, DIM > & res_n, double tval_n )
{
  // Tangent was calculated on setup
  (void)tval_n;
  res_n = _tangent;
}

template < std::size_t DIM >
double
Evaluator_Linear_D< DIM >::calc_length ()
{
  // Approximated length is as precise as possible
  return _length_approx;
}

template < std::size_t DIM >
void
Evaluator_Linear_D< DIM >::calc_extents (
    sev::lag::Vector< double, DIM > & min_n,
    sev::lag::Vector< double, DIM > & max_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    min_n[ ii ] = std::min ( _pos_begin[ ii ], _pos_end[ ii ] );
  }
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    max_n[ ii ] = std::max ( _pos_begin[ ii ], _pos_end[ ii ] );
  }
}

// -- Instantiation

template class Evaluator_Linear_D< 1 >;
template class Evaluator_Linear_D< 2 >;
template class Evaluator_Linear_D< 3 >;
template class Evaluator_Linear_D< 4 >;
template class Evaluator_Linear_D< 5 >;
template class Evaluator_Linear_D< 6 >;

} // namespace snc::mpath::curve
