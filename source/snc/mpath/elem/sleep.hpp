/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/math/exponents.hpp>
#include <snc/mpath/element.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

class Sleep : public snc::mpath::Element
{
  public:
  // -- Types

  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::elem::Type::SLEEP;

  // -- Construction

  Sleep ( std::uint_fast32_t usecs_n = sev::math::mega_ui )
  : snc::mpath::Element ( class_elem_type, sizeof ( Sleep ) )
  , _usecs ( usecs_n )
  {
  }

  // -- Duration

  std::uint_fast32_t
  usecs () const
  {
    return _usecs;
  }

  void
  set_usecs ( std::uint_fast32_t usecs_n )
  {
    _usecs = usecs_n;
  }

  private:
  // -- Attributes
  std::uint_fast32_t _usecs = 0;
};

} // namespace snc::mpath::elem
