/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <snc/mpath/speed_database.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

/** Sets a value in the speeds database
 */
class Speed_Db_Value : public snc::mpath::Element
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::elem::Type::SPEED_DB_VALUE;

  // -- Construction

  Speed_Db_Value ()
  : snc::mpath::Element ( class_elem_type, sizeof ( Speed_Db_Value ) )
  {
  }

  Speed_Db_Value ( Speed_Db_Id id_n, double value_n )
  : snc::mpath::Element ( class_elem_type, sizeof ( Speed_Db_Value ) )
  , _speed_id ( id_n )
  , _speed_value ( value_n )
  {
  }

  // -- Speed id

  Speed_Db_Id
  speed_id () const
  {
    return _speed_id;
  }

  void
  set_speed_id ( Speed_Db_Id id_n )
  {
    _speed_id = id_n;
  }

  // -- Speed value

  double
  speed_value () const
  {
    return _speed_value;
  }

  void
  set_speed_value ( double value_n )
  {
    _speed_value = value_n;
  }

  private:
  // -- Attributes
  Speed_Db_Id _speed_id = Speed_Db_Id::LIST_END;
  double _speed_value = 0.0;
};

} // namespace snc::mpath::elem
