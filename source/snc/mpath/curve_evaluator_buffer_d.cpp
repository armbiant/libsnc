/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "curve_evaluator_buffer_d.hpp"
#include <snc/mpath/curve/evaluator_linear_d.hpp>

namespace snc::mpath
{

template < std::size_t DIM >
Curve_Evaluator_Buffer_D< DIM >::Curve_Evaluator_Buffer_D ()
{
  reset ();
}

template < std::size_t DIM >
void
Curve_Evaluator_Buffer_D< DIM >::reset ()
{
  private_new< snc::mpath::curve::Evaluator_Linear_D< DIM > > ();
}

// -- Instantiation

template class Curve_Evaluator_Buffer_D< 1 >;
template class Curve_Evaluator_Buffer_D< 2 >;
template class Curve_Evaluator_Buffer_D< 3 >;
template class Curve_Evaluator_Buffer_D< 4 >;
template class Curve_Evaluator_Buffer_D< 5 >;
template class Curve_Evaluator_Buffer_D< 6 >;

} // namespace snc::mpath
