/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/d2/elem/curve_circle.hpp>
#include <snc/mpath/d2/elem_type.hpp>
#include <snc/mpath/elements_common.hpp>

namespace snc::mpath::d2::elem
{
using Curve_Linear = snc::mpath::elem::Curve_Linear_DT< 2, Type::CURVE_LINEAR >;
using Curve_Cubic = snc::mpath::elem::Curve_Cubic_DT< 2, Type::CURVE_CUBIC >;
} // namespace snc::mpath::d2::elem
