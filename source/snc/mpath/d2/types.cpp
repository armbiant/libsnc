/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "types.hpp"
#include <sev/assert.hpp>
#include <snc/mpath/curve/evaluator_cubic_d.hpp>
#include <snc/mpath/curve/evaluator_linear_d.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath/d2/curve/evaluator_circle.hpp>
#include <snc/mpath/d2/elem/curve_circle.hpp>

namespace snc::mpath
{

template <>
void
Curve_Evaluator_Buffer_D< 2 >::new_from_curve (
    const sev::lag::Vector< double, 2 > & pos_begin_n,
    const snc::mpath::elem::Curve_D< 2 > & curve_n )
{
  using namespace snc::mpath::curve;
  using namespace snc::mpath::d2::curve;
  using namespace snc::mpath::d2::elem;
  using Type = snc::mpath::d2::elem::Type;

  switch ( curve_n.elem_type () ) {
  case Type::CURVE_LINEAR:
    private_new< Evaluator_Linear_D< 2 > > (
        pos_begin_n, static_cast< const Curve_Linear & > ( curve_n ) );
    break;

  case Type::CURVE_CUBIC:
    private_new< Evaluator_Cubic_D< 2 > > (
        pos_begin_n, static_cast< const Curve_Cubic & > ( curve_n ) );
    break;

  case Type::CURVE_CIRCLE:
    private_new< Evaluator_Circle > (
        pos_begin_n, static_cast< const Curve_Circle & > ( curve_n ) );
    break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

} // namespace snc::mpath

namespace snc::mpath::elem
{
template <>
void
curve_translate_d ( snc::mpath::elem::Curve_D< 2 > & curve_n,
                    const sev::lag::Vector< double, 2 > & delta_n )
{
  using namespace snc::mpath::d2::elem;
  using Type = snc::mpath::d2::elem::Type;

  switch ( curve_n.elem_type () ) {
  case Type::CURVE_LINEAR:
    curve_translate< Curve_Linear > ( curve_n, delta_n );
    break;
  case Type::CURVE_CUBIC:
    curve_translate< Curve_Cubic > ( curve_n, delta_n );
    break;
  case Type::CURVE_CIRCLE:
    curve_translate< Curve_Circle > ( curve_n, delta_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}
} // namespace snc::mpath::elem
