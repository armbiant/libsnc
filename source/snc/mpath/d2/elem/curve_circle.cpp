/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "curve_circle.hpp"
#include <sev/math/numbers.hpp>

namespace snc::mpath::d2::elem
{

Curve_Circle::Curve_Circle ()
: snc::mpath::elem::Curve_D< 2 > ( class_elem_type, sizeof ( Curve_Circle ) )
, _pos_center ( sev::lag::init::zero )
{
}

void
Curve_Circle::translate ( const sev::lag::Vector2d & delta_n )
{
  _pos_center += delta_n;
}

} // namespace snc::mpath::d2::elem
