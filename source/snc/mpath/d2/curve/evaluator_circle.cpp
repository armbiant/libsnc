/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "evaluator_circle.hpp"
#include <sev/math/numbers.hpp>

namespace snc::mpath::d2::curve
{

Evaluator_Circle::Evaluator_Circle (
    const sev::lag::Vector2d & pos_begin_n,
    const snc::mpath::d2::elem::Curve_Circle & curve_n )
{
  _pos_begin = pos_begin_n;
  _pos_center = curve_n.pos_center ();

  sev::lag::Vector2d vdir ( _pos_begin );
  vdir -= _pos_center;

  _radius = sev::lag::magnitude ( vdir );
  if ( _radius > 1.0e-6 ) {
    vdir /= _radius;
    _angle_radians_offset = std::atan2 ( vdir[ 1 ], vdir[ 0 ] );
    _angle_radians = curve_n.angle_radians ();
  } else {
    _radius = 0.0;
    _angle_radians_offset = 0.0;
    _angle_radians = 0.0;
  }
  // Length
  _length_approx = ( _radius * sev::math::abs ( _angle_radians ) );
}

void
Evaluator_Circle::calc_pos ( sev::lag::Vector2d & res_n, double tval_n )
{
  sev::lag::Vector2d vpos;
  {
    double angle ( ( _angle_radians * tval_n ) + _angle_radians_offset );
    vpos[ 0 ] = std::cos ( angle );
    vpos[ 1 ] = std::sin ( angle );
  }
  vpos *= _radius;
  vpos += _pos_center;

  res_n = vpos;
}

void
Evaluator_Circle::calc_end_pos ( sev::lag::Vector2d & res_n )
{
  calc_pos ( res_n, 1.0 );
}

void
Evaluator_Circle::calc_tangent ( sev::lag::Vector2d & res_n, double tval_n )
{
  sev::lag::Vector2d vdir;
  {
    double angle ( ( _angle_radians * tval_n ) + _angle_radians_offset );
    vdir[ 0 ] = std::sin ( angle );
    vdir[ 1 ] = std::cos ( angle );
    if ( _angle_radians >= 0 ) {
      vdir[ 0 ] = -vdir[ 0 ];
    } else {
      vdir[ 1 ] = -vdir[ 1 ];
    }
  }

  res_n = vdir;
}

double
Evaluator_Circle::calc_length ()
{
  // Approximated length is as precise as possible
  return _length_approx;
}

void
Evaluator_Circle::calc_extents ( sev::lag::Vector2d & min_n,
                                 sev::lag::Vector2d & max_n )
{
  // Most simple, full circle bbox
  sev::lag::Vector2d pos_min ( sev::lag::init::Fill< double > ( 1.0 ) );
  sev::lag::Vector2d pos_max ( sev::lag::init::Fill< double > ( -1.0 ) );
  {
    double angle_begin ( 0.0 );
    double angle_end ( 0.0 );
    if ( _angle_radians > 0.0 ) {
      // Positive angle
      const double angle ( std::min ( _angle_radians, sev::lag::two_pi_d ) );
      angle_begin = sev::lag::radians_in_two_pi ( _angle_radians_offset );
      angle_end = angle_begin + angle;
    } else {
      // Negative angle
      const double angle ( std::min ( -_angle_radians, sev::lag::two_pi_d ) );
      angle_begin =
          sev::lag::radians_in_two_pi ( _angle_radians_offset - angle );
      angle_end = angle_begin + angle;
    }

    double quad_angle_min ( std::floor ( angle_begin / sev::lag::pi_half_d ) *
                            sev::lag::pi_half_d );
    double quad_angle_max ( quad_angle_min + sev::lag::pi_half_d );

    const unsigned int num_quads ( 4 );
    for ( unsigned int ii = 0; ii != num_quads; ++ii ) {
      const double angle_min ( std::max ( quad_angle_min, angle_begin ) );
      const double angle_max ( std::min ( quad_angle_max, angle_end ) );
      {
        double cos_val[ 2 ] = { std::cos ( angle_min ),
                                std::cos ( angle_max ) };
        if ( cos_val[ 0 ] > cos_val[ 1 ] ) {
          std::swap ( cos_val[ 0 ], cos_val[ 1 ] );
        }
        sev::math::assign_smaller< double > ( pos_min[ 0 ], cos_val[ 0 ] );
        sev::math::assign_larger< double > ( pos_max[ 0 ], cos_val[ 1 ] );
      }
      {
        double sin_val[ 2 ] = { std::sin ( angle_min ),
                                std::sin ( angle_max ) };
        if ( sin_val[ 0 ] > sin_val[ 1 ] ) {
          std::swap ( sin_val[ 0 ], sin_val[ 1 ] );
        }
        sev::math::assign_smaller< double > ( pos_min[ 1 ], sin_val[ 0 ] );
        sev::math::assign_larger< double > ( pos_max[ 1 ], sin_val[ 1 ] );
      }
      if ( quad_angle_max >= angle_end ) {
        break;
      }
      quad_angle_min = quad_angle_max;
      quad_angle_max += sev::lag::pi_half_d;
    }
  }
  pos_min *= _radius;
  pos_max *= _radius;
  pos_min += _pos_center;
  pos_max += _pos_center;

  min_n = pos_min;
  max_n = pos_max;
}

} // namespace snc::mpath::d2::curve
