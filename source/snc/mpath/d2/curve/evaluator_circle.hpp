/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath/curve_evaluator_d.hpp>
#include <snc/mpath/d2/elem/curve_circle.hpp>

namespace snc::mpath::d2::curve
{

/// @brief Abstract base class for curve evaluators
///
class Evaluator_Circle : public snc::mpath::Curve_Evaluator_D< 2 >
{
  public:
  // -- Construction

  Evaluator_Circle ( const sev::lag::Vector2d & pos_begin_n,
                     const snc::mpath::d2::elem::Curve_Circle & curve_n );

  // -- Abstract interface

  void
  calc_pos ( sev::lag::Vector2d & res_n, double tval_n ) override;

  void
  calc_end_pos ( sev::lag::Vector2d & res_n ) override;

  void
  calc_tangent ( sev::lag::Vector2d & res_n, double tval_n ) override;

  double
  calc_length () override;

  void
  calc_extents ( sev::lag::Vector2d & min_n,
                 sev::lag::Vector2d & max_n ) override;

  private:
  // -- Attributes
  using snc::mpath::Curve_Evaluator_D< 2 >::_length_approx;
  using snc::mpath::Curve_Evaluator_D< 2 >::_pos_begin;

  sev::lag::Vector2d _pos_center;
  double _angle_radians_offset;
  double _angle_radians;
  double _radius;
};

} // namespace snc::mpath::d2::curve
