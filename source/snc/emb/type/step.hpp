/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::type
{

/// @brief Step type
enum class Step : std::uint8_t
{
  // Invalid job type
  INVALID,

  // Movement
  HOLD,
  FORWARD,
  BACKWARD,

  // Switches
  SWITCH_SET_LOW,
  SWITCH_SET_HIGH,

  // List end
  LIST_END
};

inline Step
step_move ( bool forward_n, bool flipped_direction_n )
{
  return ( forward_n != flipped_direction_n ) ? Step::FORWARD : Step::BACKWARD;
}

} // namespace snc::emb::type
