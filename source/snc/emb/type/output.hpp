/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::type
{

/// @brief Output type
enum class Output : std::uint8_t
{
  UNUSED,
  SWITCH_PLAIN,
  SWITCH_FREQUENCY,
  STEPPER_DIRECTION,
  STEPPER_CLOCK
};

} // namespace snc::emb::type
