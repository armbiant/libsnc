/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::type
{

/// @brief Stepper sync type
enum class Stepper_Sync : std::uint8_t
{
  SYNC_NULL,
  LOCK_INSTALL,
  LOCK_RELEASE
};

} // namespace snc::emb::type
