/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/serial_device/stats_io.hpp>
#include <chrono>

namespace snc::emb
{

class IO_Stats
{
  public:
  // -- Types

  using Latency = std::chrono::nanoseconds;

  // -- Construction

  IO_Stats ();

  void
  reset ();

  // -- Serial statistics

  const snc::serial_device::Stats_IO &
  serial () const
  {
    return _serial;
  }

  void
  set_serial ( const snc::serial_device::Stats_IO & stats_n )
  {
    _serial = stats_n;
  }

  // -- Latency

  const Latency &
  latency () const
  {
    return _latency;
  }

  void
  set_latency ( const Latency & latency_n )
  {
    _latency = latency_n;
  }

  private:
  // -- Attributes
  snc::serial_device::Stats_IO _serial;
  Latency _latency = Latency::zero ();
};

} // namespace snc::emb
