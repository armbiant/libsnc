/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/view.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/emb/state/controls.hpp>
#include <snc/emb/state/sensors.hpp>
#include <snc/emb/state/stepper.hpp>
#include <cstdint>
#include <memory>
#include <vector>

namespace snc::emb::state
{

class State
{
  public:
  // -- Construction

  State ();

  State ( std::shared_ptr< const snc::emb::config::Config > config_n );

  State ( const State & state_n );

  State ( State && state_n );

  // -- Assignment operators

  State &
  operator= ( const State & state_n );

  State &
  operator= ( State && state_n );

  // -- Setup

  /// @brief Reset to default construction state
  void
  reset ();

  /// @brief Reset to construction state
  void
  reset ( std::shared_ptr< const snc::emb::config::Config > config_n );

  // -- Configuration

  const std::shared_ptr< const snc::emb::config::Config > &
  config () const
  {
    return _config;
  }

  // -- Sensors: bool

  const sev::mem::View< Sensor_Bool > &
  sensors_bool ()
  {
    return _sensors_bool;
  }

  const sev::mem::View< const Sensor_Bool > &
  sensors_bool () const
  {
    return reinterpret_cast< const sev::mem::View< const Sensor_Bool > & > (
        _sensors_bool );
  }

  // -- Control bool

  const sev::mem::View< Control_Bool > &
  controls_bool ()
  {
    return _controls_bool;
  }

  const sev::mem::View< const Control_Bool > &
  controls_bool () const
  {
    return reinterpret_cast< const sev::mem::View< const Control_Bool > & > (
        _controls_bool );
  }

  // -- Steppers

  const sev::mem::View< Stepper > &
  steppers ()
  {
    return _steppers;
  }

  const sev::mem::View< const Stepper > &
  steppers () const
  {
    return reinterpret_cast< const sev::mem::View< const Stepper > & > (
        _steppers );
  }

  // -- Latest message uid

  std::uint16_t
  lastest_message_uid () const
  {
    return _lastest_message_uid;
  }

  void
  set_lastest_message_uid ( std::uint16_t id_n )
  {
    _lastest_message_uid = id_n;
  }

  private:
  // -- Utility
  void
  allocate ();

  private:
  // -- Attributes
  // - Configuration
  std::shared_ptr< const snc::emb::config::Config > _config;
  // - Views
  sev::mem::View< Sensor_Bool > _sensors_bool;
  sev::mem::View< Control_Bool > _controls_bool;
  sev::mem::View< Stepper > _steppers;
  // - Values
  std::uint16_t _lastest_message_uid = 0;
  // - Data buffer
  std::vector< std::byte > _buffer;
};

} // namespace snc::emb::state
