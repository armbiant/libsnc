/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::state
{

class Stepper
{
  public:
  void
  reset ()
  {
    _steps_queue_length = 0;
  }

  std::uint16_t
  steps_queue_length () const
  {
    return _steps_queue_length;
  }

  void
  set_steps_queue_length ( std::uint16_t value_n )
  {
    _steps_queue_length = value_n;
  }

  private:
  // -- Attributes
  std::uint16_t _steps_queue_length = 0;
};

} // namespace snc::emb::state
