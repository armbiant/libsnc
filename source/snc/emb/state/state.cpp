/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "state.hpp"
#include <sev/mem/align.hpp>
#include <type_traits>
#include <utility>

namespace
{
template < typename T >
std::size_t
aligned_size ( std::size_t num_n )
{
  return sev::mem::aligned_up ( sizeof ( T ) * num_n );
}
} // namespace

namespace snc::emb::state
{

State::State ()
{
  static_assert ( std::is_trivially_destructible< Stepper >::value );
  static_assert ( std::is_trivially_copyable< Stepper >::value );
  static_assert ( std::is_trivially_destructible< Sensor_Bool >::value );
  static_assert ( std::is_trivially_copyable< Sensor_Bool >::value );
  static_assert ( std::is_trivially_destructible< Control_Bool >::value );
  static_assert ( std::is_trivially_copyable< Control_Bool >::value );
}

State::State ( std::shared_ptr< const snc::emb::config::Config > config_n )
: _config ( std::move ( config_n ) )
{
  allocate ();
}

State::State ( const State & state_n )
: State ( state_n._config )
{
  // Plain buffer copy
  _buffer = state_n._buffer;
}

State::State ( State && state_n ) = default;

State &
State::operator= ( const State & state_n )
{
  // Synchronize configuration
  if ( _config != state_n.config () ) {
    reset ( config () );
  }
  // Plain buffer copy
  _buffer = state_n._buffer;
  return *this;
}

State &
State::operator= ( State && state_n ) = default;

void
State::reset ()
{
  if ( !config () ) {
    return;
  }
  // References
  _config.reset ();
  // Views
  _sensors_bool.clear ();
  _controls_bool.clear ();
  _steppers.clear ();
  // Values
  _lastest_message_uid = 0;
  // Buffer
  _buffer.clear ();
  _buffer.shrink_to_fit ();
}

void
State::reset ( std::shared_ptr< const snc::emb::config::Config > config_n )
{
  reset ();
  _config = std::move ( config_n );
  allocate ();
}

void
State::allocate ()
{
  if ( !config () ) {
    return;
  }

  const auto & cfg = *config ();
  // -- Compute sizes
  const std::size_t sensors_bool_size = //
      aligned_size< Sensor_Bool > ( cfg.num_sensors_bool () );
  const std::size_t controls_bool_size = //
      aligned_size< Control_Bool > ( cfg.num_controls_bool () );
  const std::size_t steppers_size = //
      aligned_size< Stepper > ( cfg.steppers ().size () );

  // - Total size
  const std::size_t size =
      ( sensors_bool_size + controls_bool_size + steppers_size );
  if ( size == 0 ) {
    return;
  }

  _buffer.resize ( size, std::byte ( 0 ) );
  _buffer.shrink_to_fit ();

  // -- Setup views
  {
    std::byte * ptr = _buffer.data ();

    _sensors_bool.reset ( reinterpret_cast< Sensor_Bool * > ( ptr ),
                          cfg.num_sensors_bool () );
    ptr += sensors_bool_size;

    _controls_bool.reset ( reinterpret_cast< Control_Bool * > ( ptr ),
                           cfg.num_controls_bool () );
    ptr += controls_bool_size;

    _steppers.reset ( reinterpret_cast< Stepper * > ( ptr ),
                      cfg.num_steppers () );
    ptr += steppers_size;
  }

  // -- Construct items
  for ( std::size_t ii = 0; ii != cfg.num_sensors_bool (); ++ii ) {
    new ( &_sensors_bool[ ii ] )
        Sensor_Bool ( cfg.sensors_bool ()[ ii ].init_state () );
  }
  for ( std::size_t ii = 0; ii != cfg.num_controls_bool (); ++ii ) {
    new ( &_controls_bool[ ii ] )
        Control_Bool ( cfg.controls_bool ()[ ii ].init_state () );
  }
  for ( auto & item : _steppers ) {
    new ( &item ) Stepper ();
  }
}

} // namespace snc::emb::state
