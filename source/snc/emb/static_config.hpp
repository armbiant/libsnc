/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb
{

constexpr std::size_t static_num_outputs ( 22 );
constexpr std::size_t static_num_axes ( 6 );
constexpr std::size_t static_num_sensors ( 10 );
constexpr std::size_t static_num_switches ( 10 );
constexpr std::size_t static_num_frequency_generators ( 3 );

} // namespace snc::emb
