/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "step_splitter.hpp"
#include <sev/math/numbers.hpp>

namespace snc::emb
{

/// @brief 16 bit maximum value
static const std::uint_fast32_t usecs_max_limit = 65535;
static const std::uint_fast32_t usecs_max_default = usecs_max_limit;

Step_Splitter::Step_Splitter ()
: _usecs_max ( usecs_max_default )
{
  reset ();
}

Step_Splitter::Step_Splitter ( snc::emb::type::Step job_type_n,
                               std::uint_fast32_t usecs_n )
: _usecs_max ( usecs_max_default )
{
  setup_step ( job_type_n, usecs_n );
}

Step_Splitter::Step_Splitter ( snc::emb::type::Step job_type_n,
                               std::uint_fast32_t usecs_n,
                               std::uint_fast32_t usecs_max_n )
{
  sev::math::assign_smaller< std::uint_fast32_t > ( usecs_max_n,
                                                    usecs_max_limit );
  _usecs_max = usecs_max_n;
  setup_step ( job_type_n, usecs_n );
}

Step_Splitter::~Step_Splitter () {}

void
Step_Splitter::reset ()
{
  _job_type = snc::emb::type::Step::INVALID;
  _first = true;
  _num_chunks_remain = 0;
  _chunk_usecs = 0;
  _chunk_usecs_extra = 0;
}

void
Step_Splitter::set_usecs_max ( std::uint_fast32_t usecs_max_n )
{
  sev::math::assign_smaller< std::uint_fast32_t > ( usecs_max_n,
                                                    usecs_max_limit );
  _usecs_max = usecs_max_n;
}

} // namespace snc::emb
