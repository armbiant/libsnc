/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/type/output.hpp>
#include <cstdint>

namespace snc::emb::config
{

class Control_Bool
{
  public:
  // -- Construction

  Control_Bool () = default;

  void
  reset ()
  {
    _init_state = false;
  }

  // -- Init state

  /// @brief Initial sensor state
  bool
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( bool state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  bool _init_state = false;
};

} // namespace snc::emb::config
