/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "deserializer.hpp"
#include <cnce/frog/messages_in.hpp> // embedded
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/mem/serial.hpp>
#include <snc/emb/msg/in/all.hpp>
#include <algorithm>

namespace
{

void
ds_state_sensor ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Sensor > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Sensor > ();

  msg.set_sensor_index ( dev_msg.sensor_index );
  msg.set_sensor_state ( dev_msg.sensor_state );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_sensors_32 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg =
      args_n.dev< cnce::Frog_Message_In_State_Sensors_32 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Sensors_32 > ();

  // Sensor states
  {
    const std::uint32_t sstates =
        sev::mem::read_uint32_8le ( &dev_msg.sensors_states[ 0 ] );
    for ( std::size_t ii = 0; ii != 32; ++ii ) {
      const bool is_high ( ( sstates & ( 1 << ii ) ) != 0 );
      msg.set_is_high ( ii, is_high );
    }
  }
  // Latest message uid
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_sensor_8 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Sensor_8 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Sensor_8 > ();

  msg.set_sensor_index ( dev_msg.sensor_index );
  msg.set_sensor_state ( dev_msg.sensor_state );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_sensor_16 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Sensor_16 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Sensor_16 > ();

  msg.set_sensor_index ( dev_msg.sensor_index );
  msg.set_sensor_state (
      sev::mem::read_uint16_8le ( &dev_msg.sensor_state[ 0 ] ) );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_sensor_32 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Sensor_32 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Sensor_32 > ();

  msg.set_sensor_index ( dev_msg.sensor_index );
  msg.set_sensor_state (
      sev::mem::read_uint32_8le ( &dev_msg.sensor_state[ 0 ] ) );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_switch ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Switch > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Switch > ();

  msg.set_switch_index ( dev_msg.switch_index );
  msg.set_switch_state ( dev_msg.switch_state );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_switches_32 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg =
      args_n.dev< cnce::Frog_Message_In_State_Switches_32 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Switches_32 > ();

  // Switch states
  {
    const std::uint32_t sstates =
        sev::mem::read_uint32_8le ( &dev_msg.switches_states[ 0 ] );
    for ( std::size_t ii = 0; ii != 32; ++ii ) {
      const bool is_high ( ( sstates & ( 1 << ii ) ) != 0 );
      msg.set_is_high ( ii, is_high );
    }
  }
  // Latest message uid
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_fader_8 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Fader_8 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Fader_8 > ();

  msg.set_fader_index ( dev_msg.fader_index );
  msg.set_fader_value ( dev_msg.fader_value );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_fader_16 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Fader_16 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Fader_16 > ();

  msg.set_fader_index ( dev_msg.fader_index );
  msg.set_fader_value (
      sev::mem::read_uint16_8le ( &dev_msg.fader_value[ 0 ] ) );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_fader_32 ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Fader_32 > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Fader_32 > ();

  msg.set_fader_index ( dev_msg.fader_index );
  msg.set_fader_value (
      sev::mem::read_uint32_8le ( &dev_msg.fader_value[ 0 ] ) );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

void
ds_state_stepper ( const snc::emb::serial::Deserializer::Args & args_n )
{
  const auto & dev_msg = args_n.dev< cnce::Frog_Message_In_State_Stepper > ();
  auto & msg = args_n.make< snc::emb::msg::in::State_Stepper > ();

  msg.set_stepper_index ( dev_msg.stepper_index );
  msg.set_steps_queue_length (
      sev::mem::read_uint16_8le ( &dev_msg.steps_queue_length[ 0 ] ) );
  msg.set_latest_message_uid (
      sev::mem::read_uint16_8le ( &dev_msg.latest_message_uid[ 0 ] ) );
}

} // namespace

namespace snc::emb::serial
{

Deserializer::Deserializer ()
{
  // Initialize deserialization arguments

  _deseri_args.sbuffer = _sbuffer.begin ();
  _deseri_args.message_buffer = &_message_buffer;
  _deseri_args.deserializer = this;

  // Set header id

  set_header_id ( cnce::frog_message_in_header,
                  cnce::frog_message_in_header_size );

  // Register message deserialization functions

  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSOR,
      cnce::Frog_Message_In_State_Sensor > ( ds_state_sensor );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSORS_32,
      cnce::Frog_Message_In_State_Sensors_32 > ( ds_state_sensors_32 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSOR,
      cnce::Frog_Message_In_State_Sensor > ( ds_state_sensor );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSOR_8,
      cnce::Frog_Message_In_State_Sensor_8 > ( ds_state_sensor_8 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSOR_16,
      cnce::Frog_Message_In_State_Sensor_16 > ( ds_state_sensor_16 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SENSOR_32,
      cnce::Frog_Message_In_State_Sensor_32 > ( ds_state_sensor_32 );

  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SWITCH,
      cnce::Frog_Message_In_State_Switch > ( ds_state_switch );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_SWITCHES_32,
      cnce::Frog_Message_In_State_Switches_32 > ( ds_state_switches_32 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_FADER_8,
      cnce::Frog_Message_In_State_Fader_8 > ( ds_state_fader_8 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_FADER_16,
      cnce::Frog_Message_In_State_Fader_16 > ( ds_state_fader_16 );
  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_FADER_32,
      cnce::Frog_Message_In_State_Fader_32 > ( ds_state_fader_32 );

  register_message_type<
      cnce::Frog_Message_In_Type::FROG_MESSAGE_IN_STATE_STEPPER,
      cnce::Frog_Message_In_State_Stepper > ( ds_state_stepper );
}

template < auto EID, typename EMT >
void
Deserializer::register_message_type ( Deseri_Func func_n )
{
  static_assert (
      std::is_same_v< decltype ( EID ), cnce::Frog_Message_In_Type > );
  constexpr std::size_t emb_message_type = static_cast< std::size_t > ( EID );
  static_assert ( emb_message_type < std::tuple_size< Message_Metas >::value );

  _message_metas[ emb_message_type ] = { sizeof ( EMT ), func_n };
}

void
Deserializer::set_header_id ( const std::uint8_t * id_bytes_n,
                              std::size_t num_bytes_n )
{
  DEBUG_ASSERT ( num_bytes_n < _header_id.size () );
  _header_id_size = num_bytes_n;
  sev::math::assign_smaller ( _header_id_size, _header_id.size () );
  std::copy_n ( id_bytes_n, _header_id_size, _header_id.begin () );
}

void
Deserializer::session_begin ()
{
  // Reset state
  _input_tile = nullptr;
  _input_data_pread = nullptr;
  _input_data_pend = nullptr;

  _deseri_method = nullptr;
  _parse_message_meta.reset ();

  _sbuffer_pos = 0;
}

void
Deserializer::session_end ()
{
}

void
Deserializer::install_input_data ( const snc::utility::Data_Tile * tile_n )
{
  if ( tile_n == nullptr ) {
    return;
  }

  _input_tile = tile_n;
  _input_data_pread =
      reinterpret_cast< const std::uint8_t * > ( _input_tile->data () );
  _input_data_pend = _input_data_pread + _input_tile->size ();
}

void
Deserializer::release_input_data ()
{
  _input_tile = nullptr;
  _input_data_pread = nullptr;
  _input_data_pend = nullptr;

  // Reset parsing state
  _sbuffer_pos = 0;
  _parse_message_meta.reset ();
}

bool
Deserializer::deserialize ()
{
  if ( _deseri_method == nullptr ) {
    _deseri_method = &Deserializer::deserialize_header;
  }

  // -- Read bytes
  while ( _input_data_pread != _input_data_pend ) {
    const std::uint8_t rbyte = *_input_data_pread;
    ++_input_data_pread;
    ( this->*_deseri_method ) ( rbyte );
    // Leave is there's nothing more to read
    if ( _deseri_method == nullptr ) {
      break;
    }
  }

  // -- Return false if the message isn't complete, yet.
  if ( ( _sbuffer_pos == 0 ) ||
       ( _sbuffer_pos != _parse_message_meta.num_bytes ) ) {
    return false;
  }

  // -- Evaluate complete message and reset parsing state
  _parse_message_meta.deseri_func ( _deseri_args );

  _parse_message_meta.reset ();
  _deseri_method = nullptr;
  _sbuffer_pos = 0;

  return true;
}

void
Deserializer::deserialize_header ( std::uint8_t byte_n )
{
  // -- Read message header byte

  // Check if this is a valid header byte
  if ( byte_n != _header_id[ _sbuffer_pos ] ) {
    // Invalid header id byte. Discard all bytes found so far.
    _sbuffer_pos = 0;
    return;
  }

  // Accept byte
  _sbuffer[ _sbuffer_pos ] = byte_n;
  ++_sbuffer_pos;

  // Change function if the header was read
  if ( _sbuffer_pos == _header_id_size ) {
    _deseri_method = &Deserializer::deserialize_type;
  }
}

void
Deserializer::deserialize_type ( std::uint8_t byte_n )
{
  if ( byte_n < _message_metas.size () ) {
    _parse_message_meta = _message_metas[ byte_n ];
  } else {
    _parse_message_meta.reset ();
  }

  if ( ( _parse_message_meta.num_bytes == 0 ) ||
       ( _parse_message_meta.deseri_func == nullptr ) ) {
    // Invalid type. Discard all bytes found so far.
    _sbuffer_pos = 0;
    _deseri_method = &Deserializer::deserialize_header;
    return;
  }

  // Accept byte
  _sbuffer[ _sbuffer_pos ] = byte_n;
  ++_sbuffer_pos;

  // Update deserialization method
  if ( _sbuffer_pos < _parse_message_meta.num_bytes ) {
    // Message not complete, yet. Read more bytes.
    _deseri_method = &Deserializer::deserialize_data;
  } else {
    // Message complete
    _deseri_method = nullptr;
  }
}

void
Deserializer::deserialize_data ( std::uint8_t byte_n )
{
  // Accept byte
  _sbuffer[ _sbuffer_pos ] = byte_n;
  ++_sbuffer_pos;

  // Check if message reading is finished
  if ( _sbuffer_pos == _parse_message_meta.num_bytes ) {
    _deseri_method = nullptr;
  }
}

} // namespace snc::emb::serial
