/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/byte_array.hpp>
#include <snc/emb/msg/message.hpp>
#include <snc/utility/data_tile.hpp>
#include <array>
#include <cstddef>
#include <cstdint>

namespace snc::emb::serial
{

class Deserializer
{
  public:
  // -- Types

  using Message_Buffer = std::array< std::byte, 16 >;

  /// @brief Function that serializes the message into the given buffer
  struct Args
  {
    std::uint8_t * sbuffer = nullptr;
    Message_Buffer * message_buffer = nullptr;
    snc::emb::serial::Deserializer * deserializer = nullptr;

    /// @brief Cast to device message
    template < typename T >
    const T &
    dev () const
    {
      return *reinterpret_cast< const T * > ( sbuffer );
    }

    /// @brief Make message in the elem buffer
    template < typename T >
    T &
    make () const
    {
      static_assert ( sizeof ( T ) <=
                      std::tuple_size< Message_Buffer >::value );
      return *new ( message_buffer->data () ) T ();
    }
  };

  /// @brief Function that serializes the message into the given buffer
  using Deseri_Func = void ( * ) ( const Args & args_n );
  using Deseri_Method = void ( Deserializer::* ) ( std::uint8_t byte_n );

  struct Message_Meta
  {
    void
    reset ()
    {
      num_bytes = 0;
      deseri_func = nullptr;
    }

    std::size_t num_bytes = 0;
    Deseri_Func deseri_func = nullptr;
  };

  using Message_Metas = std::array< Message_Meta, 16 >;

  // -- Construction

  Deserializer ();

  void
  set_header_id ( const std::uint8_t * id_bytes_n, std::size_t num_bytes_n );

  void
  session_begin ();

  void
  session_end ();

  // -- Input data

  bool
  input_data_done () const
  {
    return ( _input_data_pread == _input_data_pend );
  }

  bool
  needs_input_data () const
  {
    return !_input_tile;
  }

  void
  install_input_data ( const snc::utility::Data_Tile * tile_n );

  void
  release_input_data ();

  // -- Output sequence

  bool
  deserialize ();

  void
  deserialize_header ( std::uint8_t byte_n );

  void
  deserialize_type ( std::uint8_t byte_n );

  void
  deserialize_data ( std::uint8_t byte_n );

  const snc::emb::msg::Message &
  message () const
  {
    return *reinterpret_cast< const snc::emb::msg::Message * > (
        _message_buffer.data () );
  }

  protected:
  // -- Utility

  template < auto EID, typename EMT >
  void
  register_message_type ( Deseri_Func func_n );

  private:
  // -- Attributes
  const snc::utility::Data_Tile * _input_tile = nullptr;
  std::uint8_t const * _input_data_pread = nullptr;
  std::uint8_t const * _input_data_pend = nullptr;

  Message_Meta _parse_message_meta;
  Args _deseri_args;
  Deseri_Method _deseri_method = nullptr;

  std::size_t _header_id_size = 0;
  std::array< std::uint8_t, 8 > _header_id;

  std::size_t _sbuffer_pos = 0;
  alignas ( sizeof ( double ) ) std::array< std::uint8_t, 512 > _sbuffer;

  alignas ( sizeof ( double ) ) Message_Buffer _message_buffer;
  Message_Metas _message_metas;
};

} // namespace snc::emb::serial
