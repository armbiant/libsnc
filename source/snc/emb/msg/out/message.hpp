/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/message.hpp>
#include <cstdint>

namespace snc::emb::msg::out
{

enum class Type : std::uint8_t
{
  // Incoming
  HEARTBEAT,
  STEPS_U16_64,
  STEPPER_SYNC_CONFIG,
  STEPPER_SYNC,
  SWITCH,
  FADER_8,
  FADER_16,
  FADER_32,
  OUTPUT_CONFIG,
  GENERATOR_FREQUENCY
};

class Message : public snc::emb::msg::Message
{
  public:
  Message ( Type type_n )
  : snc::emb::msg::Message ( static_cast< std::uint8_t > ( type_n ) )
  {
  }
};

} // namespace snc::emb::msg::out
