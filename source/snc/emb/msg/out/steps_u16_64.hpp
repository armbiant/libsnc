/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <snc/emb/msg/out/message.hpp>
#include <snc/emb/type/step.hpp>

namespace snc::emb::msg::out
{

class Steps_U16_64 : public Message
{
  public:
  // -- Statics

  static constexpr std::size_t _num_values_max = 28;

  // -- Interface

  Steps_U16_64 ()
  : Message ( Type::STEPS_U16_64 )
  {
  }

  void
  reset ()
  {
    _stepper_index = 0;
    _job_type = snc::emb::type::Step::INVALID;
    _num_values = 0;
    _usecs_sum = 0;
  }

  std::uint8_t
  stepper_index () const
  {
    return _stepper_index;
  }

  void
  set_stepper_index ( std::uint8_t index_n )
  {
    _stepper_index = index_n;
  }

  snc::emb::type::Step
  job_type () const
  {
    return _job_type;
  }

  void
  set_job_type ( snc::emb::type::Step type_n )
  {
    _job_type = type_n;
  }

  static std::size_t
  num_values_max ()
  {
    return _num_values_max;
  }

  std::uint16_t
  num_values () const
  {
    return _num_values;
  }

  bool
  values_empty () const
  {
    return ( num_values () == 0 );
  }

  bool
  values_full () const
  {
    return ( num_values () == num_values_max () );
  }

  std::uint32_t
  usecs_sum () const
  {
    return _usecs_sum;
  }

  std::uint16_t
  usecs ( std::size_t index_n ) const
  {
    return _usecs[ index_n ];
  }

  void
  push_usecs ( std::uint16_t usecs_n )
  {
    DEBUG_ASSERT ( num_values () < num_values_max () );
    DEBUG_ASSERT ( usecs_n != 0 );
    _usecs_sum += usecs_n;
    _usecs[ num_values () ] = usecs_n;
    ++_num_values;
  }

  std::uint8_t
  switch_index ( std::size_t index_n )
  {
    return ( _usecs[ index_n ] & 0x00FF );
  }

  void
  push_switch_index ( std::uint8_t switch_index_n )
  {
    DEBUG_ASSERT ( num_values () < num_values_max () );
    _usecs[ num_values () ] = ( std::uint16_t ( switch_index_n ) | 0xFF00 );
    ++_num_values;
  }

  private:
  // -- Attributes
  std::uint8_t _stepper_index = 0;
  snc::emb::type::Step _job_type = snc::emb::type::Step::INVALID;
  std::uint16_t _num_values = 0;
  std::uint32_t _usecs_sum = 0;
  std::uint16_t _usecs[ _num_values_max ];
};

} // namespace snc::emb::msg::out
