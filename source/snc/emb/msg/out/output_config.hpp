/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>
#include <snc/emb/type/output.hpp>

namespace snc::emb::msg::out
{

class Output_Config : public Message
{
  public:
  Output_Config ()
  : Message ( Type::OUTPUT_CONFIG )
  {
  }

  void
  reset ()
  {
    _output_index = 0;
    _output_type = snc::emb::type::Output::UNUSED;
    _stepper_switch_index = 0;
    _freq_gen_index = 0;
  }

  std::uint8_t
  output_index () const
  {
    return _output_index;
  }

  void
  set_output_index ( std::uint8_t index_n )
  {
    _output_index = index_n;
  }

  snc::emb::type::Output
  output_type () const
  {
    return _output_type;
  }

  void
  set_output_type ( snc::emb::type::Output type_n )
  {
    _output_type = type_n;
  }

  std::uint8_t
  stepper_switch_index () const
  {
    return _stepper_switch_index;
  }

  void
  set_stepper_switch_index ( std::uint8_t index_n )
  {
    _stepper_switch_index = index_n;
  }

  std::uint8_t
  freq_gen_index () const
  {
    return _freq_gen_index;
  }

  void
  set_freq_gen_index ( std::uint8_t index_n )
  {
    _freq_gen_index = index_n;
  }

  private:
  // -- Attributes
  std::uint8_t _output_index = 0;
  snc::emb::type::Output _output_type = snc::emb::type::Output::UNUSED;
  /// @brief Axis index or switch index
  std::uint8_t _stepper_switch_index = 0;
  std::uint8_t _freq_gen_index = 0;
};

} // namespace snc::emb::msg::out
