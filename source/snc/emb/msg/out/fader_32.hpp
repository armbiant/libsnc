/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>

namespace snc::emb::msg::out
{

class Fader_32 : public Message
{
  public:
  Fader_32 ()
  : Message ( Type::FADER_32 )
  {
  }

  void
  reset ()
  {
    _fader_index = 0;
    _fader_value = 0;
  }

  std::uint8_t
  fader_index () const
  {
    return _fader_index;
  }

  void
  set_fader_index ( std::uint8_t fader_index_n )
  {
    _fader_index = fader_index_n;
  }

  std::uint32_t
  fader_value () const
  {
    return _fader_value;
  }

  void
  set_fader_value ( std::uint32_t fader_value_n )
  {
    _fader_value = fader_value_n;
  }

  private:
  // -- Attributes
  std::uint8_t _fader_index = 0;
  std::uint32_t _fader_value = 0;
};

} // namespace snc::emb::msg::out
