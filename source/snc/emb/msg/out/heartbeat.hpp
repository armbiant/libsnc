/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>

namespace snc::emb::msg::out
{

class Heartbeat : public Message
{

  public:
  Heartbeat ()
  : Message ( Type::HEARTBEAT )
  {
  }
};

} // namespace snc::emb::msg::out
