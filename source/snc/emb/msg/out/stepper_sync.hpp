/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>
#include <snc/emb/type/stepper_sync.hpp>

namespace snc::emb::msg::out
{

class Stepper_Sync : public Message
{
  public:
  Stepper_Sync ()
  : Message ( Type::STEPPER_SYNC )
  , _sync_group_index ( 0 )
  , _sync_type ( snc::emb::type::Stepper_Sync::SYNC_NULL )
  {
  }

  void
  reset ()
  {
    _sync_group_index = 0;
    _sync_type = snc::emb::type::Stepper_Sync::SYNC_NULL;
  }

  std::uint8_t
  sync_group_index () const
  {
    return _sync_group_index;
  }

  void
  set_sync_group_index ( std::uint8_t index_n )
  {
    _sync_group_index = index_n;
  }

  snc::emb::type::Stepper_Sync
  sync_type () const
  {
    return _sync_type;
  }

  void
  set_sync_type ( snc::emb::type::Stepper_Sync sync_type_n )
  {
    _sync_type = sync_type_n;
  }

  private:
  // -- Attributes
  std::uint8_t _sync_group_index = 0;
  snc::emb::type::Stepper_Sync _sync_type =
      snc::emb::type::Stepper_Sync::SYNC_NULL;
};

} // namespace snc::emb::msg::out
