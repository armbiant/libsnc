/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>

namespace snc::emb::msg::out
{

class Stepper_Sync_Config : public Message
{
  public:
  Stepper_Sync_Config ()
  : Message ( Type::STEPPER_SYNC_CONFIG )
  {
  }

  void
  reset ()
  {
    _sync_group_index = 0;
    _stepper_register = 0;
  }

  std::uint8_t
  sync_group_index () const
  {
    return _sync_group_index;
  }

  void
  set_sync_group_index ( std::uint8_t index_n )
  {
    _sync_group_index = index_n;
  }

  void
  register_stepper ( std::size_t index_n )
  {
    _stepper_register |= ( 1 << index_n );
  }

  bool
  stepper_registered ( std::size_t index_n ) const
  {
    return ( ( _stepper_register & ( 1 << index_n ) ) != 0 );
  }

  const std::uint32_t &
  stepper_register () const
  {
    return _stepper_register;
  }

  private:
  // -- Attributes
  std::uint8_t _sync_group_index = 0;
  std::uint32_t _stepper_register = 0;
};

} // namespace snc::emb::msg::out
