/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/rails/type_rails.hpp>
#include <snc/emb/msg/message.hpp>
#include <cstdint>

namespace snc::emb::msg
{

class Stream
{
  public:
  // -- Types

  using Buffer = sev::mem::rails::Type_Rails< snc::emb::msg::Message >;

  // -- Construction

  Stream ();

  // -- Setup

  /// @brief Reset the message_uid() and clear all messages from the buffer.
  void
  clear ();

  // -- Accessors: State

  /// @brief The latest emitted message uid

  std::size_t
  size () const
  {
    return _buffer.size ();
  }

  bool
  is_empty () const
  {
    return _buffer.is_empty ();
  }

  std::uint_fast16_t
  message_uid () const
  {
    return _message_uid;
  }

  // -- Accessors: Buffer

  Buffer &
  buffer ()
  {
    return _buffer;
  }

  const Buffer &
  buffer () const
  {
    return _buffer;
  }

  // -- Message access

  const snc::emb::msg::Message &
  front () const
  {
    return _buffer.front ();
  }

  void
  pop_front ()
  {
    _buffer.pop_front ();
  }

  // -- Message generation

  /// @brief Create a new message at the back
  template < typename T, typename... Args >
  T &
  emplace ( Args &&... args_n )
  {
    _message_uid = ( _message_uid + 1 ) & 0xffff;
    T & res = _buffer.emplace_back< T > ( std::forward< Args > ( args_n )... );
    res.set_message_uid ( _message_uid );
    return res;
  }

  /// @brief Create a new message at the back and set @a msg_n to it.
  template < typename T, typename... Args >
  void
  acquire ( T *& msg_n, Args &&... args_n )
  {
    msg_n = &emplace< T > ( std::forward< Args > ( args_n )... );
  }

  /// @brief Create a copy of @a at the back.
  template < typename T >
  T &
  push ( const T & msg_n )
  {
    return emplace< T > ( msg_n );
  }

  private:
  // -- Attributes
  Buffer _buffer;
  std::uint_fast16_t _message_uid = 0;
};

} // namespace snc::emb::msg
