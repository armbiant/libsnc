/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::msg
{

class Message
{
  // -- Construction
  public:
  Message ( std::uint8_t message_type_n )
  : _message_type ( message_type_n )
  {
  }

  // -- Accessors

  std::uint8_t
  message_type () const
  {
    return _message_type;
  }

  std::uint16_t
  message_uid () const
  {
    return _message_uid;
  }

  void
  set_message_uid ( std::uint16_t uid_n )
  {
    _message_uid = uid_n;
  }

  private:
  // -- Attributes
  std::uint8_t _message_type;
  std::uint16_t _message_uid;
};

} // namespace snc::emb::msg
