/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Switches_32 : public State
{
  public:
  State_Switches_32 ()
  : State ( Type::STATE_SWITCHES_32 )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _high_states = 0;
  }

  bool
  is_high ( std::size_t index_n ) const
  {
    return ( _high_states & ( std::uint32_t ( 1 ) << index_n ) ) != 0;
  }

  void
  set_is_high ( std::size_t index_n, bool flag_n )
  {
    if ( flag_n ) {
      _high_states |= ( std::uint32_t ( 1 ) << index_n );
    } else {
      _high_states &= ~( std::uint32_t ( 1 ) << index_n );
    }
  }

  private:
  // -- Attributes
  std::uint32_t _high_states = 0;
};

} // namespace snc::emb::msg::in
