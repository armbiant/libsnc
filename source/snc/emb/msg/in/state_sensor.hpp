/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Sensor : public State
{
  public:
  State_Sensor ()
  : State ( Type::STATE_SENSOR )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _sensor_index = 0;
    _sensor_state = false;
  }

  std::uint8_t
  sensor_index () const
  {
    return _sensor_index;
  }

  void
  set_sensor_index ( std::uint8_t index_n )
  {
    _sensor_index = index_n;
  }

  bool
  sensor_state () const
  {
    return _sensor_state;
  }

  void
  set_sensor_state ( bool flag_n )
  {
    _sensor_state = flag_n;
  }

  private:
  // -- Attributes
  std::uint8_t _sensor_index = 0;
  bool _sensor_state = false;
};

} // namespace snc::emb::msg::in
