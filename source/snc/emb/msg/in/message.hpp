/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/message.hpp>
#include <cstdint>

namespace snc::emb::msg::in
{

enum class Type : std::uint8_t
{
  STATE_SENSOR,
  STATE_SENSOR_8,
  STATE_SENSOR_16,
  STATE_SENSOR_32,

  STATE_SWITCH,
  STATE_FADER_8,
  STATE_FADER_16,
  STATE_FADER_32,

  STATE_STEPPER,

  STATE_SENSORS_32,
  STATE_SWITCHES_32
};

class Message : public snc::emb::msg::Message
{
  public:
  Message ( Type type_n )
  : snc::emb::msg::Message ( static_cast< std::uint8_t > ( type_n ) )
  {
  }
};

} // namespace snc::emb::msg::in
