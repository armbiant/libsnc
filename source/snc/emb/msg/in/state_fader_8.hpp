/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Fader_8 : public State
{
  public:
  State_Fader_8 ()
  : State ( Type::STATE_FADER_8 )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _fader_index = 0;
    _fader_value = 0;
  }

  std::uint8_t
  fader_index () const
  {
    return _fader_index;
  }

  void
  set_fader_index ( std::uint8_t index_n )
  {
    _fader_index = index_n;
  }

  std::uint8_t
  fader_value () const
  {
    return _fader_value;
  }

  void
  set_fader_value ( std::uint8_t value_n )
  {
    _fader_value = value_n;
  }

  private:
  // -- Attributes
  std::uint8_t _fader_index = 0;
  std::uint8_t _fader_value = 0;
};

} // namespace snc::emb::msg::in
