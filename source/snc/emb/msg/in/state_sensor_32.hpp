/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Sensor_32 : public State
{
  public:
  State_Sensor_32 ()
  : State ( Type::STATE_SENSOR_32 )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _sensor_index = 0;
    _sensor_state = 0;
  }

  std::uint8_t
  sensor_index () const
  {
    return _sensor_index;
  }

  void
  set_sensor_index ( std::uint8_t index_n )
  {
    _sensor_index = index_n;
  }

  std::uint32_t
  sensor_state () const
  {
    return _sensor_state;
  }

  void
  set_sensor_state ( std::uint32_t value_n )
  {
    _sensor_state = value_n;
  }

  private:
  // -- Attributes
  std::uint8_t _sensor_index = 0;
  std::uint32_t _sensor_state = 0;
};

} // namespace snc::emb::msg::in
