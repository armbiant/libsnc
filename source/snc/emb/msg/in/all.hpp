/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/in/state.hpp>
#include <snc/emb/msg/in/state_fader_16.hpp>
#include <snc/emb/msg/in/state_fader_32.hpp>
#include <snc/emb/msg/in/state_fader_8.hpp>
#include <snc/emb/msg/in/state_sensor.hpp>
#include <snc/emb/msg/in/state_sensor_16.hpp>
#include <snc/emb/msg/in/state_sensor_32.hpp>
#include <snc/emb/msg/in/state_sensor_8.hpp>
#include <snc/emb/msg/in/state_sensors_32.hpp>
#include <snc/emb/msg/in/state_stepper.hpp>
#include <snc/emb/msg/in/state_switch.hpp>
#include <snc/emb/msg/in/state_switches_32.hpp>
