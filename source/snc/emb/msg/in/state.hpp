/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/in/message.hpp>

namespace snc::emb::msg::in
{

class State : public Message
{
  protected:
  State ( Type message_type_n )
  : Message ( message_type_n )
  {
  }

  public:
  void
  reset ()
  {
    _latest_message_uid = 0;
  }

  std::uint16_t
  latest_message_uid () const
  {
    return _latest_message_uid;
  }

  void
  set_latest_message_uid ( std::uint16_t uid_n )
  {
    _latest_message_uid = uid_n;
  }

  private:
  // -- Attributes
  std::uint16_t _latest_message_uid = 0;
};

} // namespace snc::emb::msg::in
