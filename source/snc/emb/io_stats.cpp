/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "io_stats.hpp"

namespace snc::emb
{

IO_Stats::IO_Stats () = default;

void
IO_Stats::reset ()
{
  _serial.reset ();
  _latency = Latency::zero ();
}

} // namespace snc::emb
