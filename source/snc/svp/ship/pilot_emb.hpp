/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/pilot.hpp>
#include <snc/svp/ship/pilot/emb_writer.hpp>
#include <snc/svs/fac/serial_device/ship/client_out.hpp>
#include <snc/svs/fac/tracker/ship/client.hpp>

namespace snc::svp::ship
{

class Pilot_Emb : public Pilot
{
  public:
  // -- Types
  using Super = Pilot;
  using Emb_Writer = pilot::Emb_Writer;

  // -- Construction

  Pilot_Emb ( const Sailor_Init & init_n,
              sev::unicode::View cell_type_n,
              sev::unicode::View log_name_n );

  Pilot_Emb ( const Sailor_Init & init_n, sev::unicode::View name_n );

  ~Pilot_Emb ();

  // -- Accessors

  const snc::svs::fac::tracker::ship::Client &
  tracker () const
  {
    return _tracker;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Embedded device message generation registration

  /// @brief Whether or not this pilot is registered for device message pick up
  bool
  device_messages_registered () const
  {
    return _serial_device.messages_registered;
  }

  /// @brief Register this pilot for device message pick up
  void
  device_messages_register ( bool flag_n = true );

  /// @brief Register this pilot for device message pick up
  void
  device_messages_unregister ()
  {
    device_messages_register ( false );
  }

  // -- Embedded device message generation

  /// @brief Request device message pick up at the next possible time
  void
  device_messages_request ();

  virtual void
  device_messages_acquire ( Emb_Writer stream_n );

  private:
  // -- Device information
  snc::svs::fac::tracker::ship::Client _tracker;

  // -- Serial device
  struct
  {
    snc::svs::fac::serial_device::ship::Client_Out client;
    bool messages_registered = false;
  } _serial_device;
};

} // namespace snc::svp::ship
