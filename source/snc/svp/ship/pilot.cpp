/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot.hpp"
#include <sev/assert.hpp>

namespace snc::svp::ship
{

Pilot::Pilot ( const Sailor_Init & init_n,
               sev::unicode::View cell_type_n,
               sev::unicode::View log_name_n )
: Super ( init_n, cell_type_n, log_name_n )
, _device_info ( init_n.device_info () )
{
  DEBUG_ASSERT ( _device_info );
}

Pilot::Pilot ( const Sailor_Init & init_n, sev::unicode::View name_n )
: Pilot ( init_n, name_n, name_n )
{
}

Pilot::~Pilot () = default;

} // namespace snc::svp::ship
