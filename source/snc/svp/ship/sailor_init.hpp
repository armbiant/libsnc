/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_init.hpp>
#include <snc/device/handle.hpp>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
} // namespace snc::svp::office

namespace snc::svp::ship
{

/** Sailor default construction arguments */
class Sailor_Init
{
  public:
  // -- Types

  using Cell_Init = snc::bat::Cell_Init;
  using Factor = snc::svp::office::Factor;

  // -- Construction

  Sailor_Init ( const Cell_Init & cell_n,
                Factor * factor_n,
                const snc::device::handle::Info_Writeable & device_info_n )
  : _cell ( cell_n )
  , _factor ( factor_n )
  , _device_info ( device_info_n )
  {
  }

  // -- Accessors

  const Cell_Init &
  cell () const
  {
    return _cell;
  }

  Factor *
  factor () const
  {
    return _factor;
  }

  const snc::device::handle::Info_Writeable &
  device_info () const
  {
    return _device_info;
  }

  private:
  // -- Attributes
  const Cell_Init & _cell;
  Factor * _factor = nullptr;
  const snc::device::handle::Info_Writeable & _device_info;
};

} // namespace snc::svp::ship
