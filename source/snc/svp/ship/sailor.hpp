/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/bat/cell.hpp>
#include <snc/bat/signal.hpp>
#include <snc/svp/ship/events.hpp>
#include <snc/svp/ship/sailor_init.hpp>
#include <snc/svs/frame/ship/ship/client_office_io.hpp>

namespace snc::svp::ship
{

/** Bridge cell base class */
class Sailor : public snc::bat::Cell
{
  // -- Types
  private:
  using Super = snc::bat::Cell;

  public:
  using Sailor_Init = snc::svp::ship::Sailor_Init;
  using Office_Event = const snc::svp::ship::event::In;

  // -- Construction

  Sailor ( const Sailor_Init & init_n,
           sev::unicode::View cell_type_n,
           sev::unicode::View log_name_n );

  Sailor ( const Sailor_Init & init_n, sev::unicode::View name_n );

  ~Sailor ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Office event processing

  /// @brief Processes incoming bridge client events
  virtual void
  office_event ( Office_Event & event_n );

  // -- Office notification generation

  /// @brief Generate office events in overrides of this method
  virtual void
  office_notify ();

  // -- Office event io interface

  auto &
  office_io ()
  {
    return _office_io;
  }

  const auto &
  office_io () const
  {
    return _office_io;
  }

  private:
  // -- Office event io
  snc::svs::frame::ship::ship::Client_Office_IO _office_io;
};

} // namespace snc::svp::ship
