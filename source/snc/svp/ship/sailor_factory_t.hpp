/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/sailor_factory_abstract.hpp>
#include <snc/svp/ship/sailor_init.hpp>
#include <new>
#include <utility>

namespace snc::svp::ship
{

/// @brief Sailor factory
///
template < typename ST, typename... Args >
class Sailor_Factory_T : public Sailor_Factory_Abstract
{
  public:
  // -- Types

  using Sailor_Type = ST;
  using Clerk = Sailor_Factory_Abstract::Clerk;
  using Tuple = std::tuple< Args... >;

  // -- Construction

  Sailor_Factory_T ( Clerk * clerk_n, Args... args_n )
  : Sailor_Factory_Abstract ( clerk_n )
  , _args ( std::move ( args_n )... )
  {
  }

  // -- Tuple arguments

  const Tuple &
  args () const
  {
    return _args;
  }

  Tuple &
  args ()
  {
    return _args;
  }

  template < std::size_t I >
  typename std::tuple_element< I, std::tuple< Args... > >::type const &
  arg ( std::size_t index_n ) const
  {
    return std::get< I > ( _args );
  }

  template < std::size_t I, typename T >
  void
  set_arg ( const T & value_n )
  {
    std::get< I > ( _args ) = value_n;
  }

  protected:
  // -- Cell construction interface

  std::size_t
  cell_size () const override;

  snc::bat::Cell *
  cell_init ( void * memory_n, const Cell_Init & cell_init_n ) const override;

  private:
  // -- Utility

  template < std::size_t... I >
  snc::bat::Cell *
  _cell_init ( void * memory_n,
               const Cell_Init & cell_init_n,
               std::index_sequence< I... > ) const
  {
    return new ( memory_n )
        Sailor_Type ( Sailor_Init ( cell_init_n, clerk (), device_info () ),
                      std::get< I > ( _args )... );
  }

  private:
  // -- Attributes
  Tuple _args;
};

template < typename ST, typename... Args >
std::size_t
Sailor_Factory_T< ST, Args... >::cell_size () const
{
  return sizeof ( Sailor_Type );
}

template < typename ST, typename... Args >
snc::bat::Cell *
Sailor_Factory_T< ST, Args... >::cell_init (
    void * memory_n, const Cell_Init & cell_init_n ) const
{
  return _cell_init (
      memory_n, cell_init_n, std::make_index_sequence< sizeof...( Args ) >{} );
}

} // namespace snc::svp::ship
