/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/bat/cell.hpp>
#include <snc/settings/settings.hpp>
#include <snc/svp/dash/events.hpp>
#include <snc/svp/office/clerk_init.hpp>
#include <snc/svs/frame/office/office/client_dash_io.hpp>
#include <snc/svs/frame/settings/office/client.hpp>
#include <vector>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
}

namespace snc::svp::office
{

/** Clerk cell base class */
class Clerk : public snc::bat::Cell
{
  // -- Types
  private:
  using Super = snc::bat::Cell;

  public:
  using Clerk_Init = snc::svp::office::Clerk_Init;
  using Dash_Event = const snc::svp::dash::event::Out;
  using Dash_Notification = snc::svp::dash::event::In;

  /// @brief Front event sender helper class
  ///
  template < class T >
  class Dash_Notifier
  {
    public:
    Dash_Notifier ( Clerk & ctl_n,
                    std::uint_fast32_t message_n,
                    sev::event::Pool< T > & pool_n )
    : _ctl ( ctl_n )
    {
      DEBUG_ASSERT ( pool_n.size_owned () != 0 );
      if ( _ctl.dash_io ().notifications ().test_any ( message_n ) &&
           !pool_n.is_empty () ) {
        _ctl.dash_io ().notification_unset ( message_n );
        event = pool_n.pop_not_empty ();
      }
    }

    ~Dash_Notifier ()
    {
      if ( event != nullptr ) {
        _ctl.dash_io ().submit ( event );
      }
    }

    /// @brief A valid sender holds an event and submits it on destruction
    operator bool () const { return event != nullptr; }

    T * event = nullptr;

    private:
    Clerk & _ctl;
  };

  // -- Construction

  protected:
  Clerk ( const Clerk_Init & init_n,
          sev::unicode::View cell_type_n,
          sev::unicode::View log_name_n );

  Clerk ( const Clerk_Init & init_n, sev::unicode::View name_n );

  public:
  virtual ~Clerk ();

  // -- Accessors: Dash IO

  auto &
  dash_io ()
  {
    return _dash_io;
  }

  const auto &
  dash_io () const
  {
    return _dash_io;
  }

  // -- Accessors: Settings

  const snc::settings::Settings &
  settings () const
  {
    return *_settings.settings ();
  }

  snc::settings::Settings &
  settings ()
  {
    return *_settings.settings ();
  }

  // -- Cell session interface

  public:
  void
  cell_session_begin () override;

  void
  cell_session_end () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Dash event processing

  virtual void
  dash_event ( Dash_Event & event_n );

  // -- Dash event submission

  template < class T >
  Dash_Notifier< T >
  dash_notifier ( std::uint_fast32_t message_n, sev::event::Pool< T > & pool_n )
  {
    return Dash_Notifier< T > ( *this, message_n, pool_n );
  }

  /// @brief Generate front events in overrides of this method
  virtual void
  dash_notify ();

  private:
  // -- Resources
  /// @brief Settings
  snc::svs::frame::settings::office::Client _settings;
  /// @brief Dash event io
  snc::svs::frame::office::office::Client_Dash_IO _dash_io;
};

} // namespace snc::svp::office
