/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk_factory_abstract.hpp"
#include <snc/svp/dash/Panel.hpp>

namespace snc::svp::office
{

Clerk_Factory_Abstract::Clerk_Factory_Abstract ( Panel * panel_n )
: _panel ( panel_n )
{
}

} // namespace snc::svp::office
