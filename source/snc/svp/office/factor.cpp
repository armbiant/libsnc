/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "factor.hpp"
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/svp/ship/sailor.hpp>
#include <snc/svs/fac/base/service.hpp>

namespace snc::svp::office
{

Factor::Factor ( const Clerk_Init & init_n,
                 sev::unicode::View cell_type_n,
                 sev::unicode::View log_name_n )
: Super ( init_n, cell_type_n, log_name_n )
, _tracker ( this )
, _ship_io ( this )
{
}

Factor::Factor ( const Clerk_Init & init_n, sev::unicode::View name_n )
: Factor ( init_n, name_n, name_n )
{
}

Factor::~Factor () = default;

void
Factor::cell_session_begin ()
{
  Super::cell_session_begin ();
  _tracker.connect_required ();
  _ship_io.connect_required ();
}

bool
Factor::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () &&
         !bridge_session ().is_running;
}

void
Factor::bridge_session_begin ()
{
  // Update bridge session state
  DEBUG_ASSERT ( !bridge_session ().is_running );
  _bridge_session.is_running = true;
  _bridge_session.is_good = true;
}

void
Factor::bridge_session_begin_factory ( Sailor_Picker & pick_n [[maybe_unused]] )
{
}

void
Factor::bridge_session_begin_sailor ()
{
  DEBUG_ASSERT ( !ship_io ().sailors ().empty () );
}

void
Factor::bridge_session_abort ()
{
  // Update bridge session state
  DEBUG_ASSERT ( bridge_session ().is_good );
  _bridge_session.is_aborting = true;
  _bridge_session.is_good = false;
}

bool
Factor::bridge_session_is_ready_to_end () const
{
  if ( !bridge_session ().is_aborting ) {
    return false;
  }
  for ( auto * service : services () ) {
    if ( service->controlled () ) {
      return false;
    }
  }
  for ( auto * service : services () ) {
    if ( auto * svs =
             dynamic_cast< snc::svs::fac::base::Service * > ( service ) ) {
      if ( !svs->actor ().is_idle () ) {
        return false;
      }
      break;
    }
  }

  return _ship_io.notifications ().is_empty () &&
         _ship_io.epool_tracker ().all_home ();
}

void
Factor::bridge_session_end ()
{
  // Reset bridge session state
  DEBUG_ASSERT ( bridge_session ().is_aborting );
  _bridge_session.is_running = false;
  _bridge_session.is_aborting = false;
  _bridge_session.is_good = false;
}

void
Factor::bridge_event ( Bridge_Event & event_n )
{
  (void)event_n;
}

void
Factor::bridge_notify ()
{
}

snc::bat::Cell_Post_Init
Factor::sailor_post_init ()
{
  return [ this ] ( snc::bat::Cell * cell_n ) {
    this->_ship_io.sailors_push (
        static_cast< snc::svp::ship::Sailor * > ( cell_n ) );
  };
}

} // namespace snc::svp::office
