/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_factory_abstract.hpp>
#include <memory>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
} // namespace snc::svp::dash

namespace snc::svp::office
{

/// @brief Clerk abstract factory class
///
class Clerk_Factory_Abstract : public snc::bat::Cell_Factory_Abstract
{
  public:
  // -- Types

  using Panel = snc::svp::dash::Panel;

  // -- Construction

  Clerk_Factory_Abstract ( Panel * panel_n );

  // -- Panel

  Panel *
  panel () const
  {
    return _panel;
  }

  private:
  // -- Attributes
  Panel * _panel = nullptr;
};

/// @brief Sailor factory handle
///
using Clerk_Factory_Handle = std::shared_ptr< Clerk_Factory_Abstract >;

} // namespace snc::svp::office
