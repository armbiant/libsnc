/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/clerk_factory_abstract.hpp>
#include <snc/svp/office/clerk_init.hpp>
#include <new>
#include <utility>

namespace snc::svp::office
{

/// @brief Cell factory and owner
///
template < typename CT, typename... Args >
class Clerk_Factory : public Clerk_Factory_Abstract
{
  public:
  // -- Types

  using Panel = Clerk_Factory_Abstract::Panel;
  using Tuple = std::tuple< Args... >;

  // -- Construction

  Clerk_Factory ( Panel * panel_n, Args... args_n )
  : Clerk_Factory_Abstract ( panel_n )
  , _args ( std::move ( args_n )... )
  {
  }

  // -- Tuple arguments

  const Tuple &
  args () const
  {
    return _args;
  }

  Tuple &
  args ()
  {
    return _args;
  }

  template < std::size_t I >
  typename std::tuple_element< I, std::tuple< Args... > >::type const &
  arg ( std::size_t index_n ) const
  {
    return std::get< I > ( _args );
  }

  template < std::size_t I, typename T >
  void
  set_arg ( const T & value_n )
  {
    std::get< I > ( _args ) = value_n;
  }

  protected:
  // -- Cell construction interface

  std::size_t
  cell_size () const override;

  snc::bat::Cell *
  cell_init ( void * memory_n, const Cell_Init & cell_init_n ) const override;

  private:
  // -- Utility

  template < std::size_t... I >
  snc::bat::Cell *
  _cell_init ( void * memory_n,
               const Cell_Init & cell_init_n,
               std::index_sequence< I... > ) const
  {
    return new ( memory_n )
        CT ( Clerk_Init ( cell_init_n, panel () ), std::get< I > ( _args )... );
  }

  private:
  // -- Attributes
  Tuple _args;
};

template < typename CT, typename... Args >
std::size_t
Clerk_Factory< CT, Args... >::cell_size () const
{
  return sizeof ( CT );
}

template < typename CT, typename... Args >
snc::bat::Cell *
Clerk_Factory< CT, Args... >::cell_init ( void * memory_n,
                                          const Cell_Init & cell_init_n ) const
{
  return _cell_init (
      memory_n, cell_init_n, std::make_index_sequence< sizeof...( Args ) >{} );
}

} // namespace snc::svp::office
