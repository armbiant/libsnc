/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::svp::office
{

Clerk::Clerk ( const Clerk_Init & init_n,
               sev::unicode::View cell_type_n,
               sev::unicode::View log_name_n )
: Super ( init_n.cell (), cell_type_n, log_name_n )
, _settings ( this )
, _dash_io ( this, init_n.panel () )
{
}

Clerk::Clerk ( const Clerk_Init & init_n, sev::unicode::View name_n )
: Clerk ( init_n, name_n, name_n )
{
}

Clerk::~Clerk () {}

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _settings.connect_required ();
  _dash_io.connect_required ();
  _dash_io.session ().begin ();
}

void
Clerk::cell_session_end ()
{
  _dash_io.session ().end ();
  Super::cell_session_end ();
}

bool
Clerk::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () &&
         dash_io ().epool_tracker ().all_home ();
}

void
Clerk::dash_event ( Dash_Event & event_n [[maybe_unused]] )
{
  // Dummy implementation
  DEBUG_ASSERT ( false );
}

void
Clerk::dash_notify ()
{
  // Dummy implementation
}

} // namespace snc::svp::office
