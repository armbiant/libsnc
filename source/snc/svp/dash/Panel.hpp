/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/event.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <sev/logt/context.hpp>
#include <snc/bat/cell_factory.hpp>
#include <snc/settings/qt/Settings.hpp>
#include <snc/svp/dash/events.hpp>
#include <snc/svp/dash/office_io.hpp>
#include <snc/svp/dash/panel_init.hpp>
#include <snc/svp/office/clerk_factory.hpp>
#include <QObject>
#include <functional>
#include <memory>

// -- Forward declaration
namespace snc::svp::dash
{
class Dash;
}
namespace snc::svp::office
{
class Clerk;
}

namespace snc::svp::dash
{

class Panel : public QObject
{
  Q_OBJECT

  public:
  // -- Types

  using Panel_Init = snc::svp::dash::Panel_Init;
  using Clerk = snc::svp::office::Clerk;
  using Clerk_Factory_Handle = snc::svp::office::Clerk_Factory_Handle;
  using Office_Event = const snc::svp::dash::event::In;
  using Office_Notification = snc::svp::dash::event::Out;

  // -- Construction

  protected:
  Panel ( const Panel_Init & init_n, sev::unicode::View log_name_n );

  public:
  virtual ~Panel ();

  // -- Accessors

  Dash &
  dash () const
  {
    return _dash;
  }

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  Office_IO &
  office_io ()
  {
    return _office_io;
  }

  const Office_IO &
  office_io () const
  {
    return _office_io;
  }

  // -- Session control interface

  virtual void
  session_begin ();

  virtual void
  session_abort ();

  virtual void
  session_end ();

  // -- Session state interface

  bool
  session_is_running () const
  {
    return _session.is_running;
  }

  bool
  session_is_aborting () const
  {
    return _session.is_aborting;
  }

  virtual bool
  session_is_ready_to_end () const;

  // -- Office session interface

  /// @brief Office session begin
  virtual void
  office_session_begin ();

  /// @brief Office session begin: Clerk factory generation
  ///
  /// Reimplement to create the cell factory
  virtual Clerk_Factory_Handle
  office_session_begin_factory ();

  /// @brief Office session begin: Clerk is constructed
  ///
  /// Allocate office events here
  virtual void
  office_session_begin_clerk ();

  /// @brief Office session abort
  virtual void
  office_session_abort ();

  /// @brief Returns true when the session is ready to end
  virtual bool
  office_session_is_ready_to_end () const;

  /// @brief Office session end
  virtual void
  office_session_end ();

  // -- Office session state interface

  bool
  office_session_is_running () const
  {
    return _office_session.is_running;
  }

  bool
  office_session_is_aborting () const
  {
    return _office_session.is_aborting;
  }

  bool
  office_session_is_good () const
  {
    return _office_session.is_good;
  }

  // -- Office event processing

  virtual void
  office_event ( Office_Event & event_n );

  template < typename ET, typename ST >
  void
  office_event_state_t ( Office_Event & event_n, ST & state_n )
  {
    auto & cevent = static_cast< const ET & > ( event_n );
    office_io ().stats_out_sub ( cevent.dash_event_count () );
    state_n = cevent.state ();
    register_state_change ();
  }

  // -- Central processing

  void
  request_processing ();

  const std::function< void () > &
  processing_request () const
  {
    return _event_accu.callback ();
  }

  /// @brief Central processing callback
  virtual void
  process ();

  protected:
  // -- Event accumulator

  sev::event::Bit_Accu_Fast32 &
  event_accu ()
  {
    return _event_accu;
  }

  const sev::event::Bit_Accu_Fast32 &
  event_accu () const
  {
    return _event_accu;
  }

  // -- Settings

  const snc::settings::qt::Settings &
  settings () const
  {
    return *_settings;
  }

  snc::settings::qt::Settings &
  settings ()
  {
    return *_settings;
  }

  // -- State change

  void
  register_state_change ();

  /// @brief State change callback
  virtual void
  state_changed ();

  // -- Clerk creation utility

  template < typename ST, typename... Args >
  Clerk_Factory_Handle
  make_clerk_factory ( Args... args_n )
  {
    Clerk_Factory_Handle res =
        std::make_shared< snc::svp::office::Clerk_Factory< ST, Args... > > (
            this, std::move ( args_n )... );
    res->set_post_init ( clerk_post_init () );
    return res;
  }

  private:
  snc::bat::Cell_Post_Init
  clerk_post_init ();

  // -- Request processing

  friend class snc::svp::dash::Dash;

  void
  pick_up_process ();

  void
  pick_up_state ();

  private:
  // -- Context
  Dash & _dash;
  sev::logt::Context const _log;

  // -- Request flags
  bool _state_changed = false;
  bool _processing_requested = false;

  // -- Session state
  struct
  {
    bool is_running = false;
    bool is_aborting = false;
  } _session;

  // -- Office session
  struct
  {
    bool is_running = false;
    bool is_aborting = false;
    bool is_good = false;
  } _office_session;
  Office_IO _office_io;
  snc::settings::qt::Settings * _settings = nullptr;

  // -- Event registers
  sev::event::Bit_Accu_Fast32 _event_accu;
};

} // namespace snc::svp::dash
