/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/actor/dash/Panel.hpp>
#include <snc/svs/frame/office/dash/Panel.hpp>
#include <snc/svs/frame/settings/dash/Panel.hpp>
#include <snc/svs/frame/ship/dash/Panel.hpp>

namespace snc::svp::dash
{

class BasicDash : public snc::svp::dash::Dash
{
  Q_OBJECT

  // -- Properties
  Q_PROPERTY ( snc::settings::qt::Settings * settings READ settings CONSTANT )

  // -- Types
  private:
  using Super = snc::svp::dash::Dash;

  // -- Properties
  Q_PROPERTY ( QObject * actor READ actor CONSTANT )

  public:
  // -- Construction

  BasicDash ( sev::logt::Context log_context_n,
              sev::thread::Tracker * thread_tracker_n,
              QObject * qparent_n = nullptr );

  ~BasicDash ();

  // -- Settings

  snc::settings::qt::Settings *
  settings ()
  {
    return _settings.settings_ptr ();
  }

  // -- Accessor: Actor

  QObject *
  actor ()
  {
    return &_actor;
  }

  protected:
  // -- Session interface

  void
  session_begin () override;

  private:
  // -- Attributes
  snc::svs::frame::office::dash::Panel _office;
  snc::svs::frame::settings::dash::Panel _settings;
  snc::svs::frame::ship::dash::Panel _ship;
  snc::svs::ctl::actor::dash::Panel _actor;
};

} // namespace snc::svp::dash
