/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Dash.hpp"
#include <sev/mem/align.hpp>
#include <sev/utility.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <QCoreApplication>
#include <QEvent>
#include <utility>

namespace snc::svp::dash
{

namespace
{
// QEvent types
constexpr int _qevent_type_sync = QEvent::Type::User + 100;
} // namespace

Dash::Dash ( sev::logt::Context log_n, QObject * qparent_n )
: QObject ( qparent_n )
, _log ( std::move ( log_n ) )
{
}

Dash::~Dash () = default;

void
Dash::session_begin ()
{
  if ( session_is_running () ) {
    return;
  }

  // Allocate request buffers
  _processing_sync.panels_process.set_capacity ( _panels.size () );
  _processing_sync.panels_state.set_capacity ( _panels.size () );

  // Beginning panel sessions
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Session start: Beginning sessions of " << _panels.size ()
         << " panels.";
  }
  for ( auto * fctl : _panels ) {
    fctl->session_begin ();
  }

  // Update session state
  _session.is_running = true;
  _session.is_aborting = false;
  _session.is_good = true;
  emit sessionStateChanged ();
}

void
Dash::session_abort ()
{
  if ( !session_is_good () ) {
    return;
  }

  // Abort control sessions
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Session abort: Aborting sessions of " << _panels.size ()
         << " panels.";
  }
  for ( auto * fctl : _panels ) {
    fctl->session_abort ();
  }

  // Update session state
  _session.is_running = true;
  _session.is_aborting = true;
  _session.is_good = false;
  // Signalize run state change
  emit sessionStateChanged ();
}

bool
Dash::session_is_ready_to_end ()
{
  // Are we even aborting?
  if ( !session_is_aborting () ) {
    return false;
  }

  // Panels ready to end?
  for ( Panel * fctl : _panels ) {
    if ( !fctl->session_is_ready_to_end () ) {
      return false;
    }
  }

  return true;
}

void
Dash::session_end ()
{
  if ( !session_is_aborting () ) {
    return;
  }

  // End control sessions
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Session abort: Ending sessions of " << _panels.size ()
         << " panels.";
  }
  for ( Panel * fctl : sev::reverse ( _panels ) ) {
    fctl->session_end ();
  }

  // Update session state
  _session.is_running = false;
  _session.is_aborting = false;
  _session.is_good = false;
  emit sessionStateChanged ();

  log ().cat ( sev::logt::FL_DEBUG_0, "Session ended." );
}

void
Dash::start ()
{
  if ( session_is_running () ) {
    return;
  }

  this->session_begin ();
  emit started ();
}

void
Dash::abort ()
{
  if ( session_is_aborting () ) {
    return;
  }

  this->session_abort ();
}

bool
Dash::event ( QEvent * event_n )
{
  if ( event_n->type () == _qevent_type_sync ) {
    // Process synchronous
    this->process ();
    // Process abort
    this->process_abort ();

    // Accept event
    event_n->accept ();
    return true;
  }

  return QObject::event ( event_n );
}

void
Dash::process ()
{
  // Check if processing was requested at all
  if ( !sev::change ( _processing_sync.requested, false ) ) {
    return;
  }

  // Process panels
  {
    auto & plist = _processing_sync.panels_process;
    while ( !plist.is_empty () ) {
      auto * ctl = plist.front ();
      plist.pop_front_not_empty ();
      ctl->pick_up_process ();
    }
  }

  // Emit panels state change signals
  {
    auto & plist = _processing_sync.panels_state;
    while ( !plist.is_empty () ) {
      auto * ctl = plist.front ();
      plist.pop_front_not_empty ();
      ctl->pick_up_state ();
    }
  }
}

void
Dash::process_abort ()
{
  // Check if we're aborting and ready to end
  if ( !session_is_aborting () ) {
    return;
  }
  if ( !session_is_ready_to_end () ) {
    return;
  }

  // End session
  this->session_end ();
  emit stopped ();
}

void
Dash::register_processing ( Panel * panel_n )
{
  DEBUG_ASSERT ( !_processing_sync.panels_process.is_full () );
  _processing_sync.panels_process.push_back_not_full ( panel_n );
  register_processing_request ();
}

void
Dash::register_state_change ( Panel * panel_n )
{
  DEBUG_ASSERT ( !_processing_sync.panels_state.is_full () );
  _processing_sync.panels_state.push_back_not_full ( panel_n );
  register_processing_request ();
}

void
Dash::register_processing_request ()
{
  if ( sev::change ( _processing_sync.requested, true ) ) {
    QCoreApplication::postEvent (
        this,
        new QEvent ( static_cast< QEvent::Type > ( _qevent_type_sync ) ) );
  }
}

} // namespace snc::svp::dash
