/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::svp::dash
{

// -- Forward declaration
class Dash;

/** Panel default construction arguments */
class Panel_Init
{
  public:
  // -- Construction

  Panel_Init ( Dash & dash_n )
  : _dash ( dash_n )
  {
  }

  // -- Accessors

  Dash &
  dash () const
  {
    return _dash;
  }

  private:
  // -- Attributes
  Dash & _dash;
};

} // namespace snc::svp::dash
