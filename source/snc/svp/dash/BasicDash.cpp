/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "BasicDash.hpp"
#include <utility>

namespace snc::svp::dash
{

BasicDash::BasicDash ( sev::logt::Context log_context_n,
                       sev::thread::Tracker * thread_tracker_n,
                       QObject * qparent_n )
: Super ( std::move ( log_context_n ), qparent_n )
, _office ( *this, thread_tracker_n )
, _settings ( *this )
, _ship ( *this )
, _actor ( *this )
{
}

BasicDash::~BasicDash () = default;

void
BasicDash::session_begin ()
{
  Super::session_begin ();
  // Start office
  _office.start_office ();
}

} // namespace snc::svp::dash
