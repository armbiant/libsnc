cmake_minimum_required ( VERSION 3.7.2 )
project ( libsnc
    VERSION 0.1.0
    LANGUAGES CXX )

# --- Select a default build type
set ( CMAKE_BUILD_TYPE Release CACHE STRING "Build configuration" )

# --- Library name
set ( LIB_NAME_CORE "snc" )
set ( LIB_NAME_CORE_BINARY_RELEASE ${LIB_NAME_CORE} )
set ( LIB_NAME_CORE_BINARY_DEBUG ${LIB_NAME_CORE}_debug )
if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
  set ( LIB_NAME_CORE_BINARY ${LIB_NAME_CORE_BINARY_DEBUG} )
else ()
  set ( LIB_NAME_CORE_BINARY ${LIB_NAME_CORE_BINARY_RELEASE} )
endif()

set ( LIB_NAME_QT "snc-qt" )
set ( LIB_NAME_QT_BINARY_RELEASE ${LIB_NAME_QT} )
set ( LIB_NAME_QT_BINARY_DEBUG ${LIB_NAME_QT}_debug )
if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
  set ( LIB_NAME_QT_BINARY ${LIB_NAME_QT_BINARY_DEBUG} )
else ()
  set ( LIB_NAME_QT_BINARY ${LIB_NAME_QT_BINARY_RELEASE} )
endif()


# --- Installation directories
set ( INSTALL_DIR_INCLUDE 
    "include/${LIB_NAME_CORE}"
    CACHE PATH
    "Installation directory for header files" )

set ( INSTALL_DIR_LIB 
    "lib"
    CACHE PATH
    "Installation directory for libraries" )

set ( INSTALL_DIR_CMAKE
    "lib/cmake"
    CACHE PATH
    "Installation directory for CMake files" )

set ( INSTALL_DIR_DOC 
    share/doc/lib${LIB_NAME_CORE}
    CACHE PATH
    "Installation directory for documentation" )

# --- Process subdirectories
add_subdirectory ( source )
add_subdirectory ( cmake )
add_subdirectory ( doc )

enable_testing()
add_subdirectory ( test )
