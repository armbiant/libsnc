# -- Find modules
# - Sev library
if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
  find_package ( sev COMPONENTS core_debug REQUIRED )
else ()
  find_package ( sev COMPONENTS core REQUIRED )
endif ()
# - Qt5 library
find_package (
  Qt5
  COMPONENTS
    Core
    Qml
  REQUIRED )

# -- Programs
add_executable ( address_parser address_parser.cpp )
target_link_libraries ( address_parser ${LIB_NAME_CORE_BINARY} )
add_test (NAME settings_address_parser COMMAND address_parser )

add_executable ( value value.cpp )
target_link_libraries ( value ${LIB_NAME_CORE_BINARY} )
add_test (NAME settings_value COMMAND value )

add_executable ( set_get set_get.cpp )
target_link_libraries ( set_get ${LIB_NAME_CORE_BINARY} )
add_test (NAME settings_set_get COMMAND set_get )

add_executable ( json json.cpp )
target_link_libraries ( json ${LIB_NAME_CORE_BINARY} )
add_test (NAME settings_json COMMAND json )
