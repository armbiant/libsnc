#include <snc/settings/settings.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

using Settings = snc::settings::Settings;
using Value = snc::settings::Value;

void
debug_print ( Settings & sett_n )
{
  std::ostringstream oss;
  oss << sett_n.json_export_string () << "\n";
  std::cout << oss.str ();
}

void
import ( Settings & sett_n, std::string_view json_n )
{
  auto ret = sett_n.json_import_string ( json_n );
  if ( !ret.first ) {
    throw std::runtime_error ( "Bad json import: " + ret.second );
  }
}

Settings
reimported ( const Settings & sett_n )
{
  Settings res;
  import ( res, sett_n.json_export_string () );
  return res;
}

void
require_value_type ( const Value & val_n, Value::Type type_n )
{
  if ( val_n.type () == type_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Value type was expected to be " << Value::type_name ( type_n )
      << " but is " << val_n.type_name () << "\n";
  throw std::runtime_error ( oss.str () );
}

void
require_bool ( const Settings & sett_n, std::string_view key_n, bool value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::BOOL );
  auto val_bool = val.as_bool ();
  if ( val_bool == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Boolean value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_bool << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
value_bool ( const Settings & sett_n, std::string_view key_n, bool value_n )
{
  require_bool ( sett_n, key_n, value_n );
  require_bool ( reimported ( sett_n ), key_n, value_n );
}

void
require_double ( const Settings & sett_n,
                 std::string_view key_n,
                 double value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::DOUBLE );
  auto val_double = val.as_double ();
  if ( val_double == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Double value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_double << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
value_double ( const Settings & sett_n, std::string_view key_n, double value_n )
{
  require_double ( sett_n, key_n, value_n );
  require_double ( reimported ( sett_n ), key_n, value_n );
}

void
require_string ( const Settings & sett_n,
                 std::string_view key_n,
                 std::string_view value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::STRING );
  auto val_string = val.as_string ();
  if ( val_string == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "String value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_string << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
value_string ( const Settings & sett_n,
               std::string_view key_n,
               std::string_view value_n )
{
  require_string ( sett_n, key_n, value_n );
  require_string ( reimported ( sett_n ), key_n, value_n );
}

void
json ()
{
  // Boolean
  {
    Settings sett;
    import ( sett, "{ \"one\" : true }" );
    value_bool ( sett, "one", true );
    import ( sett, "{ \"one\" : false }" );
    value_bool ( sett, "one", false );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : true }" );
    import ( sett, "{ \"two\" : false }" );
    value_bool ( sett, "one", true );
    value_bool ( sett, "two", false );
  }

  {
    Settings sett;
    import ( sett, "{ \"one\" : false }" );
    import ( sett, "{ \"two\" : { \"three\" : true } }" );
    value_bool ( sett, "one", false );
    value_bool ( sett, "two.three", true );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : false }" );
    import ( sett, "{ \"two\" : [ false, true ] }" );
    value_bool ( sett, "one", false );
    value_bool ( sett, "two[0]", false );
    value_bool ( sett, "two[1]", true );
  }

  // Double
  {
    Settings sett;
    import ( sett, "{ \"one\" : 1.2 }" );
    value_double ( sett, "one", 1.2 );
    import ( sett, "{ \"one\" : 2.3 }" );
    value_double ( sett, "one", 2.3 );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : 1.2 }" );
    import ( sett, "{ \"two\" : 2.3 }" );
    value_double ( sett, "one", 1.2 );
    value_double ( sett, "two", 2.3 );
  }

  {
    Settings sett;
    import ( sett, "{ \"one\" : 1.2 }" );
    import ( sett, "{ \"two\" : { \"three\" : 2.3 } }" );
    value_double ( sett, "one", 1.2 );
    value_double ( sett, "two.three", 2.3 );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : 1.2 }" );
    import ( sett, "{ \"two\" : [ 2.3, 3.4 ] }" );
    value_double ( sett, "one", 1.2 );
    value_double ( sett, "two[0]", 2.3 );
    value_double ( sett, "two[1]", 3.4 );
  }

  // String
  {
    Settings sett;
    import ( sett, "{ \"one\" : \"two\" }" );
    value_string ( sett, "one", "two" );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : \"two\" }" );
    import ( sett, "{ \"two\" : \"three\" }" );
    value_string ( sett, "one", "two" );
    value_string ( sett, "two", "three" );
  }

  {
    Settings sett;
    import ( sett, "{ \"one\" : \"two\" }" );
    import ( sett, "{ \"two\" : { \"three\" : \"four\" } }" );
    value_string ( sett, "one", "two" );
    value_string ( sett, "two.three", "four" );
  }
  {
    Settings sett;
    import ( sett, "{ \"one\" : \"two\" }" );
    import ( sett, "{ \"two\" : [ \"three\", \"four\" ] }" );
    value_string ( sett, "one", "two" );
    value_string ( sett, "two[0]", "three" );
    value_string ( sett, "two[1]", "four" );
  }

  {
    Settings sett;
    import ( sett, "{ \"one\" : \"two\" }" );
    import ( sett, "{ \"two\" : { \"three\" : \"four\" } }" );
    import ( sett, "{ \"two\" : { \"five\" : { \"six\" : \"seven\" } } }" );
    value_string ( sett, "one", "two" );
    value_string ( sett, "two.three", "four" );
    value_string ( sett, "two.five.six", "seven" );
  }

  // Mixed
  {
    Settings sett;
    import ( sett, "{ \"bool\" : true }" );
    import ( sett, "{ \"double\" : 1.2 }" );
    import ( sett, "{ \"string\" : \"one\" }" );
    import ( sett,
             "{ \"obj\" : { \"b\" : false, \"d\" : 2.3, \"s\" : \"two\" } }" );
    import ( sett, "{ \"arr\" : [ true, 3.4, \"three\" ] }" );

    value_bool ( sett, "bool", true );
    value_double ( sett, "double", 1.2 );
    value_string ( sett, "string", "one" );

    value_bool ( sett, "obj.b", false );
    value_double ( sett, "obj.d", 2.3 );
    value_string ( sett, "obj.s", "two" );

    value_bool ( sett, "arr[0]", true );
    value_double ( sett, "arr[1]", 3.4 );
    value_string ( sett, "arr[2]", "three" );
  }
}

int
main ( int argc, char * argv[] )
{
  (void)argc;
  (void)argv;

  json ();

  return 0;
}
