#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygraphviz as pgv

graph_name = "efrog_cockpit"

graph = pgv.AGraph ( strict=False, directed=True, name="Top" )

qgraph = graph.add_subgraph ( name='cluster_queue' )
qgraph.add_node ( 'Queue' )

sgraph = graph.add_subgraph ( name='cluster_cockpit' )
sgraph.graph_attr[ 'rank' ] = 'same'
sgraph.graph_attr[ 'label' ] = 'Cockpit'
sgraph.add_node ( 'pilot_0' )
sgraph.add_node ( 'pilot_1' )
sgraph.add_node ( 'pilot_2' )

dgraph = graph.add_subgraph ( name='cluster_device' )
dgraph.add_node ( 'Device' )

cgraph = graph.add_subgraph ( name='cluster_control' )
cgraph.graph_attr[ 'rank' ] = 'same'
cgraph.graph_attr[ 'label' ] = 'Control'
cgraph.add_node ( 'control_0' )
cgraph.add_node ( 'control_1' )
cgraph.add_node ( 'control_2' )

graph.add_edge ( 'pilot_0', 'Device' )
graph.add_edge ( 'pilot_1', 'Device' )
graph.add_edge ( 'pilot_2', 'Device' )

graph.add_edge ( 'pilot_0', 'Queue' )
graph.add_edge ( 'pilot_1', 'Queue' )
graph.add_edge ( 'pilot_2', 'Queue' )

graph.add_edge ( 'control_0', 'Queue' )
graph.add_edge ( 'control_1', 'Queue' )
graph.add_edge ( 'control_2', 'Queue' )

graph.draw ( graph_name + str ( ".png" ), prog='dot' )
print ( graph )
